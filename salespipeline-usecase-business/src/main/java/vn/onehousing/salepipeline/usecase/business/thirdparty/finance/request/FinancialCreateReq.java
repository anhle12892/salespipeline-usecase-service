package vn.onehousing.salepipeline.usecase.business.thirdparty.finance.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class FinancialCreateReq {

    @NotBlank
    String leadUuid;

    String agentUuid;

    String leadSource;

    String partnerSaleId;

    String partnerSaleName;

    String partnerSalePhone;

    String partnerSaleCode;

    String partnerUuid;

    String resourceTypeInterest;

    String projectUuid;

    String partnerSaleEmail;

    String budgetLimit;

    String propertyType;

    String propertyCode;

    String propertyUuid;

    String nbBedroom;

    @JsonProperty("is_sent")
    Boolean isSent;

    String partnerLeadId;

    String mortgageStatus;

    String mortgageSubStatus;

    Boolean hasMortgageProperty;

    Instant lastResponseTime;

    Boolean isClosed;

    String applicationId;

    Instant applicationCreatedDate;

    String status;

    BigDecimal minHousePrice;

    BigDecimal maxHousePrice;

    String consentForm;

    String appVersion;

    String requestId;

    EmiInformationDto emiInformation;

}
