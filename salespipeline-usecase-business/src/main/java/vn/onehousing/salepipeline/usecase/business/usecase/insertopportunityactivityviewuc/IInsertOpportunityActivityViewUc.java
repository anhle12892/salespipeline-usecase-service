package vn.onehousing.salepipeline.usecase.business.usecase.insertopportunityactivityviewuc;

import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;

import javax.annotation.Nullable;

public interface IInsertOpportunityActivityViewUc {
    void process(OpportunityActivityView activityView, @Nullable Long timestamp);
}
