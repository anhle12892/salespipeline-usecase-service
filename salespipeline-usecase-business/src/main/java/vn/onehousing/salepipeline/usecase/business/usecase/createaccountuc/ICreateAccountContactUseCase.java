package vn.onehousing.salepipeline.usecase.business.usecase.createaccountuc;

import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;

public interface ICreateAccountContactUseCase {
    SalesPipeline process(String leadUuid) ;
}
