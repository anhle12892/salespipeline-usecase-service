package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskMigrationRequest {
    private String uuid;
}
