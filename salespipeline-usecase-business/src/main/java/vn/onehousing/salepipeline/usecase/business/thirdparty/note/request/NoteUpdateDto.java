package vn.onehousing.salepipeline.usecase.business.thirdparty.note.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NoteUpdateDto {
    //    @NotBlank
    public Long id;
    public String noteCode;
    //    @NotBlank
//    @UUIDFormat(message = "UUID is not valid")
    public UUID noteUuid;
    //    @UUIDFormat(message = "UUID is not valid")
    public UUID ownerUuid;
    //    @UUIDFormat(message = "UUID is not valid")
    public UUID parentUuid;
    //    @NoteParentTypeFormat
    public String parentType;
    //    @NotBlank
    public String title;
    //    @NotBlank
    public String content;

    @JsonIgnore
    public Long lastModifiedDate;
    @JsonIgnore
//    @UUIDFormat(message = "UUID is not valid")
    public String lastModifiedBy;
}
