package vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AddressDto {
    private String address;
    private ProvinceDto province;
    private DistrictDto district;
    private WardDto ward;
    private String phoneNumber;
    private String addressName;
    private String contactName;
}
