package vn.onehousing.salepipeline.usecase.business.thirdparty.masterservice;

import vn.onehousing.salepipeline.usecase.business.thirdparty.masterservice.response.ProjectResponseDto;

import java.util.List;

public interface IMasterService {
    List<ProjectResponseDto> getAllProject();
}
