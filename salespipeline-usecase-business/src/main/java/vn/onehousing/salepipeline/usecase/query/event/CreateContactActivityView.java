package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
public class CreateContactActivityView {
    public String contactCode;
    public UUID contactUuid;
    public String contactName;
    public String contactStatus;
    public PhoneNumber phoneNumber;
    public String email;
    public LocalDate dateOfBirth;
    public String job;
    public Instant lastRequestContactDate;
    public String gender;
    public Instant lastContactedDate;
    public UUID accountUuid;
    public String accountCode;
    public String leadSource;
    public String description;
    public String facebook;
    public String maritalStatus;
    public UUID referredByUserUuid;
    public String referredByUserCode;
    public String contactRoles;
    public Integer numberOfChildren;
    private Instant lastModifiedDate;

    public Integer rangeAge;

    public String childrenSchool;

    public String nationality;

    public String permanentAddressId;

    public BigDecimal annualRevenue;
    private Long timestamp;
}
