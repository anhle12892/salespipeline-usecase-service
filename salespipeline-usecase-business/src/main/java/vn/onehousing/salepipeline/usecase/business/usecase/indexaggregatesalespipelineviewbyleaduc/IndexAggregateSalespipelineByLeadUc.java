package vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbyleaduc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.onehousing.salepipeline.usecase.business.mapper.SalespipelineAggregateViewMapper;
import vn.onehousing.salepipeline.usecase.business.thirdparty.distributedlockservice.IDistributedLockService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.IWorkflowService;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.utils.CommonUtils;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEntityType;
import vn.onehousing.salepipeline.usecase.common.shared.constants.RedisLockName;
import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.ISalespipelineAggregateViewRepository;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.SalespipelineAggregateView;

import java.io.IOException;

@Slf4j
@Service
@AllArgsConstructor
public class IndexAggregateSalespipelineByLeadUc implements IIndexAggregateSalespipelineByLeadUc {

    private final SalespipelineAggregateViewMapper mapper;
    private final ISalespipelineAggregateViewRepository repository;
    private final IWorkflowService workflowService;

    @Autowired
    IDistributedLockService distributedLockService;

    @Override
    public void process(Lead data) throws IOException {
        log.info("[IndexAggregateSalespipelineByLeadUc] - start sync - lead_uuid: [{}] - lastModified: [{}]", data.getLeadUuid(), data.getLastModifiedDate().toEpochMilli());
        var key = CommonUtils.buildRedisLockKey(
            RedisLockName.SALESPIPELINE_USECASE_INDEX_AGGREGATE_LOCK, data.getLeadUuid()
        );
        var ret = distributedLockService.getLock(key, data.getLeadUuid());
        if (!ret) {
            log.error("[IndexAggregateSalespipelineByLeadUc] - cannot get lock - lead_uuid: [{}] - lastModified: [{}]", data.getLeadUuid(), data.getLastModifiedDate().toEpochMilli());
            return;
        }
        processAggregate(data);
        distributedLockService.releaseLock(key);
    }

    private void processAggregate(Lead lead) throws IOException {

        SalespipelineAggregateView currentData = repository.getByUuid(lead.getLeadUuid());
        if (currentData == null) {
            var aggregate = mapper.from(lead);
            String[] agentRolesNeeded = workflowService.getListAgentRolesNeeded(lead.getSource(), lead.getChannel(), "1");
            aggregate.setNumberRequiredUser(agentRolesNeeded.length);
            repository.insert(aggregate);
            return;
        }

        if (ActEntityType.OPPORTUNITY.equals(currentData.getType())) {
            return;
        }


        if (currentData.getLastModifiedDate().toEpochMilli() > lead.getLastModifiedDate().toEpochMilli()) {
            return;
        }
        mapper.updateFields(lead, currentData);
        if (currentData.getAssignedUsers() != null) {
            currentData.setNumberAssignedUser(currentData.getAssignedUsers().size());
        } else {
            currentData.setNumberAssignedUser(0);
        }

        repository.insert(currentData);
    }
}
