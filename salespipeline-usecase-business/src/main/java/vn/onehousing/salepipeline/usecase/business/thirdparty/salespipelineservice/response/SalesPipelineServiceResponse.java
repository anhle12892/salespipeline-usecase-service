package vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.response;

import lombok.Builder;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;


@Data
@Builder
public class SalesPipelineServiceResponse extends BaseResponse<SalesPipeline> {
}
