package vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbysalespipelineuc;

import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;

import java.io.IOException;


public interface IIndexAggregateSalespipelineViewBySalespipelineUc {
    void process(SalesPipeline data) throws IOException;
}
