package vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import vn.onehousing.salepipeline.usecase.common.shared.model.Address;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

import java.lang.reflect.Field;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString
public class AccountUpdateRequest {
    String accountUuid;
    String accountName;
    String email;
    String description;
    String bankAccountNumber;
    String bankAccountHolder;
    String bankCode;
    String bankName;
    String bankBranch;
    BigDecimal annualRevenue;
    String campaignCode;
    String campaignUuid;
    String referredByUuid;
    String referredByUserCode;
    String primaryContactUuid;
    String primaryContactCode;
    Address address;
    BigDecimal monthlyIncome;
    BigDecimal monthlyPaymentLoan;
    BigDecimal monthlyExpense;
    String note;
    PhoneNumber phoneNumber;
    Boolean isMigration;

    public boolean isAllFieldsNull() throws IllegalAccessException {
        for (Field f : getClass().getDeclaredFields())
            if (f.get(this) != null)
                return false;
        return true;
    }
}
