package vn.onehousing.salepipeline.usecase.business.thirdparty.note.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NoteCreateDto {
    public String noteCode;
    public String noteUuid;
    public String ownerUuid;
    public String ownerType;
    public String ownerEmail;
    public String parentUuid;
    public String parentType;
    public String title;
    public String content;
}
