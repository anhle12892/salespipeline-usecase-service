package vn.onehousing.salepipeline.usecase.business.usecase.getsalespipelineaggregateviewuc;

import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.SalespipelineAggregateView;

public interface IGetSalesPipelineAggregateViewUc {
    SalespipelineAggregateView process(String coreUuid);
}
