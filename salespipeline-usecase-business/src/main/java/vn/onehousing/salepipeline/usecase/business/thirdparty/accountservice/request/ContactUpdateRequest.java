package vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import vn.onehousing.salepipeline.usecase.common.shared.model.Address;
import vn.onehousing.salepipeline.usecase.common.shared.model.LeadIdDocument;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ContactUpdateRequest {
    String contactUuid;
    String contactName;
    String email;
    LocalDate dateOfBirth;
    String job;
    Instant lastRequestContactDate;
    String gender;
    Instant lastContactedDate;
    String description;
    String facebook;
    String referredByUuid;
    String referredByUserCode;
    String contactRoles;
    Integer numberOfChildren;
    Integer rangeAge;
    String childrenSchool;
    String nationality;
    String maritalStatus;
    List<LeadIdDocument> idDocuments;
    PhoneNumber phoneNumber;
    Address permanentAddress;
    BigDecimal annualRevenue;
    String company;
    Boolean isMigration;

    public boolean isAllFieldsNull() throws IllegalAccessException {
        for (Field f : getClass().getDeclaredFields())
            if (f.get(this) != null)
                return false;
        return true;
    }
}
