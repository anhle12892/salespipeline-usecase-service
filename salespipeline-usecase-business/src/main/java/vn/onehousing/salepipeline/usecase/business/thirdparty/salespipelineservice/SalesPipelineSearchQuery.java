package vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SalesPipelineSearchQuery {
    String leadUuid;
}
