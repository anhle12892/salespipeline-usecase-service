package vn.onehousing.salepipeline.usecase.business.usecase.projectuc;

import vn.onehousing.salepipeline.usecase.business.usecase.projectuc.request.Project;

import java.util.List;
import java.util.Optional;

public interface IProjectUseCase {
    Project getProjectByUuid(String projectUuid);
    List<Project> getAllProjects();
    Project getProjectByProjectCode(String projectCode);
    Optional<String> getUuidByCode(String projectCode);
}
