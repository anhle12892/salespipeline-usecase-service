package vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbycontactuc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import vn.onehousing.salepipeline.usecase.business.mapper.SalespipelineAggregateViewMapper;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.IAccountService;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.utils.CommonUtils;
import vn.onehousing.salepipeline.usecase.common.shared.constants.RedisLockName;
import vn.onehousing.salepipeline.usecase.common.shared.model.Contact;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.ISalespipelineAggregateViewRepository;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.SalespipelineAggregateView;
import vn.onehousing.salepipeline.usecase.business.thirdparty.distributedlockservice.IDistributedLockService;

import java.io.IOException;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class IndexAggregateSalespipelineByContactUc implements IIndexAggregateSalespipelineByContactUc {

    private final ISalespipelineAggregateViewRepository repository;
    private final SalespipelineAggregateViewMapper mapper;
    private final IAccountService accountService;
    private final IDistributedLockService distributedLockService;

//    @Override
//    public void process(Contact contact) throws IOException {
//        List<SalespipelineAggregateView> datas = repository.searchByContactUuid(contact.getContactUuid());
//        for (SalespipelineAggregateView data : datas) {
//
//            if (StringUtils.isEmpty(data.getLeadUuid()))
//                continue;
//
//            Account account = accountService.getAccountByUuid(data.getAccountUuid());
//
//            mapper.updateFields(contact, data);
//            mapper.updateFields(account, data);
//
//            repository.insert(data);
//        }
//    }

    @Override
    public void process(Contact contact) throws IOException {
        List<SalespipelineAggregateView> datas = repository.searchByContactUuid(contact.getContactUuid());
        for (SalespipelineAggregateView data : datas) {

            if (StringUtils.isEmpty(data.getLeadUuid()))
                continue;

            var key = CommonUtils.buildRedisLockKey(RedisLockName.SALESPIPELINE_USECASE_INDEX_AGGREGATE_LOCK, data.getLeadUuid());
            var ret = distributedLockService.getLock(key, data.getLeadUuid());
            if (!ret) {
                log.error("[IndexAggregateSalespipelineByLeadUc] - cannot get lock - lead_uuid: [{}] - lastModified: [{}]", data.getLeadUuid(), data.getLastModifiedDate().toEpochMilli());
                return;
            }

            // get lastest data in es
            // search by contact uuid does not return the lastest data
            data = repository.getByUuid(data.getLeadUuid());

            mapper.updateFields(contact, data);

            if (data.getProfile() != null) {
                mapper.updateFields(contact, data.getProfile());
            }

            repository.insert(data);

            distributedLockService.releaseLock(key);
        }
    }

}
