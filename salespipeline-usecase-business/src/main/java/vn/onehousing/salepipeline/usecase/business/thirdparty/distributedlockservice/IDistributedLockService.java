package vn.onehousing.salepipeline.usecase.business.thirdparty.distributedlockservice;

import org.springframework.stereotype.Component;


@Component(value = "distributedLockService")
public interface IDistributedLockService {
    boolean getLock(String key, String value);

    void releaseLock(String key);
}
