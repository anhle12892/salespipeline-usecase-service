package vn.onehousing.salepipeline.usecase.domain.model.saleprocess;

import lombok.Data;

@Data
public class SaleProcessStage {
    String leadUuid;
    String stage;
}
