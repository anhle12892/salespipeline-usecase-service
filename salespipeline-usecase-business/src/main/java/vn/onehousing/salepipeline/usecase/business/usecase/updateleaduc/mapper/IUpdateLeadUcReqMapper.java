package vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.request.CreateLeadReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.request.UpdateLeadReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.request.UpdateOpportunityReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.request.UpdateSalesPipelineReq;
import vn.onehousing.salepipeline.usecase.business.usecase.prequalifiedleaduc.request.UpdateLeadPreQualifiedReq;
import vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.request.UpdateLeadUcReq;

@Mapper(componentModel = "spring")
public interface IUpdateLeadUcReqMapper {

    @Mapping(target = "leadCode", source = "code")
    UpdateLeadReq toUpdateLeadReq(UpdateLeadUcReq req);

    CreateLeadReq toCreateLeadReq(UpdateLeadUcReq req);

    @Mapping(target = "name", source = "leadName")
    @Mapping(target = "opportunityCode", source = "code")
    UpdateOpportunityReq toUpdateOpportunityReq(UpdateLeadUcReq req);

    UpdateSalesPipelineReq toUpdateSalesPipelineReq(UpdateLeadUcReq req);

    UpdateLeadUcReq toUpdatePreQualifiedLead(UpdateLeadPreQualifiedReq req);
}
