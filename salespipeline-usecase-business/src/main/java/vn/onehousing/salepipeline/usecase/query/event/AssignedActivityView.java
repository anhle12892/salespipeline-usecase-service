package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;

import java.time.Instant;

@Data
public class AssignedActivityView {
    Instant assignedDate;
    Long timestamp;
    private String policyUuid;
    private String policyName;
    private AssignmentContext assignmentContext;
    private String assignedUserName;
    private int numberOfAssignedTimes;
}
