package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

@Data
public class AssignmentContextView {
    private String lead_source;
    private String lead_uuid;
    private String opportunity_uuid;
    private String agent_uuid;
    private String entityType;
    private PhoneNumber phoneNumber;
}