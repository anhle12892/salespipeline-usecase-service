package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

@Data
public class LeadStoppedEventView {
    private String leadUuid;
    private PhoneNumber phoneNumber;
    private Long timestamp;
}
