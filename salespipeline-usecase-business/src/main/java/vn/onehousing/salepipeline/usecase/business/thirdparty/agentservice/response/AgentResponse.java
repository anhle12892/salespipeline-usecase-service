package vn.onehousing.salepipeline.usecase.business.thirdparty.agentservice.response;

import lombok.Builder;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.model.AgentDto;

@Data
@Builder
public class AgentResponse extends BaseResponse<AgentDto> {

}
