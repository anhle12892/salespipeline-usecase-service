package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.processsalestage;

import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;

import java.io.IOException;

public interface IProcessSaleProcessStage {
    void process(SaleProcessInputActivity inputActivity) throws IOException;
}
