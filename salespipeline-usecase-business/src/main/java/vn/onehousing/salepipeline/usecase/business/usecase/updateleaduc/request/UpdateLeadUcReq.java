package vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.onehousing.salepipeline.usecase.common.shared.model.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateLeadUcReq {

    @JsonProperty("lead_name")
    String leadName;

    @JsonProperty("phone_number")
    PhoneNumber phoneNumber;

    @JsonProperty("email")
    String email;

    @JsonProperty("referred_by_user_uuid")
    String referredByUserUuid;

    @JsonProperty("referred_by_user_code")
    String referredByUserCode;

    @JsonProperty("score")
    String score;

    @JsonProperty("priority")
    String priority;

    @JsonProperty("version")
    String version;

    @JsonProperty("source")
    String source;

    @JsonProperty("gender")
    String gender;

    @JsonProperty("bank_account")
    BankAccount bankAccount;

    @JsonProperty("duplicated_lead_uuid")
    String duplicatedLeadUuid;

    @JsonProperty("duplicated_lead_code")
    String duplicatedLeadCode;

    @JsonProperty("address")
    Address address;

    @JsonProperty("permanent_address")
    Address permanentAddress;

    @JsonProperty("profile")
    LeadProfile profile;

    @JsonProperty("unqualified_reason")
    String unqualifiedReason;

    @JsonProperty("lead_profile_uuid")
    String leadProfileUuid;

    @JsonProperty("account_uuid")
    String accountUuid;

    @JsonProperty("account_code")
    String accountCode;

    @JsonProperty("contact_uuid")
    String contactUuid;

    @JsonProperty("contact_code")
    String contactCode;

    @JsonProperty("opportunity_uuid")
    String opportunityUuid;

    String code;

    @JsonProperty("attribute_set_id")
    String attributeSetId;

    @JsonProperty("assigned_user_uuid")
    String assignedUserUuid;

    @JsonProperty("assigned_user_code")
    String assignedUserCode;

    @JsonProperty("campaign_uuid")
    String campaignUuid;

    @JsonProperty("campaign_code")
    String campaignCode;

    @JsonProperty("status")
    String status;

    @JsonProperty("attributes")
    @Valid
    List<AttributeValue> attributes = new ArrayList<>();

    @JsonProperty("channel")
    String channel;

    @JsonProperty("expected_assign_user_code")
    String expectedAssignedUserCode;

    @JsonProperty("expected_assign_user_uuid")
    String expectedAssignUserUuid;
    String primaryContactUuid;
    String primaryContactCode;
    Instant recordedDate;
    Instant lastRequestContactDate;
    Instant lastContactedDate;
    String contactRoles;
    @JsonProperty("id_documents")
    List<LeadIdDocument> idDocuments;
    @JsonProperty("name")
    String name;
    @JsonProperty("description")
    String description;
    @JsonProperty("product_code")
    String productCode;
    @JsonProperty("product_uuid")
    String productUuid;
    @JsonProperty("note")
    String note;
    @JsonProperty("closed_date")
    Instant closedDate;
    @JsonProperty("is_sign_contract")
    Boolean isSignContract;
    @JsonProperty("unassigned_reason")
    String unassignedReason;

    @JsonProperty("assigned_date")
    Instant assignedDate;

    List<User> assignedUsers;
    
    List<User> expectedUsers;
}
