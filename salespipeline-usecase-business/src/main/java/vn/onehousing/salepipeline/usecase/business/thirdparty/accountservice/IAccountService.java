package vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice;


import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.AccountContactCreateDto;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.AccountUpdateRequest;
import vn.onehousing.salepipeline.usecase.common.shared.model.Account;
import vn.onehousing.salepipeline.usecase.common.shared.model.Contact;

public interface IAccountService {
    Account createAccount(AccountContactCreateDto request);

    Contact createContact(AccountContactCreateDto request);

    Account updateAccount(AccountUpdateRequest request);

    Account getAccountByUuid(String accountUuid);

    Contact getContactByUuid(String contactUuid);
}