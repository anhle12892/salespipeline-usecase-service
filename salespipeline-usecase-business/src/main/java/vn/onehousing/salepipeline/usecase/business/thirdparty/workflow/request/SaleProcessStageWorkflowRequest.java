package vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.request;

import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@JsonNaming()
public class SaleProcessStageWorkflowRequest {
    Map<String, Map<String, List<String>>> variables;
}