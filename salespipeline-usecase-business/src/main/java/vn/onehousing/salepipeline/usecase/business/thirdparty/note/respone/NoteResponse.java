package vn.onehousing.salepipeline.usecase.business.thirdparty.note.respone;

import lombok.Builder;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;

@Data
@Builder
public class NoteResponse extends BaseResponse<Note> {

}
