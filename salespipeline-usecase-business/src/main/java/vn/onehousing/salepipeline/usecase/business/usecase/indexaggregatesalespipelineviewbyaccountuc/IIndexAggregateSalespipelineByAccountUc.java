package vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbyaccountuc;

import vn.onehousing.salepipeline.usecase.common.shared.model.Account;

import java.io.IOException;

public interface IIndexAggregateSalespipelineByAccountUc {
    void process(Account account) throws IOException;
}
