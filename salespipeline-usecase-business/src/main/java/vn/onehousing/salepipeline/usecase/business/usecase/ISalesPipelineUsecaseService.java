package vn.onehousing.salepipeline.usecase.business.usecase;

import org.springframework.data.domain.Pageable;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.AgentHistoryView;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;

import java.io.IOException;
import java.util.List;

public interface ISalesPipelineUsecaseService {
    List<OpportunityActivityView> getByUuid(String uuid) throws IOException;
    List<OpportunityActivityView> getByPhone(String phoneNumber) throws IOException;
    List<AgentHistoryView> getOppHistories(String uuid,
                                           Pageable pageable) throws IOException;
}
