package vn.onehousing.salepipeline.usecase.business.usecase.createsalespipelineuc;

import com.fasterxml.jackson.core.JsonProcessingException;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;

public interface ICreateSalesPipelineUseCase {
    SalesPipeline process(String leadUuid) throws JsonProcessingException;
}
