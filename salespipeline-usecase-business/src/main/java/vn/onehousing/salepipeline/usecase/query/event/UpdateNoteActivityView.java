package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

import java.time.Instant;

@Data
public class UpdateNoteActivityView {
    String id;
    String noteUuid;
    String noteCode;
    String ownerUuid;
    String ownerType;
    String ownerEmail;
    String parentUuid;
    String parentType;
    String title;
    String content;
    Instant createdDate;
    String createdBy;
    Instant lastModifiedDate;
    String lastModifiedBy;

    PhoneNumber phoneNumber;
    Long timestamp;
}
