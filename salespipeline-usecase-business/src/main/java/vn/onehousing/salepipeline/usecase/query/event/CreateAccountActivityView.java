package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.Address;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Data
public class CreateAccountActivityView {
    public String accountCode;
    public UUID accountUuid;
    public String accountName;
    public String accountStatus;
    public PhoneNumber phoneNumber;
    public String email;
    public Instant lastModifiedDate;
    public String lastModifiedBy;
    public Instant createdDate;
    public String createdBy;
    public String leadSource;
    public String leadChannel;
    public String description;
    public String code;
    public String bankAccountNumber;
    public String bankAccountHolder;
    public String bankCode;
    public String bankName;
    public UUID shippingAddressUuid;
    public BigDecimal annualRevenue;
    public String campaignCode;
    public UUID campaignUuid;
    public UUID referredByUserUuid;
    public String referredByUserCode;
    public UUID primaryContactUuid;
    public String primaryContactCode;
    private Address address;
    String note;
    private BigDecimal monthlyIncome;
    private BigDecimal monthlyPaymentLoan;
    private BigDecimal monthlyExpense;
    private Long timestamp;
}
