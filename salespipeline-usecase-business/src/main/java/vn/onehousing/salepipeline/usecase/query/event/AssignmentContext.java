package vn.onehousing.salepipeline.usecase.query.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AssignmentContext {
    public static final String LEAD_SOURCE = "lead_source";
    public static final String UUID = "uuid";
    public static final String OPPORTUNITY_UUID = "opportunity_uuid";
    public static final String AGENT_UUID = "agent_uuid";
    public static final String OPPORTUNITY_SOURCE = "opportunity_source";
    private Map<String, Object> data = new HashMap<>();
}