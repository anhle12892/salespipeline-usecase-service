package vn.onehousing.salepipeline.usecase.business.usecase.createhistoryuc;

import vn.onehousing.salepipeline.usecase.business.usecase.createhistoryuc.response.OpportunityHistoryRes;

import javax.annotation.Nullable;

public interface IInsertOppHistories {
    Boolean insert(OpportunityHistoryRes data, @Nullable Long timestamp);
}
