package vn.onehousing.salepipeline.usecase.business.thirdparty.task;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateTaskReq {
    @NotBlank
    String entityType;
    String entityUuid;
    @NotBlank
    String assignedToUserUuid;
    String assignedEmail;
    @NotBlank
    String name;
    String code;
    String description;
    String comments;
    @NotBlank
    String status;
    Instant startDate;
    Instant dueDate;
    String priority;
    String taskTypeCode;
    Instant remindDate;
    Instant completedDate;
    Boolean isDeleted;

    String createdBy;
    String lastModifiedBy;
    Instant createdDate;
    Instant lastModifiedDate;
}
