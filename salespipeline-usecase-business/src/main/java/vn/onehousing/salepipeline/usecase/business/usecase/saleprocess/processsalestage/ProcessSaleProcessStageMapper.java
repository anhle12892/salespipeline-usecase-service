package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.processsalestage;

import org.mapstruct.Mapper;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessActivity;

@Mapper(componentModel = "spring")
public interface ProcessSaleProcessStageMapper {
    SaleProcessActivity from(SaleProcessInputActivity input);
}
