package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;

@Data
public class PartnerBookingOpportunityUpdatedEvent {
    private Long bookingId;
    private String opportunityUuid;
    private String leadUuid; //leadUuid of OMRE-CRM (equal to leadUuid(LMS)/coreUuid(Agent)
    private String assignedAgentUuid;
    private String partnerBookingStatus;
}
