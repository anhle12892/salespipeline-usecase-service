package vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbyoppuc;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.onehousing.salepipeline.usecase.business.mapper.SalespipelineAggregateViewMapper;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.IAccountService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.ILeadService;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.utils.CommonUtils;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEntityType;
import vn.onehousing.salepipeline.usecase.common.shared.constants.RedisLockName;
import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;
import vn.onehousing.salepipeline.usecase.common.shared.model.Opportunity;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.ISalespipelineAggregateViewRepository;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.SalespipelineAggregateView;
import vn.onehousing.salepipeline.usecase.business.thirdparty.distributedlockservice.IDistributedLockService;

import java.io.IOException;

@Slf4j
@Service
@AllArgsConstructor
public class IndexAggregateSalespipelineViewByOppUc implements IIndexAggregateSalespipelineViewByOppUc {

    private final ObjectMapper objectMapper;

    private final SalespipelineAggregateViewMapper mapper;
    private final ISalespipelineAggregateViewRepository repository;
    private final ILeadService leadService;
    private final IAccountService accountService;

    @Autowired
    IDistributedLockService distributedLockService;

    @Override
    public void process(Opportunity data) throws IOException {

        log.info("[IndexAggregateSalespipelineViewByOppUc] - start sync - lead_uuid: [{}] - opp_uuid: [{}]  - lastModified: [{}]", data.getLeadUuid(), data.getOpportunityUuid());

        var key = CommonUtils.buildRedisLockKey(RedisLockName.SALESPIPELINE_USECASE_INDEX_AGGREGATE_LOCK, data.getLeadUuid());
        var ret = distributedLockService.getLock(key, data.getLeadUuid());
        if (!ret) {
            log.error("[IndexAggregateSalespipelineViewByOppUc] - cannot get lock - lead_uuid: [{}] - opp_uuid: [{}] - lastModified: [{}]", data.getLeadUuid(), data.getOpportunityUuid());
            return;
        }
        processAggregate(data);
        distributedLockService.releaseLock(key);
    }

    private void processAggregate(Opportunity data) throws IOException {
        SalespipelineAggregateView currentData = repository.getByUuid(data.getLeadUuid());

        if (currentData == null) {
            Lead lead = leadService.get(data.getLeadUuid());
            currentData = mapper.from(lead);
        }

        // not update when current's lastModifiedDate > data's lastModifiedDate
        // only when current type is OPPORTUNITY
        // when current type is LEAD, we can update and don't care about current's lastModifiedDate
        if (ActEntityType.OPPORTUNITY.equals(currentData.getType()) && !Boolean.TRUE.equals(data.getIsFromSys())) {
            if (currentData.getLastModifiedDate().toEpochMilli() > data.getLastModifiedDate().toEpochMilli()) {
                return;
            }
        }

//        Account account = accountService.getAccountByUuid(data.getAccountUuid());
//        if (!CollectionUtils.isEmpty(account.getPhoneNumber().getAdditionalPhones())) {
//            data.setAdditionalPhones(account.getPhoneNumber().getAdditionalPhones());
//        }
        mapper.updateFields(data, currentData);
        if (currentData.getAssignedUsers() != null) {
            currentData.setNumberAssignedUser(currentData.getAssignedUsers().size());
        } else {
            currentData.setNumberAssignedUser(0);
        }
//        currentData.setAddress(account.getAddress());
        repository.insert(currentData);
    }
}
