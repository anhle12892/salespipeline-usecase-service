package vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice;


import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.ContactSearchRequest;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.ContactUpdateRequest;
import vn.onehousing.salepipeline.usecase.common.shared.model.Contact;

public interface IContactService {
    Contact updateContact(ContactUpdateRequest request);
    Contact search(ContactSearchRequest contactSearchRequest);
    Contact getByContactUuid(String contactUuid);
}