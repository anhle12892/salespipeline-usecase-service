package vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice;


import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.request.CreateSalesPipelineReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.request.UpdateSalesPipelineReq;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;

public interface ISalesPipelineService {
    SalesPipeline update(String salesPipelineUuid, UpdateSalesPipelineReq req);

    SalesPipeline create(CreateSalesPipelineReq req);

    SalesPipeline getByLeadUuid(String leadUuid);

}
