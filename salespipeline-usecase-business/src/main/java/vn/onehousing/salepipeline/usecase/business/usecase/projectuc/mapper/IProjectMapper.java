package vn.onehousing.salepipeline.usecase.business.usecase.projectuc.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.thirdparty.masterservice.response.ProjectResponseDto;
import vn.onehousing.salepipeline.usecase.business.usecase.projectuc.request.Project;

@Component
public class IProjectMapper implements Converter<ProjectResponseDto, Project> {
    @Override
    public Project convert(ProjectResponseDto dto) {

        Project project = Project.builder()
                .id(dto.getProjectId())
                .projectUuid(dto.getProjectId())
                .name(dto.getProjectName())
                .code(dto.getProjectCode())
                .fbProjectCode(dto.getFbProjectCode())
                .tcbProjectCode(dto.getTcbProjectCode())
                .build();

        return project;
    }
}
