package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response;

import lombok.Data;

@Data
public class SaleStageResponse {
    Integer value;
    String title;
}
