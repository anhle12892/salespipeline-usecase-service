package vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration;

import lombok.Data;

import java.util.List;

@Data
public class OpportunityFormSubmissionMigration {
    List<FormSubmissionMigration> forms;
}
