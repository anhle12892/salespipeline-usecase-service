package vn.onehousing.salepipeline.usecase.business.usecase.insertopportunityactivityviewuc;

import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEntityType;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEventConst;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.IOpportunityActivityViewRepository;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class InsertOpportunityActivityViewUc implements IInsertOpportunityActivityViewUc {

    private final IOpportunityActivityViewRepository repository;

    @Override
    public void process(OpportunityActivityView activityView, @Nullable Long timestamp) {
        log.info("[InsertOpportunityActivityViewUc.process] Start, {}", activityView.getLeadUuid());
        //enrich data
        Map<String, Object> payload = activityView.getData();
        try {
            if (Strings.isNullOrEmpty(activityView.getLeadUuid())) {
                log.info("[InsertOpportunityActivityViewUc.process] Insert data account contact with body = {}", activityView);
                if (activityView.getParentType().equals(ActEntityType.ACCOUNT)) {
                    Optional<OpportunityActivityView> result = repository
                            .searchByChildData(List.of("phone_number", "event"),
                                    List.of(activityView.getPhoneNumber(), ActEventConst.CREATED_LEAD));
                    result.ifPresent(view -> {
                        activityView.setLeadUuid(view.getLeadUuid());
                    });
                } else if (activityView.getParentType().equals(ActEntityType.CONTACT)) {
                    Optional<OpportunityActivityView> result = repository
                            .searchByChildData(List.of("phone_number", "event"),
                                    List.of(activityView.getPhoneNumber(), ActEventConst.CREATED_LEAD));
                    result.ifPresent(view -> {
                        activityView.setLeadUuid(view.getLeadUuid());
                    });
                }
            }
            if (Strings.isNullOrEmpty(activityView.getPhoneNumber())) {
                if (!Strings.isNullOrEmpty(activityView.getLeadUuid())) {
                    Optional<OpportunityActivityView> result = repository.searchLeadInformation(activityView.getLeadUuid());
                    result.ifPresent(view -> activityView.setPhoneNumber(view.getPhoneNumber()));
                }
            }
        } catch (Exception ex) {
            log.error("[InsertOpportunityActivityViewUc.process] insert elasticsearch error with uuid {}", activityView.getUuid(), ex);
        }
        log.info("[InsertOpportunityActivityViewUc.process] Insert, {}", activityView.getLeadUuid());
        repository.insert(activityView, timestamp);
    }
}
