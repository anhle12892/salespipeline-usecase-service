package vn.onehousing.salepipeline.usecase.business.usecase.createopportunityuc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.ILeadService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.request.UpdateLeadReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.IOpportunityService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.mapper.CreateOpportunityReqMapper;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.request.CreateOpportunityReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.ISalesPipelineService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.request.UpdateSalesPipelineReq;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.HousingErrors;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.HousingException;
import vn.onehousing.salepipeline.usecase.common.shared.model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
@AllArgsConstructor
public class CreateOpportunityUseCase implements ICreateOpportunityUseCase {

    private final IOpportunityService opportunityServiceClient;
    private final ILeadService leadServiceClient;
    private final ISalesPipelineService salesPipelineServiceClient;
    private final CreateOpportunityReqMapper createOpportunityReqMapper;
    private final ObjectMapper objectMapper;

    @Override
    public Opportunity create(String leadUuid) throws JsonProcessingException {
        Lead lead = this.leadServiceClient.get(leadUuid);

        SalesPipeline salesPipeline = this.salesPipelineServiceClient.getByLeadUuid(leadUuid);
        if (StringUtils.isEmpty(salesPipeline.getAccountUuid())
            || StringUtils.isEmpty(salesPipeline.getContactUuid())) {
            throw new HousingException(
                HousingErrors.MISSING_ACCOUNT_CONTACT,
                String.format(
                    "lead_uuid: [%s] - salespipeline_uuid: [%s] - must have account/contact in order to create opportunity",
                    leadUuid,
                    salesPipeline.getUuid()
                )
            );
        }

        var createOpportunityReq = createOpportunityReqMapper.from(lead);
        createOpportunityReq.setAccountUuid(salesPipeline.getAccountUuid());
        createOpportunityReq.setAccountCode(salesPipeline.getAccountCode());
        removeDynamicAttributeDontHaveConfig(createOpportunityReq);
        log.info("CreateOpportunityUseCase.create - lead_uuid: [{}] - data: [{}]", leadUuid, objectMapper.writeValueAsString(createOpportunityReq));
        var opportunity = this.opportunityServiceClient.create(createOpportunityReq);

        var leadUpdateReq = toLeadUpdateRequest(opportunity);
        leadUpdateReq.setIsConverted(true);
        this.leadServiceClient.update(leadUuid, toLeadUpdateRequest(opportunity));

        this.salesPipelineServiceClient.update(salesPipeline.getUuid(), toUpdateSalesPipelineReq(opportunity));

        return opportunity;
    }

    private void removeDynamicAttributeDontHaveConfig(CreateOpportunityReq req) {

        if (req.getAttributes() == null) {
            return;
        }

        var attributeConfig = this.opportunityServiceClient.getAttributeConfigs();
        Map<String, AttributeConfig> attributeConfigMap = Arrays.stream(attributeConfig)
            .collect(Collectors.toMap(AttributeConfig::getFieldName, a -> a));

        List<AttributeValue> rightList = new ArrayList<>();

        for (var attribute : req.getAttributes()) {
            if (!attributeConfigMap.containsKey(attribute.getName()))
                continue;

            rightList.add(attribute);
        }

        req.setAttributes(rightList);

    }

    private UpdateLeadReq toLeadUpdateRequest(Opportunity opportunityDto) {
        return UpdateLeadReq.builder()
            .opportunityCode(opportunityDto.getOpportunityCode())
            .opportunityUuid(opportunityDto.getOpportunityUuid())
            .build();
    }

    private UpdateSalesPipelineReq toUpdateSalesPipelineReq(Opportunity opportunity) {
        return UpdateSalesPipelineReq.builder()
            .opportunityCode(opportunity.getOpportunityCode())
            .opportunityUuid(opportunity.getOpportunityUuid())
            .build();
    }
}
