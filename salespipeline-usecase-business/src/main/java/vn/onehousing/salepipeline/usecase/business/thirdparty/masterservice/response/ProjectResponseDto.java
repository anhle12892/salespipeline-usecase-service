package vn.onehousing.salepipeline.usecase.business.thirdparty.masterservice.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class ProjectResponseDto {
    private static final long serialVersionUID = 8950301490064835716L;

    @JsonProperty("uuid")
    private String projectId;
    @JsonProperty("code")
    private String projectCode;
    @JsonProperty("name")
    private String projectName;
    @JsonProperty("group")
    private String projectGroup;
    @JsonProperty("status")
    private String projectStatus;
    @JsonProperty("fbProjectCode")
    private String fbProjectCode;
    @JsonProperty("tcbProjectCode")
    private String tcbProjectCode;
}
