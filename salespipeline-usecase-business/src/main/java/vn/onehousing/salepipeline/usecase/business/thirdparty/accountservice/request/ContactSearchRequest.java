package vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactSearchRequest {
    String phoneNumber;
}
