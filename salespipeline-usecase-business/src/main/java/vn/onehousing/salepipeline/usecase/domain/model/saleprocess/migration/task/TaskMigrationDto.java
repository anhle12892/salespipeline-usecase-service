package vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.task;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskMigrationDto {
    Long opportunityId;
    String coreUuid;
    String data;
    String formPath;
    String content;
    String agentUuid;
    String agentEmail;
    Instant createTime;
}
