package vn.onehousing.salepipeline.usecase.business.usecase.createhistoryuc.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
public class OpportunityHistoryRes {
    private String historyUuid;
    private String parentUuid;
    private String parentType;
    private String entityType;
    private String entityUuid;
    private String leadUuid;
    private String opportunitySource;
    private String content;
    private String noteSource;
    private String recordType;
    private List<String> interactionWay;
    private List<String> suggestionNotes;
    private String buyingNeedType;
    private String landingPageUrl;
    private Instant createdTime;
    private Long timestamp;

    public OpportunityHistoryRes genHistoryUuid() {
        if (Objects.isNull(this.historyUuid)) {
            this.historyUuid = UUID.randomUUID().toString();
        }
        return this;
    }
}
