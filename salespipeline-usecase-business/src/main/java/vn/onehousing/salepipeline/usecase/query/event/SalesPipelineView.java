package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

import java.time.Instant;

@Data
public class SalesPipelineView {
    String id;
    String uuid;
    String leadUuid;
    String leadCode;
    String assignedUserUuid;
    String assignedUserCode;
    String accountUuid;
    String accountCode;
    String contactUuid;
    String contactCode;
    String campaignUuid;
    String campaignCode;
    String opportunityUuid;
    String opportunityCode;
    String referredByUserUuid;
    String referredByUserCode;
    String saleStage;
    String source;
    String note;
    String duplicatedLeadUuid;
    String duplicatedLeadCode;
    Instant closedDate;
    Instant createdDate;
    Instant lastModifiedDate;
    String createdBy;
    String lastModifiedBy;

    PhoneNumber phoneNumber;
    Long timestamp;
}
