package vn.onehousing.salepipeline.usecase.business.mapper;

import org.mapstruct.Mapper;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

@Mapper(componentModel = "spring")
public interface PhoneMapper {
    PhoneNumber from(String value);

    default String to(PhoneNumber phone) {
        return phone == null ? null : phone.getNumber();
    }
}
