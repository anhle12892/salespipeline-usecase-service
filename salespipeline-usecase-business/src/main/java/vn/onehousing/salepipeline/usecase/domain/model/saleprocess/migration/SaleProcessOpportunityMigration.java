package vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration;

import lombok.Data;
import org.springframework.util.StringUtils;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.AgentSaleProcessBookingStatus;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.OpportunityAttribute;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessTaskWorkflow;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
public class SaleProcessOpportunityMigration {
    String leadUuid;

    // opportunity attributes
    String id;
    String opportunityUuid;
    String mainViews;
    String balconyDirections;
    String propertyRangeFloor;
    String numOfBedroom;
    String budgetLimitId;
    String apartmentTypeIds;
    boolean mortgageNeed;

    String createTime;
    String updateTime;
    String status;

    // form actions

    // booking status
    // boolean bookingSent;
    // boolean bookingConfirmedToSuccess;


    // appointment status
    // boolean appointmentCreated;

    List<String> formActivities = new ArrayList<>();
    List<String> appointmentsCreated = new ArrayList<>();
    List<String> bookingsStatus = new ArrayList<>();

    public List<String> toActivities() {
        var activities = new ArrayList<String>();
        if (StringUtils.hasText(mainViews)) {
            activities.add(OpportunityAttribute.MAIN_VIEWS);
            // activities.add(SaleProcessTaskWorkflow.UPDATED_ANY_INFORMATION_OF_OPPORTUNITY);
        }

        if (StringUtils.hasText(balconyDirections)) {
            activities.add(OpportunityAttribute.BALCONY_DIRECTIONS);
            // activities.add(SaleProcessTaskWorkflow.UPDATED_ANY_INFORMATION_OF_OPPORTUNITY);
        }

        if (StringUtils.hasText(propertyRangeFloor)) {
            activities.add(OpportunityAttribute.PROPERTY_RANGE_FLOOR);
            // activities.add(SaleProcessTaskWorkflow.UPDATED_ANY_INFORMATION_OF_OPPORTUNITY);
        }

        if (StringUtils.hasText(numOfBedroom)) {
            activities.add(OpportunityAttribute.NB_BEDROOM);
            // activities.add(SaleProcessTaskWorkflow.UPDATED_ANY_INFORMATION_OF_OPPORTUNITY);
        }
        if (StringUtils.hasText(budgetLimitId)) {
            activities.add(OpportunityAttribute.BUDGET_LIMIT);
            // activities.add(SaleProcessTaskWorkflow.UPDATED_ANY_INFORMATION_OF_OPPORTUNITY);
        }

        if (StringUtils.hasText(apartmentTypeIds)) {
            activities.add(OpportunityAttribute.PROPERTY_TYPES);
            // activities.add(SaleProcessTaskWorkflow.UPDATED_ANY_INFORMATION_OF_OPPORTUNITY);
        }

        if (mortgageNeed) {
            activities.add(OpportunityAttribute.MORTGAGE_NEED);
            // activities.add(SaleProcessTaskWorkflow.UPDATED_ANY_INFORMATION_OF_OPPORTUNITY);
        }

        if (StringUtils.hasText(status)
            && !"REVOKED".equals(status)
            && StringUtils.hasText(updateTime)
            && !updateTime.equals(createTime)) {
            activities.add(SaleProcessTaskWorkflow.UPDATED_ANY_INFORMATION_OF_OPPORTUNITY);
//            try {
//                var createdTime = Timestamp.valueOf(createTime).toLocalDateTime();
//                var updatedTime = Timestamp.valueOf(updateTime).toLocalDateTime();
//                updatedTime = updatedTime.minusSeconds(5);
//                if (updatedTime.compareTo(createdTime) > 0) {
//
//                }
//            } catch (Exception exception) {
//
//            }
        }

//        if (bookingConfirmedToSuccess) {
//            activities.add(AgentSaleProcessBookingStatus.AGENT_PARTNER_UPDATED_BOOKING_STATUS_TO_SUCCESS);
//        }
//
//        if (bookingSent) {
//            activities.add(AgentSaleProcessBookingStatus.AGENT_SENT_BOOKING_TO_PARTNER);
//        }
//
//        if (appointmentCreated) {
//            activities.add(SaleProcessTaskWorkflow.CREATED_AN_APPOINTMENT);
//        }

        activities.addAll(formActivities);
        activities.addAll(appointmentsCreated);
        activities.addAll(bookingsStatus);

        return activities;
    }
}
