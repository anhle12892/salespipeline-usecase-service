package vn.onehousing.salepipeline.usecase.business.mapper;

import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.InvalidParameterException;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper(componentModel = "spring")
public interface EavAttributeViewMapper {

    @Named("EavAttributeModelMapper")
    default Map<String, Object> fromModel(List<AttributeValue> values) {
        if (values == null || values.isEmpty()) {
            return null;
        }
        Map<String, Object> result = new HashMap<>();
        for (int i = 0; i < values.size(); i++) {
            AttributeValue attributeValue = values.get(i);
            if (StringUtils.isEmpty(attributeValue.getName())) {
                throw new InvalidParameterException("attribute at index [" + i + "] is have name that be null");
            }
            if (attributeValue.getValue() == null) {
                throw new InvalidParameterException("attribute " + attributeValue.getName() + " don't have value");
            }
            result.put(attributeValue.getName(), attributeValue.getValue());
        }
        return result;
    }

}
