package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SaleProcessStageResponse {
    Integer saleStage;
    List<SaleStageResponse> saleStages;
}
