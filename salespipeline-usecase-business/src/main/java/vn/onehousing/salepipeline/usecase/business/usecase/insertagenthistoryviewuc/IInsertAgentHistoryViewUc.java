package vn.onehousing.salepipeline.usecase.business.usecase.insertagenthistoryviewuc;


import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.AgentHistoryView;

import javax.annotation.Nullable;


public interface IInsertAgentHistoryViewUc {
    void process(AgentHistoryView lead, @Nullable Long timestamp);
}
