package vn.onehousing.salepipeline.usecase.domain.model.saleprocess;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessInputActivityType;

import java.util.ArrayList;
import java.util.List;

@Data
public class SaleProcessInputActivity {
    Integer activityType = SaleProcessInputActivityType.UNKNOWN;

    String leadUuid;
    List<String> activities = new ArrayList<>();
}
