package vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration;

import vn.onehousing.salepipeline.usecase.common.shared.model.Task;

import java.util.List;

public interface OpportunityAgentRepository {
    List<SaleProcessOpportunityMigration> getOpportunities(List<String> uuids);

    List<SaleProcessOpportunityMigration> get(String id);

    List<Task> getListTask(String uuid);

    void writeMigrationHistory(String source,
                               String leadUuid,
                               String opportunityId,
                               String type,
                               String stage,
                               Object data,
                               String status,
                               String message);
}
