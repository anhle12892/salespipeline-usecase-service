package vn.onehousing.salepipeline.usecase.business.thirdparty.finance;

import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.request.FinancialCreateReq;

import java.util.List;

public interface IFinanceService {
    List<Financial> search(String leadUuid);
    Financial createFinancial(FinancialCreateReq request);
}
