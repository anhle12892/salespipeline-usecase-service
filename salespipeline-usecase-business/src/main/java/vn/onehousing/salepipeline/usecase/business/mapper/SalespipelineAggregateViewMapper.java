package vn.onehousing.salepipeline.usecase.business.mapper;

import org.mapstruct.*;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEntityType;
import vn.onehousing.salepipeline.usecase.common.shared.model.*;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.SalespipelineAggregateView;

@Mapper(componentModel = "spring", uses = {
    UUIDMapper.class,
    PhoneMapper.class,
    EavAttributeViewMapper.class
})
public interface SalespipelineAggregateViewMapper {

    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "event.leadName", target = "name")
    @Mapping(constant = ActEntityType.LEAD, target = "type")
    @Mapping(target = "mapAttributes", source = "event.attributes", qualifiedByName = "EavAttributeModelMapper")
    SalespipelineAggregateView from(Lead event);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "input.phoneNumber", target = "phoneNumber.number")
    @Mapping(source = "input.additionalPhones", target = "phoneNumber.additionalPhones")
    @Mapping(constant = ActEntityType.OPPORTUNITY, target = "type")
    @Mapping(target = "mapAttributes", source = "attributes", qualifiedByName = "EavAttributeModelMapper")
    @Mapping(target = "createdDate", ignore = true)
    void updateFields(Opportunity input, @MappingTarget SalespipelineAggregateView target);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(constant = ActEntityType.LEAD, target = "type")
    @Mapping(source = "input.leadName", target = "name")
    @Mapping(target = "mapAttributes", source = "attributes", qualifiedByName = "EavAttributeModelMapper")
    void updateFields(Lead input, @MappingTarget SalespipelineAggregateView target);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "input.uuid", target = "salespipeline_uuid")
    @Mapping(source = "input.lastModifiedDate", target = "salespipelineLastModifiedDate")
    @Mapping(source = "input.createdDate", target = "salespipelineCreatedDate")
    @Mapping(source = "input.createdBy", target = "salespipelineCreatedBy")
    @Mapping(source = "input.lastModifiedBy", target = "salespipelineLastModifiedBy")
    @Mapping(source = "input.lastModifiedDate", target = "lastModifiedDate", ignore = true)
    @Mapping(source = "input.createdDate", target = "createdDate", ignore = true)
    @Mapping(source = "input.createdBy", target = "createdBy", ignore = true)
    @Mapping(source = "input.lastModifiedBy", target = "lastModifiedBy", ignore = true)
    void updateFields(SalesPipeline input, @MappingTarget SalespipelineAggregateView target);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "input.description", target = "description", ignore = true)
    void updateFields(Contact input, @MappingTarget SalespipelineAggregateView target);

    void updateFields(Contact input, @MappingTarget LeadProfile target);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "input.lastModifiedDate", target = "lastModifiedDate", ignore = true)
    @Mapping(source = "input.createdDate", target = "createdDate", ignore = true)
    @Mapping(source = "input.createdBy", target = "createdBy", ignore = true)
    @Mapping(source = "input.description", target = "description", ignore = true)
    @Mapping(source = "input.bankCode", target = "target.bankAccount.bankCode")
    @Mapping(source = "input.bankAccountNumber", target = "target.bankAccount.accountNumber")
    @Mapping(source = "input.bankAccountHolder", target = "target.bankAccount.accountHolder")
    @Mapping(source = "input.bankName", target = "target.bankAccount.bankName")
    @Mapping(source = "input.bankBranch", target = "target.bankAccount.bankBranch")
    void updateFields(Account input, @MappingTarget SalespipelineAggregateView target);

    void updateFields(Account input, @MappingTarget LeadProfile target);
}


