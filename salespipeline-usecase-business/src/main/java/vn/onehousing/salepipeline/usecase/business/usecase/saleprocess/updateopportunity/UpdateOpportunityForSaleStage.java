package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.updateopportunity;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.processsalestage.IProcessSaleProcessStage;
import vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.request.UpdateLeadUcReq;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessTaskWorkflow;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeValue;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;

import java.io.IOException;
import java.util.stream.Collectors;

@Slf4j
@Component
@AllArgsConstructor
public class UpdateOpportunityForSaleStage implements IUpdateOpportunityForSaleStage {

    private final IProcessSaleProcessStage action;

    @Override
    public void process(String leadUuid, UpdateLeadUcReq request) throws IOException {

        log.info(
            "[UpdateOpportunityForSaleStage -> process] sale stage process with lead uuid: {} and body: {}",
            leadUuid,
            request
        );

        var inputActivity = new SaleProcessInputActivity();
        inputActivity.setLeadUuid(leadUuid);
        inputActivity.getActivities().add(SaleProcessTaskWorkflow.UPDATED_ANY_INFORMATION_OF_OPPORTUNITY);
        inputActivity.getActivities().addAll(
            request.getAttributes()
                .stream()
                .map(AttributeValue::getName)
                .collect(Collectors.toList())
        );

        action.process(inputActivity);
    }
}
