package vn.onehousing.salepipeline.usecase.business.usecase.insertagenthistoryviewuc;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.AgentHistoryView;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.IAgentHistoryViewRepository;

import javax.annotation.Nullable;

@Slf4j
@Service
@RequiredArgsConstructor
public class InsertAgentHistoryViewUc implements IInsertAgentHistoryViewUc {

    private final IAgentHistoryViewRepository repository;

    @Override
    public void process(AgentHistoryView lead, @Nullable Long timestamp) {
        repository.insert(lead, timestamp);
    }
}
