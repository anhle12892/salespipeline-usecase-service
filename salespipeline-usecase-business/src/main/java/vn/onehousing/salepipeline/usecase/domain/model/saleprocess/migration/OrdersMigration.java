package vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration;

import lombok.Data;

@Data
public class OrdersMigration {
    String opportunityId;
    String status;
}
