package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Data
public class CreateLeadActivityView {

    String leadId;

    String leadName;

    PhoneNumber phoneNumber;

    String email;

    String leadUuid;

    String leadCode;

    String assignedUserUuid;

    String assignedUserCode;

    String campaignUuid;

    String campaignCode;

    String status;

    Boolean isConverted;

    String referredByUserUuid;

    String referredByUserCode;

    String attributeSetUuid;

    BigDecimal score;

    String priority;

    String unqualifiedReason;

    String leadProfileUuid;

    String accountUuid;

    String accountCode;

    String contactUuid;

    String contactCode;

    String opportunityUuid;

    String opportunityCode;

    String attributeSetId;

    String source;

    String gender;

    String version;

    LeadBankAccount bankAccount;

    String duplicatedLeadUuid;

    String duplicatedLeadCode;

    Instant convertedDate;

    Address address;

    LeadProfile profile;

    List<AttributeValue> attributes;

    Instant recordedDate;

    String channel;

    String expectedAssignedUserCode;

    String expectedAssignUserUuid;

    List<LeadIdDocument> idDocuments;

    String note;
    String partnerLeadUuid;

    Instant lastModifiedDate;
    Instant createdDate;
    String createdBy;
    String lastModifiedBy;
    Long timestamp;

}
