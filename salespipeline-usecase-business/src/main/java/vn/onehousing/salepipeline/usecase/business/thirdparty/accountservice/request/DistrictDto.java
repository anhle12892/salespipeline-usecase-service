package vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DistrictDto {
    private String districtId;
    private String districtName;
    private String districtCode;
}
