package vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.request;

import lombok.Data;

@Data
public class CreateSalesPipelineReq {
    String leadUuid;
    String leadCode;
    String assignedUserUuid;
    String assignedUserCode;
    String accountUuid;
    String accountCode;
    String contactUuid;
    String contactCode;
    String campaignUuid;
    String campaignCode;
    String opportunityUuid;
    String opportunityCode;
    String referredByUserUuid;
    String referredByUserCode;
    String source;
    String channel;
    String note;
    String duplicatedLeadUuid;
    String duplicatedLeadCode;
    String saleStage;
}
