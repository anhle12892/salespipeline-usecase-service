package vn.onehousing.salepipeline.usecase.business.thirdparty.note;

import vn.onehousing.salepipeline.usecase.business.thirdparty.note.request.NoteCreateDto;
import vn.onehousing.salepipeline.usecase.business.thirdparty.note.request.NoteUpdateAudit;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;

import java.util.List;
import java.util.Optional;

public interface INoteService {
    Note createNote(NoteCreateDto req);

    Note createNoteWithoutEvent(NoteCreateDto req);

    Note updateNote(String noteUuid, NoteCreateDto req);

    Note updateNoteWithoutEvent(String noteUuid, NoteCreateDto req);

    Optional<String> getNoteUuidByParentUuidAndParentType(String parentUuid, String parentType);

    List<Note> getNotesByParentTypeAndOwnerType(String parentType, String ownerType, String parentUuid);

    Optional<Note> getNoteByUniqueData(String parentUuid, String parentType, String ownerType, String ownerUuid, List<String> ignoreUuids);

    String updateNoteAuditTime(String noteUuid, NoteUpdateAudit req);
}
