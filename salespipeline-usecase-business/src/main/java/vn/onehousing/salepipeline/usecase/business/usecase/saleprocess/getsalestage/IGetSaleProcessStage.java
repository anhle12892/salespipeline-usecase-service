package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.getsalestage;

import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request.SaleProcessStageRequest;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response.SaleProcessStageResponse;

public interface IGetSaleProcessStage {
    SaleProcessStageResponse get(SaleProcessStageRequest request);
}
