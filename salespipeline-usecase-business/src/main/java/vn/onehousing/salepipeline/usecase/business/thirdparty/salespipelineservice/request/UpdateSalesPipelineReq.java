package vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
public class UpdateSalesPipelineReq {
    String leadCode;
    String assignedUserUuid;
    String assignedUserCode;
    String accountUuid;
    String accountCode;
    String contactUuid;
    String contactCode;
    String campaignUuid;
    String campaignCode;
    String opportunityUuid;
    String opportunityCode;
    String referredByUserUuid;
    String referredByUserCode;
    String saleStage;
    String source;
    String channel;
    String note;
    String disqualifiedReason;
    String closedLostReason;
    String duplicatedLeadUuid;
    String duplicatedLeadCode;
    Instant closedDate;
}
