package vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeValue;
import vn.onehousing.salepipeline.usecase.common.shared.model.User;

import java.time.Instant;
import java.util.List;

@Data
public class UpdateOpportunityReq {
    private List<AttributeValue> attributes;
    private String opportunityCode;
    private String name;
    private String description;
    private String accountUuid;
    private String accountCode;
    private String productCode;
    private String productUuid;
    private String note;
    private String campaignUuid;
    private String campaignCode;
    private Instant closedDate;
    @JsonProperty("is_sign_contract")
    private Boolean isSignContract;
    private String status;
    private Instant recordedDate;
    private String priority;
    @JsonProperty("unassigned_reason")
    private String unassignedReason;
    private String assignedUserUuid;
    private String assignedUserCode;
    @JsonProperty("assigned_date")
    private Instant assignedDate;
    private String source;
    private String channel;
    private String gender;
    List<User> assignedUsers;
}
