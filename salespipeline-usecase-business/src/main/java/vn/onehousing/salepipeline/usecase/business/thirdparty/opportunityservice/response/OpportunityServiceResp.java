package vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.response;

import lombok.Builder;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.model.Opportunity;

@Data
@Builder
public class OpportunityServiceResp extends BaseResponse<Opportunity> {
}
