package vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
public class SalespipelineAggregateView {

    String type;

    //lead
    String name;
    PhoneNumber phoneNumber;
    String email;
    String leadUuid;
    String leadCode;
    String assignedUserUuid;
    String assignedUserCode;
    String campaignUuid;
    String campaignCode;
    String status;
    String referredByUserUuid;
    String referredByUserCode;
    String attributeSetUuid;
    BigDecimal score;
    String partnerLeadUuid;

    @JsonProperty("is_converted")
    Boolean isConverted;
    String priority;
    String unqualifiedReason;
    String leadProfileUuid;
    String accountUuid;
    String accountCode;
    String contactUuid;
    String contactCode;
    String opportunityUuid;
    String opportunityCode;
    String attributeSetId;
    String source;
    String gender;
    String version;
    LeadBankAccount bankAccount;
    String duplicatedLeadUuid;
    String duplicatedLeadCode;
    Instant convertedDate;
    Address address;
    Address permanentAddress;
    LeadProfile profile;
    Instant assignedDate;
    List<User> assignedUsers;
    List<User> expectedUsers;

    public void setAttributes(List<AttributeValue> attributes) {
        this.attributes = attributes;
        this.mapAttributes = attributes.stream().collect(Collectors.toMap(AttributeValue::getName, AttributeValue::getValue));
    }

    List<AttributeValue> attributes;

    Map<String, Object> mapAttributes;

    @JsonAnyGetter
    public Map<String, Object> getMapAttributes() {
        return mapAttributes;
    }

    String channel;
    String expectedAssignedUserCode;
    String expectedAssignUserUuid;
    List<LeadIdDocument> idDocuments;
    String note;
    String unassignedReason;

    //opp
    String description;
    String productCode;
    String productUuid;
    Instant closedDate;
    String attributeSetValue;
    Boolean isSignContract;

    // audit
    Instant recordedDate;

    Instant lastModifiedDate;
    Instant createdDate;
    String createdBy;
    String lastModifiedBy;

    Instant salespipelineLastModifiedDate;
    Instant salespipelineCreatedDate;
    String salespipelineCreatedBy;
    String salespipelineLastModifiedBy;

    // salespipeline
    String salespipeline_uuid;
    String disqualifiedReason;
    String closedLostReason;
    String saleStage;

    Instant documentVersion;

    // only in aggregate
    Integer numberRequiredUser;
    Integer numberAssignedUser;
}
