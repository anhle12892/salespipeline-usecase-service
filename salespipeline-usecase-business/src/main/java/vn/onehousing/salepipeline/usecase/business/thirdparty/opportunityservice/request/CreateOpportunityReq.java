package vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeValue;
import vn.onehousing.salepipeline.usecase.common.shared.model.User;

import javax.validation.constraints.NotBlank;
import java.time.Instant;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateOpportunityReq {
    @NotBlank
    private String leadUuid;
    private String leadCode;
    private String name;
    private String description;
    private String accountUuid;
    private String accountCode;
    private String productCode;
    private String productUuid;
    private String referredByUserUuid;
    private String referredByUserCode;
    private String assignedUserUuid;
    private String assignedUserCode;
    private String note;
    private String campaignUuid;
    private String campaignCode;
    private Instant closedDate;
    private String attributeSetValue;
    private String source;
    private String channel;
    private String phoneNumber;
    private String gender;
    private String unqualifiedReason;
    private String priority;
    private String status;
    private Instant recordedDate;
    private List<AttributeValue> attributes;
    private Instant assignedDate;
    private List<User> assignedUsers;
}
