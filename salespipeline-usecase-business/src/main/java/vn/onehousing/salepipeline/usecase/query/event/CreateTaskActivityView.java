package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

import java.time.Instant;

@Data
public class CreateTaskActivityView {
    private Long taskId;
    private String taskUuid;
    private String status;
    private String name;
    private String entityType;
    private String entityUuid;
    private String assignedToUserUuid;
    private String comments;
    private String description;
    private Instant startDate;
    private Instant dueDate;
    private String priority;
    private Instant completedDate;
    private String taskTypeCode;
    private String taskTypeName;
    private Instant remindDate;
    private Boolean isDeleted;

    private Instant lastModifiedDate;
    private String createdBy;
    private String lastModifiedBy;
    private Instant createdDate;
    private PhoneNumber phoneNumber;

    private String code;
    private Long timestamp;
}
