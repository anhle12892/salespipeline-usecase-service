package vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbycontactuc;

import vn.onehousing.salepipeline.usecase.common.shared.model.Contact;

import java.io.IOException;

public interface IIndexAggregateSalespipelineByContactUc {
    void process(Contact contact) throws IOException;
}
