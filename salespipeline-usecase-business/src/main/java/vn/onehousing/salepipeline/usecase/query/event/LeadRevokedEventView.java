package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

@Data
public class LeadRevokedEventView {
    String leadUuid;
    String revokedReason;
    String revokedType;
    PhoneNumber phoneNumber;
    Long timestamp;
}
