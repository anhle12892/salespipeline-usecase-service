package vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.IAccountService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.IContactService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.AccountUpdateRequest;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.ContactUpdateRequest;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.ILeadService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.request.UpdateLeadReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.IOpportunityService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.ISalesPipelineService;
import vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.mapper.IUpdateLeadUcReqMapper;
import vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.request.UpdateLeadUcReq;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.InvalidParameterException;
import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;
import vn.onehousing.salepipeline.usecase.common.shared.model.LeadProfile;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;
import vn.onehousing.salepipeline.usecase.common.shared.model.User;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.ISalespipelineAggregateViewRepository;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.SalespipelineAggregateView;

import java.io.IOException;
import java.time.Instant;
import java.util.List;


@Slf4j
@Service
@AllArgsConstructor
public class UpdateLeadUc implements IUpdateLeadUc {

    private final ILeadService leadService;
    private final IAccountService accountService;
    private final IContactService contactService;
    private final IOpportunityService opportunityService;
    private final ISalesPipelineService salesPipelineService;
    private final IUpdateLeadUcReqMapper updateLeadUcReqMapper;
    private final ObjectMapper objectMapper;
    private final ISalespipelineAggregateViewRepository salespipelineAggregateViewRepository;

    @Override
    public SalesPipeline process(String leadUuid, UpdateLeadUcReq req) throws JsonProcessingException, IllegalAccessException {
        log.info("UpdateLeadUc.process -  Process Lead with Lead uuid: {} with body: {}", leadUuid, objectMapper.writeValueAsString(req));

        Lead lead = leadService.get(leadUuid);
        SalesPipeline salesPipeline = salesPipelineService.getByLeadUuid(leadUuid);

        String accountUuid = salesPipeline.getAccountUuid();
        String contactUuid = salesPipeline.getContactUuid();
        String oppUuid = salesPipeline.getOpportunityUuid();

        // if the lead does not have an account, contact , we will update to lead
        if (!StringUtils.hasText(accountUuid) && !StringUtils.hasText(contactUuid)) {
            UpdateLeadReq updateLeadReq = updateLeadUcReqMapper.toUpdateLeadReq(req);
            log.info("Update Lead with Lead uuid: {} with body: {}", leadUuid, objectMapper.writeValueAsString(updateLeadReq));
            leadService.update(lead.getLeadUuid(), updateLeadReq);
            var requestUpdateSalesPipeline = updateLeadUcReqMapper.toUpdateSalesPipelineReq(req);
            requestUpdateSalesPipeline.setLeadCode(req.getCode());
            return salesPipelineService.update(salesPipeline.getUuid(), requestUpdateSalesPipeline);
        }
        if (StringUtils.hasText(accountUuid) && StringUtils.hasText(contactUuid)) {

            AccountUpdateRequest accountUpdateRequest = transformAccountFromRequest(req, accountUuid);
            if (!accountUpdateRequest.isAllFieldsNull()) {
                accountService.updateAccount(accountUpdateRequest);
            }

            ContactUpdateRequest contactUpdateRequest = transformContactFromRequest(req, contactUuid);
            if (!contactUpdateRequest.isAllFieldsNull()) {
                contactService.updateContact(contactUpdateRequest);
            }

            //  if the lead already has an account , contact but does not have the opportunity , we will update to account, contact
            // and update eav information to lead
            if (!StringUtils.hasText(oppUuid)) {

                // update attribute's value to lead
                UpdateLeadUcReq updateEav = new UpdateLeadUcReq();
                updateEav.setAttributes(req.getAttributes());
                leadService.update(lead.getLeadUuid(), updateLeadUcReqMapper.toUpdateLeadReq(updateEav));

            } else {
                //  if the lead already has an account , contact and opportunity , we will update to account, contact and opportunity
                opportunityService.update(salesPipeline.getOpportunityUuid(), updateLeadUcReqMapper.toUpdateOpportunityReq(req));
            }
        } else {
            throw new InvalidParameterException("account_uuid and contact_uuid must not be null");
        }

        var requestUpdateSalesPipeline = updateLeadUcReqMapper.toUpdateSalesPipelineReq(req);
        requestUpdateSalesPipeline.setOpportunityCode(req.getCode());
        return salesPipelineService.update(salesPipeline.getUuid(), requestUpdateSalesPipeline);
    }

    @Override
    public SalesPipeline processMigration(String leadUuid, UpdateLeadUcReq req) {
        log.info("Migrate multi assigned users to crm model process - Lead uuid {}", leadUuid);

        Lead lead = leadService.get(leadUuid);
        SalesPipeline salesPipeline = salesPipelineService.getByLeadUuid(leadUuid);

        String accountUuid = salesPipeline.getAccountUuid();
        String contactUuid = salesPipeline.getContactUuid();
        String oppUuid = salesPipeline.getOpportunityUuid();

        // if the lead does not have an account, contact , we will update to lead
        if (!StringUtils.hasText(accountUuid) && !StringUtils.hasText(contactUuid)) {
            UpdateLeadReq updateLeadReq = updateLeadUcReqMapper.toUpdateLeadReq(req);
            log.info("Update Migrate Lead with Lead uuid: {}", leadUuid);
            leadService.updateMigration(lead.getLeadUuid(), updateLeadReq);
        }
        if (StringUtils.hasText(accountUuid) && StringUtils.hasText(contactUuid) && StringUtils.hasText(oppUuid)) {
            log.info("Update Migrate opportunity with Lead uuid: {}", leadUuid);
            opportunityService.updateMigration(salesPipeline.getOpportunityUuid(), updateLeadUcReqMapper.toUpdateOpportunityReq(req));
        }
        return salesPipeline;
    }

    @Override
    public Boolean updateAllLead() {
        log.info("Start migrate assigned users {}", Instant.now());
        int count = 0;
        int countError = 0;
        int countSuccess = 0;
        Instant lastModifyTime = null;
        var aggregateViews = salespipelineAggregateViewRepository.findAllAssignedLeadByLastModifiedDate(Instant.now());
        while (!aggregateViews.isEmpty()) {
            for (SalespipelineAggregateView aggregateView : aggregateViews) {
                var request = new UpdateLeadUcReq();
                if (StringUtils.hasText(aggregateView.getAssignedUserUuid())) {
                    request.setAssignedUsers(this.buildAssignedUsersBySalesPipeline(aggregateView));
                }

                if (StringUtils.hasText(aggregateView.getExpectedAssignUserUuid())) {
                    request.setExpectedUsers(this.buildExpectedUsersBySalesPipeline(aggregateView));
                }
                try {
                    processMigration(aggregateView.getLeadUuid(), request);
                    aggregateView.setNumberRequiredUser(1);
                    salespipelineAggregateViewRepository.insert(aggregateView);
                    countSuccess++;
                } catch (Exception e) {
                    countError++;
                    log.info("migrate lead [{}] fail exception {}", aggregateView.getLeadUuid(), e);
                } finally {
                    count++;
                    lastModifyTime = aggregateView.getLastModifiedDate();
                }
            }
            log.info("process query migrate assigned user with time {}", lastModifyTime);
            aggregateViews = salespipelineAggregateViewRepository.findAllAssignedLeadByLastModifiedDate(lastModifyTime);
        }
        log.info("Finish migrate assigned users have total record {} " +
            " with record updated {} and total record errors {}, at time {}", count, countSuccess, countError, Instant.now());
        return true;
    }

    private List<User> buildAssignedUsersBySalesPipeline(SalespipelineAggregateView aggregateView) {
        return List.of(User.builder()
            .userUuid(aggregateView.getAssignedUserUuid())
            .userCode(aggregateView.getAssignedUserCode())
            .role("Sale_Advisor")
            .build());
    }

    private List<User> buildExpectedUsersBySalesPipeline(SalespipelineAggregateView aggregateView) {
        return List.of(User.builder()
            .userUuid(aggregateView.getExpectedAssignUserUuid())
            .userCode(aggregateView.getExpectedAssignedUserCode())
            .role("Sale_Advisor")
            .build());
    }


    private AccountUpdateRequest transformAccountFromRequest(UpdateLeadUcReq req, String accountUuid) throws JsonProcessingException {
        var request = AccountUpdateRequest.builder()
            .accountName(req.getName())
            .accountUuid(accountUuid)
            .email(req.getEmail())
            .description(req.getDescription())
            .bankCode(req.getBankAccount() == null ? null : req.getBankAccount().getBankCode())
            .bankName(req.getBankAccount() == null ? null : req.getBankAccount().getBankName())
            .bankAccountNumber(req.getBankAccount() == null ? null : req.getBankAccount().getAccountNumber())
            .bankAccountHolder(req.getBankAccount() == null ? null : req.getBankAccount().getAccountHolder())
            .bankBranch(req.getBankAccount() == null ? null : req.getBankAccount().getBankBranch())
            .annualRevenue(req.getProfile() == null ? null : req.getProfile().getAnnualRevenue())
            .campaignUuid(req.getCampaignUuid())
            .campaignCode(req.getCampaignCode())
            .referredByUuid(req.getReferredByUserUuid())
            .referredByUserCode(req.getReferredByUserCode())
            .primaryContactUuid(req.getPrimaryContactUuid())
            .primaryContactCode(req.getPrimaryContactCode())
            .address(req.getAddress())
            .monthlyIncome(req.getProfile() == null ? null : req.getProfile().getMonthlyIncome())
            .monthlyExpense(req.getProfile() == null ? null : req.getProfile().getMonthlyExpense())
            .monthlyPaymentLoan(req.getProfile() == null ? null : req.getProfile().getMonthlyPaymentLoan())
            .note(req.getNote())
            .phoneNumber(buildUpdatePhoneRequest(req.getPhoneNumber()))
            .build();
        log.info("UpdateLeadUc.transformAccountFromRequest : data update: [{}]", objectMapper.writeValueAsString(request));
        return request;
    }

    private ContactUpdateRequest transformContactFromRequest(UpdateLeadUcReq req, String contactUuid) throws JsonProcessingException {
        ContactUpdateRequest updateRequest = new ContactUpdateRequest();
        updateRequest.setContactName(req.getName());
        updateRequest.setEmail(req.getEmail());
        updateRequest.setContactUuid(contactUuid);
        if (req.getProfile() != null) {
            LeadProfile profile = req.getProfile();
            updateRequest.setDateOfBirth(profile.getDateOfBirth());
            updateRequest.setJob(profile.getJob());
            updateRequest.setFacebook(profile.getFacebook());
            updateRequest.setMaritalStatus(profile.getMaritalStatus());
            updateRequest.setNumberOfChildren(profile.getNumberOfChildren());
            updateRequest.setRangeAge(profile.getRangeAge());
            updateRequest.setChildrenSchool(profile.getChildrenSchool());
            updateRequest.setNationality(profile.getNationality());
            updateRequest.setAnnualRevenue(profile.getAnnualRevenue());
            updateRequest.setCompany(profile.getCompany());
        }
        if (req.getPermanentAddress() != null) {
            updateRequest.setPermanentAddress(req.getPermanentAddress());
        }
        updateRequest.setPhoneNumber(buildUpdatePhoneRequest(req.getPhoneNumber()));
        updateRequest.setLastRequestContactDate(req.getLastRequestContactDate());
        updateRequest.setLastContactedDate(req.getLastContactedDate());
        updateRequest.setDescription(req.getDescription());
        updateRequest.setReferredByUuid(req.getReferredByUserUuid());
        updateRequest.setReferredByUserCode(req.getReferredByUserCode());
        updateRequest.setContactRoles(req.getContactRoles());
        updateRequest.setIdDocuments(req.getIdDocuments());
        log.info("UpdateLeadUc.transformContactFromRequest : data update: [{}]", objectMapper.writeValueAsString(updateRequest));
        return updateRequest;
    }

    private PhoneNumber buildUpdatePhoneRequest(PhoneNumber phoneNumber) {
        if (phoneNumber != null && phoneNumber.getAdditionalPhones() != null && !phoneNumber.getAdditionalPhones().isEmpty()) {
            phoneNumber.setNumber(null);
            phoneNumber.setPhoneType(null);
        }
        return phoneNumber;
    }
}
