package vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.*;
import javax.validation.Valid;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
public class CreateLeadReq {

    @JsonProperty("lead_name")
    String leadName;

    String leadUuid;

    String leadCode;

    PhoneNumber phoneNumber;

    String email;

    @JsonProperty("referred_by_user_uuid")
    String referredByUserUuid;

    @JsonProperty("referred_by_user_code")
    String referredByUserCode;

    @JsonProperty("score")
    String score;

    @JsonProperty("priority")
    String priority;

    @JsonProperty("version")
    String version;

    @JsonProperty("source")
    String source;

    @JsonProperty("gender")
    String gender;

    @JsonProperty("bank_account")
    BankAccount bankAccount;

    @JsonProperty("duplicated_lead_uuid")
    String duplicatedLeadUuid;

    @JsonProperty("duplicated_lead_code")
    String duplicatedLeadCode;

    @JsonProperty("address")
    Address address;

    @JsonProperty("profile")
    @Valid
    LeadProfile profile;

    @JsonProperty("attributes")
    List<AttributeValue> attributes = new ArrayList<>();

    @JsonProperty("channel")
    String channel;

    @JsonProperty("expected_assign_user_code")
    String expectedAssignedUserCode;

    @JsonProperty("expected_assign_user_uuid")
    String expectedAssignUserUuid;

    @JsonProperty("id_documents")
    List<LeadIdDocument> idDocuments;

    @JsonProperty("assigned_user_uuid")
    String assignedUserUuid;

    @JsonProperty("assigned_user_code")
    String assignedUserCode;

    @JsonProperty("campaign_uuid")
    String campaignUuid;

    @JsonProperty("campaign_code")
    String campaignCode;

    String status;

    Instant recordedDate;

    @JsonProperty("assigned_date")
    Instant assignedDate;
}
