package vn.onehousing.salepipeline.usecase.business.usecase.createhistoryuc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.usecase.createhistoryuc.response.OpportunityHistoryRes;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.OpportunityAgentRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.note.OpportunityHistoryRepository;

import java.sql.SQLException;
import java.util.List;

@Slf4j
@Component
@AllArgsConstructor
public class CreateOppHistoryUseCase implements ICreateOppHistoryUseCase {

    private final OpportunityHistoryRepository repository;
    private final IInsertOppHistories insertOppHistories;
    private final OpportunityAgentRepository opportunityAgentRepository;

    @Override
    public Boolean migrate(String coreUuid) throws SQLException {
        List<OpportunityHistoryRes> histories = repository.getAllOpportunityHistory(coreUuid);
        log.info("[CRM][NoteMigration] Total Histories Migrated = {}", histories.size());
        histories.forEach(
                event -> {
                    log.info("[CRM][HistoryMigration] Create Opp History with core_uuid = {}",
                            event.getLeadUuid());
                    try {
                        insertOppHistories.insert(event.genHistoryUuid(),
                                event.getCreatedTime().toEpochMilli());
                    } catch (Exception ex) {
                        log.error("[CRM][NoteMigration] Error while create note", ex);
                        opportunityAgentRepository.writeMigrationHistory("HISTORY_MIGRATION",
                                event.getParentUuid(),
                                "",
                                event.getRecordType(),
                                "",
                                event,
                                "ERROR",
                                ex.getMessage());
                    }
                });
        log.info("[CRM][HistoryMigration] Finish History Migration");
        return true;
    }
}
