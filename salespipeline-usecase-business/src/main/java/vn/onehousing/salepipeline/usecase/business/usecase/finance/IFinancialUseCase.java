package vn.onehousing.salepipeline.usecase.business.usecase.finance;

import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.Financial;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.request.FinancialCreateReq;

public interface IFinancialUseCase {
    Financial createFinancial(FinancialCreateReq request);
}
