package vn.onehousing.salepipeline.usecase.query.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LeadDuplicateMessageView {
    String leadUuid;
    Long timestamp;
}
