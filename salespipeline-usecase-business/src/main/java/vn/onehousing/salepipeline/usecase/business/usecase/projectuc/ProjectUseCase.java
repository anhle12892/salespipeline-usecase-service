package vn.onehousing.salepipeline.usecase.business.usecase.projectuc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import vn.onehousing.salepipeline.usecase.business.thirdparty.masterservice.IMasterService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.masterservice.response.ProjectResponseDto;
import vn.onehousing.salepipeline.usecase.business.usecase.projectuc.mapper.IProjectMapper;
import vn.onehousing.salepipeline.usecase.business.usecase.projectuc.request.Project;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class ProjectUseCase implements IProjectUseCase {

    private final IMasterService masterService;
    private final IProjectMapper mapper;

    @Override
    public Project getProjectByUuid(String projectUuid) {
        if(StringUtils.isEmpty(projectUuid)) {
            return null;
        }
        return masterService.getAllProject().stream()
                .filter(p -> projectUuid.equals(p.getProjectId()))
                .findFirst().map(mapper::convert)
                .orElse(null);
    }

    @Override
    public List<Project> getAllProjects() {
        return masterService.getAllProject().stream()
                .map(mapper::convert).collect(Collectors.toList());
    }

    @Override
    public Project getProjectByProjectCode(String projectCode) {
        if(StringUtils.isEmpty(projectCode)) {
            return null;
        }
        return masterService.getAllProject().stream()
                .filter(p -> projectCode.equals(p.getProjectCode()))
                .findFirst().map(mapper::convert)
                .orElse(null);
    }

    @Override
    public Optional<String> getUuidByCode(String projectCode) {
        if(StringUtils.isEmpty(projectCode)) {
            return null;
        }
        return masterService.getAllProject().stream()
                .filter(p -> projectCode.equals(p.getProjectCode()))
                .map(ProjectResponseDto::getProjectId)
                .findFirst();
    }
}
