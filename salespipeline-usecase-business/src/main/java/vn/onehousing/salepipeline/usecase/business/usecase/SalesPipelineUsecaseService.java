package vn.onehousing.salepipeline.usecase.business.usecase;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEntityType;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.AgentHistoryView;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.IAgentHistoryViewRepository;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.IOpportunityActivityViewRepository;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
@AllArgsConstructor
public class SalesPipelineUsecaseService implements ISalesPipelineUsecaseService {

    private final IOpportunityActivityViewRepository opportunityActivityViewRepository;
    private final IAgentHistoryViewRepository agentHistoryViewRepository;

    @Override
    public List<OpportunityActivityView> getByUuid( String uuid) throws IOException {
        return opportunityActivityViewRepository.get(uuid, StringUtils.EMPTY);
    }
    @Override
    public List<OpportunityActivityView> getByPhone(@Nullable String phoneNumber) throws IOException {
        return opportunityActivityViewRepository.get(StringUtils.EMPTY, phoneNumber);
    }

    @Override
    public List<AgentHistoryView> getOppHistories(String uuid, Pageable pageable) {
        List<AgentHistoryView> agentHistoryViews = agentHistoryViewRepository.getOpportunityHistories(uuid, pageable);
        List<AgentHistoryView> taskEventList = agentHistoryViews
                .stream()
                .filter(event -> Objects.nonNull(event.getEntityType()) &&
                        ActEntityType.TASK.equals(event.getEntityType()))
                .collect(Collectors.toList());
        if (!taskEventList.isEmpty()) {
            agentHistoryViews.removeAll(taskEventList);
            Set<AgentHistoryView> setView = new TreeSet<>((o1, o2) -> {
                if (o1.getContent().equalsIgnoreCase(o2.getContent())) {
                    return 0;
                }
                return 1;
            });
            setView.addAll(taskEventList);
            agentHistoryViews.addAll(setView);
        }
        agentHistoryViews.sort(Comparator
                .comparing(AgentHistoryView::getTimestamp)
                .reversed());
        return agentHistoryViews;
    }
}
