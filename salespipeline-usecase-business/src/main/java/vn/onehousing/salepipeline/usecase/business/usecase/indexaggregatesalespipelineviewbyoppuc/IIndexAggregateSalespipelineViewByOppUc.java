package vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbyoppuc;

import vn.onehousing.salepipeline.usecase.common.shared.model.Opportunity;

import java.io.IOException;

public interface IIndexAggregateSalespipelineViewByOppUc {
    void process (Opportunity opportunity) throws IOException;
}
