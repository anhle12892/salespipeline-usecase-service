package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.updateopportunity;

import vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.request.UpdateLeadUcReq;

import java.io.IOException;

public interface IUpdateOpportunityForSaleStage {
    void process(String leadUuid, UpdateLeadUcReq request) throws IOException;
}
