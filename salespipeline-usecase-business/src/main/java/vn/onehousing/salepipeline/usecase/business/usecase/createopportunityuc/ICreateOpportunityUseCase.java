package vn.onehousing.salepipeline.usecase.business.usecase.createopportunityuc;

import com.fasterxml.jackson.core.JsonProcessingException;
import vn.onehousing.salepipeline.usecase.common.shared.model.Opportunity;

public interface ICreateOpportunityUseCase {
    Opportunity create(String leadUuid) throws JsonProcessingException;
}
