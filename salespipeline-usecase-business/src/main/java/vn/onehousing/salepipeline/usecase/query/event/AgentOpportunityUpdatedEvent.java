package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeValue;

import java.util.List;

@Data
public class AgentOpportunityUpdatedEvent {
    private String leadUuid;
    private List<AttributeValue> attributes;
}
