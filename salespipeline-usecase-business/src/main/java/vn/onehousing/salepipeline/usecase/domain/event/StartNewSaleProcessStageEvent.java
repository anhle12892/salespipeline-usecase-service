package vn.onehousing.salepipeline.usecase.domain.event;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.*;
import lombok.experimental.FieldDefaults;
import net.vinid.core.event.AbstractEvent;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessStage;

@ToString(callSuper = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class StartNewSaleProcessStageEvent extends AbstractEvent {
    @JsonUnwrapped
    SaleProcessStage data;
}
