package vn.onehousing.salepipeline.usecase.business.usecase.noteuc.migrate;

public interface INoteMigrationUsecase {

    Boolean migrateNote(String coreUuid);

    Boolean migrateRequestMcNote(String coreUuid);

    Boolean migrateAuditTime(String coreUuid, String migratedDate);
}
