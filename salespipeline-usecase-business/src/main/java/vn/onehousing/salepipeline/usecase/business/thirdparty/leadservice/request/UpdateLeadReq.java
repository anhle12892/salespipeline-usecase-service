package vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;
import vn.onehousing.salepipeline.usecase.common.shared.model.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateLeadReq {

    @JsonProperty("account_uuid")
    String accountUuid;

    @JsonProperty("account_code")
    String accountCode;

    @JsonProperty("contact_uuid")
    String contactUuid;

    @JsonProperty("contact_code")
    String contactCode;

    @JsonProperty("opportunity_uuid")
    String opportunityUuid;

    @JsonProperty("opportunity_code")
    String opportunityCode;

    @JsonProperty("lead_code")
    String leadCode;

    @JsonProperty("is_converted")
    Boolean isConverted;

    @JsonProperty("converted_date")
    Instant convertedDate;

    @JsonProperty("lead_name")
    String leadName;

    @JsonProperty("phone_number")
    PhoneNumber phoneNumber;

    @JsonProperty("email")
    String email;

    @JsonProperty("referred_by_user_uuid")
    String referredByUserUuid;

    @JsonProperty("referred_by_user_code")
    String referredByUserCode;

    @JsonProperty("score")
    String score;

    @JsonProperty("priority")
    String priority;

    @JsonProperty("version")
    String version;

    @JsonProperty("source")
    String source;

    @JsonProperty("gender")
    String gender;

    @JsonProperty("bank_account")
    BankAccount bankAccount;

    @JsonProperty("duplicated_lead_uuid")
    String duplicatedLeadUuid;

    @JsonProperty("duplicated_lead_code")
    String duplicatedLeadCode;

    @JsonProperty("address")
    Address address;

    @JsonProperty("permanent_address")
    Address permanentAddress;

    @JsonProperty("profile")
    LeadProfile profile;

    @JsonProperty("unqualified_reason")
    String unqualifiedReason;

    @JsonProperty("lead_profile_uuid")
    String leadProfileUuid;

    @JsonProperty("attribute_set_id")
    String attributeSetId;

    @JsonProperty("assigned_user_uuid")
    String assignedUserUuid;

    @JsonProperty("assigned_user_code")
    String assignedUserCode;

    @JsonProperty("campaign_uuid")
    String campaignUuid;

    @JsonProperty("campaign_code")
    String campaignCode;

    @JsonProperty("status")
    String status;

    @JsonProperty("attributes")
    List<AttributeValue> attributes = new ArrayList<>();

    @JsonProperty("channel")
    String channel;

    @JsonProperty("expected_assign_user_code")
    String expectedAssignedUserCode;

    @JsonProperty("expected_assign_user_uuid")
    String expectedAssignUserUuid;

    Instant recordedDate;

    List<LeadIdDocument> idDocuments;

    String note;

    @JsonProperty("unassigned_reason")
    String unassignedReason;

    @JsonProperty("closed_date")
    Instant closedDate;

    @JsonProperty("assigned_date")
    Instant assignedDate;

    List<User> assignedUsers;

    List<User> expectedUsers;
}
