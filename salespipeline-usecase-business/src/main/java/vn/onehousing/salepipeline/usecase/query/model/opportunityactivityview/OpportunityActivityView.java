package vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview;

import lombok.Data;

import java.time.Instant;
import java.util.Map;
import java.util.Objects;

@Data
public class OpportunityActivityView {
    String actorUuid;
    /* WHO: uuid of parent, for example note event, parent is account or lead */
    String parentUuid;
    String parentType;
    String phoneNumber;
    String leadUuid;
    /* WHAT: uuid of activity (event) */
    String uuid;
    /* name of activity (event) */
    String event;
    /* WHICH: uuid of related entity, for example note event, entity is note */
    String entityUuid;
    String entityType;

    Map<String, Object> data;
    String createdBy;
    Instant createdDate;
    String lastModifiedBy;
    Instant lastModifiedDate;
    Long timestamp;

    public OpportunityActivityView mappingActor(String actorUuid, String event) {
        if (Objects.isNull(this.actorUuid)) {
            this.actorUuid = actorUuid;
        }
        if (Objects.isNull(this.event)) {
            this.event = event;
        }
        if (Objects.isNull(this.createdDate)) {
            this.createdDate = Instant.now();
        }
        if (Objects.isNull(this.lastModifiedDate)) {
            this.lastModifiedDate = Instant.now();
        }

        return this;
    }

}
