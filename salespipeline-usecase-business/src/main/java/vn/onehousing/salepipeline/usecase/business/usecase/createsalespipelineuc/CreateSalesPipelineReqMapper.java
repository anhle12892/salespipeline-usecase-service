package vn.onehousing.salepipeline.usecase.business.usecase.createsalespipelineuc;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.request.CreateSalesPipelineReq;
import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;

@Mapper(componentModel = "spring")
public interface CreateSalesPipelineReqMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateFields(Lead input, @MappingTarget CreateSalesPipelineReq req);
}
