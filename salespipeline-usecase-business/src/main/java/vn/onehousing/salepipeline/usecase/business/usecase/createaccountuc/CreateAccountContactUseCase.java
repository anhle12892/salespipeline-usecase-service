package vn.onehousing.salepipeline.usecase.business.usecase.createaccountuc;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.IAccountService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.IContactService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.mapper.AccountContactCreateDtoMapper;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.ContactSearchRequest;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.ILeadService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.request.UpdateLeadReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.ISalesPipelineService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.request.UpdateSalesPipelineReq;
import vn.onehousing.salepipeline.usecase.common.shared.model.Account;
import vn.onehousing.salepipeline.usecase.common.shared.model.Contact;
import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;

import java.time.Instant;
import java.util.Objects;

@Slf4j
@Component
@AllArgsConstructor
public class CreateAccountContactUseCase implements ICreateAccountContactUseCase {

    private final IAccountService accountService;
    private final IContactService contactService;
    private final ILeadService leadService;
    private final ISalesPipelineService salesPipelineService;
    private final AccountContactCreateDtoMapper accountContactCreateDtoMapper;

    @Override
    public SalesPipeline process(String leadUuid) {
        Lead lead = this.leadService.get(leadUuid);

        Account account;
        Contact contact;

        SalesPipeline salesPipeline = salesPipelineService.getByLeadUuid(leadUuid);
        String accountUuid = salesPipeline.getAccountUuid();
        String contactUuid = salesPipeline.getContactUuid();

        if (!StringUtils.hasText(accountUuid) && !StringUtils.hasText(contactUuid)) {
            contact = contactService.search(ContactSearchRequest.builder().phoneNumber(lead.getPhoneNumber().getNumber()).build());
            if (Objects.nonNull(contact)) {
                account = new Account();
                account.setAccountCode(contact.getAccountCode());
                account.setAccountUuid(contact.getAccountUuid());
            } else {
                account = this.accountService.createAccount(
                    accountContactCreateDtoMapper.fromLeadToCreateAccount(
                        lead,
                        lead.getBankAccount(),
                        lead.getProfile()
                    )
                );
                contact = this.accountService.createContact(
                    accountContactCreateDtoMapper.fromLeadToCreateContact(
                        lead,
                        lead.getBankAccount(),
                        account.getAccountUuid(),
                        lead.getProfile()
                    )
                );
            }
            var leadUpdateReq = toLeadUpdateRequest(account, contact);
            this.leadService.update(leadUuid, leadUpdateReq);
            return this.salesPipelineService.update(
                salesPipeline.getUuid(),
                toUpdateSalePipelineReq(
                    account,
                    contact,
                    lead
                )
            );
        }

        return salesPipeline;
    }

    private UpdateLeadReq toLeadUpdateRequest(Account accountDto, Contact contactDto) {
        return UpdateLeadReq.builder()
            .accountCode(accountDto.getAccountCode())
            .accountUuid(accountDto.getAccountUuid())
            .contactCode(contactDto.getContactCode())
            .contactUuid(contactDto.getContactUuid())
            .isConverted(true)
            .convertedDate(Instant.now())
            .build();
    }

    private UpdateSalesPipelineReq toUpdateSalePipelineReq(Account account, Contact contact, Lead leadDto) {
        return UpdateSalesPipelineReq.builder()
            .accountCode(account.getAccountCode())
            .accountUuid(account.getAccountUuid())
            .contactCode(contact.getContactCode())
            .contactUuid(contact.getContactUuid())
            .duplicatedLeadCode(leadDto.getDuplicatedLeadCode())
            .duplicatedLeadUuid(leadDto.getDuplicatedLeadUuid())
            .build();
    }
}
