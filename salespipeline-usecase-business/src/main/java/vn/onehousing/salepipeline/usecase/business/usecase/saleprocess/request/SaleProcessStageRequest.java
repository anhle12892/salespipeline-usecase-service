package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleProcessStageRequest {
    @NotBlank
    private String coreUuid;
}
