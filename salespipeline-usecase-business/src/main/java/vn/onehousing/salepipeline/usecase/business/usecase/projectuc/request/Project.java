package vn.onehousing.salepipeline.usecase.business.usecase.projectuc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Project implements Serializable {
    String id;
    @JsonProperty("uuid")
    String projectUuid;
    String code;
    String name;
    String fbProjectCode;
    String tcbProjectCode;
    Boolean shouldRbondSupported;
}
