package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.migrate;

import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request.SaleProcessMigrationRequest;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response.SaleProcessMigrationResponse;

public interface IMigrateStageJob {
    SaleProcessMigrationResponse migrate(SaleProcessMigrationRequest request);
}
