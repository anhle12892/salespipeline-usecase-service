package vn.onehousing.salepipeline.usecase.business.thirdparty.masterservice.response;

import lombok.Builder;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;

import java.util.List;

@Data
@Builder
public class MasterServiceResp extends BaseResponse<List<ProjectResponseDto>> {
}
