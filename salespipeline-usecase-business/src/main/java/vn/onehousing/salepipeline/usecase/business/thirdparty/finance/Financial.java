package vn.onehousing.salepipeline.usecase.business.thirdparty.finance;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Financial {

    Long id;

    String financeUuid;

    String leadUuid;

    String agentUuid;

    String partnerSaleId;

    String partnerSaleName;

    String partnerSalePhone;

    String partnerSaleCode;

    String partnerUuid;

    String resourceTypeInterest;

    String projectUuid;

    String partnerSaleEmail;

    String budgetLimit;

    String propertyType;

    String propertyCode;

    String propertyUuid;

    String nbBedroom;

    @JsonProperty("is_sent")
    Boolean isSent;

    String partnerLeadId;

    String mortgageStatus;

    String mortgageSubStatus;

    Boolean hasMortgageProperty;

    Instant lastResponseTime;

    Boolean isClosed;

    String applicationId;

    Instant applicationCreatedDate;

    String status;

    BigDecimal minHousePrice;

    BigDecimal maxHousePrice;

    String requestId;

    String consentForm;

    String appVersion;

    public Financial updateStatus(String status) {
        if (Objects.nonNull(status)) {
            this.status = status;
        }
        return this;
    }

}
