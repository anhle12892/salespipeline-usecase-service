package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.getsalestage;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.config.saleprocess.SaleProcessStageConfiguration;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request.SaleProcessStageRequest;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response.SaleProcessStageResponse;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response.SaleStageResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessSchemaProfile;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.ISaleProcessActivityRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.ISaleProcessStageRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessActivity;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessStage;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Component
@AllArgsConstructor
public class GetSaleProcessStage implements IGetSaleProcessStage {
    private final ISaleProcessStageRepository repository;
    private final ISaleProcessActivityRepository activityRepository;
    private final SaleProcessStageConfiguration configuration;

    @Override
    public SaleProcessStageResponse get(SaleProcessStageRequest request) {
        Optional<SaleProcessStage> optionalSaleProcessStage = repository.get(request.getCoreUuid());

        if (optionalSaleProcessStage.isEmpty()) {
            try {
                var uuid = UUID.fromString(request.getCoreUuid());
                var saleProcessStage = new SaleProcessStage();
                saleProcessStage.setLeadUuid(request.getCoreUuid());
                saleProcessStage.setStage(SaleProcessSchemaProfile.IN_HOUSE_INITIAL_STAGE.toString());
                optionalSaleProcessStage = repository.insert(saleProcessStage);

                var saleProcessActivity = new SaleProcessActivity();
                saleProcessActivity.setLeadUuid(request.getCoreUuid());
                saleProcessActivity.setActivities(new ArrayList<>());
                activityRepository.getLockForUpdate(saleProcessActivity.getLeadUuid());
                activityRepository.update(saleProcessActivity);
                activityRepository.releaseLockForUpdate(saleProcessActivity.getLeadUuid());
            } catch (Exception exception){
                log.error("[GetSaleProcessStage -> get] can not get stage for the lead_uuid: {}",
                    request.getCoreUuid(),
                    exception);
            }
        }

        var response = new SaleProcessStageResponse();
        if (optionalSaleProcessStage.isEmpty()) {
            response.setSaleStage(SaleProcessSchemaProfile.IN_HOUSE_INITIAL_STAGE);
        } else {
            response.setSaleStage(NumberUtils.toInt(optionalSaleProcessStage.get().getStage(),
                SaleProcessSchemaProfile.IN_HOUSE_INITIAL_STAGE));
        }

        var stages = new ArrayList<SaleStageResponse>();
        if (configuration.getStages() != null) {
            for (var index = 0; index < configuration.getStages().size(); index++) {
                var stage = new SaleStageResponse();
                stage.setValue(index + 1);
                stage.setTitle(configuration.getStages().get(index));
                stages.add(stage);
            }
        }
        response.setSaleStages(stages);

        return response;
    }
}
