package vn.onehousing.salepipeline.usecase.business.usecase.noteuc;

import vn.onehousing.salepipeline.usecase.common.shared.model.Note;

public interface INoteUseCase {
    Note createNote(String coreUuid, Note note);

    Note updateNote(String coreUuid, String noteUuid, Note note);
}
