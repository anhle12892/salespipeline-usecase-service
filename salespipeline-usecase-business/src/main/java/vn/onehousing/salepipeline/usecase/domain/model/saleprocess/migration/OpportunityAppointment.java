package vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration;

import lombok.Data;

@Data
public class OpportunityAppointment {
    String opportunityId;
    String appointmentStatus;
}
