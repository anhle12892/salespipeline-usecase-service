package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.migrate;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.thirdparty.note.INoteService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.task.ITaskService;
import vn.onehousing.salepipeline.usecase.business.usecase.noteuc.mapper.NoteCreateNoteDtoMapper;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.mapper.TaskReqMapper;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request.SaleProcessMigrationRequest;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request.TaskMigrationRequest;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response.SaleProcessMigrationResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessSchemaProfile;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessTaskWorkflow;
import vn.onehousing.salepipeline.usecase.common.shared.constants.NoteOwnerType;
import vn.onehousing.salepipeline.usecase.common.shared.constants.NoteParentType;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;
import vn.onehousing.salepipeline.usecase.common.shared.model.Task;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.ISaleProcessActivityRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.ISaleProcessStageRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessActivity;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessStage;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.OpportunityAgentRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.SaleProcessOpportunityMigration;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@AllArgsConstructor
@Slf4j
public class MigrateSaleProcessStage implements IMigrateSaleProcessStage {
    private final TaskExecutor taskExecutor;

    private final OpportunityAgentRepository opportunityAgentRepository;
    private final ISaleProcessStageRepository repository;
    private final ISaleProcessActivityRepository activityRepository;
    private final TaskReqMapper taskReqMapper;
    private final ITaskService iTaskService;
    private final INoteService noteService;
    private final NoteCreateNoteDtoMapper createNoteDtoMapper;

    private final ObjectMapper objectMapper;

    @Override
    public SaleProcessMigrationResponse migrate(SaleProcessMigrationRequest request) {
        var response = new SaleProcessMigrationResponse();
        response.setResult("OK");

        taskExecutor.execute(new MigrationBackgroundTask(request.getUuids()));

        return response;
    }

    @Override
    public Boolean migrateTask(TaskMigrationRequest request) {
        log.info("[CRM][TaskMigration] Start Migrate Agent Submission to Task with input {}", request);
        Instant startTime = Instant.now();
        List<Task> taskList = opportunityAgentRepository.getListTask(request.getUuid());
        log.info("[CRM][TaskMigration] Total Task migrated = {}", taskList.size());
        taskList.stream().map(taskReqMapper::to).forEach(task -> {
            log.info("[CRM][TaskMigration] Create task with data {}", task);
            try {
                Task taskRes = iTaskService.createTask(task);
                if (StringUtils.isNotBlank(task.getComments())) {
                    final Note note = new Note();
                    note.setContent(task.getComments());
                    note.setOwnerType(NoteOwnerType.AGENT);
                    note.setOwnerUuid(task.getAssignedToUserUuid());
                    note.setTitle(task.getName());
                    note.setParentType(NoteParentType.TASK);
                    note.setParentUuid(taskRes.getTaskUuid().getTaskUuid());
                    note.setOwnerEmail(task.getAssignedEmail());
                    noteService.createNote(createNoteDtoMapper.to(note));
                }
            } catch (Exception ex) {
                log.info("[CRM][TaskMigration] Error while create task", ex);
                opportunityAgentRepository.writeMigrationHistory("TASK_MIGRATION",
                        task.getEntityUuid(),
                        "",
                        task.getCode(),
                        "",
                        task,
                        "ERROR",
                        ex.getMessage());
            }
            log.info("[CRM][TaskMigration] Finish create task with core_uuid {}", task.getEntityUuid());
        });
        log.info("[CRM][TaskMigration] Migration Task done in {} seconds", Instant.now().getEpochSecond() - startTime.getEpochSecond());
        return true;
    }

    private class MigrationBackgroundTask implements Runnable {
        List<String> uuids;

        public MigrationBackgroundTask(List<String> uuids) {
            this.uuids = uuids;
        }

        public void run() {
            List<SaleProcessOpportunityMigration> results = opportunityAgentRepository.getOpportunities(uuids);

            if (results != null && !results.isEmpty()) {
                log.info("[CRM-Agent][SaleProcessStageMigration][Start] migration for sale stage");
                results.forEach(item -> {
                    log.info("[CRM-Agent][SaleProcessStageMigration] migrate for: lead_uuid={}",
                        item.getLeadUuid());
                    var saleProcessStage = new SaleProcessStage();
                    saleProcessStage.setLeadUuid(item.getLeadUuid());
                    saleProcessStage.setStage(SaleProcessSchemaProfile.IN_HOUSE_INITIAL_STAGE.toString());
                    repository.update(saleProcessStage);

                    var saleProcessActivity = new SaleProcessActivity();
                    saleProcessActivity.setLeadUuid(item.getLeadUuid());
                    Set<String> targetSet = new HashSet<>(item.toActivities());
                    saleProcessActivity.setActivities(new ArrayList<>(targetSet));
                    saleProcessActivity.getActivities().add(SaleProcessTaskWorkflow.ASSIGNED);
                    activityRepository.update(saleProcessActivity);
                });

                log.info("[CRM-Agent][SaleProcessStageMigration][End] migration for sale stage");
            }
        }
    }

}
