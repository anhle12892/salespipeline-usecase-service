package vn.onehousing.salepipeline.usecase.business.usecase.createsalespipelineuc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.ILeadService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.ISalesPipelineService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.request.CreateSalesPipelineReq;
import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;

@Slf4j
@Component
@AllArgsConstructor
public class CreateSalesPipelineUseCase implements ICreateSalesPipelineUseCase {

    private final ILeadService leadService;
    private final ISalesPipelineService salesPipelineService;
    private final CreateSalesPipelineReqMapper mapper;
    private final ObjectMapper objectMapper;

    @Override
    public SalesPipeline process(String leadUuid) throws JsonProcessingException {
        Lead lead = leadService.get(leadUuid);
        CreateSalesPipelineReq createReq = new CreateSalesPipelineReq();
        mapper.updateFields(lead, createReq);
        return salesPipelineService.create(createReq);
    }
}
