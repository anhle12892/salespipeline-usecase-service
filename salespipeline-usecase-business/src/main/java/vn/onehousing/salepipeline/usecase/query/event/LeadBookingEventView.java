package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

@Data
public class LeadBookingEventView {
    private String leadUuid;
    private String bookingCode;
    private PhoneNumber phoneNumber;
    private Long timestamp;
}
