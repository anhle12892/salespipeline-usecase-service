package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.processsalestage;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.IWorkflowService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.request.SaleProcessStageWorkflowRequest;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.response.DecisionResult;
import vn.onehousing.salepipeline.usecase.domain.event.StartNewSaleProcessStageEvent;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.*;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.ISalespipelineAggregateViewRepository;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.SalespipelineAggregateView;

import java.io.IOException;
import java.util.*;

@Slf4j
@Component
@AllArgsConstructor
public class ProcessSaleProcessStage implements IProcessSaleProcessStage {
    private final ISaleProcessStageRepository repository;
    private final ISaleProcessActivityRepository activityRepository;
    private final ISalespipelineAggregateViewRepository salespipelineAggregateViewRepository;
    private final ProcessSaleProcessStageMapper mapper;
    private final EventPublisher eventPublisher;
    private final IWorkflowService workflowService;

    @Override
    public void process(SaleProcessInputActivity inputActivity) throws IOException {
        if (inputActivity == null) {
            return;
        }

        // clean up activities
        var activities = new ArrayList<String>();
        inputActivity.getActivities().forEach(activity -> {
            if (StringUtils.hasText(activity) && !activities.contains(activity)) {
                activities.add(activity);
            }
        });
        inputActivity.setActivities(activities);

        activityRepository.getLockForUpdate(inputActivity.getLeadUuid());
        activityRepository.update(mapper.from(inputActivity));
        activityRepository.releaseLockForUpdate(inputActivity.getLeadUuid());

        Optional<SaleProcessStageWorkflowRequest> optionalRequest
            = from(activityRepository.get(inputActivity.getLeadUuid()));

        if (!optionalRequest.isEmpty()) {
            Optional<DecisionResult> optionalStageWorkflow = workflowService.get(optionalRequest.get());
            // if failure:
            //  - update activities success, but keep STAGE at salespipeline-usecase-service
            if (!optionalStageWorkflow.isEmpty()) {
                String newStage = String.valueOf(optionalStageWorkflow.get().getValue());
                log.info("[ProcessSaleProcessStage -> process] new stage={}, lead_uuid: {}", newStage, inputActivity.getLeadUuid());
                var result = new SaleProcessStage();
                result.setLeadUuid(inputActivity.getLeadUuid());
                result.setStage(newStage);

                // get current stage to check if the stage is completed
                // in case of the sale process completed, publish event SaleProcessStageCompletedEvent
                var optionalSaleProcessStage = repository.get(inputActivity.getLeadUuid());
                if (optionalSaleProcessStage.isEmpty()) {
                    return;
                }

                var currentStage = optionalSaleProcessStage.get();
                log.info("[ProcessSaleProcessStage -> process] new stage:{}, current stage: {}", newStage, currentStage.getStage());
                if (!StringUtils.hasText(currentStage.getStage()) || currentStage.getStage().equalsIgnoreCase(newStage)) {
                    return;
                }

                // if failure:
                //  - keep STAGE at salespipeline-usecase-service
                //  - will update in the next time
                repository.getLockForUpdate(inputActivity.getLeadUuid());

                // update
                SalespipelineAggregateView sp = salespipelineAggregateViewRepository.getByUuid(inputActivity.getLeadUuid());
                if (sp == null) {
                    log.error("[ProcessSaleProcessStage -> process] cannot find SalespipelineAggregateView in order to update, lead_uuid: [{}]", inputActivity.getLeadUuid());
                    return;
                }
                String[] agentRolesNeeded = workflowService.getListAgentRolesNeeded(sp.getSource(), sp.getChannel(), newStage);
                sp.setNumberRequiredUser(agentRolesNeeded.length);
                salespipelineAggregateViewRepository.insert(sp);
                repository.update(result);
                eventPublisher.publishEvent(new StartNewSaleProcessStageEvent(result));

                repository.releaseLockForUpdate(inputActivity.getLeadUuid());

            } else {
                log.info("[ProcessSaleProcessStage -> process] can not get stage for the lead_uuid: {}",
                    inputActivity.getLeadUuid());
            }
        } else {
            log.info("[ProcessSaleProcessStage -> process] can not get activities " +
                    "to calculate stage for the lead_uuid: {}",
                inputActivity.getLeadUuid());
        }
    }

    private Optional<SaleProcessStageWorkflowRequest> from(Optional<SaleProcessActivity> saleProcessActivity) {
        if (!saleProcessActivity.isEmpty()) {
            var valueMap = new HashMap<String, List<String>>();
            valueMap.put("value", saleProcessActivity.get().getActivities());

            var variables = new HashMap<String, Map<String, List<String>>>();
            variables.put("actionList", valueMap);

            var request = new SaleProcessStageWorkflowRequest();
            request.setVariables(variables);

            return Optional.of(request);
        }

        return Optional.empty();
    }
}
