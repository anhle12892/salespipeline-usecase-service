package vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbyleaduc;

import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;

import java.io.IOException;

public interface IIndexAggregateSalespipelineByLeadUc {
    void process(Lead lead) throws IOException;
}
