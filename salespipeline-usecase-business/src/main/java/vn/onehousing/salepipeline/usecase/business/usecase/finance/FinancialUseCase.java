package vn.onehousing.salepipeline.usecase.business.usecase.finance;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.Financial;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.IFinanceService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.request.FinancialCreateReq;

@Slf4j
@Service
@AllArgsConstructor
public class FinancialUseCase implements IFinancialUseCase {

    private final IFinanceService service;

    @Override
    public Financial createFinancial(FinancialCreateReq request) {
        return service.createFinancial(request);
    }
}
