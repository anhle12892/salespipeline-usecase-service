package vn.onehousing.salepipeline.usecase.business.mapper;

import org.mapstruct.Mapper;

import java.util.UUID;

@Mapper(componentModel = "spring")
public interface UUIDMapper {
    default UUID from(String value) {
        return value == null ? UUID.fromString("00000000-0000-0000-0000-000000000000") : UUID.fromString(value);
    }

    default String to(UUID uuid) {
        return uuid == null ? null : uuid.toString();
    }
}
