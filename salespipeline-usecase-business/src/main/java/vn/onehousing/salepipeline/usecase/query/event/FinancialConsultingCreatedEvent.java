package vn.onehousing.salepipeline.usecase.query.event;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Data;

@Data
public class FinancialConsultingCreatedEvent {
    @JsonUnwrapped
    public CreateLeadActivityView lead;
}
