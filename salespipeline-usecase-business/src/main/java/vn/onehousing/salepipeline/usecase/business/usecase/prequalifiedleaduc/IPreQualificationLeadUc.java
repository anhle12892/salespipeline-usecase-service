package vn.onehousing.salepipeline.usecase.business.usecase.prequalifiedleaduc;

import com.fasterxml.jackson.core.JsonProcessingException;
import vn.onehousing.salepipeline.usecase.business.usecase.prequalifiedleaduc.request.UpdateLeadPreQualifiedReq;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;

public interface IPreQualificationLeadUc {
    SalesPipeline process(String leadUuid, UpdateLeadPreQualifiedReq request) throws JsonProcessingException, IllegalAccessException;
}
