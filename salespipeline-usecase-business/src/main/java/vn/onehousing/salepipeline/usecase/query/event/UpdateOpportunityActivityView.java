package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeValue;

import java.time.Instant;
import java.util.List;

@Data
public class UpdateOpportunityActivityView {
    private Long opportunityId;
    private String opportunityUuid;
    private String opportunityCode;
    private String name;
    private String status;
    private String leadUuid;
    private String description;
    private String accountUuid;
    private String accountCode;
    private String productCode;
    private String productUuid;
    private String referredByUserUuid;
    private String referredByUserCode;
    private String assignedUserUuid;
    private String assignedUserCode;
    private String note;
    private String campaignUuid;
    private String campaignCode;
    private Instant closedDate;
    private String attributeSetValue;
    private String source;
    private Boolean isSignContract;
    Instant lastModifiedDate;
    String createdBy;
    String lastModifiedBy;
    Instant createdDate;
    private String channel;
    private String gender;
    private String unqualifiedReason;
    private String priority;
    Instant recordedDate;
    List<AttributeValue> attributes;
    Long timestamp;
    Boolean isFromSys;
}
