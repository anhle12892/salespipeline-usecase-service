package vn.onehousing.salepipeline.usecase.business.usecase.noteuc.migrate;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.Financial;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.IFinanceService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.note.INoteService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.note.request.NoteCreateDto;
import vn.onehousing.salepipeline.usecase.business.thirdparty.note.request.NoteUpdateAudit;
import vn.onehousing.salepipeline.usecase.business.usecase.noteuc.mapper.NoteCreateNoteDtoMapper;
import vn.onehousing.salepipeline.usecase.common.shared.constants.NoteParentType;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.OpportunityAgentRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.note.OpportunityFinanceRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.note.OpportunityHistoryRepository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Component
@AllArgsConstructor
public class NoteMigrationUsecase implements INoteMigrationUsecase {
    private final OpportunityHistoryRepository repository;
    private final OpportunityFinanceRepository agentFinanceRepository;
    private final INoteService noteService;
    private final NoteCreateNoteDtoMapper mapper;
    private final OpportunityAgentRepository opportunityAgentRepository;
    private final ObjectMapper objectMapper;
    private final IFinanceService financeService;

    @Override
    public Boolean migrateNote(String coreUuid) {
        log.info("[CRM][NoteMigration] Start migration at {}", Instant.now());
        List<Note> notes = repository.getAllNotes(coreUuid, null);
        log.info("[CRM][NoteMigration] Total Note Migrated = {}", notes.size());
        notes.forEach(note -> {
            try {
                log.info("[CRM][NoteMigration] Create note with data = {}",
                    objectMapper.writeValueAsString(note));
                noteService.createNote(mapper.to(note));
                Thread.sleep(10);
            } catch (Exception ex) {
                log.error("[CRM][NoteMigration] Error while create note", ex);
                opportunityAgentRepository.writeMigrationHistory("NOTE_MIGRATION",
                    note.getParentUuid(),
                    "",
                    note.getOwnerType(),
                    "",
                    note,
                    "ERROR",
                    ex.getMessage());
            }
        });
        log.info("[CRM][NoteMigration] Finish Note Migrated = {}", notes.size());
        return true;
    }

    @Override
    public Boolean migrateRequestMcNote(String coreUuid) {
        log.info("[CRM][NoteRequestMCMigration] Start migration at {}", Instant.now());
        List<Note> notes = noteService.getNotesByParentTypeAndOwnerType(NoteParentType.REQUEST_MC, "AGENT", coreUuid);
        log.info("[CRM][NoteRequestMCMigration] Total Note Migrated = {}", notes.size());
        Set<String> leadUuidWithMultiResourceType = new HashSet<>();
        notes.forEach(note -> {
            List<Financial> financials = financeService.search(note.getParentUuid());
            if (financials.size() == 1) {
                log.info("NoteRequestMCMigration update note with 1 request mc");
                Financial financial = financials.get(0);
                Note agentNote = getAgentFinanceByLeadUuidAndResourceType(financial.getLeadUuid(), financial.getResourceTypeInterest());
                if (agentNote == null) {
                    return;
                }
                NoteCreateDto noteCreateDto = buildNewNote(note, financial.getFinanceUuid(), agentNote.getOwnerUuid(), agentNote.getOwnerEmail());
                try {
                    noteService.updateNoteWithoutEvent(note.getNoteUuid(), noteCreateDto);
                } catch (Exception e) {
                    log.error("NoteRequestMCMigration update note occurs {} ", e.getMessage());
                }
                return;
            }

            if (financials.size() == 2) {
                leadUuidWithMultiResourceType.add(note.getParentUuid());
                return;
            }
            log.error("[CRM][MigrateNoteRequestMC]note uuid {} has parent uuid {} and type = REQUEST_MC is finance invalid", note.getNoteUuid(), note.getParentUuid());
        });
        leadUuidWithMultiResourceType.forEach(leadUuid -> {
            log.info("[NoteRequestMCMigration2MC] lead {} have 2 request mc ", leadUuid);
            List<Note> agentNotes = agentFinanceRepository.getFinanceByLeadUuidAndResourceType(leadUuid, null);
            log.info("[NoteRequestMCMigration2MC] total notes of agent {} ", agentNotes.size());
            agentNotes.stream().filter(a -> StringUtils.hasText(a.getContent())).forEach(agentNote -> {
                List<Note> crmNotes = noteService.getNotesByParentTypeAndOwnerType(NoteParentType.REQUEST_MC, "AGENT", agentNote.getParentUuid());
                List<Financial> financials = financeService.search(agentNote.getParentUuid());
                if (crmNotes.isEmpty()) {
                    log.info("[NoteRequestMCMigration2MC][CreateNote] lead {} ", leadUuid);
                    Optional<Financial> financialOp = financials.stream().filter(f -> f.getLeadUuid().equals(agentNote.getParentUuid())
                        && f.getResourceTypeInterest().equals(agentNote.getParentType())).findFirst();
                    if (financialOp.isEmpty()) {
                        log.error("[NoteRequestMCMigration2MC] NoteRequestMCMigration2MCCreateNote lead {} , resource type {} , content agent {} is empty finance ",
                            agentNote.getParentUuid(), agentNote.getParentType(), agentNote.getContent());
                    }
                    if (!StringUtils.hasText(agentNote.getContent())) {
                        log.info("[NoteRequestMCMigration2MC] NoteRequestMCMigration2MCCreateNote lead uuid {} doesn't have content with resource {}", agentNote.getParentUuid(), agentNote.getParentType());
                        return;
                    }
                    Financial financial = financialOp.get();
                    NoteCreateDto noteCreateDto = new NoteCreateDto();
                    noteCreateDto.setOwnerUuid(agentNote.getOwnerUuid());
                    noteCreateDto.setOwnerEmail(agentNote.getOwnerEmail());
                    noteCreateDto.setContent(agentNote.getContent());
                    noteCreateDto.setOwnerType("AGENT");
                    noteCreateDto.setParentType("REQUEST_MC");
                    noteCreateDto.setParentUuid(financial.getFinanceUuid());
                    noteCreateDto.setTitle("Bạn đã tạo yêu cầu tư vấn tài chính");
                    try {
                        noteService.createNoteWithoutEvent(noteCreateDto);
                    } catch (Exception e) {
                        log.error("[NoteRequestMCMigration2MC] NoteRequestMCMigration2MC create note occurs {} ", e.getMessage());
                    }

                } else {
                    for (Note crmNote : crmNotes) {
                        if (agentNote.getContent().equals(crmNote.getContent())) {
                            log.info("[NoteRequestMCMigration2MC][UpdateNote] lead {} ", leadUuid);
                            Optional<Financial> financialOp = financials.stream().filter(f -> f.getLeadUuid().equals(agentNote.getParentUuid()) &&
                                f.getResourceTypeInterest().equals(agentNote.getParentType())).findFirst();
                            if (financialOp.isEmpty()) {
                                log.error("[NoteRequestMCMigration2MC]NoteRequestMCMigration2MCUpdateNote lead {} , resource type {} , content crm {} is empty finance ",
                                    agentNote.getParentUuid(), agentNote.getParentType(), crmNote.getContent());
                                continue;
                            }
                            Financial financial = financialOp.get();
                            NoteCreateDto noteCreateDto = buildNewNote(crmNote, financial.getFinanceUuid(), agentNote.getOwnerUuid(), agentNote.getOwnerEmail());
                            try {
                                noteService.updateNoteWithoutEvent(crmNote.getNoteUuid(), noteCreateDto);
                            } catch (Exception e) {
                                log.error("[NoteRequestMCMigration2MC]NoteRequestMCMigration2MC update note occurs {} ", e.getMessage());
                            }
                        }
                    }
                }
            });
        });

        log.info("[CRM][NoteMigration] Finish Note Migrated = {}", notes.size());
        return true;
    }

    private NoteCreateDto buildNewNote(Note note, String financeUuid, String ownerUuid, String ownerEmail) {
        NoteCreateDto noteCreateDto = mapper.to(note);
        noteCreateDto.setParentUuid(financeUuid);
        noteCreateDto.setTitle("Bạn đã tạo yêu cầu tư vấn tài chính");
        noteCreateDto.setOwnerUuid(ownerUuid);
        noteCreateDto.setOwnerEmail(ownerEmail);
        return noteCreateDto;
    }

    private Note getAgentFinanceByLeadUuidAndResourceType(String leadUuid, String resourceType) {
        List<Note> agentNotes = agentFinanceRepository.getFinanceByLeadUuidAndResourceType(leadUuid, resourceType);
        if (agentNotes.isEmpty()) {
            log.error("[CRM][MigrateNoteRequestMC]note uuid {} has parent uuid {} and type = REQUEST_MC is finance invalid", leadUuid, resourceType);
            return null;
        }
        return agentNotes.get(0);
    }

    @Override
    public Boolean migrateAuditTime(String coreUuid, String migratedDate) {
        List<Note> notes = repository.getAllNotes(coreUuid, migratedDate);
        log.info("[CRM][NoteMigration] Total Note Migrated Time = {}", notes.size());
        var listUuids = new ArrayList<String>();
        notes.forEach(note -> {
            try {
                log.info("[CRM][NoteMigration] Update note create time with data = {}",
                    objectMapper.writeValueAsString(note));
                Optional<Note> item = noteService.getNoteByUniqueData(note.getParentUuid(),
                    note.getParentType(),
                    note.getOwnerType(),
                    note.getOwnerUuid(),
                    listUuids);
                item.ifPresent(row -> {
                    noteService.updateNoteAuditTime(row.getNoteUuid(),
                        NoteUpdateAudit.builder()
                            .createdDate(note.getCreatedDate())
                            .lastModifiedDate(note.getLastModifiedDate())
                            .build());
                    listUuids.add(row.getNoteUuid());
                });
                Thread.sleep(10);
            } catch (Exception ex) {
                log.error("[CRM][NoteMigration] Error while update note time", ex);
                opportunityAgentRepository.writeMigrationHistory("NOTE_MIGRATION",
                    note.getParentUuid(),
                    "",
                    note.getOwnerType(),
                    "",
                    note,
                    "ERROR",
                    ex.getMessage());
            }
        });
        log.info("[CRM][NoteMigration] Finish Note Migrated = {}", notes.size());
        return true;
    }
}
