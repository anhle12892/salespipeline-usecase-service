package vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.request.CreateOpportunityReq;
import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;

@Mapper(componentModel = "spring", uses = {PhoneNumberMapper.class})
public interface CreateOpportunityReqMapper {
    @Mapping(target = "name", source = "lead.leadName")
    @Mapping(target = "phoneNumber", source = "phoneNumber", qualifiedByName = "mapPhoneToString")
    CreateOpportunityReq from(Lead lead);
}
