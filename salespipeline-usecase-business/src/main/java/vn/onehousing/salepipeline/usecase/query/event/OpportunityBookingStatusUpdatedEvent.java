package vn.onehousing.salepipeline.usecase.query.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpportunityBookingStatusUpdatedEvent {
    private String leadUuid;
    private String opportunityStatus;
    private String bookingCode;
    private String activity;
    private Long timestamp = Instant.now().toEpochMilli();
}
