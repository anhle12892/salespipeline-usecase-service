package vn.onehousing.salepipeline.usecase.business.thirdparty.finance.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@NoArgsConstructor
public class EmiInformationDto {

    String leadUuid;
    String emiUuid;
    String customerName;
    String phoneNumber;
    String emailAddress;
    String transactionType;

    String personalType;
    String accommodationType;
    String currentProvinceAddress;
    String dateOfBirth;
    String educationalDegree;
    String expCurrentCompanyYear;
    String expWorkingYear;
    String maritalStatus;
    String transportationType;

    BigDecimal creditCardLimit;
    BigDecimal familyTotalMonthlyDuty;
    Boolean hasOverdue;
    Boolean interestRateSupportMonth;
    Integer originalGraceMonth;
    BigDecimal spouseCreditCardLimit;
    Boolean spouseHasLoan;
    BigDecimal spouseTotalOverdraftLimit;

    BigDecimal totalAmountCreditCardL1m;
    BigDecimal totalOverdraftLimit;
    Integer totalOverdueDays;

    String projectCode;
    BigDecimal apartmentAmount;

    BigDecimal totalIncomeAmount;
    BigDecimal familyTotalMonthlyExpense;

    // PreQualifiedEmi
    Integer tenorMin;
    Integer tenorMax;
    BigDecimal monthlyPayment;
    BigDecimal loanAmount;
    BigDecimal maxApartmentAmount;
    BigDecimal loanDutySuggestedThreshold;
    BigDecimal ltvmax;
    Integer tenor;
    BigDecimal lTV;
    String paymentMethod;

    //CustomerRequestEmi
    BigDecimal propertyValue;
    BigDecimal preQualifiedLimit;
    String calculationMethod;
    Boolean mortgageConsultingNeed;
    Integer loanTerm;
    Boolean useVisaCard;
    BigDecimal submitLtv;
    BigDecimal submitMonthlyPayment;
    Integer tcbCalculationMethod;

    Instant createdDate;
    Instant lastModifiedDate;
}
