package vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbyaccountuc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import vn.onehousing.salepipeline.usecase.business.mapper.SalespipelineAggregateViewMapper;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.IAccountService;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.utils.CommonUtils;
import vn.onehousing.salepipeline.usecase.common.shared.constants.RedisLockName;
import vn.onehousing.salepipeline.usecase.common.shared.model.Account;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.ISalespipelineAggregateViewRepository;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.SalespipelineAggregateView;
import vn.onehousing.salepipeline.usecase.business.thirdparty.distributedlockservice.IDistributedLockService;

import java.io.IOException;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class IndexAggregateSalespipelineByAccountUc implements IIndexAggregateSalespipelineByAccountUc {

    private final ISalespipelineAggregateViewRepository repository;
    private final SalespipelineAggregateViewMapper mapper;
    private final IAccountService accountService;
    private final IDistributedLockService distributedLockService;

//    @Override
//    public void process(Account account) throws IOException {
//        List<SalespipelineAggregateView> datas = repository.searchByAccountUuid(account.getAccountUuid());
//        for (SalespipelineAggregateView data : datas) {
//
//            if (StringUtils.isEmpty(data.getLeadUuid()))
//                continue;
//
//            Contact contact = accountService.getContactByUuid(data.getContactUuid());
//
//            mapper.updateFields(account, data);
//            mapper.updateFields(contact, data);
//
//            repository.insert(data);
//        }
//    }

    @Override
    public void process(Account account) throws IOException {
        List<SalespipelineAggregateView> datas = repository.searchByAccountUuid(account.getAccountUuid());
        for (SalespipelineAggregateView data : datas) {

            if (StringUtils.isEmpty(data.getLeadUuid()))
                continue;

            var key = CommonUtils.buildRedisLockKey(RedisLockName.SALESPIPELINE_USECASE_INDEX_AGGREGATE_LOCK, data.getLeadUuid());
            var ret = distributedLockService.getLock(key, data.getLeadUuid());
            if (!ret) {
                log.error("[IndexAggregateSalespipelineByLeadUc] - cannot get lock - lead_uuid: [{}] - lastModified: [{}]", data.getLeadUuid(), data.getLastModifiedDate().toEpochMilli());
                return;
            }

            // get lastest data in es
            // search by account uuid does not return the lastest data
            data = repository.getByUuid(data.getLeadUuid());
            mapper.updateFields(account, data);

            if (data.getProfile() != null) {
                mapper.updateFields(account, data.getProfile());
            }

            repository.insert(data);

            distributedLockService.releaseLock(key);
        }
    }
}
