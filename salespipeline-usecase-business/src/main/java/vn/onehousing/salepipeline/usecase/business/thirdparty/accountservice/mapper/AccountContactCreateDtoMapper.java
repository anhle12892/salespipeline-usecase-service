package vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.AccountContactCreateDto;
import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;
import vn.onehousing.salepipeline.usecase.common.shared.model.LeadBankAccount;
import vn.onehousing.salepipeline.usecase.common.shared.model.LeadProfile;

@Mapper(componentModel = "spring")
public interface AccountContactCreateDtoMapper {

    @Mapping(target = "accountName", source = "lead.leadName")
    @Mapping(target = "leadSource", source = "lead.source")
    @Mapping(target = "bankAccountNumber", source = "bankAccount.accountNumber")
    @Mapping(target = "bankAccountHolder", source = "bankAccount.accountHolder")
    @Mapping(target = "bankName", source = "bankAccount.bankName")
    @Mapping(target = "bankCode", source = "bankAccount.bankCode")
    @Mapping(target = "code", source = "lead.accountCode")
    @Mapping(target = "leadChannel", source = "lead.channel")
    @Mapping(target = "maritalStatus", source = "profile.maritalStatus")
    @Mapping(target = "numberOfChildren", source = "profile.numberOfChildren")
    @Mapping(target = "rangeAge", source = "profile.rangeAge")
    @Mapping(target = "childrenSchool", source = "profile.childrenSchool")
    @Mapping(target = "nationality", source = "profile.nationality")
    @Mapping(target = "annualRevenue", source = "profile.annualRevenue")
    @Mapping(target = "monthlyIncome", source = "profile.monthlyIncome")
    @Mapping(target = "monthlyPaymentLoan", source = "profile.monthlyPaymentLoan")
    @Mapping(target = "monthlyExpense", source = "profile.monthlyExpense")
    AccountContactCreateDto fromLeadToCreateAccount(Lead lead, LeadBankAccount bankAccount, LeadProfile profile);

    @Mapping(target = "accountName", source = "lead.leadName")
    @Mapping(target = "leadSource", source = "lead.source")
    @Mapping(target = "bankAccountNumber", source = "bankAccount.accountNumber")
    @Mapping(target = "bankAccountHolder", source = "bankAccount.accountHolder")
    @Mapping(target = "bankName", source = "bankAccount.bankName")
    @Mapping(target = "bankCode", source = "bankAccount.bankCode")
    @Mapping(target = "code", source = "lead.accountCode")
    @Mapping(target = "accountUuid", source = "accountUuid")
    @Mapping(target = "contactName", source = "lead.leadName")
    @Mapping(target = "leadChannel", source = "lead.channel")
    @Mapping(target = "leadUuid", source = "lead.leadUuid")
    @Mapping(target = "leadCode", source = "lead.leadCode")
    @Mapping(target = "dateOfBirth", source = "profile.dateOfBirth")
    @Mapping(target = "facebook", source = "profile.facebook")
    @Mapping(target = "job", source = "profile.job")
    @Mapping(target = "maritalStatus", source = "profile.maritalStatus")
    @Mapping(target = "numberOfChildren", source = "profile.numberOfChildren")
    @Mapping(target = "rangeAge", source = "profile.rangeAge")
    @Mapping(target = "childrenSchool", source = "profile.childrenSchool")
    @Mapping(target = "nationality", source = "profile.nationality")
    @Mapping(target = "annualRevenue", source = "profile.annualRevenue")
    @Mapping(target = "monthlyIncome", source = "profile.monthlyIncome")
    @Mapping(target = "monthlyPaymentLoan", source = "profile.monthlyPaymentLoan")
    @Mapping(target = "monthlyExpense", source = "profile.monthlyExpense")
    @Mapping(target = "company", source = "profile.company")
    AccountContactCreateDto fromLeadToCreateContact(Lead lead, LeadBankAccount bankAccount, String accountUuid, LeadProfile profile);
}
