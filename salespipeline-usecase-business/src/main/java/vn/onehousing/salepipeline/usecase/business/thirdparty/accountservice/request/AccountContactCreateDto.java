package vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request;

import lombok.Builder;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.Address;
import vn.onehousing.salepipeline.usecase.common.shared.model.LeadIdDocument;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class AccountContactCreateDto {
    String accountName;
    String code;
    PhoneNumber phoneNumber;
    String email;
    String assignedUserUuid;
    String assignedUserCode;
    String campaignUuid;
    String campaignCode;
    String referredByUserUuid;
    String referredByUserCode;
    String accountUuid;
    String contactUuid;
    String contactCode;
    String leadSource;
    String bankAccountNumber;
    String bankAccountHolder;
    String bankName;
    String bankCode;
    String maritalStatus;
    String gender;
    String facebook;
    String job;
    String contactName;
    String leadChannel;
    BigDecimal annualRevenue;
    Address address;
    LocalDate dateOfBirth;

    Integer numberOfChildren;
    Integer rangeAge;
    String childrenSchool;
    String nationality;
    BigDecimal monthlyIncome;
    BigDecimal monthlyPaymentLoan;
    BigDecimal monthlyExpense;
    String note;
    String leadUuid;
    String leadCode;
    List<LeadIdDocument> idDocuments;
    Address permanentAddress;
    String company;

}
