package vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.note;

import vn.onehousing.salepipeline.usecase.business.usecase.createhistoryuc.response.OpportunityHistoryRes;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;

import java.sql.SQLException;
import java.util.List;

public interface OpportunityHistoryRepository {
    List<Note> getAllNotes(String coreUuid, String migratedDate);

    List<OpportunityHistoryRes> getAllOpportunityHistory(String coreUuid) throws SQLException;
}
