package vn.onehousing.salepipeline.usecase.business.usecase.activityuc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.thirdparty.agentservice.IAgentService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.note.INoteService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.task.CreateTaskReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.task.ITaskService;
import vn.onehousing.salepipeline.usecase.business.usecase.noteuc.mapper.NoteCreateNoteDtoMapper;
import vn.onehousing.salepipeline.usecase.common.shared.constants.NoteOwnerType;
import vn.onehousing.salepipeline.usecase.common.shared.constants.NoteParentType;
import vn.onehousing.salepipeline.usecase.common.shared.model.AgentDto;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;
import vn.onehousing.salepipeline.usecase.common.shared.model.Task;

import java.util.Objects;
import java.util.Optional;

@Slf4j
@Component
@AllArgsConstructor
public class TaskUseCase implements ITaskUseCase {
    private final INoteService noteService;
    private final ITaskService iTaskService;
    private final IAgentService agentService;
    private final NoteCreateNoteDtoMapper createNoteDtoMapper;

    @Override
    public Task createTask(CreateTaskReq req) {
        String agentEmail = null;
        AgentDto agentDto = agentService.getAgent(req.getAssignedToUserUuid());
        if (Objects.nonNull(agentDto)) {
            agentEmail = agentDto.getEmail();
        }
        req.setAssignedEmail(agentEmail);
        final Task task = iTaskService.createTask(req);
        if (StringUtils.isNotBlank(req.getComments())) {
            final Note note = new Note();
            note.setContent(req.getComments());
            // TODO LTN do not hardcode AGENT
            note.setOwnerType(NoteOwnerType.AGENT);
            note.setOwnerUuid(task.getAssignedToUserUuid());
            note.setTitle(task.getName());
            note.setParentType(NoteParentType.TASK);
            note.setParentUuid(task.getTaskUuid().getTaskUuid());
            note.setOwnerEmail(agentEmail);
            noteService.createNote(createNoteDtoMapper.to(note));
        }
        return task;
    }

    @Override
    public Task updateTask(String taskUuid, CreateTaskReq req) {
        String agentEmail = null;
        AgentDto agentDto = agentService.getAgent(req.getAssignedToUserUuid());
        if (Objects.nonNull(agentDto)) {
            agentEmail = agentDto.getEmail();
        }
        req.setAssignedEmail(agentEmail);
        final Task task = iTaskService.updateTask(taskUuid, req);
        final Optional<String> noteUuid = noteService.getNoteUuidByParentUuidAndParentType(taskUuid
                , NoteParentType.TASK);

        final Note note = new Note();
        note.setContent(req.getComments());
        // TODO LTN do not hardcode AGENT
        note.setOwnerType(NoteOwnerType.AGENT);
        note.setOwnerUuid(req.getAssignedToUserUuid());
        note.setTitle(req.getName());
        note.setParentType(NoteParentType.TASK);
        note.setParentUuid(taskUuid);
        note.setOwnerEmail(agentEmail);

        if (noteUuid.isPresent()) {
            note.setNoteUuid(noteUuid.get());
            noteService.updateNote(noteUuid.get(), createNoteDtoMapper.to(note));
        } else {
            if (StringUtils.isNotBlank(req.getComments())) {
                noteService.createNote(createNoteDtoMapper.to(note));
            }
        }
        return task;
    }
}
