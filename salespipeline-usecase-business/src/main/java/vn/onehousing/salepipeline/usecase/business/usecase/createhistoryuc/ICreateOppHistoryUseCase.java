package vn.onehousing.salepipeline.usecase.business.usecase.createhistoryuc;


import java.sql.SQLException;

public interface ICreateOppHistoryUseCase {
    Boolean migrate(String coreUuid) throws SQLException;
}
