package vn.onehousing.salepipeline.usecase.business.usecase.getsalespipelineaggregateviewuc;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import vn.onehousing.salepipeline.usecase.business.mapper.SalespipelineAggregateViewMapper;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.IAccountService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.IContactService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.ILeadService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.IOpportunityService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.ISalesPipelineService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.IWorkflowService;
import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.ISaleProcessStageRepository;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.SalespipelineAggregateView;

@Component
@AllArgsConstructor
public class GetSalesPipelineAggregateViewUc implements IGetSalesPipelineAggregateViewUc {

    private final ILeadService leadService;
    private final ISalesPipelineService salesPipelineService;
    private final IAccountService accountService;
    private final IContactService contactService;
    private final SalespipelineAggregateViewMapper mapper;
    private final IOpportunityService opportunityService;
    private final ISaleProcessStageRepository saleProcessStageRepository;
    private final IWorkflowService workflowService;

    @Override
    public SalespipelineAggregateView process(String leadUuid) {
        Lead lead = leadService.get(leadUuid);

        var salesPipelineAggregateData = mapper.from(lead);
        SalesPipeline salesPipeline = salesPipelineService.getByLeadUuid(leadUuid);
        mapper.updateFields(salesPipeline, salesPipelineAggregateData);
        String accountUuid = salesPipeline.getAccountUuid();
        String contactUuid = salesPipeline.getContactUuid();
        String oppUuid = salesPipeline.getOpportunityUuid();

        if (StringUtils.hasText(accountUuid) && StringUtils.hasText(contactUuid)) {
            var account = accountService.getAccountByUuid(accountUuid);
            var contact = contactService.getByContactUuid(contactUuid);
            mapper.updateFields(account, salesPipelineAggregateData);
            mapper.updateFields(contact, salesPipelineAggregateData);
            var profile = salesPipelineAggregateData.getProfile();
            mapper.updateFields(account, profile);
            mapper.updateFields(contact, profile);
            salesPipelineAggregateData.setProfile(profile);
            if (StringUtils.hasText(oppUuid)) {
                var opportunity = opportunityService.get(oppUuid);
                mapper.updateFields(opportunity, salesPipelineAggregateData);
            }
        }

        salesPipelineAggregateData.setNumberAssignedUser(0);
        if (salesPipelineAggregateData.getAssignedUsers() != null) {
            salesPipelineAggregateData.setNumberAssignedUser(salesPipelineAggregateData.getAssignedUsers().size());
        }

        String currentStage = "1";
        var optionalSaleProcessStage = saleProcessStageRepository.get(leadUuid);
        if (optionalSaleProcessStage.isPresent()) {
            currentStage = optionalSaleProcessStage.get().getStage();
        }

        String[] agentRolesNeeded = workflowService.getListAgentRolesNeeded(lead.getSource(), lead.getChannel(), currentStage);
        salesPipelineAggregateData.setNumberRequiredUser(agentRolesNeeded.length);

        return salesPipelineAggregateData;
    }
}
