package vn.onehousing.salepipeline.usecase.business.usecase.activityuc;

import vn.onehousing.salepipeline.usecase.business.thirdparty.task.CreateTaskReq;
import vn.onehousing.salepipeline.usecase.common.shared.model.Task;

public interface ITaskUseCase {
    Task createTask(CreateTaskReq req);

    Task updateTask(String taskUuid, CreateTaskReq req);
}
