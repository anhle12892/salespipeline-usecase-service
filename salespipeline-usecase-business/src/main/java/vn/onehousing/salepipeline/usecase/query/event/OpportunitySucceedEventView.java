package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

@Data
public class OpportunitySucceedEventView {
    private String leadUuid;
    private String opportunityUuid;
    private PhoneNumber phoneNumber;
    private Long timestamp;
}
