package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response;

import lombok.Data;

@Data
public class SaleStageSchemaResponse {
    String value;
    String title;
}
