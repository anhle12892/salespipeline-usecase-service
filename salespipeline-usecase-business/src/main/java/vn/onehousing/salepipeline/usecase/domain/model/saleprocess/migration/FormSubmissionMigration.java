package vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration;

import lombok.Data;

@Data
public class FormSubmissionMigration {
    String formPath;
    String data;
}
