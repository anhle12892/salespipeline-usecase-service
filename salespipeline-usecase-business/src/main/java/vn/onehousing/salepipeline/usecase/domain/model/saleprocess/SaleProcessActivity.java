package vn.onehousing.salepipeline.usecase.domain.model.saleprocess;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SaleProcessActivity {
    String leadUuid;
    List<String> activities = new ArrayList<>();
}
