package vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Named;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

@Mapper(componentModel = "spring")
public interface PhoneNumberMapper {

    @Named("mapPhoneToObject")
    default PhoneNumber toObject(String phoneNumber) {
        return PhoneNumber.builder().number(phoneNumber).build();
    }

    @Named("mapPhoneToString")
    default String toString(PhoneNumber phoneNumber) {
        return phoneNumber.getNumber();
    }
}
