package vn.onehousing.salepipeline.usecase.business.usecase.noteuc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.thirdparty.agentservice.IAgentService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.note.INoteService;
import vn.onehousing.salepipeline.usecase.business.usecase.noteuc.mapper.NoteCreateNoteDtoMapper;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.hisActivity.RecordSource;
import vn.onehousing.salepipeline.usecase.common.shared.model.AgentDto;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;

import java.util.Objects;

@Slf4j
@Component
@AllArgsConstructor
public class NoteUseCase implements INoteUseCase {
    private final INoteService noteService;
    private final IAgentService agentService;
    private final NoteCreateNoteDtoMapper createNoteDtoMapper;

    @Override
    public Note createNote(String coreUuid, Note note) {
        if (Objects.isNull(note.getParentType())) {
            note.markParentTypeToLead();
        }
        if (StringUtils.isBlank(note.getTitle())) {
            note.defaultTitle();
        }
        if (RecordSource.AGENT.equals(note.getOwnerType())
            && Objects.isNull(note.getOwnerEmail())) {
            AgentDto agent = agentService.getAgent(note.getOwnerUuid());
            if (Objects.nonNull(agent)) {
                note.setOwnerEmail(agent.getEmail());
            }
        }
        return noteService.createNote(createNoteDtoMapper.to(note));
    }

    @Override
    public Note updateNote(String coreUuid, String noteUuid, Note note) {
        if (RecordSource.AGENT.equals(note.getOwnerType())
            && Objects.isNull(note.getOwnerEmail())) {
            AgentDto agent = agentService.getAgent(note.getOwnerUuid());
            if (Objects.nonNull(agent)) {
                note.setOwnerEmail(agent.getEmail());
            }
        }
        return noteService.updateNote(noteUuid, createNoteDtoMapper.to(note));
    }
}
