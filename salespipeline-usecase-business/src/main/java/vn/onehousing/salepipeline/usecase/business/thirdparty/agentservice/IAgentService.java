package vn.onehousing.salepipeline.usecase.business.thirdparty.agentservice;

import vn.onehousing.salepipeline.usecase.common.shared.model.AgentDto;

public interface IAgentService {
    AgentDto getAgent(String uuid);
}
