package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.migrate;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.IWorkflowService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.request.SaleProcessStageWorkflowRequest;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.response.DecisionResult;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request.SaleProcessMigrationRequest;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response.SaleProcessMigrationResponse;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.ISaleProcessActivityRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.ISaleProcessStageRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessActivity;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessStage;

import java.util.*;

@Slf4j
@Component
@AllArgsConstructor
public class MigrateStageJob implements IMigrateStageJob {
    private final TaskExecutor taskExecutor;

    private final ISaleProcessStageRepository repository;
    private final ISaleProcessActivityRepository activityRepository;

    private final IWorkflowService workflowService;

    @Override
    public SaleProcessMigrationResponse migrate(SaleProcessMigrationRequest request) {
        var response = new SaleProcessMigrationResponse();
        response.setResult("OK");

        taskExecutor.execute(new MigrationBackgroundTask(request.getUuids()));

        return response;
    }

    private Optional<SaleProcessStageWorkflowRequest> from(List<String> activities) {
        var valueMap = new HashMap<String, List<String>>();
        valueMap.put("value", activities);

        var variables = new HashMap<String, Map<String, List<String>>>();
        variables.put("actionList", valueMap);

        var request = new SaleProcessStageWorkflowRequest();
        request.setVariables(variables);

        return Optional.of(request);
    }

    private class MigrationBackgroundTask implements Runnable {
        List<String> uuids;

        public MigrationBackgroundTask(List<String> uuids) {
            this.uuids = uuids;
        }

        public void run() {
            List<SaleProcessActivity> saleProcessActivities = null;

            if (uuids == null || uuids.isEmpty()) {
                saleProcessActivities = activityRepository.getAll();
            } else {
                saleProcessActivities = new ArrayList<>();
                for (var index = 0; index < uuids.size(); index++) {
                    var result = activityRepository.get(uuids.get(index));
                    if (!result.isEmpty()) {
                        saleProcessActivities.add(result.get());
                    }
                }
            }

            log.info("[CRM-Agent][SaleProcessStageCamuda][Start] call camuda to get stage");
            saleProcessActivities.forEach(item -> {
                log.info("[CRM-Agent][SaleProcessStageCamuda] call camuda to get stage for lead_uuid={}",
                    item.getLeadUuid());

                /**
                 * migrate activities
                 * */
                var activities = new ArrayList<String>();
                item.getActivities().forEach(activity -> {
                    if (StringUtils.hasText(activity) && !activities.contains(activity)) {
                        activities.add(activity);
                    }
                });

                item.setActivities(activities);
                activityRepository.update(item);

                /**
                 * migrate stage
                 * */
                Optional<SaleProcessStageWorkflowRequest> optionalRequest = from(activities);
                if (!optionalRequest.isEmpty()) {
                    Optional<DecisionResult> optionalStageWorkflow = workflowService.get(optionalRequest.get());
                    if (!optionalStageWorkflow.isEmpty()) {
                        String newStage = String.valueOf(optionalStageWorkflow.get().getValue());
                        log.info("[CRM-Agent][SaleProcessStageCamuda] new stage for lead_uuid={}, newStage={}",
                            item.getLeadUuid(),
                            newStage);
                        var result = new SaleProcessStage();
                        result.setLeadUuid(item.getLeadUuid());
                        result.setStage(newStage);

                        repository.update(result);
                    } else {
                        log.error("[CRM-Agent][SaleProcessStageCamuda] call camuda failure for lead_uuid={}",
                            item.getLeadUuid());
                    }
                }
            });

            log.info("[CRM-Agent][SaleProcessStageMigration][End] call camuda to get stage");
        }
    }
}
