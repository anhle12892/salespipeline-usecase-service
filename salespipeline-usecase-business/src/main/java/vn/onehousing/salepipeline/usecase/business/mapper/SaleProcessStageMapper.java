package vn.onehousing.salepipeline.usecase.business.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.onehousing.salepipeline.usecase.query.event.*;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;

@Mapper(componentModel = "spring")
public interface SaleProcessStageMapper {
    @Mapping(source = "data.leadUuid", target = "leadUuid")
    SaleProcessInputActivity from(SalesPipelineView data);

    @Mapping(source = "data.leadUuid", target = "leadUuid")
    SaleProcessInputActivity from(AppointmentCreatedEvent data);

    @Mapping(source = "data.leadUuid", target = "leadUuid")
    SaleProcessInputActivity from(PartnerBookingOpportunityUpdatedEvent data);

    @Mapping(source = "data.leadUuid", target = "leadUuid")
    SaleProcessInputActivity from(CreateOpportunityActivityView data);

    @Mapping(source = "data.leadUuid", target = "leadUuid")
    SaleProcessInputActivity from(UpdateOpportunityActivityView data);

    @Mapping(source = "data.entityUuid", target = "leadUuid")
    SaleProcessInputActivity from(CreateTaskActivityView data);

    @Mapping(source = "data.entityUuid", target = "leadUuid")
    SaleProcessInputActivity from(UpdateTaskActivityView data);

    @Mapping(source = "data.lead_uuid", target = "leadUuid")
    SaleProcessInputActivity from(AssignmentContextView data);
}
