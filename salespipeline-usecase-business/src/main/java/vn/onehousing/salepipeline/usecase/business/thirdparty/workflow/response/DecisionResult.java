package vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DecisionResult {
    String type;
    Object value;
    Object valueInfo;
}
