package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.migrate;

import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request.SaleProcessMigrationRequest;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request.TaskMigrationRequest;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response.SaleProcessMigrationResponse;

public interface IMigrateSaleProcessStage {
    SaleProcessMigrationResponse migrate(SaleProcessMigrationRequest request);

    Boolean migrateTask(TaskMigrationRequest request);

}
