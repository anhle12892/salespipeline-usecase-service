package vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbysalespipelineuc;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salepipeline.usecase.business.mapper.SalespipelineAggregateViewMapper;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.utils.CommonUtils;
import vn.onehousing.salepipeline.usecase.common.shared.constants.RedisLockName;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.ISalespipelineAggregateViewRepository;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.SalespipelineAggregateView;
import vn.onehousing.salepipeline.usecase.business.thirdparty.distributedlockservice.IDistributedLockService;

import java.io.IOException;

@Slf4j
@Service
@AllArgsConstructor
public class IndexAggregateSalespipelineViewBySalespipelineUc implements IIndexAggregateSalespipelineViewBySalespipelineUc {

    private final ObjectMapper objectMapper;
    private final SalespipelineAggregateViewMapper mapper;
    private final ISalespipelineAggregateViewRepository repository;

    private final IDistributedLockService distributedLockService;

    @Override
    public void process(SalesPipeline data) throws IOException {

        log.info("[IndexAggregateSalespipelineViewBySalespipelineUc] - start sync - lead_uuid: [{}] - opp_uuid: [{}]  - lastModified: [{}]", data.getLeadUuid(), data.getOpportunityUuid(), data.getLastModifiedDate().toEpochMilli());
        var key = CommonUtils.buildRedisLockKey(RedisLockName.SALESPIPELINE_USECASE_INDEX_AGGREGATE_LOCK, data.getLeadUuid());
        var ret = distributedLockService.getLock(key, data.getLeadUuid());
        if (!ret) {
            log.error("[IndexAggregateSalespipelineViewBySalespipelineUc] - cannot get lock - lead_uuid: [{}] - opp_uuid: [{}] - lastModified: [{}]", data.getLeadUuid(), data.getOpportunityUuid(), data.getLastModifiedDate().toEpochMilli());
            return;
        }

        processAggregate(data);
        distributedLockService.releaseLock(key);
    }

    private void processAggregate(SalesPipeline data) throws IOException {
        SalespipelineAggregateView currentData = repository.getByUuid(data.getLeadUuid());

        if (currentData == null) {
            log.error("[IndexAggregateSalespipelineViewBySalespipelineUc] - cannot find aggregate data with lead_uuid: [{}]", data.getLeadUuid());
            throw new IllegalStateException(String.format("[IndexAggregateSalespipelineViewBySalespipelineUc] - cannot find aggregate data with lead_uuid: [%s]", data.getLeadUuid()));
        }

        if (currentData.getSalespipelineLastModifiedDate() == null) {
            mapper.updateFields(data, currentData);
            repository.insert(currentData);
            log.info("IndexAggregateSalespipelineViewBySalespipelineUc.processAggregate - success insert data with [{}]", objectMapper.writeValueAsString(currentData));
            return;
        }

        if (
            currentData.getSalespipelineLastModifiedDate().toEpochMilli() > data.getLastModifiedDate().toEpochMilli()
        ) {
            return;
        }

        mapper.updateFields(data, currentData);
        if (currentData.getAssignedUsers() != null) {
            currentData.setNumberAssignedUser(currentData.getAssignedUsers().size());
        } else {
            currentData.setNumberAssignedUser(0);
        }
        repository.insert(currentData);
        log.info("IndexAggregateSalespipelineViewBySalespipelineUc.processAggregate - success insert data with [{}]", objectMapper.writeValueAsString(currentData));
    }
}
