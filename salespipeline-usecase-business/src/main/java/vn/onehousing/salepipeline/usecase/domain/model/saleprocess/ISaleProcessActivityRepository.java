package vn.onehousing.salepipeline.usecase.domain.model.saleprocess;

import java.util.List;
import java.util.Optional;

public interface ISaleProcessActivityRepository {
    Optional<SaleProcessActivity> insert(SaleProcessActivity data);
    Optional<SaleProcessActivity> update(SaleProcessActivity data);
    Optional<SaleProcessActivity> get(String leadUuid);

    List<SaleProcessActivity> getAll();

    boolean getLockForUpdate(String leadUuid);
    void releaseLockForUpdate(String leadUuid);
}
