package vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice;


import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.request.CreateLeadReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.request.UpdateLeadReq;
import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;

public interface ILeadService {
    Lead get(String leadUUID);

    Lead update(String leadUuid, UpdateLeadReq request);

    Lead updateMigration(String leadUuid, UpdateLeadReq request);

    Lead create(CreateLeadReq req);
}
