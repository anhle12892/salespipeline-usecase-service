package vn.onehousing.salepipeline.usecase.query.model.agenthistoryview;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import vn.onehousing.salepipeline.usecase.query.event.*;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.hisActivity.RecordSource;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.hisActivity.RecordType;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AgentHistoryView {
    private String historyUuid;
    private String parentUuid;
    private String parentType;
    private String entityUuid;
    private String entityType;
    private String leadUuid;
    private String actorUuid;
    private String opportunitySource;
    private String content;
    private String noteSource;
    private String recordType;
    private List<String> interactionWay;
    private List<String> suggestionNotes;
    private String buyingNeedType;
    private String landingPageUrl;
    private Instant createdTime;
    private Long timestamp;

    public AgentHistoryView mappingUuid(String uuid) {
        if (!StringUtils.isEmpty(uuid)) {
            this.entityUuid = uuid;
        }
        return this;
    }

    public AgentHistoryView mappingEnumValue(Class<?> viewEntity, Map<String, Object> data) {
        String className = viewEntity.getName();
        if (className.equals(CreateNoteActivityView.class.getName())
            || className.equals(UpdateNoteActivityView.class.getName())) {
            this.content = String.valueOf(data.get("content"));
            if (Objects.nonNull(data.get("owner_type"))) {
                String source = String.valueOf(data.get("owner_type"));
                if (RecordSource.AGENT.equals(source)) {
                    this.noteSource = RecordSource.AGENT;
                    this.recordType = RecordType.AGENT_NOTE;
                } else if (RecordSource.CONTACT_CENTER.equals(source)) {
                    this.noteSource = RecordSource.CONTACT_CENTER;
                    this.recordType = RecordType.CONTACT_CENTER_NOTE;
                } else if (RecordSource.CUSTOMER.equals(source)) {
                    this.noteSource = RecordSource.CUSTOMER;
                    this.recordType = RecordType.CUSTOMER_MSG;
                } else if (RecordSource.LANDING_PAGE.equals(source)) {
                    this.noteSource = RecordSource.CUSTOMER;
                    this.recordType = RecordType.CUSTOMER_MSG;
                    this.landingPageUrl = String.valueOf(data.get("content"));
                }
            }
            if (Objects.nonNull(data.get("title")))
                this.suggestionNotes = Collections.singletonList(String.valueOf(data.get("title")));

        } else if (className.equals(CreateTaskActivityView.class.getName())
            || className.equals(UpdateTaskActivityView.class.getName())) {
            this.noteSource = RecordSource.AGENT;
            this.recordType = RecordType.AGENT_NOTE;
            if (Objects.nonNull(data.get("task_type_name")))
                this.suggestionNotes = Collections.singletonList(String.valueOf(data.get("task_type_name")));
            if (Objects.nonNull(data.get("task_type_code")))
                this.interactionWay = Collections.singletonList(String.valueOf(data.get("task_type_code")));
            this.content = String.valueOf(data.get("description"));
        } else if (className.equals(CreateEventActivityView.class.getName())
            || className.equals(UpdateEventActivityView.class.getName())) {
            this.noteSource = RecordSource.AGENT;
            this.recordType = RecordType.AGENT_NOTE;
            this.interactionWay = Collections.singletonList(String.valueOf(data.get("event_type_code")));
        } else if (className.equals(AssignedActivityView.class.getName())) {
            String assignedUserName = String.valueOf(data.get("assigned_user_name"));
            var assignmentContext = new ObjectMapper().convertValue(data.get("assignment_context"), Map.class);
            int assignedTime = Objects.nonNull(data.get("number_of_assigned_times")) ?
                Integer.parseInt(String.valueOf(data.get("number_of_assigned_times"))) : 1;
            if (assignedTime == 1) {
                this.recordType = RecordType.ASSIGN_OPP;
                this.content = "Cơ hội đã được phân bổ đến agent " + assignedUserName;
                if (Objects.nonNull(assignmentContext)) {
                    var context = new ObjectMapper().convertValue(assignmentContext.get("data"), Map.class);
                    if (Objects.nonNull(context) && context.containsKey(AssignmentContext.OPPORTUNITY_SOURCE))
                        this.opportunitySource = String.valueOf(context.get(AssignmentContext.OPPORTUNITY_SOURCE));
                }
            } else {
                this.noteSource = RecordSource.TRANSACTION;
                this.recordType = RecordType.REDISTRIBUTE_OPP;
                this.content = "Cơ hội được tái phân bổ đến agent " + assignedUserName;
            }
        } else if (className.equals(AgentOpportunityUpdatedEvent.class.getName())) {
            this.content = "Bạn đã cập nhật thông tin cơ hội";
            this.noteSource = RecordSource.AGENT;
            this.recordType = RecordType.UPDATE_OPP;
        } else if (className.equals(LeadRevokedEventView.class.getName())) {
            this.content = "Cơ hội đã được thu hồi";
            this.suggestionNotes = new ArrayList<>();
            if (data.containsKey("revoked_type")) {
                this.suggestionNotes.add(String.valueOf(data.get("revoked_type")));
            }
            if (data.containsKey("revoked_reason")) {
                this.suggestionNotes.add(String.valueOf(data.get("revoked_reason")));
            }
            this.noteSource = RecordSource.TRANSACTION;
            this.recordType = RecordType.REVOKE_OPP;
        } else if (className.equals(LeadStoppedEventView.class.getName())) {
            this.noteSource = RecordSource.AGENT;
            this.recordType = RecordType.STOP_OPP;
        } else if (className.equals(OpportunityStoppedEventView.class.getName())) {
            this.noteSource = RecordSource.AGENT;
            this.recordType = RecordType.STOP_OPP;
        } else if (className.equals(OpportunityBookingStatusUpdatedEvent.class.getName())) {
            String bookingCode = "";
            if (data.containsKey("booking_code")) {
                bookingCode = Objects.toString(data.get("booking_code"));
            }
            this.content = "Bạn đã tạo booking " + bookingCode;
            this.noteSource = RecordSource.AGENT;
            this.recordType = RecordType.BOOKING;
        } else if (className.equals(OpportunitySucceedEventView.class.getName())) {
            this.content = "Bạn đã chuyển cơ hội sang trạng thái Thành công";
            this.noteSource = RecordSource.AGENT;
            this.recordType = RecordType.SUCCESS_OPP;
        } else if (className.equals(FinancialConsultingCreatedEvent.class.getName())) {
            this.content = "Bạn đã tạo yêu cầu tư vấn tài chính";
            this.noteSource = RecordSource.AGENT;
            this.recordType = RecordType.REQUEST_MC;
        } else if (className.equals(AppointmentCreatedEvent.class.getName())) {
            StringBuilder builder = new StringBuilder();
            builder.append("Bạn đã tạo lịch hẹn ");
            builder.append(data.get("appointment_name"));
            if (Objects.nonNull(data.get("start_time"))) {
                double dateTime = Double.parseDouble(data.get("start_time").toString());
                Date formattedDate = Date.from(Instant.ofEpochSecond((long) dateTime));
                builder.append(" - ").append(new SimpleDateFormat("dd/MM/yyyy").format(formattedDate));
                builder.append(" - ").append(new SimpleDateFormat("HH:mm:ss").format(formattedDate));
            }
            this.content = builder.toString();
            this.noteSource = RecordSource.AGENT;
            this.recordType = RecordType.APPOINTMENT;
        }
        return this;
    }
}
