package vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview;

import org.elasticsearch.index.engine.VersionConflictEngineException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface ISalespipelineAggregateViewRepository {

    @Retryable(
        exclude = {VersionConflictEngineException.class},
        maxAttemptsExpression = "${elasticsearch.retry.index.maxAttempts:5}",
        backoff = @Backoff(delayExpression = "${elasticsearch.retry.index.maxDelay:2000}")
    )
    SalespipelineAggregateView insert(SalespipelineAggregateView leadView);

    @Retryable(
        maxAttemptsExpression = "${elasticsearch.retry.index.maxAttempts:5}",
        backoff = @Backoff(delayExpression = "${elasticsearch.retry.index.maxDelay:2000}")
    )
    void delete(SalespipelineAggregateView leadView);

    @Retryable(
        maxAttemptsExpression = "${elasticsearch.retry.search.maxAttempts:2}",
        backoff = @Backoff(delayExpression = "${elasticsearch.retry.search.maxDelay:2000}")
    )
    List<SalespipelineAggregateView> searchByAccountUuid(String accountUuid) throws IOException;

    @Retryable(
        maxAttemptsExpression = "${elasticsearch.retry.search.maxAttempts:2}",
        backoff = @Backoff(delayExpression = "${elasticsearch.retry.search.maxDelay:2000}")
    )
    List<SalespipelineAggregateView> searchByContactUuid(String contactUuid) throws IOException;

    @Retryable(
        maxAttemptsExpression = "${elasticsearch.retry.search.maxAttempts:2}",
        backoff = @Backoff(delayExpression = "${elasticsearch.retry.search.maxDelay:2000}")
    )
    SalespipelineAggregateView getByUuid(String leadUuid) throws IOException;

    @Retryable(
        maxAttemptsExpression = "${elasticsearch.retry.search.maxAttempts:2}",
        backoff = @Backoff(delayExpression = "${elasticsearch.retry.search.maxDelay:2000}")
    )
    Optional<SalespipelineAggregateView> searchByChildData(List<String> fields, List<String> values) throws IOException;


    List<SalespipelineAggregateView> findAllAssignedLeadByLastModifiedDate(Instant lastModifiedDate);

}
