package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SaleProcessMigrationResponse {
    String result;
}
