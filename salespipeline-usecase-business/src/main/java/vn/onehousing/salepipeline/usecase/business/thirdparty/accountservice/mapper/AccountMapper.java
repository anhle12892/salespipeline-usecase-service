package vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.mapper;

import org.mapstruct.Mapper;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.AccountUpdateRequest;
import vn.onehousing.salepipeline.usecase.common.shared.model.Account;

@Mapper(componentModel = "spring")
public interface AccountMapper {
    AccountUpdateRequest toAccountCreateRequest(Account account);
}
