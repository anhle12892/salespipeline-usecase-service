package vn.onehousing.salepipeline.usecase.query.model.agenthistoryview;

import org.springframework.data.domain.Pageable;

import javax.annotation.Nullable;
import java.util.List;

public interface IAgentHistoryViewRepository {
    AgentHistoryView insert(AgentHistoryView leadView, @Nullable Long timestamp);
    List<AgentHistoryView> getOpportunityHistories(String uuid, Pageable pageable);
}
