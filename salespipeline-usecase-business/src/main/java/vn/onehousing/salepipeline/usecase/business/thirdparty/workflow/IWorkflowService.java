package vn.onehousing.salepipeline.usecase.business.thirdparty.workflow;

import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.request.SaleProcessStageWorkflowRequest;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.response.DecisionResult;

import java.util.Optional;

public interface IWorkflowService {
    Optional<DecisionResult> get(SaleProcessStageWorkflowRequest req);

    String[] getListAgentRolesNeeded(String leadSource, String leadChannel, String leadStage);
}
