package vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice;


import com.fasterxml.jackson.core.JsonProcessingException;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.request.CreateOpportunityReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.request.UpdateOpportunityReq;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeConfig;
import vn.onehousing.salepipeline.usecase.common.shared.model.Opportunity;

public interface IOpportunityService {
    Opportunity create(CreateOpportunityReq req) throws JsonProcessingException;

    Opportunity update(String opportunityUuid, UpdateOpportunityReq req);

    Opportunity updateMigration(String opportunityUuid, UpdateOpportunityReq req);

    Opportunity get(String opportunityUuid);

    AttributeConfig[] getAttributeConfigs();
}
