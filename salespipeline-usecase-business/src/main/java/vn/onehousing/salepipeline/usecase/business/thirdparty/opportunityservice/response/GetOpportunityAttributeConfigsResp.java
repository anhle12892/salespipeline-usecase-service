package vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.response;

import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeConfig;

public class GetOpportunityAttributeConfigsResp extends BaseResponse<AttributeConfig[]> {
}
