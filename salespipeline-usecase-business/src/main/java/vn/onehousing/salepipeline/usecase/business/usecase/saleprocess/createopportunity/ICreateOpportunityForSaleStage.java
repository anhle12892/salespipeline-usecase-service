package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.createopportunity;

import java.io.IOException;

public interface ICreateOpportunityForSaleStage {
    void process(String leadUuid) throws IOException;
}
