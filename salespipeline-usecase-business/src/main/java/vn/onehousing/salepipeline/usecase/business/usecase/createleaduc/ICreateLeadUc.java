package vn.onehousing.salepipeline.usecase.business.usecase.createleaduc;

import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;

public interface ICreateLeadUc {
    Lead process();
}
