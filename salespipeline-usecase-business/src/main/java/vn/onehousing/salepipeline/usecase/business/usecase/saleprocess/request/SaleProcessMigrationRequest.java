package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleProcessMigrationRequest {
    private List<String> uuids;
}
