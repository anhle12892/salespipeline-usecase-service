package vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.response;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeConfig;

@Data
public class OpportunityAttributeConfigs {
    AttributeConfig[] data;
}
