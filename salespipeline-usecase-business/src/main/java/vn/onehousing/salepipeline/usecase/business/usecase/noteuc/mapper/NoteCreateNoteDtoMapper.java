package vn.onehousing.salepipeline.usecase.business.usecase.noteuc.mapper;

import org.mapstruct.Mapper;
import vn.onehousing.salepipeline.usecase.business.thirdparty.note.request.NoteCreateDto;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;

@Mapper(componentModel = "spring")
public interface NoteCreateNoteDtoMapper {
    NoteCreateDto to(Note note);

    Note from(NoteCreateDto dto);
}
