package vn.onehousing.salepipeline.usecase.business.usecase.prequalifiedleaduc;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.request.FinancialCreateReq;
import vn.onehousing.salepipeline.usecase.business.usecase.finance.IFinancialUseCase;
import vn.onehousing.salepipeline.usecase.business.usecase.noteuc.INoteUseCase;
import vn.onehousing.salepipeline.usecase.business.usecase.noteuc.mapper.NoteCreateNoteDtoMapper;
import vn.onehousing.salepipeline.usecase.business.usecase.prequalifiedleaduc.request.UpdateLeadPreQualifiedReq;
import vn.onehousing.salepipeline.usecase.business.usecase.projectuc.IProjectUseCase;
import vn.onehousing.salepipeline.usecase.business.usecase.projectuc.request.Project;
import vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.IUpdateLeadUc;
import vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.mapper.IUpdateLeadUcReqMapper;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeValue;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;

import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class PreQualificationLeadUc implements IPreQualificationLeadUc {

    private final IUpdateLeadUc updateLeadUc;
    private final INoteUseCase noteUseCase;
    private final IFinancialUseCase financialUseCase;
    private final IUpdateLeadUcReqMapper mapper;
    private final NoteCreateNoteDtoMapper noteCreateNoteDtoMapper;
    private final IProjectUseCase projectUseCase;

    @Override
    public SalesPipeline process(String leadUuid, UpdateLeadPreQualifiedReq request) throws JsonProcessingException, IllegalAccessException {
        log.info("[pre-qualification] process pre-qualified lead {} with body {}", leadUuid, request);
        addProjectCode(request.getAttributes(), leadUuid);
        SalesPipeline salesPipeline = updateLeadUc.process(leadUuid, mapper.toUpdatePreQualifiedLead(request));
        if(Objects.nonNull(request.getNotes())) {
            request.getNotes().forEach(note -> noteUseCase.createNote(leadUuid, noteCreateNoteDtoMapper.from(note)));
        }
        if(request.hasMortgageNeed()) {
            FinancialCreateReq financialBody = FinancialCreateReq.builder()
                    .leadUuid(leadUuid)
                    .projectUuid(getProjectUuid(request.getAttributes()))
                    .build();
            financialUseCase.createFinancial(financialBody);
        }
        log.info("[pre-qualification] leadUuid {} handle success", leadUuid);
        return salesPipeline;
    }

    private String getProjectUuid(List<AttributeValue> attributes) {
        if (Objects.isNull(attributes)) return null;
        var attr = attributes
                .stream()
                .filter(a -> a.getName().equalsIgnoreCase("project_uuid"))
                .findFirst();
        return attr.map(attributeValue -> attributeValue.getValue().toString()).orElse(null);
    }

    private void addProjectCode(List<AttributeValue> attributes, String leadUuid) {
        try {
            String projectUuid = getProjectUuid(attributes);
            Project project = projectUseCase.getProjectByUuid(projectUuid);
            if (Objects.nonNull(project)) {
                AttributeValue projectCode = new AttributeValue();
                projectCode.setName("project_code");
                projectCode.setValue(project.getCode());
                attributes.add(projectCode);
            }
        } catch (Exception e) {
            log.error("[Project-code] leadUuid {} build body has exception {}", leadUuid, e.getMessage());
        }
    }
}
