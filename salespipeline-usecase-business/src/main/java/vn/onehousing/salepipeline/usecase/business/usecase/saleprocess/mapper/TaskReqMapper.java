package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.mapper;

import org.mapstruct.Mapper;
import vn.onehousing.salepipeline.usecase.business.thirdparty.task.CreateTaskReq;
import vn.onehousing.salepipeline.usecase.common.shared.model.Task;

@Mapper(componentModel = "spring")
public interface TaskReqMapper {
    CreateTaskReq to(Task task);
}
