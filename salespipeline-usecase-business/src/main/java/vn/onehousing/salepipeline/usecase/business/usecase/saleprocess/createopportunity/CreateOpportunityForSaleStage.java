package vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.createopportunity;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.processsalestage.IProcessSaleProcessStage;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;

import java.io.IOException;

@Component
@AllArgsConstructor
public class CreateOpportunityForSaleStage implements ICreateOpportunityForSaleStage {
    private final IProcessSaleProcessStage action;

    @Override
    public void process(String leadUuid) throws IOException {
        var inputActivity = new SaleProcessInputActivity();
        inputActivity.setLeadUuid(leadUuid);

        action.process(inputActivity);
    }
}
