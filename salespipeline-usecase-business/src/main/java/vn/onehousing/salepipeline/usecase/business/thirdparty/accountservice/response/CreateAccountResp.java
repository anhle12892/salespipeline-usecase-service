package vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.response;

import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.model.Account;

public class CreateAccountResp extends BaseResponse<Account> {
}
