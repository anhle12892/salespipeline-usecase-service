package vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.response;

import lombok.Builder;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;


@Data
@Builder
public class LeadServiceResponse extends BaseResponse<Lead> {
}
