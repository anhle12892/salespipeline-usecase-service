package vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.note;

import vn.onehousing.salepipeline.usecase.common.shared.model.Note;

import java.util.List;

public interface OpportunityFinanceRepository {

    List<Note> getFinanceByLeadUuidAndResourceType(String leadUuid, String resourceType);
}
