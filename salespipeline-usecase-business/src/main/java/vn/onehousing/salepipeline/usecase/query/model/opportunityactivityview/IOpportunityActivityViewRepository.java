package vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview;


import javax.annotation.Nullable;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface IOpportunityActivityViewRepository {
    OpportunityActivityView insert(OpportunityActivityView activitesView, @Nullable Long timestamp);
    List<OpportunityActivityView> get(String uuid, String phoneNumber) throws IOException;
    Optional<OpportunityActivityView> searchLeadInformation(String leadUuid) throws IOException;
    Optional<OpportunityActivityView> searchByChildData(List<String> fields, List<String> values) throws IOException;
}
