package vn.onehousing.salepipeline.usecase.domain.model.saleprocess;

import java.util.Optional;

public interface ISaleProcessStageRepository {
    Optional<SaleProcessStage> insert(SaleProcessStage data);
    Optional<SaleProcessStage> update(SaleProcessStage data);
    Optional<SaleProcessStage> get(String leadUuid);

    boolean getLockForUpdate(String leadUuid);
    void releaseLockForUpdate(String leadUuid);
}
