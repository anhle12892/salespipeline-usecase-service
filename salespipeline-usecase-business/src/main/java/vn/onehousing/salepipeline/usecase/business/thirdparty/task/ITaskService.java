package vn.onehousing.salepipeline.usecase.business.thirdparty.task;

import vn.onehousing.salepipeline.usecase.common.shared.model.Task;

public interface ITaskService {
    Task createTask(CreateTaskReq req);
    Task updateTask(String taskUuid, CreateTaskReq req);
}
