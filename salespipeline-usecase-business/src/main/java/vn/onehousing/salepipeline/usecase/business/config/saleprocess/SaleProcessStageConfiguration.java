package vn.onehousing.salepipeline.usecase.business.config.saleprocess;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "saleprocess")
public class SaleProcessStageConfiguration {
    private List<String> stages;
}
