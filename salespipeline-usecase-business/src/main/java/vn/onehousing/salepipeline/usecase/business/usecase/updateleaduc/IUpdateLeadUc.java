package vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc;

import com.fasterxml.jackson.core.JsonProcessingException;
import vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.request.UpdateLeadUcReq;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;

import java.io.IOException;

public interface IUpdateLeadUc {
    SalesPipeline process(String leadUuid, UpdateLeadUcReq req) throws JsonProcessingException, IllegalAccessException;

    SalesPipeline processMigration(String leadUuid, UpdateLeadUcReq req) throws JsonProcessingException, IllegalAccessException;

    Boolean updateAllLead() ;

}
