package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

import java.time.Instant;

@Data
public class UpdateEventActivityView {
    private Long eventId;
    private String eventUuid;
    private String entityType;
    private String entityUuid;
    private Boolean isAllDayEvent;
    private String assignedToUserUuid;
    private String attendees;
    private String description;
    private String eventTypeCode;
    private String location;
    private String name;
    private Instant startDate;
    private Instant endDate;
    private Long duration;
    private String status;

    private String createdBy;
    private String lastModifiedBy;
    private Instant createdDate;
    private Instant lastModifiedDate;

    private PhoneNumber phoneNumber;
    private Long timestamp;
}
