package vn.onehousing.salepipeline.usecase.business.thirdparty.task;

import lombok.Builder;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.model.Task;

@Data
@Builder
public class TaskResponse extends BaseResponse<Task> {

}
