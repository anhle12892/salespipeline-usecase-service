package vn.onehousing.salepipeline.usecase.query.event;

import lombok.Data;

import java.time.Instant;

@Data
public class AppointmentCreatedEvent {
    private Long appointmentId;
    private String opportunityUuid;
    private String leadUuid; //leadUuid of OMRE-CRM (equal to leadUuid(LMS)/coreUuid(Agent)
    private String appointmentName;
    private Instant startTime;
}
