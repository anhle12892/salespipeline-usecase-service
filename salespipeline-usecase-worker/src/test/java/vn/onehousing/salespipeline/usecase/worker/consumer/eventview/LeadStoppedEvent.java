package vn.onehousing.salespipeline.usecase.worker.consumer.eventview;

import lombok.Data;
import net.vinid.core.event.AbstractEvent;

@Data
public class LeadStoppedEvent extends AbstractEvent {
    private String leadUuid;
}
