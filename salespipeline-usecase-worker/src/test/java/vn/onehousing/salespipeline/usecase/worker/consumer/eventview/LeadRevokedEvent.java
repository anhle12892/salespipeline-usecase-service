package vn.onehousing.salespipeline.usecase.worker.consumer.eventview;

import lombok.*;
import net.vinid.core.event.AbstractEvent;

@ToString(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LeadRevokedEvent extends AbstractEvent {
    String leadUuid;
    String revokedReason;
    String revokedType;
}
