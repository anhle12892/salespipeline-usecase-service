package vn.onehousing.salespipeline.usecase.worker.consumer;

import net.vinid.core.event.EventPublisher;
import org.assertj.core.api.Assertions;
import org.awaitility.Awaitility;
import org.awaitility.Durations;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEventConst;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;
import vn.onehousing.salespipeline.usecase.test.IntegrationTest;
import vn.onehousing.salespipeline.usecase.worker.consumer.eventview.LeadCreatedEvent;

import java.io.IOException;
import java.time.Instant;

import static org.elasticsearch.client.RequestOptions.DEFAULT;

public class LeadActivityHandlerIT extends IntegrationTest {
    public static final String index = "db-onehousing-salespipeline-usecases-lead-opp-activities-test";

    @Autowired
    private EventPublisher eventPublisher;

    @Autowired
    private RestHighLevelClient esClient;

    @Before
    public void cleanDb() throws IOException {
        final DeleteByQueryRequest deleteRequest = new DeleteByQueryRequest();
        deleteRequest.setQuery(QueryBuilders.matchAllQuery());
        deleteRequest.indices(index);
        deleteRequest.setRefresh(true);
        esClient.deleteByQuery(deleteRequest, DEFAULT);
    }

    @Test
    public void CreateLead_index_into_elasticsearch() throws IOException {
        // given
        final LeadCreatedEvent event = new LeadCreatedEvent();
        event.setLeadUuid("94601c89-eadc-43cb-bf18-1aa12a032020");
        PhoneNumber phoneNumber = new PhoneNumber();
        event.setPhoneNumber(phoneNumber);
        event.setLeadCode("123");
        event.setLeadName("abc");
        event.setLastModifiedDate(Instant.now());

        // verify
        final TermQueryBuilder byEntityUuid = new TermQueryBuilder("entity_uuid", "94601c89-eadc-43cb-bf18-1aa12a032020");
        final TermQueryBuilder byEventType = new TermQueryBuilder("event_type", ActEventConst.CREATED_LEAD);
        final BoolQueryBuilder bool = new BoolQueryBuilder()
                .must(byEntityUuid)
                .must(byEventType);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(bool);
        searchSourceBuilder.sort("last_modified_date", SortOrder.DESC);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);

        // when
        eventPublisher.publishEvent(event);

        // then
        Awaitility.await().atMost(Durations.TEN_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
        });
        SearchResponse response = esClient.search(searchRequest, DEFAULT);
        Object data = response.getHits().getHits()[0];
        System.out.println(data.toString());
    }
    @Test
    public void event_lead_revoked_index_elastic(){

    }
}
