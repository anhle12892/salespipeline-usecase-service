package vn.onehousing.salespipeline.usecase.worker.consumer.eventview;

import lombok.*;
import net.vinid.core.event.AbstractEvent;

import java.time.Instant;

@ToString(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OpportunityCreatedEvent extends AbstractEvent {
    Instant lastModifiedDate;
    String createdBy;
    String lastModifiedBy;
    Instant createdDate;
    private Long opportunityId;
    private String opportunityUuid;
    private String opportunityCode;
    private String name;
    private String status;
    private String leadUuid;
    private String description;
    private String accountUuid;
    private String accountCode;
    private String productCode;
    private String productUuid;
    private String referredByUserUuid;
    private String referredByUserCode;
    private String assignedUserUuid;
    private String assignedUserCode;
    private String note;
    private String campaignUuid;
    private String campaignCode;
    private Instant closedDate;
    private String attributeSetValue;
    private String leadSource;
    private Boolean isSignContract;
}
