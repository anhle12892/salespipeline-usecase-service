package vn.onehousing.salespipeline.usecase.worker.consumer;

import lombok.*;
import net.vinid.core.event.AbstractEvent;
import net.vinid.core.event.EventPublisher;
import org.assertj.core.api.Assertions;
import org.awaitility.Awaitility;
import org.awaitility.Durations;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import vn.onehousing.salepipeline.usecase.query.event.AssignmentContext;
import vn.onehousing.salepipeline.usecase.common.shared.constants.NoteParentType;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;
import vn.onehousing.salepipeline.usecase.business.usecase.insertopportunityactivityviewuc.IInsertOpportunityActivityViewUc;
import vn.onehousing.salespipeline.usecase.test.IntegrationTest;
import vn.onehousing.salespipeline.usecase.worker.consumer.eventview.ActivityTaskCreatedEvent;
import vn.onehousing.salespipeline.usecase.worker.consumer.eventview.AssignedActivityEvent;
import vn.onehousing.salespipeline.usecase.worker.consumer.eventview.NoteCreatedEvent;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.elasticsearch.client.RequestOptions.DEFAULT;

public class SalesPipelineActivityCreatedHandlerIT extends IntegrationTest {

    public static final String index = "db-onehousing-salespipeline-usecases-lead-opp-activities-test";

    @Autowired
    private EventPublisher eventPublisher;

    @Autowired
    private RestHighLevelClient esClient;

    @MockBean
    private IInsertOpportunityActivityViewUc viewUc;

    @Before
    public void cleanDb() throws IOException {
        final DeleteByQueryRequest deleteRequest = new DeleteByQueryRequest();
        deleteRequest.setQuery(QueryBuilders.matchAllQuery());
        deleteRequest.indices(index);
        deleteRequest.setRefresh(true);
        esClient.deleteByQuery(deleteRequest, DEFAULT);
    }

    @Test
    public void CreateEvent_index_into_elasticsearch() throws IOException {
        // given
        final ActivityEventCreatedEvent event = new ActivityEventCreatedEvent();
        event.setEventUuid("94601c89-eadc-43cb-bf18-1aa12a032020");
        event.setEntityUuid("94601c89-eadc-43cb-bf18-1aa12a032020");
        event.setEntityType("LEAD");
        event.setLastModifiedBy("123-123-123");
        event.setLastModifiedDate(Instant.now());

        // verify
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("entity_uuid", "94601c89-eadc-43cb-bf18-1aa12a032020");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);

        // when
        eventPublisher.publishEvent(event);

        // then
        Awaitility.await().atMost(Durations.TEN_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
            //
            Object data = response.getHits().getHits()[0].getSourceAsString();
            OpportunityActivityView search = objectMapper.readValue(data.toString(), OpportunityActivityView.class);
            Assertions.assertThat(search.getParentUuid()).isEqualTo(("94601c89-eadc-43cb-bf18-1aa12a032020"));

        });
    }

    @Test
    public void GetEvent_index_from_elasticsearch() throws IOException {
        // given
        final ActivityEventCreatedEvent event = new ActivityEventCreatedEvent();
        event.setEventUuid("94601c89-eadc-43cb-bf18-1aa12a032020");
        event.setEntityUuid("94601c89-eadc-43cb-bf18-1aa12a032020");
        event.setEntityType("LEAD");
        event.setLastModifiedBy("123-123-123");
        event.setLastModifiedDate(Instant.now());

        // verify
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("entity_uuid", "94601c89-eadc-43cb-bf18-1aa12a032020");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);

        // when
        eventPublisher.publishEvent(event);

        // then
        Awaitility.await().atMost(Durations.TEN_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
            //
        });
    }

    @Test
    public void CreateTask_index_into_elasticsearch() throws IOException {
        // given
        String leadUUid = "94601c89-eadc-43cb-bf18-1aa12a032021";
        final ActivityTaskCreatedEvent event = new ActivityTaskCreatedEvent();
        event.setTaskUuid("94601c89-eadc-43cb-bf18-1aa12a032020");
        event.setEntityUuid(leadUUid);
        event.setTaskTypeCode("call");
        event.setStatus("status");
        event.setComments("free text");
        event.setLastModifiedDate(Instant.now());

        final ActivityTaskCreatedEvent event1 = new ActivityTaskCreatedEvent();
        event1.setTaskUuid("94601c89-eadc-43cb-bf18-1aa12a032021");
        event1.setEntityUuid(leadUUid);
        event1.setName("task");
        event1.setStatus("status1");
        event1.setComments("free text1");
        event1.setLastModifiedDate(Instant.now());

        // verify
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("entity_uuid", leadUUid);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);

        // when
        eventPublisher.publishEvent(event);
        Awaitility.await().atMost(Durations.FIVE_SECONDS);
        //
        eventPublisher.publishEvent(event1);
        // then
        Awaitility.await().atMost(Durations.TEN_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(2);
        });

    }

    @Test
    public void CreateNote_index_into_elasticsearch() throws IOException {
        // given
        String leadUUid = "94601c89-eadc-43cb-bf18-1aa12a032021";
        final NoteCreatedEvent event2 = new NoteCreatedEvent();
        event2.setNoteUuid("94601c89-eadc-43cb-bf18-1aa12a032022");
        event2.setParentUuid(leadUUid);
        event2.setTitle("name1");
        event2.setContent("note note");
        event2.setNoteCode("email");
        event2.setParentType(NoteParentType.LEAD);
        event2.setLastModifiedDate(Instant.now());

        // verify
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("entity_uuid", leadUUid);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);

        // when
        eventPublisher.publishEvent(event2);
        // then
        Awaitility.await().atMost(Durations.TEN_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
        });

    }

    @Test
    public void CreateMultiActivity_index_into_elasticsearch() throws IOException {
        // given
        String leadUUid = "94601c89-eadc-43cb-bf18-1aa12a032021";

        final ActivityEventCreatedEvent event = new ActivityEventCreatedEvent();
        event.setEventUuid("94601c89-eadc-43cb-bf18-1aa12a032020");
        event.setEntityUuid(leadUUid);
        event.setEntityType("LEAD");
        event.setLastModifiedDate(Instant.now());

        final ActivityTaskCreatedEvent event1 = new ActivityTaskCreatedEvent();
        event1.setTaskUuid("94601c89-eadc-43cb-bf18-1aa12a032021");
        event1.setEntityUuid(leadUUid);
        event1.setName("name1");
        event1.setStatus("status1");
        event1.setLastModifiedDate(Instant.now());

        final NoteCreatedEvent event2 = new NoteCreatedEvent();
        event2.setNoteUuid("94601c89-eadc-43cb-bf18-1aa12a032022");
        event2.setParentUuid(leadUUid);
        event2.setTitle("name1");
        event2.setParentType(NoteParentType.LEAD);
        event2.setLastModifiedDate(Instant.now());

        // verify
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("entity_uuid", leadUUid);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);

        // when
        eventPublisher.publishEvent(event);
        Awaitility.await().atMost(Durations.TWO_SECONDS);
        //
        eventPublisher.publishEvent(event1);
        Awaitility.await().atMost(Durations.TWO_SECONDS);
        //
        eventPublisher.publishEvent(event2);
        // then
        Awaitility.await().atMost(Durations.TEN_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(3);
        });

    }

    @Test
    public void CreateAssignmentLead_index_into_elasticsearch() throws IOException {
        // given
        String leadUUid = "94601c89-eadc-43cb-bf18-1aa12a032021";
        Map<String, Object> map = new HashMap<>();
        final AssignedActivityEvent event2 = new AssignedActivityEvent();
        event2.setPolicyUuid(UUID.randomUUID().toString());
        event2.setPolicyName("policy1");
        event2.setAssignedDate(Instant.now());

        map.put("lead_source", "source");
        map.put("lead_uuid", leadUUid);
        map.put("opportunity_uuid", null);
        map.put("agent_uuid", "123");

        AssignmentContext context = new AssignmentContext();
        context.setData(map);

        event2.setAssignmentContext(context);

        // verify
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("entity_uuid", leadUUid);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);

        // when
        eventPublisher.publishEvent(event2);
        // then
        Awaitility.await().atMost(Durations.TEN_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
        });

    }

//    @Test
//    public void CreateAssignmentOpp_index_into_elasticsearch() throws IOException {
//        SalespipelineAggregateView view = Mockito.mock(SalespipelineAggregateView.class);
//        doNothing().when(viewUc).process(isA(SalespipelineAggregateView.class));
//
//        // given
//        String leadUUid = "94601c89-eadc-43cb-bf18-1aa12a032021";
//        Map<String, Object> map = new HashMap<>();
//        final AssignedActivityEvent event2 = new AssignedActivityEvent();
//        event2.setPolicyUuid(UUID.randomUUID().toString());
//        event2.setPolicyName("policy1");
//        event2.setAssignedDate(Instant.now());
//
//        map.put("lead_source","source");
//        map.put("lead_uuid",null);
//        map.put("opportunity_uuid",leadUUid);
//        map.put("agent_uuid","123");
//
//        AssignmentContext context = new AssignmentContext();
//        context.setData(map);
//
//        event2.setAssignmentContext(context);
//
//        //Opp
//        final OpportunityCreatedEvent event1 = new OpportunityCreatedEvent();
//        event1.setOpportunityUuid(leadUUid);
//        event1.setLeadUuid("94601c89-eadc-43cb-bf18-1aa12a032020");
//        event1.setLastModifiedDate(Instant.now());
//
//        // verify
//        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("entity_uuid", "94601c89-eadc-43cb-bf18-1aa12a032020");
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(matchQueryBuilder);
//
//        SearchRequest searchRequest = new SearchRequest();
//
//        searchRequest.indices(index).source(searchSourceBuilder);
//        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);
//
//        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
//        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);
//
//        // when
//        eventPublisher.publishEvent(event1);
//        System.out.println("===pushed event1===");
//        Awaitility.await().pollDelay(Durations.FIVE_SECONDS).until(() -> true);
//        //
//        System.out.println("===pushed event2===");
//        eventPublisher.publishEvent(event2);
//        // then
//        Awaitility.await().atMost(Durations.ONE_MINUTE).untilAsserted(() -> {
//            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
//            SearchResponse response = esClient.search(searchRequest, DEFAULT);
//            Assertions.assertThat(response.getHits().getHits()).hasSize(2);
//        });
//
//    }

    @ToString(callSuper = true)
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    static class ActivityEventCreatedEvent extends AbstractEvent {
        private Long eventId;
        private String eventUuid;
        private String entityType;
        private String entityUuid;
        private Boolean isAllDayEvent;
        private String assignedToUserUuid;
        private String attendees;
        private String description;
        private String eventTypeCode;
        private String location;
        private String name;
        private Instant startDate;
        private Instant endDate;
        private Long duration;
        private String status;
        private String createdBy;
        private String lastModifiedBy;
        private Instant createdDate;
        private Instant lastModifiedDate;
    }
}
