package vn.onehousing.salespipeline.usecase.worker.consumer.agenthistories;

import net.vinid.core.event.EventPublisher;
import org.assertj.core.api.Assertions;
import org.awaitility.Awaitility;
import org.awaitility.Durations;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.hisActivity.RecordType;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.AgentHistoryView;
import vn.onehousing.salepipeline.usecase.business.usecase.insertopportunityactivityviewuc.IInsertOpportunityActivityViewUc;
import vn.onehousing.salespipeline.usecase.test.IntegrationTest;
import vn.onehousing.salespipeline.usecase.worker.consumer.eventview.OpportunityStoppedEvent;
import vn.onehousing.salespipeline.usecase.worker.consumer.eventview.OpportunitySucceedEvent;

import java.io.IOException;
import java.util.UUID;

import static org.elasticsearch.client.RequestOptions.DEFAULT;

public class OpportunityHandlerIT extends IntegrationTest {
    public static final String index = "db-onehousing-salespipeline-usecases-agent-histories-test";

    @Autowired
    private EventPublisher eventPublisher;

    @Autowired
    private RestHighLevelClient esClient;

    @MockBean
    private IInsertOpportunityActivityViewUc viewUc;

    @Before
    public void cleanDb() throws IOException {
        final DeleteByQueryRequest deleteRequest = new DeleteByQueryRequest();
        deleteRequest.setQuery(QueryBuilders.matchAllQuery());
        deleteRequest.indices(index);
        deleteRequest.setRefresh(true);
        esClient.deleteByQuery(deleteRequest, DEFAULT);
    }

    @Test
    public void create_Opportunity_Stop_to_Index() throws IOException {
        String leadUuid = UUID.randomUUID().toString();
        OpportunityStoppedEvent event = new OpportunityStoppedEvent();
        event.setLeadUuid(leadUuid);
        event.setOpportunityUuid(UUID.randomUUID().toString());

        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("lead_uuid", leadUuid);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);
        //
        eventPublisher.publishEvent(event);
        // then
        Awaitility.await().atMost(Durations.FIVE_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
            var search = response.getHits().getHits()[0];
            AgentHistoryView obj = objectMapper.readValue(search.getSourceAsString(), AgentHistoryView.class);
            Assertions.assertThat(obj.getRecordType()).isEqualTo(RecordType.STOP_OPP);
        });
    }

    @Test
    public void create_Opportunity_Success_To_Index() throws IOException {
        String leadUuid = UUID.randomUUID().toString();
        OpportunitySucceedEvent event = new OpportunitySucceedEvent();
        event.setLeadUuid(leadUuid);
        event.setOpportunityUuid(UUID.randomUUID().toString());

        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("lead_uuid", leadUuid);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);
        //
        eventPublisher.publishEvent(event);
        // then
        Awaitility.await().atMost(Durations.FIVE_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
            var search = response.getHits().getHits()[0];
            AgentHistoryView obj = objectMapper.readValue(search.getSourceAsString(), AgentHistoryView.class);
            Assertions.assertThat(obj.getRecordType()).isEqualTo(RecordType.SUCCESS_OPP);
        });
    }

}
