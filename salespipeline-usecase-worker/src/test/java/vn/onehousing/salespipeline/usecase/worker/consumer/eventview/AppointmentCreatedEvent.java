package vn.onehousing.salespipeline.usecase.worker.consumer.eventview;

import lombok.Data;
import net.vinid.core.event.AbstractEvent;

import java.time.Instant;

@Data
public class AppointmentCreatedEvent extends AbstractEvent {
    private Long appointmentId;
    private String opportunityUuid;
    private String leadUuid; //leadUuid of OMRE-CRM (equal to leadUuid(LMS)/coreUuid(Agent)
    private String appointmentName;
    private Instant startTime;
}
