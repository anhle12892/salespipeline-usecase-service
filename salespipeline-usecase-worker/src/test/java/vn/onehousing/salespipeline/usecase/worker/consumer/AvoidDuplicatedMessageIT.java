package vn.onehousing.salespipeline.usecase.worker.consumer;

import com.fasterxml.jackson.core.type.TypeReference;
import net.vinid.core.event.EventMessage;
import org.assertj.core.api.Assertions;
import org.awaitility.Awaitility;
import org.awaitility.Durations;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;
import vn.onehousing.salespipeline.usecase.test.IntegrationTest;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

import static org.elasticsearch.client.RequestOptions.DEFAULT;

public class AvoidDuplicatedMessageIT extends IntegrationTest {

    private static final TypeReference<Map<String, Object>> MAP_TYPE_REFERENCE = new TypeReference<Map<String, Object>>() {
    };

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    private SearchRequest buildSearchRequestByAccountId(AccountActivityHandlerIT.AccountCreatedEvent accountCreatedEvent) {
        IdsQueryBuilder uuidFilter = QueryBuilders.idsQuery().addIds(accountCreatedEvent.getId());
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery().filter(uuidFilter);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(boolQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);
        return searchRequest;
    }

    @Test
    public void skip_process_message_if_already_processed() {
        // given
        final AccountActivityHandlerIT.AccountCreatedEvent accountCreatedEvent = new AccountActivityHandlerIT.AccountCreatedEvent();
        accountCreatedEvent.setAccountUuid( UUID.fromString("94601c89-eadc-43cb-bf18-1aa12a032020"));
        PhoneNumber phoneNumber = new PhoneNumber();
        accountCreatedEvent.setId("12345");
        phoneNumber.setNumber("123456789");
        phoneNumber.setPhoneType("a");
        accountCreatedEvent.setPhone(phoneNumber);
        accountCreatedEvent.setCreatedDate(Instant.now());
        accountCreatedEvent.setCreatedBy(UUID.fromString("94601c89-eadc-43cb-bf18-1aa12a032020"));
        accountCreatedEvent.setLastModifiedDate(Instant.now());

        final EventMessage message1 = new EventMessage("test", AccountActivityHandlerIT.AccountCreatedEvent.class.getSimpleName(),
                objectMapper.convertValue(accountCreatedEvent, MAP_TYPE_REFERENCE));
        this.applicationEventPublisher.publishEvent(message1);

        final SearchRequest searchRequest = buildSearchRequestByAccountId(accountCreatedEvent);

        // verify
        Awaitility.await().atMost(Durations.TEN_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
        });

//        accountCreatedEvent.setLastModifiedDate(Instant.now());
        accountCreatedEvent.setId("12345.2");
//        accountCreatedEvent.setAccountUuid( UUID.fromString("94601c89-eadc-43cb-bf18-1aa12a032021"));
        final EventMessage message2 = new EventMessage("test", AccountActivityHandlerIT.AccountCreatedEvent.class.getSimpleName(),
                objectMapper.convertValue(accountCreatedEvent, MAP_TYPE_REFERENCE));
        message2.setId(message1.getId());
        this.applicationEventPublisher.publishEvent(message2);

        final SearchRequest searchRequest2 = buildSearchRequestByAccountId(accountCreatedEvent);

        // verify
        Awaitility.await().pollDelay(Durations.TEN_SECONDS).atMost(Durations.ONE_MINUTE).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest2, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(0);
        });

        final String value = stringRedisTemplate.opsForValue().get("duplication:onehs-crm_account_service-account_created-test:onehs-crm_account_worker-account_service-account_created-elasticsearch_indexing_handler:" + message2.getId());
        Assertions.assertThat(value).isNotEmpty();
    }
}
