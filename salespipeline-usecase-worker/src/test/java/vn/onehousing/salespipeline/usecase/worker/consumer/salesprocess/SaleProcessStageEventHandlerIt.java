package vn.onehousing.salespipeline.usecase.worker.consumer.salesprocess;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Data;
import net.vinid.core.event.AbstractEvent;
import net.vinid.core.event.EventPublisher;
import org.awaitility.Awaitility;
import org.awaitility.Durations;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import vn.onehousing.salepipeline.usecase.query.event.*;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.ISaleProcessActivityRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.ISaleProcessStageRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessStage;
import vn.onehousing.salespipeline.usecase.test.IntegrationTest;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author ltnguyen on 12/11/2021
 */
public class SaleProcessStageEventHandlerIt extends IntegrationTest {

    @Autowired
    private EventPublisher eventPublisher;

    @Autowired
    ISaleProcessStageRepository repository;

    @Autowired
    ISaleProcessActivityRepository activityRepository;

    @Test
    public void should_return_stage_2() {
        // given
        final SaleProcessStage stage = new SaleProcessStage();
        stage.setLeadUuid("e59f9ac2-6ca9-4079-93ac-ab5873f0a251");
        stage.setStage("1");
        repository.insert(stage);

        final CreateTaskActivityView task1 = new CreateTaskActivityView();
        task1.setTaskUuid("12345");
        task1.setStatus("COMPLETED");
        task1.setEntityUuid("e59f9ac2-6ca9-4079-93ac-ab5873f0a251");
        task1.setEntityType("TASK");
        task1.setCode("APPROACH_CONTACTED_CTV");

        final ActivityTaskCreatedEvent event = new ActivityTaskCreatedEvent();
        event.setCreateTaskActivityView(task1);

        wireMockRule.stubFor(post(urlEqualTo("/engine-rest/decision-definition/key/saleprocess-stage-decision/evaluate"))
            .willReturn(
                aResponse()
                    .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .withBodyFile("salesprocess-stage-decision-2.json")));

        // when
        eventPublisher.publishEvent(event);

        // then
        Awaitility.await().atMost(Durations.TEN_SECONDS).untilAsserted(() -> {
            assertThat(repository.get(stage.getLeadUuid()).isPresent()).isTrue();
            assertThat(repository.get(stage.getLeadUuid()).get().getStage()).isEqualTo("2");
        });
    }

    @Data
    public static class ActivityTaskCreatedEvent extends AbstractEvent {
        @JsonUnwrapped
        private CreateTaskActivityView createTaskActivityView;
    }

    @Data
    public static class ActivityTaskUpdatedEvent extends AbstractEvent {
        @JsonUnwrapped
        private UpdateTaskActivityView updateTaskActivityView;
    }

    @Data
    public static class AppointmentEvent extends AbstractEvent {
        @JsonUnwrapped
        private AppointmentCreatedEvent appointmentCreatedEvent;
    }

    @Data
    public static class PartnerBookingUpdatedEvent extends AbstractEvent {
        @JsonUnwrapped
        private PartnerBookingOpportunityUpdatedEvent partnerBookingOpportunityUpdatedEvent;
    }

    @Data
    public static class OpportunitySaleProcessStageCreatedEvent extends AbstractEvent {
        @JsonUnwrapped
        private CreateOpportunityActivityView createOpportunityActivityView;
    }

    @Data
    public static class OpportunitySaleProcessStageUpdatedEvent extends AbstractEvent {
        @JsonUnwrapped
        private UpdateOpportunityActivityView updateOpportunityActivityView;
    }

    @Data
    public static class LeadSaleProcessStageCreatedEvent extends AbstractEvent {
        @JsonUnwrapped
        private CreateLeadActivityView createLeadActivityView;
    }

    @Data
    public static class LeadSaleProcessStageUpdatedEvent extends AbstractEvent {
        @JsonUnwrapped
        private UpdateLeadActivityView updateLeadActivityView;
    }

    @Data
    public static class AssignmentSaleProcessStageEvent extends AbstractEvent {
        @JsonUnwrapped
        private AssignedActivityView assignedActivityView;
    }
}
