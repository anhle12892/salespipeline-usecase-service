package vn.onehousing.salespipeline.usecase.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import org.awaitility.Awaitility;
import org.awaitility.Durations;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cache.CacheManager;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo.MongoSaleProcessStageRepository;

import java.io.IOException;
import java.util.Set;

import static org.elasticsearch.client.RequestOptions.DEFAULT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = {"test"})
public abstract class IntegrationTest {

    private static final int WIREMOCK_PORT = 8089;
    public static final String index = "db-onehousing-salespipeline-usecases-lead-opp-activities-test";
    public static final String indexHistory = "db-onehousing-salespipeline-usecases-agent-histories-test";

    static {
        System.setProperty("log.json.disable", "true");
    }

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(WIREMOCK_PORT);

    @LocalServerPort
    protected int serverPort;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired(required = false)
    protected CacheManager cacheManager;

    @Autowired
    private CleanMessageInterceptor cleanMessageInterceptor;

    @Autowired
    protected RestHighLevelClient esClient;

    @Autowired
    protected StringRedisTemplate stringRedisTemplate;

    @Autowired
    protected MongoSaleProcessStageRepository repository;

    @Before
    public final void setup() throws IOException {
        RestAssured.port = this.serverPort;
        RestAssured.config = RestAssured.config().objectMapperConfig(RestAssured.config().getObjectMapperConfig()
                .jackson2ObjectMapperFactory((cls, charset) -> objectMapper));
        RestAssured.requestSpecification = new RequestSpecBuilder() //
                .setContentType(MediaType.APPLICATION_JSON_VALUE) //
                .setAccept(MediaType.APPLICATION_JSON_VALUE) //
                .build();

        if (cacheManager != null) {
            cacheManager.getCacheNames().forEach(name -> cacheManager.getCache(name).clear());
        }
        ignoreKafkaMessage();
        cleanES();
        cleanRedis();
        clearMongo();
    }

    private void clearMongo() {
        repository.deleteAll();
        // TODO
    }

    private void ignoreKafkaMessage() {
        cleanMessageInterceptor.setIgnore(true);
        Awaitility.waitAtMost(Durations.TEN_SECONDS).pollDelay(Durations.FIVE_SECONDS).until(() -> true);
        cleanMessageInterceptor.setIgnore(false);
    }

    private void cleanES() throws IOException {
        deleteEsIndex(index);
        deleteEsIndex(indexHistory);
    }

    private void deleteEsIndex(String index) throws IOException {
        DeleteByQueryRequest deleteRequest = new DeleteByQueryRequest();
        deleteRequest.setQuery(QueryBuilders.matchAllQuery());
        deleteRequest.indices(index);
        deleteRequest.setRefresh(true);
        esClient.deleteByQuery(deleteRequest, DEFAULT);
    }

    private void cleanRedis() {
        final Set<String> keys = stringRedisTemplate.keys("*");
        if (keys != null) {
            keys.forEach(key -> {
                if (key != null) {
                    stringRedisTemplate.delete(key);
                }
            });
        }
    }
}

