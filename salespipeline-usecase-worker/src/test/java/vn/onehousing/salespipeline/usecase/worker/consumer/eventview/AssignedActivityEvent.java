package vn.onehousing.salespipeline.usecase.worker.consumer.eventview;

import lombok.*;
import net.vinid.core.event.AbstractEvent;
import vn.onehousing.salepipeline.usecase.query.event.AssignmentContext;

import java.time.Instant;

@ToString(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AssignedActivityEvent extends AbstractEvent {
    Instant assignedDate;
    String assignedUserName;
    Integer numberOfAssignedTimes;
    private String policyUuid;
    private String policyName;
    private AssignmentContext assignmentContext;
}
