package vn.onehousing.salespipeline.usecase.worker.consumer.eventview;

import lombok.Data;
import net.vinid.core.event.AbstractEvent;
import vn.onehousing.salepipeline.usecase.query.event.AssignmentContext;

import java.time.Instant;

@Data
public class AssignmentCreatedEvent extends AbstractEvent {
    String policyUuid;

    String policyName;

    String assignedUserUuid;

    AssignmentContext assignmentContext;

    Instant assignedDate;

    String assignedUserName;

    Integer numberOfAssignedTimes;
}
