package vn.onehousing.salespipeline.usecase.worker.consumer.eventview;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Data;
import net.vinid.core.event.AbstractEvent;
import vn.onehousing.salepipeline.usecase.query.event.CreateLeadActivityView;

@Data
public class FinancialConsultingCreatedEvent extends AbstractEvent {
    @JsonUnwrapped
    public CreateLeadActivityView lead;
}
