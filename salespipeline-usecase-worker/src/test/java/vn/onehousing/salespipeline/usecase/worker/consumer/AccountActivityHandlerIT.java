package vn.onehousing.salespipeline.usecase.worker.consumer;

import lombok.*;
import net.vinid.core.event.AbstractEvent;
import net.vinid.core.event.EventPublisher;
import org.assertj.core.api.Assertions;
import org.awaitility.Awaitility;
import org.awaitility.Durations;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;
import vn.onehousing.salespipeline.usecase.test.IntegrationTest;

import java.io.IOException;
import java.time.Instant;
import java.util.UUID;

import static org.elasticsearch.client.RequestOptions.DEFAULT;

public class AccountActivityHandlerIT extends IntegrationTest {
    public static final String index = "db-onehousing-salespipeline-usecases-lead-opp-activities-test";

    @Autowired
    private EventPublisher eventPublisher;

    @Autowired
    private RestHighLevelClient esClient;

    @Before
    public void cleanDb() throws IOException {
        final DeleteByQueryRequest deleteRequest = new DeleteByQueryRequest();
        deleteRequest.setQuery(QueryBuilders.matchAllQuery());
        deleteRequest.indices(index);
        deleteRequest.setRefresh(true);
        esClient.deleteByQuery(deleteRequest, DEFAULT);
    }

//    @Test
//    public void CreateAccount_index_into_elasticsearch() throws IOException {
//        // given
//        final AccountCreatedEvent event = new AccountCreatedEvent();
//        event.setAccountUuid( UUID.fromString("94601c89-eadc-43cb-bf18-1aa12a032020"));
//        PhoneNumber phoneNumber = new PhoneNumber();
//        phoneNumber.setNumber("123456789");
//        phoneNumber.setPhoneType("a");
//        event.setPhone(phoneNumber);
//        event.setCreatedDate(Instant.now());
//        event.setCreatedBy(UUID.fromString("94601c89-eadc-43cb-bf18-1aa12a032020"));
//        event.setLastModifiedDate(Instant.now());
//
//        // verify
//        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("phone_number", "123456789");
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(matchQueryBuilder);
//
//        SearchRequest searchRequest = new SearchRequest();
//
//        searchRequest.indices(index).source(searchSourceBuilder);
//        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);
//
//        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
//        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);
//
//        // when
//        eventPublisher.publishEvent(event);
//        // then
//        Awaitility.await().atMost(Durations.FIVE_SECONDS).untilAsserted(() -> {
//            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
//            SearchResponse response = esClient.search(searchRequest, DEFAULT);
//            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
//        });
//    }
    @ToString(callSuper = true)
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    static class AccountCreatedEvent extends AbstractEvent {
        public String accountCode;
        public UUID accountUuid;
        public String accountName;
        public String accountStatus;
        public PhoneNumber phone;
        public String email;
        public Instant lastModifiedDate;
        public UUID lastModifiedBy;
        public Instant createdDate;
        public UUID createdBy;
        public UUID addressUuid;
        public String leadSource;
        public String leadChannel;
        public String description;
        public String code;
        public String bankAccountNumber;
        public String bankAccountHolder;
        public String bankCode;
        public String bankName;
        public UUID shippingAddressUuid;
        public UUID attributeSetUuid;
        public Long annualRevenue;
        public String campaignCode;
        public UUID campaignUuid;
        public UUID referredByUuid;
        public String referredByUserCode;
        public UUID primaryContactUuid;
        public String primaryContactCode;
    }
}
