package vn.onehousing.salespipeline.usecase.worker.consumer.eventview;

import lombok.Data;
import net.vinid.core.event.AbstractEvent;

@Data
public class OpportunityStoppedEvent extends AbstractEvent {
    private String leadUuid;
    private String opportunityUuid;
}
