package vn.onehousing.salespipeline.usecase.worker.consumer.agenthistories;

import net.vinid.core.event.EventPublisher;
import org.assertj.core.api.Assertions;
import org.awaitility.Awaitility;
import org.awaitility.Durations;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import vn.onehousing.salepipeline.usecase.business.usecase.insertopportunityactivityviewuc.IInsertOpportunityActivityViewUc;
import vn.onehousing.salespipeline.usecase.test.IntegrationTest;
import vn.onehousing.salespipeline.usecase.worker.consumer.eventview.ActivityTaskCreatedEvent;

import java.io.IOException;
import java.time.Instant;
import java.util.UUID;

import static org.elasticsearch.client.RequestOptions.DEFAULT;

public class TaskHandlerIT extends IntegrationTest {
    public static final String index = "db-onehousing-salespipeline-usecases-agent-histories-test";

    @Autowired
    private EventPublisher eventPublisher;

    @Autowired
    private RestHighLevelClient esClient;

    @MockBean
    private IInsertOpportunityActivityViewUc viewUc;

    @Before
    public void cleanDb() throws IOException {
        final DeleteByQueryRequest deleteRequest = new DeleteByQueryRequest();
        deleteRequest.setQuery(QueryBuilders.matchAllQuery());
        deleteRequest.indices(index);
        deleteRequest.setRefresh(true);
        esClient.deleteByQuery(deleteRequest, DEFAULT);
    }

    @Test
    public void CreateActivity_index_into_elasticsearch() throws IOException {
        // given
        String leadUUid = UUID.randomUUID().toString();

        final ActivityTaskCreatedEvent event1 = new ActivityTaskCreatedEvent();
        event1.setTaskUuid("94601c89-eadc-43cb-bf18-1aa12a032021");
        event1.setEntityUuid(leadUUid);
        event1.setEntityType("LEAD");
        event1.setName("name1");
        event1.setStatus("status1");
        event1.setDescription("Khách hàng tạo task");
        event1.setTaskTypeCode("sms");
        event1.setTaskTypeName("Nhắn tin");
        event1.setLastModifiedDate(Instant.now());

        // verify
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("lead_uuid", leadUUid);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);
        //
        eventPublisher.publishEvent(event1);
        // then
        Awaitility.await().atMost(Durations.FIVE_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
        });

    }
}
