package vn.onehousing.salespipeline.usecase.worker.consumer.agenthistories;

import net.vinid.core.event.EventPublisher;
import org.assertj.core.api.Assertions;
import org.awaitility.Awaitility;
import org.awaitility.Durations;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.hisActivity.RecordSource;
import vn.onehousing.salepipeline.usecase.common.shared.constants.NoteParentType;
import vn.onehousing.salepipeline.usecase.business.usecase.insertopportunityactivityviewuc.IInsertOpportunityActivityViewUc;
import vn.onehousing.salespipeline.usecase.test.IntegrationTest;
import vn.onehousing.salespipeline.usecase.worker.consumer.eventview.NoteCreatedEvent;

import java.io.IOException;
import java.time.Instant;
import java.util.UUID;

import static org.elasticsearch.client.RequestOptions.DEFAULT;

public class NotesHandlerIT extends IntegrationTest {
    public static final String index = "db-onehousing-salespipeline-usecases-agent-histories-test";

    @Autowired
    private EventPublisher eventPublisher;

    @Autowired
    private RestHighLevelClient esClient;

    @MockBean
    private IInsertOpportunityActivityViewUc viewUc;

    @Before
    public void cleanDb() throws IOException {
        final DeleteByQueryRequest deleteRequest = new DeleteByQueryRequest();
        deleteRequest.setQuery(QueryBuilders.matchAllQuery());
        deleteRequest.indices(index);
        deleteRequest.setRefresh(true);
        esClient.deleteByQuery(deleteRequest, DEFAULT);
    }

    @Test
    public void CreateNote_AGENT_index_into_Agent_Histories_Elasticsearch() throws IOException {
        final NoteCreatedEvent eventNote = new NoteCreatedEvent();
        String leadUUid = UUID.randomUUID().toString();
        eventNote.setNoteUuid("94601c89-eadc-43cb-bf18-1aa12a032022");
        eventNote.setParentUuid(leadUUid);
        eventNote.setTitle("name1");
        eventNote.setContent("note note");
        eventNote.setNoteCode("email");
        eventNote.setParentType(NoteParentType.LEAD);
        eventNote.setOwnerType(RecordSource.AGENT);
        eventNote.setOwnerUuid(UUID.randomUUID().toString());
        eventNote.setLastModifiedDate(Instant.now());

        // verify
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("lead_uuid", leadUUid);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);

        // when
        eventPublisher.publishEvent(eventNote);
        // then
        Awaitility.await().atMost(Durations.FIVE_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
        });
    }

    @Test
    public void CreateNote_CUSTOMER_index_into_Agent_Histories_Elasticsearch() throws IOException {
        final NoteCreatedEvent eventNote = new NoteCreatedEvent();
        String leadUUid = UUID.randomUUID().toString();
        eventNote.setNoteUuid("94601c89-eadc-43cb-bf18-1aa12a032022");
        eventNote.setParentUuid(leadUUid);
        eventNote.setTitle("name1");
        eventNote.setContent("note note");
        eventNote.setNoteCode("email");
        eventNote.setParentType(NoteParentType.LEAD);
        eventNote.setOwnerType(RecordSource.CUSTOMER);
        eventNote.setOwnerUuid(UUID.randomUUID().toString());
        eventNote.setLastModifiedDate(Instant.now());

        // verify
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("lead_uuid", leadUUid);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);

        // when
        eventPublisher.publishEvent(eventNote);
        // then
        Awaitility.await().atMost(Durations.FIVE_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);

        });
    }

    @Test
    public void CreateNote_CC_index_into_Agent_Histories_Elasticsearch() throws IOException {
        final NoteCreatedEvent eventNote = new NoteCreatedEvent();
        String leadUUid = UUID.randomUUID().toString();
        eventNote.setNoteUuid("94601c89-eadc-43cb-bf18-1aa12a032022");
        eventNote.setParentUuid(leadUUid);
        eventNote.setTitle("name1");
        eventNote.setContent("note note");
        eventNote.setNoteCode("email");
        eventNote.setParentType(NoteParentType.LEAD);
        eventNote.setOwnerType(RecordSource.CONTACT_CENTER);
        eventNote.setOwnerUuid(UUID.randomUUID().toString());
        eventNote.setLastModifiedDate(Instant.now());

        // verify
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("lead_uuid", leadUUid);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);

        // when
        eventPublisher.publishEvent(eventNote);
        // then
        Awaitility.await().atMost(Durations.FIVE_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
        });
    }
}
