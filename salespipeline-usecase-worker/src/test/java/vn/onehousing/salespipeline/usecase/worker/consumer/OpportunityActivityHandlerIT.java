package vn.onehousing.salespipeline.usecase.worker.consumer;

import lombok.*;
import net.vinid.core.event.AbstractEvent;
import net.vinid.core.event.EventPublisher;
import org.assertj.core.api.Assertions;
import org.awaitility.Awaitility;
import org.awaitility.Durations;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;
import vn.onehousing.salespipeline.usecase.test.IntegrationTest;

import java.io.IOException;
import java.time.Instant;

import static org.elasticsearch.client.RequestOptions.DEFAULT;

public class OpportunityActivityHandlerIT extends IntegrationTest {
    public static final String index = "db-onehousing-salespipeline-usecases-lead-opp-activities-test";

    @Autowired
    private EventPublisher eventPublisher;

    @Autowired
    private RestHighLevelClient esClient;

    @Before
    public void cleanDb() throws IOException {
        final DeleteByQueryRequest deleteRequest = new DeleteByQueryRequest();
        deleteRequest.setQuery(QueryBuilders.matchAllQuery());
        deleteRequest.indices(index);
        deleteRequest.setRefresh(true);
        esClient.deleteByQuery(deleteRequest, DEFAULT);
    }

    @Test
    public void CreateOpportunity_index_into_elasticsearch() throws IOException {
        // given
        final LeadCreatedEvent event = new LeadCreatedEvent();
        event.setLeadUuid("94601c89-eadc-43cb-bf18-1aa12a032020");
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("123456789");
        event.setPhoneNumber(phoneNumber);
        event.setLeadCode("123321123");
        event.setLeadName("abc");
        event.setLastModifiedDate(Instant.now());

        final OpportunityCreatedEvent event1 = new OpportunityCreatedEvent();
        event1.setOpportunityUuid("94601c89-eadc-43cb-bf18-1aa12a032021");
        event1.setLeadUuid("94601c89-eadc-43cb-bf18-1aa12a032020");
        event1.setLastModifiedDate(Instant.now());

        // verify
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("phone_number", "123456789");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(matchQueryBuilder);

        SearchRequest searchRequest = new SearchRequest();

        searchRequest.indices(index).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        Assertions.assertThat(searchResponse.getHits().getHits()).hasSize(0);

        // when
        eventPublisher.publishEvent(event);
        Awaitility.await().atMost(Durations.FIVE_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(1);
        });
        Awaitility.await().atMost(Durations.TWO_SECONDS);
        eventPublisher.publishEvent(event1);
        // then
        Awaitility.await().atMost(Durations.FIVE_SECONDS).untilAsserted(() -> {
            esClient.indices().refresh(new RefreshRequest(index), DEFAULT);
            SearchResponse response = esClient.search(searchRequest, DEFAULT);
            Assertions.assertThat(response.getHits().getHits()).hasSize(2);
        });
    }
    @ToString(callSuper = true)
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    static class LeadCreatedEvent extends AbstractEvent {
        String leadId;
        String leadUuid;
        String leadName;
        PhoneNumber phoneNumber;
        String leadCode;
        String assignedUserUuid;
        String assignedUserCode;
        String campaignUuid;
        String campaignCode;
        String status;
        String email;
        Boolean isConverted;
        Instant lastModifiedDate;
    }
    @ToString(callSuper = true)
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    static class OpportunityCreatedEvent extends AbstractEvent {
        private Long opportunityId;
        private String opportunityUuid;
        private String opportunityCode;
        private String name;
        private String status;
        private String leadUuid;
        private String description;
        private String accountUuid;
        private String accountCode;
        private String productCode;
        private String productUuid;
        private String referredByUserUuid;
        private String referredByUserCode;
        private String assignedUserUuid;
        private String assignedUserCode;
        private String note;
        private String campaignUuid;
        private String campaignCode;
        private Instant closedDate;
        private String attributeSetValue;
        private String leadSource;
        private Boolean isSignContract;
        Instant lastModifiedDate;
        String createdBy;
        String lastModifiedBy;
        Instant createdDate;
         PhoneNumber phoneNumber;
    }

}
