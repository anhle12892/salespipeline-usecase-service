package vn.onehousing.salespipeline.usecase.worker.consumer.eventview;

import lombok.*;
import net.vinid.core.event.AbstractEvent;

import java.time.Instant;

@ToString(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NoteCreatedEvent extends AbstractEvent {
    String id;
    String noteUuid;
    String noteCode;
    String ownerUuid;
    String ownerType;
    String ownerEmail;
    String parentUuid;
    String parentType;
    String title;
    String content;
    Instant createdDate;
    String createdBy;
    Instant lastModifiedDate;
    String lastModifiedBy;
}
