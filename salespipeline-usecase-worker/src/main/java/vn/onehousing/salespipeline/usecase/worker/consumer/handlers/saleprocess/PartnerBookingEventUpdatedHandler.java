package vn.onehousing.salespipeline.usecase.worker.consumer.handlers.saleprocess;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.mapper.SaleProcessStageMapper;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.processsalestage.IProcessSaleProcessStage;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.AgentSaleProcessBookingStatus;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessTaskWorkflow;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;
import vn.onehousing.salepipeline.usecase.query.event.PartnerBookingOpportunityUpdatedEvent;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@Slf4j
@RequiredArgsConstructor
@Component
public class PartnerBookingEventUpdatedHandler {
    private final ObjectMapper objectMapper;

    private final SaleProcessStageMapper mapper;
    private final IProcessSaleProcessStage action;

    @KafkaListener(
        topics = "#{SalesPipelineUsecasePartnerBookingOpportunityEventUpdated_SaleProcess_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecasePartnerBookingOpportunityEventUpdated_SaleProcess_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecasePartnerBookingOpportunityEventUpdated_SaleProcess_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecasePartnerBookingOpportunityEventUpdated_SaleProcess_Indexing_TopicListener.concurrency}"
    )
    public void processPartnerBookingEventUpdated(final EventMessage message) throws IOException {
        log.info("[PartnerBookingEventUpdatedHandler -> processPartnerBookingEventUpdated] receive message: {}",
            objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        PartnerBookingOpportunityUpdatedEvent data = this.objectMapper.convertValue(payload,
            PartnerBookingOpportunityUpdatedEvent.class);

        Optional<String> agentStatus = Optional.empty();
        if (AgentSaleProcessBookingStatus.AGENT_SENT_BOOKING_TO_PARTNER.equals(data.getPartnerBookingStatus())) {
            agentStatus = Optional.of(SaleProcessTaskWorkflow.AGENT_SENT_BOOKING_TO_PARTNER);
        } else if (AgentSaleProcessBookingStatus.AGENT_PARTNER_UPDATED_BOOKING_STATUS_TO_SUCCESS.equals(data.getPartnerBookingStatus())) {
            agentStatus = Optional.of(SaleProcessTaskWorkflow.PARTNER_UPDATED_BOOKING_STATUS_TO_SUCCESS);
        } else {
            log.error("[PartnerBookingEventUpdatedHandler -> processPartnerBookingEventUpdated] Sale process stage of {}",
                data.getOpportunityUuid() + ": invalid booking status.");
        }

        if (agentStatus.isPresent()) {
            var inputActivity = new SaleProcessInputActivity();
            inputActivity.setLeadUuid(data.getLeadUuid());
            inputActivity.getActivities().add(agentStatus.get());

            action.process(inputActivity);
        }
    }
}
