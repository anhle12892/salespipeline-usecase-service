package vn.onehousing.salespipeline.usecase.worker.consumer.handlers.saleprocess;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.processsalestage.IProcessSaleProcessStage;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessTaskActivity;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;
import vn.onehousing.salepipeline.usecase.query.event.CreateTaskActivityView;
import vn.onehousing.salepipeline.usecase.query.event.UpdateTaskActivityView;

import java.io.IOException;
import java.util.Map;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@Slf4j
@RequiredArgsConstructor
@Component
public class TaskActivitySaleProcessStageEventHandler {
    private final ObjectMapper objectMapper;
    private final IProcessSaleProcessStage action;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseActivityTaskCreatedEvent_SaleProcess_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseActivityTaskCreatedEvent_SaleProcess_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseActivityTaskCreatedEvent_SaleProcess_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseActivityTaskCreatedEvent_SaleProcess_Indexing_TopicListener.concurrency}"
    )
    public void createTaskActivitySaleStage(final EventMessage message) throws IOException {
        log.info("[TaskActivitySaleProcessStageEventHandler -> createTaskActivitySaleStage] receive message: {}",
            objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        final CreateTaskActivityView data = this.objectMapper.convertValue(payload, CreateTaskActivityView.class);

        if (SaleProcessTaskActivity.COMPLETED.equals(data.getStatus())) {
            if (StringUtils.hasText(data.getCode())) {
                var inputActivity = new SaleProcessInputActivity();
                inputActivity.setLeadUuid(data.getEntityUuid());
                inputActivity.getActivities().add(data.getCode());
                action.process(inputActivity);
            } else {
                log.error("[TaskActivitySaleProcessStageEventHandler -> createTaskActivitySaleStage] Sale process stage of {}",
                    data.getEntityUuid() + ": invalid task code.");
            }
        }
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseActivityTaskUpdatedEvent_SaleProcess_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseActivityTaskUpdatedEvent_SaleProcess_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseActivityTaskUpdatedEvent_SaleProcess_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseActivityTaskUpdatedEvent_SaleProcess_Indexing_TopicListener.concurrency}"
    )
    public void updateTaskActivitySaleStage(final EventMessage message) throws IOException {
        log.info("[TaskActivitySaleProcessStageEventHandler -> updateTaskActivitySaleStage] receive message: {}",
            objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        final UpdateTaskActivityView data = this.objectMapper.convertValue(payload, UpdateTaskActivityView.class);

        if (SaleProcessTaskActivity.COMPLETED.equals(data.getStatus())) {
            if (StringUtils.hasText(data.getCode())) {
                var inputActivity = new SaleProcessInputActivity();
                inputActivity.setLeadUuid(data.getEntityUuid());
                inputActivity.getActivities().add(data.getCode());
                action.process(inputActivity);
            } else {
                log.error("[TaskActivitySaleProcessStageEventHandler -> updateTaskActivitySaleStage] Sale process stage of {}",
                    data.getEntityUuid() + ": invalid task code.");
            }
        }
    }
}
