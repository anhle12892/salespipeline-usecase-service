package vn.onehousing.salespipeline.usecase.worker.consumer.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;

/**
 * @author ltnguyen on 23/09/2019
 */
@ConfigurationProperties(prefix = "onehousing.kafka.consumer.topics")
public class TopicListenersProperties extends HashMap<String, TopicListenersProperties.TopicListenerProperties> {

    @Data
    public static class TopicListenerProperties {
        private String topic;
        private boolean enable;
        private String groupId;
        private Integer concurrency = 1;
    }
}
