package vn.onehousing.salespipeline.usecase.worker.consumer.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersProperties.TopicListenerProperties;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static org.springframework.beans.factory.support.BeanDefinitionBuilder.genericBeanDefinition;

@Slf4j
@Configuration(TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@EnableConfigurationProperties({TopicListenersProperties.class})
public class TopicListenersRegistrar {

    public static final String TOPIC_LISTENERS_REGISTRAR_BEAN_NAME = "TOPIC_LISTENERS_REGISTRAR";

    @Inject
    private TopicListenersProperties topicListenersProperties;

    @Inject
    private DefaultListableBeanFactory beanFactory;

    @PostConstruct
    public void init() throws BeansException {
        registerTopicListenerBean();
    }

    /**
     * For each keys in consumer topics, create a bean for its properties. This bean will be used in the corresponding @{@link org.springframework.kafka.annotation.KafkaListener}
     */
    private void registerTopicListenerBean() {
        topicListenersProperties.forEach((key, topicListenerProperties) -> {
            String beanName = key + "_TopicListener";
            log.info("Registering dynamic bean {}", beanName);
            beanFactory.registerBeanDefinition(beanName,
                    genericBeanDefinition(TopicListenerProperties.class, () -> topicListenerProperties).getBeanDefinition());
        });
    }

}
