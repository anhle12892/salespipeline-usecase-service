package vn.onehousing.salespipeline.usecase.worker.consumer.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;
import vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbysalespipelineuc.IIndexAggregateSalespipelineViewBySalespipelineUc;

import java.io.IOException;
import java.util.Map;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@Slf4j
@Component
@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@AllArgsConstructor
public class IndexAggregateSalespipelineCreatedEventHander {

    private final ObjectMapper objectMapper;
    private final IIndexAggregateSalespipelineViewBySalespipelineUc syncAggregateSalespipelineViewBySalespipelineUc;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseAggregateSalespipelineCreatedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseAggregateSalespipelineCreatedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseAggregateSalespipelineCreatedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseAggregateSalespipelineCreatedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void handler(final EventMessage message) throws IOException {
        log.info("[IndexAggregateSalespipelineCreatedEventHander] receive message {}", objectMapper.writeValueAsString(message));

        final Map<String, Object> payload = message.getPayload();
        var salespipeline = this.objectMapper.convertValue(payload, SalesPipeline.class);

        syncAggregateSalespipelineViewBySalespipelineUc.process(salespipeline);
    }
}
