package vn.onehousing.salespipeline.usecase.worker.consumer.handlers.saleprocess;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.context.ThreadContextHolder;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.usecase.insertagenthistoryviewuc.IInsertAgentHistoryViewUc;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.processsalestage.IProcessSaleProcessStage;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEntityType;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessTaskWorkflow;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;
import vn.onehousing.salepipeline.usecase.query.event.AgentOpportunityUpdatedEvent;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.AgentHistoryView;

import java.io.IOException;
import java.time.Instant;
import java.util.Map;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@Slf4j
@RequiredArgsConstructor
@Component
public class AgentOpportunitySaleProcessStageEventHandler {
    private final ObjectMapper objectMapper;
    private final IProcessSaleProcessStage action;
    private final IInsertAgentHistoryViewUc insertAgentHistoryViewUc;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseAgentOpportunityUpdatedEvent_SaleProcess_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseAgentOpportunityUpdatedEvent_SaleProcess_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseAgentOpportunityUpdatedEvent_SaleProcess_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseAgentOpportunityUpdatedEvent_SaleProcess_Indexing_TopicListener.concurrency}"
    )
    public void processAgentOpportunityUpdatedForSaleStage(final EventMessage message) throws IOException {
        log.info("[AgentOpportunitySaleProcessStageEventHandler -> processAgentOpportunityUpdatedForSaleStage] receive message: {}",
            objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        AgentOpportunityUpdatedEvent data = this.objectMapper.convertValue(payload, AgentOpportunityUpdatedEvent.class);
        if (data == null) {
            log.error("[AgentOpportunitySaleProcessStageEventHandler -> processAgentOpportunityUpdatedForSaleStage] Sale process stage.");
            return;
        }

        var inputActivity = new SaleProcessInputActivity();
        inputActivity.setLeadUuid(data.getLeadUuid());
        inputActivity.getActivities().add(SaleProcessTaskWorkflow.UPDATED_ANY_INFORMATION_OF_OPPORTUNITY);
        action.process(inputActivity);
        //save Agent History,
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        AgentHistoryView view = AgentHistoryView.builder()
            .historyUuid(message.getId())
            .actorUuid(actorUuid)
            .leadUuid(data.getLeadUuid())
            .entityType(ActEntityType.LEAD)
            .entityUuid(message.getId())
            .parentType(ActEntityType.LEAD)
            .parentUuid(data.getLeadUuid())
            .createdTime(Instant.now())
            .timestamp(Instant.now().toEpochMilli())
            .build();
        insertAgentHistoryViewUc.process(view
                .mappingEnumValue(AgentOpportunityUpdatedEvent.class, payload),
            getTimestamp(payload, message));
    }

    private long getTimestamp(Map<String, Object> payload, EventMessage message) {
        Object timestamp = payload.get("timestamp");
        if (timestamp instanceof Long) {
            return (long) timestamp;
        }
        log.error("Use message.timestamp instead of payload [{}]", timestamp);
        return message.getTimestamp();
    }
}
