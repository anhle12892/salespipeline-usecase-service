package vn.onehousing.salespipeline.usecase.worker.consumer.handlers.saleprocess;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.processsalestage.IProcessSaleProcessStage;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessTaskWorkflow;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;
import vn.onehousing.salepipeline.usecase.query.event.AssignedActivityView;
import vn.onehousing.salepipeline.usecase.query.event.AssignmentContext;
import vn.onehousing.salepipeline.usecase.query.event.AssignmentContextView;

import java.io.IOException;
import java.util.Map;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@Slf4j
@RequiredArgsConstructor
@Component
public class AssigmentSaleProcessStageEventHandler {
    private final ObjectMapper objectMapper;
    private final IProcessSaleProcessStage action;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseAssignmentCreatedEvent_SaleProcess_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseAssignmentCreatedEvent_SaleProcess_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseAssignmentCreatedEvent_SaleProcess_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseAssignmentCreatedEvent_SaleProcess_Indexing_TopicListener.concurrency}"
    )
    public void processAssignmentEventCreated(final EventMessage message) throws IOException {
        log.info("[AssigmentSaleProcessStageEventHandler -> processAssignmentEventCreated] receive message: {}",
            objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        final AssignedActivityView data = this.objectMapper.convertValue(payload, AssignedActivityView.class);
        AssignmentContext assignmentContext = data.getAssignmentContext();
        final AssignmentContextView contextView = this.objectMapper.convertValue(assignmentContext.getData(), AssignmentContextView.class);

        if (contextView == null) {
            log.error("[AssigmentSaleProcessStageEventHandler -> processAssignmentEventCreated] Sale process stage.");
            return;
        }

        var inputActivity = new SaleProcessInputActivity();
        inputActivity.setLeadUuid(contextView.getLead_uuid());
        inputActivity.getActivities().add(SaleProcessTaskWorkflow.ASSIGNED);
        action.process(inputActivity);
    }
}
