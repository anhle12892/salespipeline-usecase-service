package vn.onehousing.salespipeline.usecase.worker.consumer.handlers.opportunityhistory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.context.ThreadContextHolder;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.query.event.*;
import vn.onehousing.salepipeline.usecase.common.shared.constants.RevokedType;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper.IEventActivityMapper;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper.IHistoryActivityMapper;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;
import vn.onehousing.salepipeline.usecase.business.usecase.insertagenthistoryviewuc.IInsertAgentHistoryViewUc;
import vn.onehousing.salepipeline.usecase.business.usecase.insertopportunityactivityviewuc.IInsertOpportunityActivityViewUc;

import java.util.Map;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@Slf4j
@RequiredArgsConstructor
@Component
public class LeadHandler {

    private final ObjectMapper objectMapper;

    private final IEventActivityMapper activityMapper;

    private final IHistoryActivityMapper historyMapper;

    private final IInsertAgentHistoryViewUc insertAgentHistoryViewUc;
    private final IInsertOpportunityActivityViewUc insertOpportunityActivityViewUc;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseLeadCreatedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseLeadCreatedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseLeadCreatedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseLeadCreatedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void indexElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("[LeadHandler] Receiving Insert request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        CreateLeadActivityView event = this.objectMapper.convertValue(payload, CreateLeadActivityView.class);
        final OpportunityActivityView view = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());
        insertOpportunityActivityViewUc.process(view, getTimestamp(payload, message));
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseLeadUpdatedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseLeadUpdatedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseLeadUpdatedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseLeadUpdatedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void updateElasticsearch(final EventMessage message) throws JsonProcessingException {

        log.info("[LeadHandler] Receiving Update request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        UpdateLeadActivityView event = this.objectMapper.convertValue(payload, UpdateLeadActivityView.class);

        if (Boolean.TRUE.equals(event.getIsFromSys())) {
            log.info("[Migration Es][Lead Update Event] migrate lead uuid {} ", event.getLeadUuid());
            return;
        }

        final OpportunityActivityView view = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());
        insertOpportunityActivityViewUc.process(view, getTimestamp(payload, message));

        //update account from lead
//        try {
//            if (StringUtils.isNotEmpty(event.getAccountUuid())) {
//                Optional<OpportunityActivityView> resultAccount = updateSalesPipelineViewToSearchStorage.searchByChildData(List.of("data.account_uuid", "eventType"), List.of(event.getAccountUuid(), ActEventConst.CREATED_ACCOUNT));
//                if (resultAccount.isPresent()) {
//                    resultAccount.get().setEntityUuid(event.getLeadUuid());
//                    insertOpportunityActivityViewUc.process(resultAccount.get(), getTimestamp(payload, message));
//                }
//            }
//
//        }catch (IOException e){
//            e.printStackTrace();
//        }
        //update contact from lead
//        try {
//            if (StringUtils.isNotEmpty(event.getContactUuid())) {
//                Optional<OpportunityActivityView> resultContact = updateSalesPipelineViewToSearchStorage.searchByChildData(List.of("data.contact_uuid", "eventType"), List.of(event.getContactUuid(), ActEventConst.CREATED_CONTACT));
//                if (resultContact.isPresent()) {
//                    resultContact.get().setEntityUuid(event.getLeadUuid());
//                    insertOpportunityActivityViewUc.process(resultContact.get(), getTimestamp(payload, message));
//                }
//            }
//        }catch (IOException e){
//            e.printStackTrace();
//        }
        //update opp from lead
//        try {
//            if (StringUtils.isNotEmpty(event.getOpportunityUuid())) {
//                Optional<OpportunityActivityView> resultOpportunity = updateSalesPipelineViewToSearchStorage.searchByChildData(List.of("data.opportunity_uuid", "eventType"), List.of(event.getOpportunityUuid(), ActEventConst.CREATED_OPPORTUNITY));
//                if (resultOpportunity.isPresent()) {
//                    resultOpportunity.get().setEntityUuid(event.getLeadUuid());
//                    resultOpportunity.get().setPhoneNumber(event.getPhoneNumber().getNumber());
//                    insertOpportunityActivityViewUc.process(resultOpportunity.get(), getTimestamp(payload, message));
//                }
//            }
//        }catch (IOException e){
//            e.printStackTrace();
//        }
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseLeadRevokedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseLeadRevokedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseLeadRevokedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseLeadRevokedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void leadRevokedElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("Receiving Insert request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        LeadRevokedEventView event = this.objectMapper.convertValue(payload, LeadRevokedEventView.class);
        final OpportunityActivityView opportunityActivityView = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());
        insertOpportunityActivityViewUc.process(opportunityActivityView, getTimestamp(payload, message));
        //Don't save revoked event from contact center
        if (!RevokedType.OPP_OVERDUE_CC_QUALIFIED.equals(event.getRevokedType())
            && !RevokedType.OPP_OVERDUE_CC_UNQUALIFIED.equals(event.getRevokedType())) {
            insertAgentHistoryViewUc.process(historyMapper
                .from(opportunityActivityView, message.getId())
                .mappingEnumValue(LeadRevokedEventView.class, payload), getTimestamp(payload, message));
        }
    }

    @KafkaListener(
        topics = "#{OpportunityBookingStatusUpdatedEvent_TopicListener.topic}",
        autoStartup = "#{OpportunityBookingStatusUpdatedEvent_TopicListener.enable}",
        groupId = "#{OpportunityBookingStatusUpdatedEvent_TopicListener.groupId}",
        concurrency = "#{OpportunityBookingStatusUpdatedEvent_TopicListener.concurrency}"
    )
    public void leadBookingElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("LeadHandler.leadBookingElasticsearch - Receiving Insert request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        OpportunityBookingStatusUpdatedEvent event = this.objectMapper.convertValue(payload, OpportunityBookingStatusUpdatedEvent.class);
        final OpportunityActivityView opportunityActivityView = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());
        insertOpportunityActivityViewUc.process(opportunityActivityView, getTimestamp(payload, message));

        insertAgentHistoryViewUc.process(
            historyMapper.from(
                opportunityActivityView,
                message.getId()
            ).mappingEnumValue(
                OpportunityBookingStatusUpdatedEvent.class,
                payload
            ),
            getTimestamp(payload, message)
        );
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseLeadStoppedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseLeadStoppedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseLeadStoppedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseLeadStoppedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void leadStoppedElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("Receiving Insert request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        LeadStoppedEventView event = this.objectMapper.convertValue(payload, LeadStoppedEventView.class);
        final OpportunityActivityView opportunityActivityView = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());

        insertOpportunityActivityViewUc.process(opportunityActivityView, getTimestamp(payload, message));

        insertAgentHistoryViewUc.process(historyMapper
            .from(opportunityActivityView, message.getId())
            .mappingEnumValue(LeadStoppedEventView.class, payload), getTimestamp(payload, message));
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseLeadDuplicateEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseLeadDuplicateEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseLeadDuplicateEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseLeadDuplicateEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void leadDuplicatedElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("LeadHandler.leadDuplicatedElasticsearch - Receiving Insert request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        LeadDuplicateMessageView event = this.objectMapper.convertValue(payload, LeadDuplicateMessageView.class);
        final OpportunityActivityView opportunityActivityView = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());

        insertOpportunityActivityViewUc.process(opportunityActivityView, getTimestamp(payload, message));
    }

    private long getTimestamp(Map<String, Object> payload, EventMessage message) {
        Object timestamp = payload.get("timestamp");
        if (timestamp instanceof Long) {
            return (long) timestamp;
        }
        log.error("Use message.timestamp instead of payload [{}]", timestamp);
        return message.getTimestamp();
    }
}
