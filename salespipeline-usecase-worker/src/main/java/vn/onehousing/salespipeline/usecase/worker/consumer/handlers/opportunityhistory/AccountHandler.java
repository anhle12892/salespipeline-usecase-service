package vn.onehousing.salespipeline.usecase.worker.consumer.handlers.opportunityhistory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.context.ThreadContextHolder;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.query.event.CreateAccountActivityView;
import vn.onehousing.salepipeline.usecase.query.event.UpdateAccountActivityView;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper.IEventActivityMapper;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;
import vn.onehousing.salepipeline.usecase.business.usecase.insertagenthistoryviewuc.IInsertAgentHistoryViewUc;
import vn.onehousing.salepipeline.usecase.business.usecase.insertopportunityactivityviewuc.IInsertOpportunityActivityViewUc;

import java.util.Map;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@Slf4j
@RequiredArgsConstructor
@Component
public class AccountHandler {

    private final ObjectMapper objectMapper;

    private final IEventActivityMapper activityMapper;

    private final IInsertAgentHistoryViewUc insertAgentHistoryViewUc;
    private final IInsertOpportunityActivityViewUc insertOpportunityActivityViewUc;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseAccountCreatedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseAccountCreatedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseAccountCreatedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseAccountCreatedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void indexElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("[AccountHandler] Receiving Insert request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        CreateAccountActivityView event = this.objectMapper.convertValue(payload, CreateAccountActivityView.class);
        OpportunityActivityView view = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());
        insertOpportunityActivityViewUc.process(view, getTimestamp(payload, message));
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseAccountUpdatedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseAccountUpdatedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseAccountUpdatedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseAccountUpdatedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void updateElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("[AccountHandler] Receiving Update request [{}]", objectMapper.writeValueAsString(message));

        boolean isMigrate = false;
        if (message.getAdditionalData().containsKey("is_migration")) {
            isMigrate = Boolean.parseBoolean(message.getAdditionalData().get("is_migration").toString());
        }

        if (isMigrate) {
            return;
        }

        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        UpdateAccountActivityView event = this.objectMapper.convertValue(payload, UpdateAccountActivityView.class);
        OpportunityActivityView view = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());
        insertOpportunityActivityViewUc.process(view, getTimestamp(payload, message));
    }


    private long getTimestamp(Map<String, Object> payload, EventMessage message) {
        Object timestamp = payload.get("timestamp");
        if (timestamp instanceof Long) {
            return (long) timestamp;
        }
        log.error("Use message.timestamp instead of payload [{}]", timestamp);
        return message.getTimestamp();
    }
}
