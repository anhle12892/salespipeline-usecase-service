package vn.onehousing.salespipeline.usecase.worker.consumer.handlers.opportunityhistory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.context.ThreadContextHolder;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.query.event.CreateContactActivityView;
import vn.onehousing.salepipeline.usecase.query.event.UpdateContactActivityView;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper.IEventActivityMapper;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;
import vn.onehousing.salepipeline.usecase.business.usecase.insertagenthistoryviewuc.IInsertAgentHistoryViewUc;
import vn.onehousing.salepipeline.usecase.business.usecase.insertopportunityactivityviewuc.IInsertOpportunityActivityViewUc;

import java.util.Map;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@Slf4j
@RequiredArgsConstructor
@Component
public class ContactHandler {

    private final ObjectMapper objectMapper;

    private final IEventActivityMapper activityMapper;

    private final IInsertAgentHistoryViewUc insertAgentHistoryViewUc;
    private final IInsertOpportunityActivityViewUc insertOpportunityActivityViewUc;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseContactCreatedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseContactCreatedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseContactCreatedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseContactCreatedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void indexElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("[ContactHandler] Receiving Insert request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        CreateContactActivityView event = this.objectMapper.convertValue(payload, CreateContactActivityView.class);
        OpportunityActivityView view = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());

        insertOpportunityActivityViewUc.process(view, getTimestamp(payload, message));
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseContactUpdatedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseContactUpdatedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseContactUpdatedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseContactUpdatedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void updateElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("[ContactHandler] Receiving Update request [{}]", objectMapper.writeValueAsString(message));

        boolean isMigrate = false;
        if (message.getAdditionalData().containsKey("is_migration")) {
            isMigrate = Boolean.parseBoolean(message.getAdditionalData().get("is_migration").toString());
        }

        if (isMigrate) {
            return;
        }

        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        UpdateContactActivityView event = this.objectMapper.convertValue(payload, UpdateContactActivityView.class);
        OpportunityActivityView view = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());

        insertOpportunityActivityViewUc.process(view, getTimestamp(payload, message));
    }

    private long getTimestamp(Map<String, Object> payload, EventMessage message) {
        Object timestamp = payload.get("timestamp");
        if (timestamp instanceof Long) {
            return (long) timestamp;
        }
        log.error("Use message.timestamp instead of payload [{}]", timestamp);
        return message.getTimestamp();
    }
}
