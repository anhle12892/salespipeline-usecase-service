package vn.onehousing.salespipeline.usecase.worker.consumer.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.common.shared.model.Contact;
import vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbycontactuc.IndexAggregateSalespipelineByContactUc;

import java.io.IOException;
import java.util.Map;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@Slf4j
@Component
@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@RequiredArgsConstructor
public class IndexAggregateContactUpdateEventHander {

    private final ObjectMapper objectMapper;
    private final IndexAggregateSalespipelineByContactUc indexAggregateSalespipelineByContactUc;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseAggregateContactUpdateEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseAggregateContactUpdateEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseAggregateContactUpdateEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseAggregateContactUpdateEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void handler(final EventMessage message) throws IOException {
        log.info("[IndexAggregateContactUpdateEventHander] receive message {}", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        var contact = this.objectMapper.convertValue(payload, Contact.class);
        this.indexAggregateSalespipelineByContactUc.process(contact);
    }

}
