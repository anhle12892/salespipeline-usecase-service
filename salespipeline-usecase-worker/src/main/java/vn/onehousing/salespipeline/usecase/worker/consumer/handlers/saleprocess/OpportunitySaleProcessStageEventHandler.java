package vn.onehousing.salespipeline.usecase.worker.consumer.handlers.saleprocess;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.processsalestage.IProcessSaleProcessStage;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessTaskWorkflow;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeValue;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;
import vn.onehousing.salepipeline.usecase.query.event.CreateOpportunityActivityView;
import vn.onehousing.salepipeline.usecase.query.event.UpdateOpportunityActivityView;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@Slf4j
@RequiredArgsConstructor
@Component
public class OpportunitySaleProcessStageEventHandler {
    private final ObjectMapper objectMapper;
    private final IProcessSaleProcessStage action;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseOpportunityCreatedEvent_SaleProcess_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseOpportunityCreatedEvent_SaleProcess_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseOpportunityCreatedEvent_SaleProcess_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseOpportunityUpdatedEvent_SaleProcess_Indexing_TopicListener.concurrency}"
    )
    public void processOpportunityCreatedForSaleStage(final EventMessage message) throws IOException {
        log.info("[OpportunitySaleProcessStageEventHandler -> processOpportunityCreatedForSaleStage] receive message: {}",
            objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        CreateOpportunityActivityView data = this.objectMapper.convertValue(payload, CreateOpportunityActivityView.class);
        if (data == null) {
            log.error("[OpportunitySaleProcessStageEventHandler -> processOpportunityCreatedForSaleStage] Sale process stage.");
            return;
        }

        if (validateAttributes(data.getAttributes())) {
            var inputActivity = new SaleProcessInputActivity();
            inputActivity.setLeadUuid(data.getLeadUuid());
            inputActivity.getActivities().add(SaleProcessTaskWorkflow.UPDATED_ANY_INFORMATION_OF_OPPORTUNITY);
            inputActivity.getActivities().addAll(
                data.getAttributes()
                    .stream()
                    .map(AttributeValue::getName)
                    .collect(Collectors.toList())
            );

            action.process(inputActivity);
        } else {
            log.error("[OpportunitySaleProcessStageEventHandler -> processOpportunityUpdatedForSaleStage] Sale process stage of {}",
                data.getLeadUuid() + ": invalid lead attributes.");
        }
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseOpportunityUpdatedEvent_SaleProcess_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseOpportunityUpdatedEvent_SaleProcess_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseOpportunityUpdatedEvent_SaleProcess_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseOpportunityUpdatedEvent_SaleProcess_Indexing_TopicListener.concurrency}"
    )
    public void processOpportunityUpdatedForSaleStage(final EventMessage message) throws IOException {
        log.info("[OpportunitySaleProcessStageEventHandler -> processOpportunityUpdatedForSaleStage] receive message: {}",
            objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        UpdateOpportunityActivityView data = this.objectMapper.convertValue(payload, UpdateOpportunityActivityView.class);
        if (data == null) {
            log.error("[OpportunitySaleProcessStageEventHandler -> processOpportunityCreatedForSaleStage] Sale process stage.");
            return;
        }

        if (validateAttributes(data.getAttributes())) {
            var inputActivity = new SaleProcessInputActivity();
            inputActivity.setLeadUuid(data.getLeadUuid());
            inputActivity.getActivities().addAll(
                data.getAttributes()
                    .stream()
                    .map(AttributeValue::getName)
                    .collect(Collectors.toList())
            );
            action.process(inputActivity);
        } else {
            log.error("[OpportunitySaleProcessStageEventHandler -> processOpportunityUpdatedForSaleStage] Sale process stage of {}",
                data.getLeadUuid() + ": invalid lead attributes.");
        }
    }

    private boolean validateAttributes(List<AttributeValue> attributes) {
        if (attributes == null) {
            return false;
        }

        var results = attributes
            .stream()
            .map(AttributeValue::getName)
            .filter(name -> StringUtils.hasText(name))
            .collect(Collectors.toList());

        return results.size() == attributes.size();
    }
}
