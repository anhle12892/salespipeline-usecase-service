package vn.onehousing.salespipeline.usecase.worker.consumer.handlers.saleprocess;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.processsalestage.IProcessSaleProcessStage;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeValue;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;
import vn.onehousing.salepipeline.usecase.query.event.CreateLeadActivityView;
import vn.onehousing.salepipeline.usecase.query.event.UpdateLeadActivityView;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@Slf4j
@RequiredArgsConstructor
@Component
public class LeadSaleProcessStageEventHandler {
    private final ObjectMapper objectMapper;
    private final IProcessSaleProcessStage action;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseLeadCreatedEvent_SaleProcess_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseLeadCreatedEvent_SaleProcess_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseLeadCreatedEvent_SaleProcess_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseLeadCreatedEvent_SaleProcess_Indexing_TopicListener.concurrency}"
    )
    public void processLeadEventCreated(final EventMessage message) throws IOException {
        log.info("[LeadSaleProcessStageEventHandler -> processLeadEventCreated] receive message: {}",
            objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        CreateLeadActivityView event = this.objectMapper.convertValue(payload, CreateLeadActivityView.class);

        if (event == null) {
            log.error("[LeadSaleProcessStageEventHandler -> processLeadEventCreated] Sale process stage.");
            return;
        }

        if (validateAttributes(event.getAttributes())) {
            var inputActivity = new SaleProcessInputActivity();
            inputActivity.setLeadUuid(event.getLeadUuid());
            inputActivity.getActivities().addAll(
                event.getAttributes()
                    .stream()
                    .map(AttributeValue::getName)
                    .collect(Collectors.toList())
            );

            action.process(inputActivity);
        } else {
            log.error("[LeadSaleProcessStageEventHandler -> processLeadEventCreated] Sale process stage of {}",
                event.getLeadUuid() + ": invalid lead attributes.");
        }
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseLeadUpdatedEvent_SaleProcess_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseLeadUpdatedEvent_SaleProcess_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseLeadUpdatedEvent_SaleProcess_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseLeadUpdatedEvent_SaleProcess_Indexing_TopicListener.concurrency}"
    )
    public void processLeadEventUpdated(final EventMessage message) throws IOException {
        log.info("[LeadSaleProcessStageEventHandler -> processLeadEventUpdated] receive message: {}",
            objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        UpdateLeadActivityView event = this.objectMapper.convertValue(payload, UpdateLeadActivityView.class);

        if (event == null) {
            log.error("[LeadSaleProcessStageEventHandler -> processLeadEventUpdated] Sale process stage.");
            return;
        }

        if (validateAttributes(event.getAttributes())) {
            var inputActivity = new SaleProcessInputActivity();
            inputActivity.setLeadUuid(event.getLeadUuid());
            inputActivity.getActivities().addAll(
                event.getAttributes()
                    .stream()
                    .map(AttributeValue::getName)
                    .collect(Collectors.toList())
            );

            action.process(inputActivity);
        } else {
            log.error("[OpportunitySaleProcessStageEventHandler -> processOpportunityUpdatedForSaleStage] Sale process stage of {}",
                event.getLeadUuid() + ": invalid lead attributes.");
        }
    }

    private boolean validateAttributes(List<AttributeValue> attributes) {
        if (attributes == null) {
            return false;
        }

        var results = attributes
            .stream()
            .map(AttributeValue::getName)
            .filter(name -> StringUtils.hasText(name))
            .collect(Collectors.toList());

        return results.size() == attributes.size();
    }
}
