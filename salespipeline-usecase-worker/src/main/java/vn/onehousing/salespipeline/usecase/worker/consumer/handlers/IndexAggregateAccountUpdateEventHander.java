package vn.onehousing.salespipeline.usecase.worker.consumer.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.common.shared.model.Account;
import vn.onehousing.salepipeline.usecase.business.usecase.indexaggregatesalespipelineviewbyaccountuc.IndexAggregateSalespipelineByAccountUc;

import java.io.IOException;
import java.util.Map;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@Slf4j
@Component
@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@RequiredArgsConstructor
public class IndexAggregateAccountUpdateEventHander {
    private final ObjectMapper objectMapper;
    private final IndexAggregateSalespipelineByAccountUc indexAggregateSalespipelineByAccountUc;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseAggregateAccountUpdateEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseAggregateAccountUpdateEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseAggregateAccountUpdateEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseAggregateAccountUpdateEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void handler(final EventMessage message) throws IOException {
        log.info("[IndexAggregateAccountUpdateEventHander] receive message {}", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        var account = this.objectMapper.convertValue(payload, Account.class);
        this.indexAggregateSalespipelineByAccountUc.process(account);
    }
}
