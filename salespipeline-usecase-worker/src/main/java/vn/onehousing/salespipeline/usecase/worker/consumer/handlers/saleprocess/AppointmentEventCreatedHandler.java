package vn.onehousing.salespipeline.usecase.worker.consumer.handlers.saleprocess;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.context.ThreadContextHolder;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.business.usecase.insertagenthistoryviewuc.IInsertAgentHistoryViewUc;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.processsalestage.IProcessSaleProcessStage;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEntityType;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessTaskWorkflow;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;
import vn.onehousing.salepipeline.usecase.query.event.AppointmentCreatedEvent;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.AgentHistoryView;

import java.io.IOException;
import java.time.Instant;
import java.util.Map;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@Slf4j
@RequiredArgsConstructor
@Component
public class AppointmentEventCreatedHandler {
    private final ObjectMapper objectMapper;
    private final IProcessSaleProcessStage action;
    private final IInsertAgentHistoryViewUc insertAgentHistoryViewUc;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseAgentAppointmentEventCreated_SaleProcess_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseAgentAppointmentEventCreated_SaleProcess_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseAgentAppointmentEventCreated_SaleProcess_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseAgentAppointmentEventCreated_SaleProcess_Indexing_TopicListener.concurrency}"
    )
    public void processAppointmentEventCreated(final EventMessage message) throws IOException {
        log.info("[AppointmentEventCreatedHandler -> processAppointmentEventCreated] receive message: {}",
            objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        AppointmentCreatedEvent data = this.objectMapper.convertValue(payload, AppointmentCreatedEvent.class);

        var inputActivity = new SaleProcessInputActivity();
        inputActivity.setLeadUuid(data.getLeadUuid());
        inputActivity.getActivities().add(SaleProcessTaskWorkflow.CREATED_AN_APPOINTMENT);

        action.process(inputActivity);

        //save Agent History,
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        AgentHistoryView view = AgentHistoryView.builder()
            .historyUuid(message.getId())
            .actorUuid(actorUuid)
            .leadUuid(data.getLeadUuid())
            .entityType(ActEntityType.APPOINTMENT)
            .entityUuid(message.getId())
            .parentType(ActEntityType.LEAD)
            .parentUuid(data.getLeadUuid())
            .createdTime(Instant.now())
            .timestamp(Instant.now().toEpochMilli())
            .build();
        insertAgentHistoryViewUc.process(view
            .mappingEnumValue(AppointmentCreatedEvent.class, payload), getTimestamp(payload, message));
    }

    private long getTimestamp(Map<String, Object> payload, EventMessage message) {
        Object timestamp = payload.get("timestamp");
        if (timestamp instanceof Long) {
            return (long) timestamp;
        }
        log.error("Use message.timestamp instead of payload [{}]", timestamp);
        return message.getTimestamp();
    }
}
