package vn.onehousing.salespipeline.usecase.worker.consumer.handlers.opportunityhistory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.context.ThreadContextHolder;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.query.event.CreateTaskActivityView;
import vn.onehousing.salepipeline.usecase.query.event.UpdateTaskActivityView;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper.IEventActivityMapper;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper.IHistoryActivityMapper;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;
import vn.onehousing.salepipeline.usecase.business.usecase.insertagenthistoryviewuc.IInsertAgentHistoryViewUc;
import vn.onehousing.salepipeline.usecase.business.usecase.insertopportunityactivityviewuc.IInsertOpportunityActivityViewUc;

import java.util.Map;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@Slf4j
@RequiredArgsConstructor
@Component
public class TaskActivityHandler {

    private final ObjectMapper objectMapper;

    private final IEventActivityMapper activityMapper;

    private final IHistoryActivityMapper historyMapper;

    private final IInsertAgentHistoryViewUc insertAgentHistoryViewUc;
    private final IInsertOpportunityActivityViewUc insertOpportunityActivityViewUc;


    @KafkaListener(
        topics = "#{SalesPipelineUsecaseActivityTaskCreatedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseActivityTaskCreatedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseActivityTaskCreatedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseActivityTaskCreatedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void indexElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("[TaskActivityHandler] Receiving request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        final CreateTaskActivityView data = this.objectMapper.convertValue(payload, CreateTaskActivityView.class);
        final OpportunityActivityView opportunityActivityView = activityMapper
            .fromCreateTask(data, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());
        insertOpportunityActivityViewUc.process(
            opportunityActivityView, getTimestamp(payload, message)
        );
        //save Agent History
        insertAgentHistoryViewUc.process(historyMapper
            .from(opportunityActivityView, opportunityActivityView.getEntityUuid())
            .mappingEnumValue(CreateTaskActivityView.class, payload), getTimestamp(payload, message));
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseActivityTaskUpdatedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseActivityTaskUpdatedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseActivityTaskUpdatedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseActivityTaskUpdatedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void updateElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("[TaskActivityHandler] Receiving request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        final UpdateTaskActivityView data = this.objectMapper.convertValue(payload, UpdateTaskActivityView.class);
        final OpportunityActivityView opportunityActivityView = activityMapper
            .fromUpdateTask(data, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());
        insertOpportunityActivityViewUc.process(opportunityActivityView, getTimestamp(payload, message));
        //save Agent History
        insertAgentHistoryViewUc.process(historyMapper
            .from(opportunityActivityView, opportunityActivityView.getEntityUuid())
            .mappingEnumValue(UpdateTaskActivityView.class, payload), getTimestamp(payload, message));
    }

    private long getTimestamp(Map<String, Object> payload, EventMessage message) {
        Object timestamp = payload.get("timestamp");
        if (timestamp instanceof Long) {
            return (long) timestamp;
        }
        log.error("Use message.timestamp instead of payload [{}]", timestamp);
        return message.getTimestamp();
    }
}
