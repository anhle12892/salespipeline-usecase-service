package vn.onehousing.salespipeline.usecase.worker.consumer.handlers.opportunityhistory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.context.ThreadContextHolder;
import net.vinid.core.event.EventMessage;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.query.event.CreateOpportunityActivityView;
import vn.onehousing.salepipeline.usecase.query.event.OpportunityStoppedEventView;
import vn.onehousing.salepipeline.usecase.query.event.OpportunitySucceedEventView;
import vn.onehousing.salepipeline.usecase.query.event.UpdateOpportunityActivityView;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper.IEventActivityMapper;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper.IHistoryActivityMapper;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;
import vn.onehousing.salepipeline.usecase.business.usecase.insertagenthistoryviewuc.IInsertAgentHistoryViewUc;
import vn.onehousing.salepipeline.usecase.business.usecase.insertopportunityactivityviewuc.IInsertOpportunityActivityViewUc;

import java.util.Map;

import static vn.onehousing.salespipeline.usecase.worker.consumer.config.TopicListenersRegistrar.TOPIC_LISTENERS_REGISTRAR_BEAN_NAME;

@DependsOn(TOPIC_LISTENERS_REGISTRAR_BEAN_NAME)
@Slf4j
@RequiredArgsConstructor
@Component
public class OpportunityHandler {

    private final ObjectMapper objectMapper;

    private final IEventActivityMapper activityMapper;

    private final IHistoryActivityMapper historyMapper;

    private final IInsertAgentHistoryViewUc insertAgentHistoryViewUc;
    private final IInsertOpportunityActivityViewUc insertOpportunityActivityViewUc;

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseOpportunityCreatedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseOpportunityCreatedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseOpportunityCreatedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseOpportunityCreatedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void indexElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("[OpportunityHandler] Receiving Insert request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        CreateOpportunityActivityView event = this.objectMapper.convertValue(payload, CreateOpportunityActivityView.class);
        final OpportunityActivityView opportunityActivityView = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());
        insertOpportunityActivityViewUc.process(opportunityActivityView, getTimestamp(payload, message));
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseOpportunityUpdatedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseOpportunityUpdatedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseOpportunityUpdatedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseOpportunityUpdatedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void updateElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("[OpportunityHandler] Receiving Update request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        UpdateOpportunityActivityView event = this.objectMapper.convertValue(payload, UpdateOpportunityActivityView.class);
        if (Boolean.TRUE.equals(event.getIsFromSys())) {
            log.info("[Migration Es][Opportunity Update Event] migrate lead uuid {} ", event.getLeadUuid());
            return;
        }
        final OpportunityActivityView opportunityActivityView = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());

        insertOpportunityActivityViewUc.process(opportunityActivityView, getTimestamp(payload, message));
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseOppStoppedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseOppStoppedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseOppStoppedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseOppStoppedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void opportunityStoppedElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("[OpportunityHandler] Receiving Update request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        OpportunityStoppedEventView event = this.objectMapper.convertValue(payload, OpportunityStoppedEventView.class);
        final OpportunityActivityView opportunityActivityView = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());

        insertOpportunityActivityViewUc.process(opportunityActivityView, getTimestamp(payload, message));
        //save Agent History
        insertAgentHistoryViewUc.process(historyMapper
            .from(opportunityActivityView, message.getId())
            .mappingEnumValue(OpportunityStoppedEventView.class, payload), getTimestamp(payload, message));
    }

    @KafkaListener(
        topics = "#{SalesPipelineUsecaseOppSucceedEvent_Elasticsearch_Indexing_TopicListener.topic}",
        autoStartup = "#{SalesPipelineUsecaseOppSucceedEvent_Elasticsearch_Indexing_TopicListener.enable}",
        groupId = "#{SalesPipelineUsecaseOppSucceedEvent_Elasticsearch_Indexing_TopicListener.groupId}",
        concurrency = "#{SalesPipelineUsecaseOppSucceedEvent_Elasticsearch_Indexing_TopicListener.concurrency}"
    )
    public void opportunitySucceedElasticsearch(final EventMessage message) throws JsonProcessingException {
        log.info("[OpportunityHandler] Receiving Update request [{}]", objectMapper.writeValueAsString(message));
        final Map<String, Object> payload = message.getPayload();
        String actorUuid = ThreadContextHolder.getTracingContext().getUserId();
        OpportunitySucceedEventView event = this.objectMapper.convertValue(payload, OpportunitySucceedEventView.class);
        final OpportunityActivityView opportunityActivityView = activityMapper
            .fromEvent(event, payload, message.getId())
            .mappingActor(actorUuid, message.getEvent());

        insertOpportunityActivityViewUc.process(opportunityActivityView, getTimestamp(payload, message));
        //save Agent History
        insertAgentHistoryViewUc.process(historyMapper
            .from(opportunityActivityView, message.getId())
            .mappingEnumValue(OpportunitySucceedEventView.class, payload), getTimestamp(payload, message));
    }

    private long getTimestamp(Map<String, Object> payload, EventMessage message) {
        Object timestamp = payload.get("timestamp");
        if (timestamp instanceof Long) {
            return (long) timestamp;
        }
        log.error("Use message.timestamp instead of payload [{}]", timestamp);
        return message.getTimestamp();
    }
}
