package vn.onehousing.salepipeline.usecase.infrastructure.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "spring.redis.lock")
public class RedisDistributedLockServiceConfiguration {
    /* Timeout in seconds */
    private long timeout;
    private int retryTimes;
}
