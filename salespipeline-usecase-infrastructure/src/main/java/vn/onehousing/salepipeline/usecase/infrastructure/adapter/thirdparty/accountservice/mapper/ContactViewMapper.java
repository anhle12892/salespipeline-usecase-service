package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.accountservice.mapper;

import org.mapstruct.Mapper;
import vn.onehousing.salepipeline.usecase.common.shared.mapper.BaseMapper;
import vn.onehousing.salepipeline.usecase.common.shared.model.Contact;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.accountservice.response.ContactViewResponse;

@Mapper(componentModel = "spring", uses = {BaseMapper.class})
public interface ContactViewMapper {

    Contact to(ContactViewResponse contactViewResponse);
}
