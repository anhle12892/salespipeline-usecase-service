package vn.onehousing.salepipeline.usecase.infrastructure.adapter.domain;

import org.springframework.data.domain.Pageable;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.AgentHistoryView;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ISalesPipelineUsecaseActivitesViewRepository {
    AgentHistoryView insert(AgentHistoryView leadView, @Nullable Long timestamp);
    List<OpportunityActivityView> get(String uuid, String phoneNumber) throws IOException;
    Optional<OpportunityActivityView> searchLeadInformation(String leadUuid) throws IOException;
    Optional<OpportunityActivityView> searchByChildData(List<String> fields, List<String> values) throws IOException;
    List<AgentHistoryView> getOpportunityHistories(String uuid, Pageable pageable);

}
