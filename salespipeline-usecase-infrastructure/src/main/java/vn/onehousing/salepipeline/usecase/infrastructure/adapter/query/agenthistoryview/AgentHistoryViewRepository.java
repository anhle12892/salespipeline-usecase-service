package vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.agenthistoryview;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.es.ElasticsearchService;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.AgentHistoryView;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.IAgentHistoryViewRepository;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class AgentHistoryViewRepository implements IAgentHistoryViewRepository {

    private final ElasticsearchService elasticsearchService;
    private final ObjectMapper objectMapper;

    private final String indexHistory = "db-onehousing-salespipeline-usecases-agent-histories";

    @Override
    public AgentHistoryView insert(AgentHistoryView activitiesView, @Nullable Long timestamp) {
        elasticsearchService.insert(
                indexHistory,
                activitiesView.getHistoryUuid(),
                timestamp == null ? activitiesView.getCreatedTime().toEpochMilli() : timestamp,
                activitiesView
        );
        return activitiesView;
    }

    @Override
    public List<AgentHistoryView> getOpportunityHistories(String uuid, Pageable pageable) {
        List<AgentHistoryView> result = new ArrayList<>();
        try {
            List<Object> data = elasticsearchService.searchPagingHistories(indexHistory, uuid, pageable.getPageNumber(), pageable.getPageSize());
            data.forEach(r -> {
                try {
                    result.add(objectMapper.readValue(r.toString(), AgentHistoryView.class));
                } catch (JsonProcessingException e) {
                    log.error("Cannot parse JSON from Elastic Search",e);
                }
            });
            return  result;
        } catch (JsonProcessingException e) {
            log.error("Cannot parse JSON from Elastic Search",e);
        } catch (Exception e){
            log.error("Error while query data: {}", e.getMessage());
        }
        return Collections.emptyList();
    }
}
