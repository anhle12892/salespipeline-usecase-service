package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.finance.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.Financial;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.request.EmiInformationDto;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;

import java.math.BigDecimal;
import java.time.Instant;


@Data
@Builder
public class FinancialResponse extends BaseResponse<Financial> {

}
