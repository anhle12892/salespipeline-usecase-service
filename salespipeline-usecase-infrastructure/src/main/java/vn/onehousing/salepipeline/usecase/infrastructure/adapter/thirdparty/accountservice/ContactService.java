package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.accountservice;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.IContactService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.ContactSearchRequest;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.ContactUpdateRequest;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.response.CreateAccountResp;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.response.CreateContactResp;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.HousingErrors;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.HousingException;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.RequestThirdpartyException;
import vn.onehousing.salepipeline.usecase.common.shared.model.Contact;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.accountservice.mapper.ContactViewMapper;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ContactService implements IContactService {

    private final WebClient webClient;

    private final ContactViewMapper viewMapper;

    public ContactService(@Lazy WebClient accountServiceClient, ContactViewMapper viewMapper) {
        this.webClient = accountServiceClient;
        this.viewMapper = viewMapper;
    }

    @Override
    public Contact updateContact(ContactUpdateRequest request) {
        Mono<CreateContactResp> contactResponse =
                this.webClient.put().uri("/v1/contacts/" + request.getContactUuid())
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(Mono.just(request), ContactUpdateRequest.class)
                        .exchangeToMono(response -> response.bodyToMono(CreateContactResp.class))
                        .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        CreateContactResp resp = contactResponse.block();
        if (BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            return resp.getData();
        } else {
            throw new RequestThirdpartyException("ContactService","updateContact",resp.getMeta().toString());
        }
    }

    @Override
    public Contact search(ContactSearchRequest contactSearchRequest) {

        EsRequest request  = buildES(contactSearchRequest);
        Mono<ContactSearchResp> contactResponse =
                this.webClient.post().uri("/v1/contacts/_search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(Mono.just(request), EsRequest.class)
                        .exchangeToMono(response -> response.bodyToMono(ContactSearchResp.class))
                        .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        ContactSearchResp resp = contactResponse.block();

        if (resp == null || resp.hits == null || resp.hits.hits == null || resp.hits.hits.isEmpty()) {
            return null;
        }
        List<Contact> contacts = resp.hits.hits.stream().map(a -> viewMapper.to(a._source)).collect(Collectors.toList());
        return contacts.get(0);
    }

    @Override
    public Contact getByContactUuid(String contactUuid) {
        Mono<CreateContactResp> contactResponse =
            this.webClient.get().uri("/v1/contacts/" + contactUuid)
                .exchangeToMono(response -> response.bodyToMono(CreateContactResp.class))
                .switchIfEmpty(Mono.error(new HousingException(HousingErrors.CREATE_ACCOUNT_ERROR)));
        CreateContactResp resp = contactResponse.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            throw new RequestThirdpartyException("AccountService", "getAccountByUuid", resp.getMeta().toString());
        }
        return resp.getData();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    private static   class QueryBuilder {
        private Match multiMatch;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    private static class EsRequest {
        QueryBuilder query;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    private static class Match {
        private String query;
        private List<String> fields;
    }

    private EsRequest buildES(ContactSearchRequest contactSearchRequest) {
        Match match = new Match();
        match.setQuery(contactSearchRequest.getPhoneNumber());
        match.setFields(List.of("phone_number.number"));
        QueryBuilder queryBuilder = new QueryBuilder();
        queryBuilder.setMultiMatch(match);
        return EsRequest.builder().query(queryBuilder).build();
    }
}
