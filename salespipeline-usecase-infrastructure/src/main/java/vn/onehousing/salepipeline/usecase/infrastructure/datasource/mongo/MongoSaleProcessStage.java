package vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document("sale_process_stage")
public class MongoSaleProcessStage implements Persistable<ObjectId> {
    ObjectId id;

    @Indexed(unique = true)
    @Field("lead_uuid")
    String leadUuid;

    @Field("sale_stage")
    String saleStage;

    @Transient
    boolean isNew = false;
}
