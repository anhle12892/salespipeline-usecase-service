//package vn.onehousing.salepipeline.usecase.infrastructure.adapter.query;
//
//import vn.onehousing.salepipeline.usecase.infrastructure.adapter.domain.model.leadactivity.OpportunityActivityView;
//import vn.onehousing.salepipeline.usecase.infrastructure.adapter.domain.model.leadactivity.AgentHistoryView;
//
//import javax.annotation.Nullable;
//import java.io.IOException;
//import java.util.List;
//import java.util.Optional;
//
//public interface IUpdateSalesPipelineUsecaseViewToSearchStorage {
//    void process(OpportunityActivityView lead, @Nullable Long timestamp);
//
//    void createHistory(AgentHistoryView lead, @Nullable Long timestamp);
//
////    Optional<OpportunityActivityView> searchLeadInformation(String leadUuid) throws IOException;
////
////    Optional<OpportunityActivityView> searchByChildData(List<String> fields, List<String> values) throws IOException;
//
//}
