package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.finance;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.Financial;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.IFinanceService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.request.FinancialCreateReq;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.finance.response.FinancialResponse;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.finance.response.FinancialsResponse;

import java.util.List;

@Slf4j
@Component
public class FinanceService implements IFinanceService {
    private final WebClient webClient;

    public FinanceService(@Lazy WebClient financialServiceClient) {
        this.webClient = financialServiceClient;
    }

    @Override
    public List<Financial> search(String leadUuid) {

        Mono<FinancialsResponse> monoResp = this.webClient
            .get()
            .uri(uriBuilder ->
                uriBuilder
                    .path("/v1/financials/search")
                    .queryParam("lead_uuids", List.of(leadUuid))
                    .build()
            ).exchangeToMono(response -> response.bodyToMono(FinancialsResponse.class));

        FinancialsResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            log.error("[CrmFinanceSearch] lead uuid {} occurs exception {} ", leadUuid, resp.getMeta().getCode());
            return null;
        }
        return resp.getData();
    }

    @Override
    public Financial createFinancial(FinancialCreateReq request) {
        log.debug("Creating financial with request {}", request);
        Mono<FinancialResponse> monoResp = this.webClient
                .post()
                .uri("/v1/financials")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(request), FinancialCreateReq.class)
                .exchangeToMono(response -> response.bodyToMono(FinancialResponse.class));

        FinancialResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            log.error("[CrmFinanceCreate] lead uuid {} occurs exception {} ", request, resp.getMeta().getCode());
            return null;
        }
        return resp.getData();
    }

}
