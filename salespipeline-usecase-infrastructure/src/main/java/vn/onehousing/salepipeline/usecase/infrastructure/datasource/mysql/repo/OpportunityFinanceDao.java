package vn.onehousing.salepipeline.usecase.infrastructure.datasource.mysql.repo;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.note.OpportunityFinanceRepository;

import javax.sql.DataSource;
import java.util.List;

@Repository
@AllArgsConstructor
@Transactional
@Slf4j
public class OpportunityFinanceDao extends JdbcDaoSupport implements OpportunityFinanceRepository {

    @Autowired
    public OpportunityFinanceDao(DataSource dataSource) {
        this.setDataSource(dataSource);
    }


    @Override
    public List<Note> getFinanceByLeadUuidAndResourceType(String leadUuid, String resourceType) {
        String sql = "select o.core_uuid as parentUuid ," +
            "       f.note as content, " +
            "       f.transfer_type as parentType ," +
            "       a.uuid as ownerUuid, " +
            "       a.email " +
            "       from opportunities o " +
            "           inner join opportunity_finances f on o.id = f.opportunity_id " +
            "           inner join agents a on f.agent_id = a.id " +
            "where o.core_uuid = '" + leadUuid + "'" ;

        if (resourceType != null) {
            sql = sql +  " and f.transfer_type = '" + resourceType + "'";
        }
        List<Note> notes = this.getJdbcTemplate().query(sql,
            (rs, rowNum) -> new Note(null, null, null,
                rs.getString("ownerUuid"),
                rs.getString("parentUuid"),
                rs.getString("parentType"),
                "Bạn đã tạo yêu cầu tư vấn tài chính",
                rs.getString("content"),
                "AGENT",
                null,
                null,
                null,
                null,
                rs.getString("email")
            ));

        return notes;
    }
}
