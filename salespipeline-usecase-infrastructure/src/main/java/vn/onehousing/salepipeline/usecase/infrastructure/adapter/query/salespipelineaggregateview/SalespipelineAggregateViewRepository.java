package vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.salespipelineaggregateview;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.es.ElasticsearchService;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.ISalespipelineAggregateViewRepository;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.SalespipelineAggregateView;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Repository("aggregateLeadAndOpportunityViewRepository")
public class SalespipelineAggregateViewRepository implements ISalespipelineAggregateViewRepository {

    private final ElasticsearchService es;
    private final String index = "db-onehousing-salespipeline-usecases-salespipeline-aggregates";
    private final ObjectMapper objectMapper;

    @Override
    public SalespipelineAggregateView insert(SalespipelineAggregateView view) {
        view.setDocumentVersion(Instant.now());
        es.insert(index, view.getLeadUuid(), null, view);
        return view;
    }

    @Override
    public void delete(SalespipelineAggregateView leadView) {
        es.delete(index, leadView.getLeadUuid());
    }

    @Override
    public List<SalespipelineAggregateView> searchByAccountUuid(String accountUuid) throws IOException {
        var result = new ArrayList<SalespipelineAggregateView>();
        List<Object> data = es.searchByAccountUuid(
            index,
            accountUuid
        );
        data.forEach(r -> {
            try {
                result.add(objectMapper.readValue(r.toString(), SalespipelineAggregateView.class));
            } catch (JsonProcessingException e) {
                log.error("Cannot parse JSON from Elastic Search", e);
            }
        });
        return result;
    }

    @Override
    public List<SalespipelineAggregateView> searchByContactUuid(String contactUuid) throws IOException {
        var result = new ArrayList<SalespipelineAggregateView>();
        List<Object> data = es.searchByContactUuid(
            index,
            contactUuid
        );
        data.forEach(r -> {
            try {
                result.add(objectMapper.readValue(r.toString(), SalespipelineAggregateView.class));
            } catch (JsonProcessingException e) {
                log.error("Cannot parse JSON from Elastic Search", e);
            }
        });
        return result;
    }

    @Override
    public SalespipelineAggregateView getByUuid(String leadUuid) throws IOException {
        var data = es.getById(index, leadUuid, SalespipelineAggregateView.class);
        return data;
    }

    @Override
    public Optional<SalespipelineAggregateView> searchByChildData(List<String> fields, List<String> values) throws IOException {
        return Optional.empty();
    }

    @Override
    public List<SalespipelineAggregateView> findAllAssignedLeadByLastModifiedDate(Instant lastModifiedDate) {
        var result = new ArrayList<SalespipelineAggregateView>();
        try {
            List<Object> data = es.findAllAssignedLeadByLastModifiedDate(index, lastModifiedDate);
            data.forEach(r -> {
                try {
                    result.add(objectMapper.readValue(r.toString(), SalespipelineAggregateView.class));
                } catch (JsonProcessingException e) {
                    log.error("Cannot parse JSON from Elastic Search", e);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            log.error("query leads from aggregate occurs {} ", e.getMessage());
        }
        return result;
    }
}
