package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.salespipelineservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.ISalesPipelineService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.request.CreateSalesPipelineReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.request.UpdateSalesPipelineReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.salespipelineservice.response.SalesPipelineServiceResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.RequestThirdpartyException;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;

import java.time.Duration;

@Slf4j
@Component
public class SalesPipelineService implements ISalesPipelineService {

    private final WebClient webClient;

    public SalesPipelineService(@Lazy WebClient salesPipelineServiceClient) {
        this.webClient = salesPipelineServiceClient;
    }

    @Override
    public SalesPipeline update(String salesPipelineUuid, UpdateSalesPipelineReq req) {
        Mono<SalesPipelineServiceResponse> monoResp =
            this.webClient.put().uri("/v1/salespipelines/" + salesPipelineUuid)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(req), UpdateSalesPipelineReq.class)
                .exchangeToMono(response -> response.bodyToMono(SalesPipelineServiceResponse.class))
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        SalesPipelineServiceResponse resp = monoResp.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            throw new RequestThirdpartyException(
                "SalesPipelineService",
                "update",
                String.format(
                    "update salepipeline but get error: [%s]",
                    resp.getMeta()
                )
            );
        }
        return resp.getData();
    }

    @Override
    public SalesPipeline create(CreateSalesPipelineReq req) {
        Mono<CreateSalesPipelineBaseResp> monoResp = this.webClient
            .post()
            .uri("/v1/salespipelines")
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), CreateSalesPipelineReq.class)
            .exchangeToMono(response -> response.bodyToMono(CreateSalesPipelineBaseResp.class))
            .doOnError(
                error -> {
                    log.error("SalesPipelineService - create - error: [{}]", error.getStackTrace().toString());
                }
            );

        CreateSalesPipelineBaseResp resp = monoResp.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            throw new RequestThirdpartyException(
                "SalesPipelineService",
                "create",
                String.format(
                    "create salepipeline but get error: [%s]",
                    resp.getMeta()
                )
            );
        }
        return resp.getData();
    }

    @Override
    public SalesPipeline getByLeadUuid(String leadUuid) {
        Mono<SalesPipelineServiceResponse> monoResp = this.webClient
            .get()
            .uri("/v1/salespipelines?lead_uuid=" + leadUuid)
            .exchangeToMono(response -> {
                return response.bodyToMono(SalesPipelineServiceResponse.class);
            });
        SalesPipelineServiceResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdpartyException("SalesPipelineService","getByLeadUuid",resp.getMeta().toString());
        return resp.getData();
    }

}
