package vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.onehousing.salepipeline.usecase.business.mapper.PhoneMapper;
import vn.onehousing.salepipeline.usecase.business.mapper.UUIDMapper;
import vn.onehousing.salepipeline.usecase.common.shared.model.OpportunityHistory;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.AgentHistoryView;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;

@Mapper(componentModel = "spring", uses = {UUIDMapper.class, PhoneMapper.class})
public interface IHistoryActivityMapper {
    OpportunityHistory toHistory(AgentHistoryView view);

    @Mapping(source = "view.createdDate", target = "createdTime")
    AgentHistoryView from(OpportunityActivityView view, String historyUuid);
}
