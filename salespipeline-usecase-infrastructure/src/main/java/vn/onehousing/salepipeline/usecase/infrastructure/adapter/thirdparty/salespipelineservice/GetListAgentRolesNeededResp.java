package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.salespipelineservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.response.DecisionResult;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetListAgentRolesNeededResp {
    DecisionResult agentRoles;
}
