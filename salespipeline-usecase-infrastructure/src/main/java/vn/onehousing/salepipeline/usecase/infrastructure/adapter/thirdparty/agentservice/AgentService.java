package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.agentservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salepipeline.usecase.business.thirdparty.agentservice.IAgentService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.agentservice.response.AgentResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.RequestThirdpartyException;
import vn.onehousing.salepipeline.usecase.common.shared.model.AgentDto;

import java.time.Duration;

@Slf4j
@Component
public class AgentService implements IAgentService {
    private final WebClient webClient;

    public AgentService(@Lazy WebClient agentServiceClient) {
        this.webClient = agentServiceClient;
    }

    @Override
    public AgentDto getAgent(String uuid) {
        Mono<AgentResponse> monoResp =
                this.webClient
                        .get()
                        .uri("/v1/agents/" + uuid)
                        .exchangeToMono(response -> response.bodyToMono(AgentResponse.class))
                        .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        AgentResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            throw new RequestThirdpartyException(
                    "AgentResponse",
                    "Get",
                    String.format(
                            "get Agent information but get error: [%s]", resp.getMeta()
                    )
            );
        }
        return resp.getData();
    }
}
