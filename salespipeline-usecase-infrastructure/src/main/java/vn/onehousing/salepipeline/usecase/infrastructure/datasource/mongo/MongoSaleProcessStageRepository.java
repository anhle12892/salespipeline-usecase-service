package vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MongoSaleProcessStageRepository extends MongoRepository<MongoSaleProcessStage, String> {
    MongoSaleProcessStage getByLeadUuid(String leadUuid);
}
