package vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo.migration;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document("sale_process_migration_history")
public class MongoSaleProcessMigrationHistory implements Persistable<ObjectId> {
    ObjectId id;

    @Field("source")
    String source;

    @Field("lead_uuid")
    String leadUuid;

    @Field("opportunity_id")
    String opportunityId;

    @Field("type")
    String type;

    @Field("stage")
    String stage;  // input - process - output

    @Field("data")
    String data;

    @Field("migration_status")
    String migrationStatus;

    @Field("message")
    String message;

    @Transient
    boolean isNew = false;
}
