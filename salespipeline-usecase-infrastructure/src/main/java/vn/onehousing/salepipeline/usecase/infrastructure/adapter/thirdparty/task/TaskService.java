package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salepipeline.usecase.business.thirdparty.task.CreateTaskReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.task.ITaskService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.task.TaskResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.RequestThirdpartyException;
import vn.onehousing.salepipeline.usecase.common.shared.model.Task;

import java.time.Duration;
import java.util.Objects;

@Slf4j
@Component
public class TaskService implements ITaskService {
    private final WebClient webClient;

    public TaskService(@Lazy WebClient activityServiceClient) {
        this.webClient = activityServiceClient;
    }

    @Override
    public Task createTask(CreateTaskReq req) {
        Mono<TaskResponse> monoResp = this.webClient
            .post()
            .uri("/v1/tasks")
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), CreateTaskReq.class)
            .exchangeToMono(response -> response.bodyToMono(TaskResponse.class))
            .doOnError(
                error -> {
                    log.error("TaskService - createTask [{}] - exception occured", req, error);
                }
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
        ;

        TaskResponse resp = monoResp.block();
        if (Objects.isNull(resp)
            || !BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            throw new RequestThirdpartyException("TaskService", "createTask", resp.getMeta().toString());
        }
        return resp.getData();
    }

    @Override
    public Task updateTask(String taskUuid, CreateTaskReq req) {

        Mono<TaskResponse> monoResp = this.webClient
            .put()
            .uri("/v1/tasks/" + taskUuid)
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), CreateTaskReq.class)
            .exchangeToMono(response -> response.bodyToMono(TaskResponse.class))
            .doOnError(
                error -> {
                    log.error("TaskService - updateTask [{}] - exception occured", req, error);
                }
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        TaskResponse resp = monoResp.block();
        if (Objects.isNull(resp)
            || !BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            throw new RequestThirdpartyException("TaskService", "updateTask", resp.getMeta().toString());
        }
        return resp.getData();
    }
}
