package vn.onehousing.salepipeline.usecase.infrastructure.usecase.saleprocess.query;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.utils.CommonUtils;
import vn.onehousing.salepipeline.usecase.common.shared.constants.RedisLockName;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.ISaleProcessActivityRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessActivity;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper.SaleProcessStageModelMapper;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo.MongoSaleProcessActivity;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo.MongoSaleProcessActivityRepository;
import vn.onehousing.salepipeline.usecase.business.thirdparty.distributedlockservice.IDistributedLockService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Repository
@AllArgsConstructor
public class SaleProcessActivityRepository implements ISaleProcessActivityRepository {
    private final MongoSaleProcessActivityRepository repository;
    private final SaleProcessStageModelMapper mapper;

    private final IDistributedLockService lockService;

    @Override
    public Optional<SaleProcessActivity> insert(SaleProcessActivity data) {
        try {
            repository.save(mapper.from(data));
            return Optional.of(data);
        } catch (Exception exception) {
            log.error("[SaleProcessActivityRepository -> insert] lead_uuid: {}, {}",
                    data.getLeadUuid(),
                    exception.getMessage());
        }

        return Optional.empty();
    }

    @Override
    public Optional<SaleProcessActivity> update(SaleProcessActivity data) {
        try {
            MongoSaleProcessActivity target = repository.getByLeadUuid(data.getLeadUuid());
            if (target == null) {
                return insert(data);
            } else {
                target.getActivities().addAll(data.getActivities());
                Set<String> targetSet = new HashSet<>(target.getActivities());

                target.setActivities(new ArrayList<>(targetSet));

                repository.save(target);
                return Optional.of(mapper.from(target));
            }
        } catch (Exception exception) {
            log.error("[SaleProcessActivityRepository -> update] lead_uuid: {}, {}",
                    data.getLeadUuid(),
                    exception.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public Optional<SaleProcessActivity> get(String leadUuid) {
        try {
            return Optional.of(mapper.from(repository.getByLeadUuid(leadUuid)));
        } catch (Exception exception) {
            log.error("[SaleProcessActivityRepository -> get] lead_uuid: {}, {}",
                    leadUuid,
                    exception.getMessage());
        }

        return Optional.empty();
    }

    @Override
    public List<SaleProcessActivity> getAll() {
        var results = new ArrayList<SaleProcessActivity>();
        List<MongoSaleProcessActivity> saleProcessActivities = repository.findAll();
        if (saleProcessActivities != null && !saleProcessActivities.isEmpty()) {
            saleProcessActivities.forEach(item -> {
                results.add(mapper.from(item));
            });
        }

        return results;
    }

    @Override
    public boolean getLockForUpdate(String leadUuid) {
        String key = CommonUtils.buildRedisLockKey(RedisLockName.SALESPIPELINE_USECASE_SALE_PROCESS, leadUuid);
        return lockService.getLock(key, leadUuid);
    }

    @Override
    public void releaseLockForUpdate(String leadUuid) {
        var key = CommonUtils.buildRedisLockKey(RedisLockName.SALESPIPELINE_USECASE_SALE_PROCESS, leadUuid);
        lockService.releaseLock(key);
    }
}
