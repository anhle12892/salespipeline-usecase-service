//package vn.onehousing.salepipeline.usecase.infrastructure.adapter.query;
//
//import lombok.AllArgsConstructor;
//import org.springframework.stereotype.Service;
//import vn.onehousing.salepipeline.usecase.infrastructure.adapter.domain.model.ISalesPipelineUsecaseActivitesViewRepository;
//import vn.onehousing.salepipeline.usecase.infrastructure.adapter.domain.model.leadactivity.OpportunityActivityView;
//import vn.onehousing.salepipeline.usecase.infrastructure.adapter.domain.model.leadactivity.AgentHistoryView;
//
//import javax.annotation.Nullable;
//import java.io.IOException;
//import java.util.List;
//import java.util.Optional;
//
//@Service
//@AllArgsConstructor
//public class UpdateSalesPipelineUsecaseActivitiesViewToSearchStorage implements IUpdateSalesPipelineUsecaseViewToSearchStorage {
//
//    private final ISalesPipelineUsecaseActivitesViewRepository repository;
//
////    @Override
////    public void process(OpportunityActivityView data, @Nullable Long timestamp) {
////        repository.insert(data, timestamp);
////    }
//
////    @Override
////    public void createHistory(AgentHistoryView data, @Nullable Long timestamp) {
////        repository.insert(data, timestamp);
////    }
//
//    @Override
//    public Optional<OpportunityActivityView> searchLeadInformation(String leadUuid) throws IOException {
//        return repository.searchLeadInformation(leadUuid);
//    }
//
//    @Override
//    public Optional<OpportunityActivityView> searchByChildData(List<String> fields, List<String> values) throws IOException {
//        return repository.searchByChildData(fields, values);
//    }
//}
