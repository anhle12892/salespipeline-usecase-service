package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.salespipelineservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TypeValue {
    String type;
    String value;
}
