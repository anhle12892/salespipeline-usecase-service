package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.accountservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.accountservice.response.ContactViewResponse;

import java.util.List;

@Data
public class ContactSearchResp {
    int took;

    @JsonProperty("timed_out")
    boolean timedOut;

    @JsonProperty("_shards")
    Shards shards;

    Hit hits;
}

@Data
class Hit {

    Total total;

    @JsonProperty("max_score")
    double max_score;

    List<HitSource> hits;
}

@Data
class HitSource {
    @JsonProperty("_index")
    String _index;

    @JsonProperty("_type")
    String _type;

    @JsonProperty("_id")
    String _id;

    @JsonProperty("_score")
    double _score;

    @JsonProperty("_source")
    ContactViewResponse _source;
}

@Data
class Shards {
    int total;
    int successful;
    int skipped;
    int failed;
}

@Data
class Total {
    int value;
    String relation;
}