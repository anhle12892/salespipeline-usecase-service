package vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MongoSaleProcessActivityRepository extends MongoRepository<MongoSaleProcessActivity, String> {
    MongoSaleProcessActivity getByLeadUuid(String leadUuid);
}
