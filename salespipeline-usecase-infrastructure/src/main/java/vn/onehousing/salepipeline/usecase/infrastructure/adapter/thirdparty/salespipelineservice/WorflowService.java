package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.salespipelineservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.IWorkflowService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.request.SaleProcessStageWorkflowRequest;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.response.DecisionResult;
import vn.onehousing.salepipeline.usecase.business.thirdparty.workflow.response.SaleProcessStageWorkflowResponse;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.RequestThirdpartyException;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Component
public class WorflowService implements IWorkflowService {
    private final WebClient webClient;

    public WorflowService(@Lazy WebClient camundaServiceClient) {
        this.webClient = camundaServiceClient;
    }

    @Override
    public Optional<DecisionResult> get(SaleProcessStageWorkflowRequest request) {
        try {
            final WebClient.RequestBodySpec clientRequest = this.webClient
                .post()
                .uri("/decision-definition/key/saleprocess-stage-decision/evaluate")
                .contentType(MediaType.APPLICATION_JSON);
            if (request != null) {
                clientRequest.bodyValue(request);
            }

            Mono<List<SaleProcessStageWorkflowResponse>> monoResp = clientRequest
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<SaleProcessStageWorkflowResponse>>() {
                })
                .doOnError(
                    error -> log.error("CamundaService - Sale process -> get sale stage - error", error)
                )
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

            var response = monoResp.block();
            if (response != null && !response.isEmpty()) {
                return Optional.of(response.get(0).getStage());
            } else {
                return Optional.empty();
            }
        } catch (Exception ex) {
            throw new RequestThirdpartyException(
                "WorflowService",
                "get",
                "CamundaService" + "Sale process -> get sale stage" + ex.getMessage()
            );
        }
    }

    @Override
    public String[] getListAgentRolesNeeded(String leadSource, String leadChannel, String leadStage) {
        try {
            final WebClient.RequestBodySpec clientRequest = this.webClient
                .post()
                .uri("/decision-definition/key/Decision_Agent_Roles_Needed/evaluate")
                .contentType(MediaType.APPLICATION_JSON);

            Map<String, TypeValue> data = new HashMap<>();
            data.put("lead_source", new TypeValue("string", leadSource));
            data.put("lead_channel", new TypeValue("string", leadChannel));
            data.put("lead_stage", new TypeValue("string", leadStage));

            clientRequest.bodyValue(new GetListAgentRolesNeededReq(data));

            Mono<List<GetListAgentRolesNeededResp>> monoResp = clientRequest
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<GetListAgentRolesNeededResp>>() {
                })
                .doOnError(
                    error -> log.error("CamundaService - evalue Decision_Agent_Roles_Needed - error", error)
                )
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

            var response = monoResp.block();
            if (response != null && !response.isEmpty()) {
                GetListAgentRolesNeededResp decisionResult = response.get(0);
                return decisionResult.getAgentRoles().getValue().toString().split(",");
            } else {
                return new String[]{};
            }
        } catch (Exception ex) {
            throw new RequestThirdpartyException(
                "WorflowService",
                "getListAgentRolesNeeded",
                "CamundaService" + "evalue Decision_Agent_Roles_Needed " + ex.getMessage()
            );
        }
    }
}
