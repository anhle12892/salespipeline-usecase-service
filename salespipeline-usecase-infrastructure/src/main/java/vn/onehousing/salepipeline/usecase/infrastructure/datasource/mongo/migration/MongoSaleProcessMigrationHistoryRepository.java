package vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo.migration;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface MongoSaleProcessMigrationHistoryRepository extends MongoRepository<MongoSaleProcessMigrationHistory, String> {
}
