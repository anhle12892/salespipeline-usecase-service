package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.salespipelineservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetListAgentRolesNeededReq {
    Map<String, TypeValue> variables;
}
