package vn.onehousing.salepipeline.usecase.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vn.onehousing.common.elasticsearch.config.ElasticsearchIndexConfig;
import vn.onehousing.common.elasticsearch.config.IndexActionListener;

@Slf4j
@Configuration
@ConditionalOnClass(ElasticsearchIndexConfig.class)
public class ElasticsearchConfiguration {

    @Bean
    public IndexActionListener indexActionListener() {
        return new IndexActionListener();
    }
}
