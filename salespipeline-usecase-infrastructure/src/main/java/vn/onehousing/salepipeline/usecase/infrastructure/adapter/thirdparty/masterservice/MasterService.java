package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.masterservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salepipeline.usecase.business.thirdparty.masterservice.IMasterService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.masterservice.response.MasterServiceResp;
import vn.onehousing.salepipeline.usecase.business.thirdparty.masterservice.response.ProjectResponseDto;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.request.UpdateOpportunityReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.response.OpportunityServiceResp;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.HousingErrors;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.HousingException;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.RequestThirdpartyException;

import java.time.Duration;
import java.util.List;

@Slf4j
@Component
public class MasterService implements IMasterService {

    private final WebClient webClient;
    private final ObjectMapper mapper;

    public MasterService(@Lazy WebClient masterServiceClient, ObjectMapper mapper) {
        this.webClient = masterServiceClient;
        this.mapper = mapper;
    }

    @Override
    public List<ProjectResponseDto> getAllProject() {
        Mono<MasterServiceResp> monoResp = this.webClient.get()
                .uri("/v1/projects")
                .exchangeToMono(response -> response.bodyToMono(MasterServiceResp.class))
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        MasterServiceResp resp = monoResp.block();
        log.info("[MasterService] call master service with result");
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdpartyException("MasterService","get",resp.getMeta().toString());

        return resp.getData();
    }
}
