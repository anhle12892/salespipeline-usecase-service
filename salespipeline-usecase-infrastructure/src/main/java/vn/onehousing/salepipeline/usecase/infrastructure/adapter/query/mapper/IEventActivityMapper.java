package vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import vn.onehousing.salepipeline.usecase.query.event.*;
import vn.onehousing.salepipeline.usecase.business.mapper.PhoneMapper;
import vn.onehousing.salepipeline.usecase.business.mapper.UUIDMapper;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEntityType;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;

import java.util.Map;

@Mapper(componentModel = "spring", uses = {UUIDMapper.class, PhoneMapper.class})
public interface IEventActivityMapper {

    @Mapping(source = "event.entityUuid", target = "parentUuid")
    @Mapping(source = "event.entityType", target = "parentType")
    @Mapping(source = "event.eventUuid", target = "entityUuid")
    @Mapping(constant = ActEntityType.EVENT, target = "entityType")
    @Mapping(source = "payload", target = "data")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(source = "event.entityUuid", target = "leadUuid")
    OpportunityActivityView fromCreateEvent(CreateEventActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.entityUuid", target = "parentUuid")
    @Mapping(source = "event.entityType", target = "parentType")
    @Mapping(source = "event.eventUuid", target = "entityUuid")
    @Mapping(constant = ActEntityType.EVENT, target = "entityType")
    @Mapping(source = "payload", target = "data")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(source = "event.entityUuid", target = "leadUuid")
    OpportunityActivityView fromUpdateEvent(UpdateEventActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.entityUuid", target = "parentUuid")
    @Mapping(source = "event.entityType", target = "parentType")
    @Mapping(source = "event.taskUuid", target = "entityUuid")
    @Mapping(constant = ActEntityType.TASK, target = "entityType")
    @Mapping(source = "payload", target = "data")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(source = "event.entityUuid", target = "leadUuid")
    OpportunityActivityView fromCreateTask(CreateTaskActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.entityUuid", target = "parentUuid")
    @Mapping(source = "event.entityType", target = "parentType")
    @Mapping(source = "event.taskUuid", target = "entityUuid")
    @Mapping(constant = ActEntityType.TASK, target = "entityType")
    @Mapping(source = "payload", target = "data")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(source = "event.entityUuid", target = "leadUuid")
    OpportunityActivityView fromUpdateTask(UpdateTaskActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.parentUuid", target = "parentUuid")
    @Mapping(source = "event.parentType", target = "parentType")
    @Mapping(source = "event.noteUuid", target = "entityUuid")
    @Mapping(constant = ActEntityType.NOTE, target = "entityType")
    @Mapping(source = "payload", target = "data")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(target = "leadUuid", source = "event", qualifiedByName = "leadUuid")
    OpportunityActivityView fromCreateNote(CreateNoteActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "context.lead_uuid", target = "leadUuid")
    @Mapping(source = "context.lead_uuid", target = "parentUuid")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(constant = ActEntityType.ASSIGNMENT, target = "entityType")
    @Mapping(source = "assignment.assignedDate", target = "createdDate")
    @Mapping(source = "assignment.assignedDate", target = "lastModifiedDate")
    @Mapping(source = "payload", target = "data")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(source = "assignment.policyUuid", target = "entityUuid")
    OpportunityActivityView fromAssignment(AssignedActivityView assignment, AssignmentContextView context
        , Map<String, Object> payload, String uuid);

    @Mapping(source = "event.parentUuid", target = "parentUuid")
    @Mapping(source = "event.parentType", target = "parentType")
    @Mapping(source = "event.noteUuid", target = "entityUuid")
    @Mapping(constant = ActEntityType.NOTE, target = "entityType")
    @Mapping(source = "payload", target = "data")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(target = "leadUuid", source = "event", qualifiedByName = "leadUuid")
    OpportunityActivityView fromUpdateNote(UpdateNoteActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.ACCOUNT, target = "entityType")
    @Mapping(constant = ActEntityType.ACCOUNT, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(source = "event.accountUuid", target = "entityUuid")
    @Mapping(source = "event.accountUuid", target = "parentUuid")
    OpportunityActivityView fromEvent(CreateAccountActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.ACCOUNT, target = "entityType")
    @Mapping(constant = ActEntityType.ACCOUNT, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(source = "event.accountUuid", target = "entityUuid")
    @Mapping(source = "event.accountUuid", target = "parentUuid")
    OpportunityActivityView fromEvent(UpdateAccountActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.CONTACT, target = "entityType")
    @Mapping(constant = ActEntityType.CONTACT, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(source = "event.contactUuid", target = "entityUuid")
    @Mapping(source = "event.contactUuid", target = "parentUuid")
    OpportunityActivityView fromEvent(CreateContactActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.CONTACT, target = "entityType")
    @Mapping(constant = ActEntityType.CONTACT, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(source = "event.contactUuid", target = "entityUuid")
    @Mapping(source = "event.contactUuid", target = "parentUuid")
    OpportunityActivityView fromEvent(UpdateContactActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.LEAD, target = "entityType")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(source = "event.leadUuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView fromEvent(CreateLeadActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.LEAD, target = "entityType")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(source = "event.leadUuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView fromEvent(UpdateLeadActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.phoneNumber", target = "phoneNumber")
    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.OPPORTUNITY, target = "entityType")
    @Mapping(constant = ActEntityType.OPPORTUNITY, target = "parentType")
    @Mapping(source = "event.opportunityUuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView fromEvent(CreateOpportunityActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.opportunityUuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.OPPORTUNITY, target = "entityType")
    @Mapping(constant = ActEntityType.OPPORTUNITY, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView fromEvent(UpdateOpportunityActivityView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.uuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.SALES_PIPELINE, target = "entityType")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView createSP(SalesPipelineView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.uuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.SALES_PIPELINE, target = "entityType")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView updateSP(SalesPipelineView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.leadUuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.LEAD, target = "entityType")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView fromEvent(LeadRevokedEventView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.leadUuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.LEAD, target = "entityType")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView fromEvent(LeadBookingEventView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.leadUuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.LEAD, target = "entityType")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView fromEvent(OpportunityBookingStatusUpdatedEvent event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.leadUuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.LEAD, target = "entityType")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView fromEvent(LeadStoppedEventView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.leadUuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.LEAD, target = "entityType")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView fromEvent(LeadDuplicateMessageView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.opportunityUuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.OPPORTUNITY, target = "entityType")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView fromEvent(OpportunityStoppedEventView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.leadUuid", target = "entityUuid")
    @Mapping(source = "event.leadUuid", target = "parentUuid")
    @Mapping(source = "event.leadUuid", target = "leadUuid")
    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.OPPORTUNITY, target = "entityType")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    OpportunityActivityView fromEvent(OpportunitySucceedEventView event, Map<String, Object> payload, String uuid);

    @Mapping(source = "event.lead.leadUuid", target = "entityUuid")
    @Mapping(source = "event.lead.leadUuid", target = "parentUuid")
    @Mapping(source = "event.lead.leadUuid", target = "leadUuid")
    @Mapping(source = "event.lead.phoneNumber", target = "phoneNumber")
    @Mapping(source = "payload", target = "data")
    @Mapping(constant = ActEntityType.FINANCE, target = "entityType")
    @Mapping(constant = ActEntityType.LEAD, target = "parentType")
    @Mapping(source = "uuid", target = "uuid")
    @Mapping(expression = "java(java.time.Instant.now().toEpochMilli())", target = "timestamp")
    OpportunityActivityView fromEvent(FinancialConsultingCreatedEvent event, Map<String, Object> payload, String uuid);

    @Named("leadUuid")
    default String getLeadUuid(CreateNoteActivityView event) {
        if (ActEntityType.LEAD.equals(event.getParentType())
            || ActEntityType.CUSTOMER_MSG.equals(event.getParentType())) {
            return event.getParentUuid();
        } else
            return null;
    }

    @Named("leadUuid")
    default String getLeadUuid(UpdateNoteActivityView event) {
        if (ActEntityType.LEAD.equals(event.getParentType())) {
            return event.getParentUuid();
        } else
            return null;
    }
}


