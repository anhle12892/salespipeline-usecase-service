package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.accountservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.IAccountService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.AccountContactCreateDto;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.request.AccountUpdateRequest;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.response.CreateAccountResp;
import vn.onehousing.salepipeline.usecase.business.thirdparty.accountservice.response.CreateContactResp;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.HousingErrors;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.HousingException;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.RequestThirdpartyException;
import vn.onehousing.salepipeline.usecase.common.shared.model.Account;
import vn.onehousing.salepipeline.usecase.common.shared.model.Contact;

@Slf4j
@Component
public class AccountService implements IAccountService {

    private final WebClient webClient;

    public AccountService(@Lazy WebClient accountServiceClient) {
        this.webClient = accountServiceClient;
    }

    @Override
    public Account createAccount(AccountContactCreateDto request) {
        Mono<CreateAccountResp> accountResponse =
            this.webClient.post().uri("/v1/accounts/")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(request), AccountContactCreateDto.class)
                .exchangeToMono(response -> response.bodyToMono(CreateAccountResp.class))
                .switchIfEmpty(Mono.error(new HousingException(HousingErrors.CREATE_ACCOUNT_ERROR)));
        CreateAccountResp resp = accountResponse.block();
        if (BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            return resp.getData();
        } else {
            throw new RequestThirdpartyException("AccountService", "createAccount", resp.getMeta().toString());
        }
    }

    @Override
    public Contact createContact(AccountContactCreateDto request) {
        Mono<CreateContactResp> contactResponse =
            this.webClient.post().uri("/v1/contacts/")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(request), AccountContactCreateDto.class)
                .exchangeToMono(response -> response.bodyToMono(CreateContactResp.class));

        CreateContactResp resp = contactResponse.block();
        if (BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            return resp.getData();
        } else {
            throw new RequestThirdpartyException("AccountService", "createContact", resp.getMeta().toString());
        }
    }

    @Override
    public Account updateAccount(AccountUpdateRequest request) {

        Mono<CreateAccountResp> accountResponse =
            this.webClient.put().uri("/v1/accounts/" + request.getAccountUuid())
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(request), AccountUpdateRequest.class)
                .exchangeToMono(response -> response.bodyToMono(CreateAccountResp.class))
                .switchIfEmpty(Mono.error(new HousingException(HousingErrors.CREATE_ACCOUNT_ERROR)));
        CreateAccountResp resp = accountResponse.block();
        if (BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            return resp.getData();
        } else {
            throw new RequestThirdpartyException("AccountService", "updateAccount", resp.getMeta().toString());
        }
    }

    @Override
    public Account getAccountByUuid(String accountUuid) {

        Mono<CreateAccountResp> accountResponse =
            this.webClient.get().uri("/v1/accounts/" + accountUuid)
                .exchangeToMono(response -> response.bodyToMono(CreateAccountResp.class))
                .switchIfEmpty(Mono.error(new HousingException(HousingErrors.CREATE_ACCOUNT_ERROR)));
        CreateAccountResp resp = accountResponse.block();
        if (BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            return resp.getData();
        } else {
            throw new RequestThirdpartyException("AccountService", "getAccountByUuid", resp.getMeta().toString());
        }
    }

    @Override
    public Contact getContactByUuid(String contactUuid) {
        Mono<CreateContactResp> accountResponse =
            this.webClient.get().uri("/v1/contacts/" + contactUuid)
                .exchangeToMono(response -> response.bodyToMono(CreateContactResp.class))
                .switchIfEmpty(Mono.error(new HousingException(HousingErrors.CREATE_ACCOUNT_ERROR)));
        CreateContactResp resp = accountResponse.block();
        if (BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            return resp.getData();
        } else {
            throw new RequestThirdpartyException("AccountService", "getContactByUuid", resp.getMeta().toString());
        }
    }
}
