package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.accountservice.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.onehousing.salepipeline.usecase.common.shared.model.LeadIdDocument;
import vn.onehousing.salepipeline.usecase.common.shared.model.PhoneNumber;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContactViewResponse {
    Long id;
    String contactCode;
    String contactUuid;
    String contactName;
    String contactStatus;
    PhoneNumber phoneNumber;
    String email;
    LocalDate dateOfBirth;
    String job;
    Instant lastRequestContactDate;
    String gender;
    Instant lastContactedDate;
    String accountString;
    String leadSource;
    String leadChannel;
    String description;
    String attributeSetString;
    String facebook;
    String martialStatus;
    String referredByString;
    String referredByUserCode;
    String contactRoles;
    Integer numberOfChildren;
    String accountUuid;
    String accountCode;

    Integer rangeAge;
    String childrenSchool;
    String nationality;
    String permanentAddressId;
    String leadUuid;
    String leadCode;
    String note;
    List<LeadIdDocument> idDocuments;
}
