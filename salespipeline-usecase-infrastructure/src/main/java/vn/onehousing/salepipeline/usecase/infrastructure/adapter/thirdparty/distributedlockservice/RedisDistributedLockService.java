package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.distributedlockservice;

import lombok.AllArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import vn.onehousing.salepipeline.usecase.infrastructure.config.RedisDistributedLockServiceConfiguration;
import vn.onehousing.salepipeline.usecase.business.thirdparty.distributedlockservice.IDistributedLockService;

import java.util.concurrent.TimeUnit;

@Component
@EnableConfigurationProperties(RedisDistributedLockServiceConfiguration.class)
@AllArgsConstructor
public class RedisDistributedLockService implements IDistributedLockService {

    private final StringRedisTemplate redisTemplate;

    private final RedisDistributedLockServiceConfiguration configuration;

    @Override
    public boolean getLock(String key, String value) {

        int retryTimes = configuration.getRetryTimes();
        Boolean success = redisTemplate.opsForValue().setIfAbsent(key, value, configuration.getTimeout(), TimeUnit.SECONDS);
        int retry = 0;
        while ((success == null || !success) && retry < retryTimes) {
            try {
                Thread.sleep(configuration.getTimeout() * 1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            success = redisTemplate.opsForValue().setIfAbsent(key, value, configuration.getTimeout(), TimeUnit.SECONDS);
            retry++;
        }
        return success;
    }

    @Override
    public void releaseLock(String key) {
        redisTemplate.delete(key);
    }
}
