package vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.opportunityactivityview;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEventConst;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.es.ElasticsearchService;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.IOpportunityActivityViewRepository;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Repository
@AllArgsConstructor
public class OpportunityActivityViewRepository implements IOpportunityActivityViewRepository {

    private final ElasticsearchService elasticsearchService;
    private final ObjectMapper objectMapper;
    private final String index = "db-onehousing-salespipeline-usecases-lead-opp-activities";

    @Override
    public OpportunityActivityView insert(OpportunityActivityView activityView, @Nullable Long timestamp) {
        elasticsearchService.insert(index, activityView.getUuid(), timestamp == null ? activityView.getLastModifiedDate().toEpochMilli() : timestamp, activityView);
        return activityView;
    }

    @Override
    public List<OpportunityActivityView> get(String uuid, String phoneNumber) throws IOException {
        List<OpportunityActivityView> result = new ArrayList<>();
        List<Object> data = elasticsearchService.searchActivity(
            index,
            uuid,
            phoneNumber
        );
        data.forEach(r -> {
            try {
                result.add(objectMapper.readValue(r.toString(), OpportunityActivityView.class));
            } catch (JsonProcessingException e) {
                log.error("Cannot parse JSON from Elastic Search",e);
            }
        });
        return  result;
    }

    @Override
    public Optional<OpportunityActivityView> searchLeadInformation(String leadUuid) throws IOException{

        try {
            Optional<Object> data = elasticsearchService.searchLeadInformation(index, leadUuid, ActEventConst.UPDATED_LEAD);
            if (data.isPresent())
                return Optional.of(objectMapper.readValue(data.get().toString(), OpportunityActivityView.class));
            else
            {
                data = elasticsearchService.searchLeadInformation(index, leadUuid, ActEventConst.CREATED_LEAD);
                if(data.isPresent())
                    return Optional.of(objectMapper.readValue(data.get().toString(), OpportunityActivityView.class));
            }

        } catch (JsonProcessingException e) {
            log.error("Cannot parse JSON from Elastic Search",e);
        } catch (Exception e){
            log.error("Error while query data: {}", e.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public Optional<OpportunityActivityView> searchByChildData(List<String> fields, List<String> values) throws IOException {
        try {
            Optional<Object> data = elasticsearchService.searchByChildData(index, fields, values);
            if (data.isPresent())
                return Optional.of(objectMapper.readValue(data.get().toString(), OpportunityActivityView.class));
        } catch (JsonProcessingException e) {
            log.error("Cannot parse JSON from Elastic Search",e);
        } catch (Exception e){
            log.error("Error while query data: {}", e.getMessage());
        }
        return Optional.empty();
    }
}
