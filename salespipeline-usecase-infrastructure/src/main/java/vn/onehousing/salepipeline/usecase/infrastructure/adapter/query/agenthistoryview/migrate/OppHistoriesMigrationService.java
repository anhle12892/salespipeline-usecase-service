package vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.agenthistoryview.migrate;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salepipeline.usecase.business.usecase.createhistoryuc.IInsertOppHistories;
import vn.onehousing.salepipeline.usecase.business.usecase.createhistoryuc.response.OpportunityHistoryRes;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.es.ElasticsearchService;

import javax.annotation.Nullable;

@Slf4j
@Service
@AllArgsConstructor
public class OppHistoriesMigrationService implements IInsertOppHistories {
    private final ElasticsearchService elasticsearchService;
    private final ObjectMapper objectMapper;

    private final String indexHistory = "db-onehousing-salespipeline-usecases-agent-histories";

    @Override
    public Boolean insert(OpportunityHistoryRes data, @Nullable Long timestamp) {
        elasticsearchService.insert(
                indexHistory,
                data.getHistoryUuid(),
                timestamp == null ? data.getCreatedTime().toEpochMilli() : timestamp,
                data
        );
        return true;
    }
}
