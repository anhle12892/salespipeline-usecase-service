package vn.onehousing.salepipeline.usecase.infrastructure.usecase.saleprocess.query;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Repository;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.utils.CommonUtils;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessSchemaProfile;
import vn.onehousing.salepipeline.usecase.common.shared.constants.RedisLockName;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.ISaleProcessStageRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessStage;
import vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper.SaleProcessStageModelMapper;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo.MongoSaleProcessStage;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo.MongoSaleProcessStageRepository;
import vn.onehousing.salepipeline.usecase.business.thirdparty.distributedlockservice.IDistributedLockService;

import java.util.Optional;

@Slf4j
@Repository
@AllArgsConstructor
public class SaleProcessStageRepository implements ISaleProcessStageRepository {
    private final MongoSaleProcessStageRepository repository;
    private final SaleProcessStageModelMapper mapper;

    private final IDistributedLockService lockService;

    @Override
    public Optional<SaleProcessStage> insert(SaleProcessStage data) {
        try {
            repository.save(mapper.from(data));
            return Optional.of(data);
        } catch (Exception exception) {
            log.error("[SaleProcessStageRepository -> insert] lead_uuid: {}, {}",
                data.getLeadUuid(),
                exception.getMessage());
        }

        return Optional.empty();
    }

    @Override
    public Optional<SaleProcessStage> update(SaleProcessStage data) {
        try {
            MongoSaleProcessStage target = repository.getByLeadUuid(data.getLeadUuid());
            if (target == null) {
                return insert(data);
            } else {
                var currentStage = NumberUtils.toInt(target.getSaleStage(),
                    SaleProcessSchemaProfile.IN_HOUSE_INITIAL_STAGE);
                var newStage = NumberUtils.toInt(data.getStage(),
                    SaleProcessSchemaProfile.IN_HOUSE_INITIAL_STAGE);
                if (newStage > currentStage) {
                    target.setSaleStage(data.getStage());
                    repository.save(target);
                }

                return Optional.of(mapper.from(target));
            }
        } catch (Exception exception) {
            log.error("[SaleProcessStageRepository -> update] lead_uuid: {}, {}",
                data.getLeadUuid(),
                exception.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public Optional<SaleProcessStage> get(String leadUuid) {
        try {
            return Optional.of(mapper.from(repository.getByLeadUuid(leadUuid)));
        } catch (Exception exception) {
            log.error("[SaleProcessStageRepository -> get] lead_uuid: {}, {}",
                leadUuid,
                exception.getMessage());
        }

        return Optional.empty();
    }

    @Override
    public boolean getLockForUpdate(String leadUuid) {
        String key = CommonUtils.buildRedisLockKey(RedisLockName.SALESPIPELINE_USECASE_SALE_PROCESS, leadUuid);
        return lockService.getLock(key, leadUuid);
    }

    @Override
    public void releaseLockForUpdate(String leadUuid) {
        var key = CommonUtils.buildRedisLockKey(RedisLockName.SALESPIPELINE_USECASE_SALE_PROCESS, leadUuid);
        lockService.releaseLock(key);
    }
}
