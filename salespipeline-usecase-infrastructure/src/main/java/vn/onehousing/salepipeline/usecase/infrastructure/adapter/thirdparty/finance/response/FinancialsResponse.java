package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.finance.response;

import lombok.Builder;
import lombok.Data;
import vn.onehousing.salepipeline.usecase.business.thirdparty.finance.Financial;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;

import java.util.List;

@Data
@Builder
public class FinancialsResponse extends BaseResponse<List<Financial>> {
}
