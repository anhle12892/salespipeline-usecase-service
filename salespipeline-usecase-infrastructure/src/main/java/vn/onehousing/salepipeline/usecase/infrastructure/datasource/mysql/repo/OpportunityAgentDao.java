package vn.onehousing.salepipeline.usecase.infrastructure.datasource.mysql.repo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEntityType;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess.SaleProcessTaskWorkflow;
import vn.onehousing.salepipeline.usecase.common.shared.model.Task;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.FormSubmissionMigration;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.OpportunityAgentRepository;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.OpportunityFormSubmissionMigration;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.SaleProcessOpportunityMigration;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.task.TaskMigrationDto;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo.migration.MongoSaleProcessMigrationHistory;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo.migration.MongoSaleProcessMigrationHistoryRepository;

import javax.sql.DataSource;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Repository
@AllArgsConstructor
@Transactional
@Slf4j
public class OpportunityAgentDao extends JdbcDaoSupport implements OpportunityAgentRepository {
    private final ObjectMapper objectMapper;

    Map<String, String> FORM_PATH = new HashMap<String, String>() {{
        put("ghichepcanhan", "ghichepcanhan");  // form
        put("cuochenthanhcong", "cuochenthanhcong");    // form 2

        put("raocan", "raocan");
        // put("capnhatvebooking", "capnhatvebooking");

        put("ghinhanhangmuctuvan", "ghinhanhangmuctuvan");
        put("approachanddefineneeds", "approachanddefineneeds");
        put("dexuatvaphanhoi", "dexuatvaphanhoi");
    }};
    /**
     * approachanddefineneeds -> contacted
     * approachanddefineneeds -> confirm_demand
     */
    Map<String, String> ACTIVITY_RESULT = new HashMap<String, String>() {{
        put("Chưa liên lạc với khách", "APPROACH_NOT_CONTACT");
        put("Đã gọi nhưng khách không nghe máy", "APPROACH_CONTACTED_NOT_ANSWER");
        put("Đã gọi nhưng khách hẹn gọi lại sau", "APPROACH_CONTACTED_RECALL_LATER");
        put("Đã liên hệ khách thành công", "APPROACH_CONTACTED_SUCCESS_NOT_CONFIRMED");
    }};

    /**
     * ghinhanhangmuctuvan -> hangmuctuvan
     */
    Map<String, String> CONSULTING_ITEMS = new HashMap<String, String>() {{
        put("Tư vấn về chủ đầu tư", "CONSULTING_INVESTOR");
        put("Tư vấn về sản phẩm bất động sản", "CONSULTING_PRODUCT");
        put("Tư vấn chính sách bán hàng", "CONSULTING_POLICY");
        put("Tư vấn nhu cầu tài chính", "CONSULTING_FINANCIAL");
        put("Tư vấn về dự án", "CONSULTING_PROJECT");
    }};

    private final String consultingFinancialPreviousKey = "Tư vấn nhu cầu tài";
    private final String CONSULTING_FINANCIAL = "CONSULTING_FINANCIAL";

    /**
     * raocan
     */
    Map<String, String> RESOLVE_HURDLE = new HashMap<>() {
        {
            put("Tôi xác nhận", "RESOLVE_ALL_HURDLES_PROBLEM");
        }
    };
    Map<String, String> contactingWayMapping = new HashMap<>() {
        {
            put("Gọi điện", "call");
            put("Nhắn tin", "sms");
            put("Email", "email");
            put("Tư vấn khách qua kênh online", "online_channel");
            put("Gặp khách tại nhà mẫu", "meeting_samplehouse");
            put("Gặp khách tại dự án", "meeting_project");
            put("Dẫn khách đi sự kiện", "meeting_event");
            put("Khác", "other");

        }
    };
    Map<String, String> raocanMappingProject = new HashMap<>() {{
        put("Vị trí không phù hợp", "RESOLVE_PROBLEM_PROJECT_LOCATION");
        put("Không thích thiết kế, cảnh quan", "RESOLVE_PROBLEM_PROJECT_VIEW");
        put("Không thích tiện ích", "RESOLVE_PROBLEM_PROJECT_UTILITY");
        put("Cộng đồng có thực sự văn minh và đẳng cấp", "RESOLVE_PROBLEM_PROJECT_CIVILIZED_COMMUNITY");
        put("Khác", "RESOLVE_PROBLEM_PROJECT_OTHER");
    }};
    Map<String, String> raocanMappingInvestor = new HashMap<>() {{
        put("Thương hiệu của CĐT chưa phổ biến\t", "RESOLVE_PROBLEM_INVESTOR_TRADEMARK");
        put("Quy trình bán hàng không thuyết phục\t", "RESOLVE_PROBLEM_INVESTOR_SALE_PROCESS");
        put("Quy trình ra hàng chưa phù hợp\t", "RESOLVE_PROBLEM_INVESTOR_DELIVERY_PROCESS");
        put("Khách hàng chưa được trải nghiệm dịch vụ vận hành\t", "RESOLVE_PROBLEM_INVESTOR_EXP_SERVICES");
        put("Tên tuổi của đối tác của CĐT\t", "RESOLVE_PROBLEM_INVESTOR_PARTNER_REPUTATION");
        put("Khác", "RESOLVE_PROBLEM_INVESTOR_OTHER");
    }};
    Map<String, String> raocanMappingProduct = new HashMap<>() {{
        put("Vật liệu bàn giao, tiêu chuẩn bàn giao", "RESOLVE_PROBLEM_PRODUCT_HANDOVER");
        put("Diện tích bé", "RESOLVE_PROBLEM_PRODUCT_SMALL_AREA");
        put("Giá không tương xứng", "RESOLVE_PROBLEM_PRODUCT_PRICE");
        put("Không chọn được căn hộ phù hợp", "RESOLVE_PROBLEM_PRODUCT_NOT_SUITABLE");
        put("Khác", "RESOLVE_PROBLEM_PRODUCT_OTHER");
    }};
    Map<String, String> raocanMappingPolicy = new HashMap<>() {{
        put("Chính sách CĐT không hấp dẫn\t", "RESOLVE_PROBLEM_POLICY_INVESTOR_NOT_ATTRACTIVE");
        put("Chính sách của OH không hấp dẫn\t", "RESOLVE_PROBLEM_POLICY_OH_NOT_ATTRACTIVE");
        put("Không đủ điều kiện vay\t", "RESOLVE_PROBLEM_POLICY_NOT_ELIGIBLE_LOAN");
        put("Khác", "RESOLVE_PROBLEM_POLICY_OTHER");
    }};

    private MongoSaleProcessMigrationHistoryRepository historyRepository;

    @Autowired
    public OpportunityAgentDao(DataSource dataSource,
                               MongoSaleProcessMigrationHistoryRepository historyRepository,
                               ObjectMapper objectMapper) {
        this.setDataSource(dataSource);
        this.historyRepository = historyRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public List<SaleProcessOpportunityMigration> getOpportunities(List<String> uuids) {
        List<SaleProcessOpportunityMigration> saleProcessMigrations = null;
        if (uuids == null || uuids.isEmpty()) {
            // migrate all
            saleProcessMigrations = getOpportunityMigration();
        } else {
            saleProcessMigrations = new ArrayList<>();
            for (var index = 0; index < uuids.size(); index++) {
                var result = get(uuids.get(index));
                if (result != null && !result.isEmpty()) {
                    saleProcessMigrations.addAll(result);
                }
            }
        }

        var results = new ArrayList<SaleProcessOpportunityMigration>();
        saleProcessMigrations.forEach(item -> {
            log.info("[OpportunityAgentDao -> getOpportunities] coreUuid={} opportunityId={}",
                    item.getLeadUuid(),
                    item.getId());

            List<String> appointments = getOpportunityAppointment(item.getLeadUuid(), item.getId());
            List<String> ordersMigrations = getOrdersMigration(item.getLeadUuid(), item.getId());
            if (appointments != null) {
                item.setAppointmentsCreated(appointments);
            }

            if (ordersMigrations != null) {
                item.setBookingsStatus(ordersMigrations);
            }

            var formSubmission = get(item.getLeadUuid(), item.getId());
            if (formSubmission != null && formSubmission.getForms() != null) {
                var activities = new ArrayList<String>();
                formSubmission.getForms().forEach(form -> {
                    if ("ghichepcanhan".equals(form.getFormPath())) {
                        writeMigrationHistory("form_submission",
                                item.getLeadUuid(),
                                item.getId(),
                                "ghichepcanhan",
                                "output",
                                form.getData(),
                                "done",
                                ""
                        );
                    } else if ("cuochenthanhcong".equals(form.getFormPath())) {
                        var result = recordMeetingSuccess(item.getLeadUuid(),
                                Long.parseLong(item.getId()),
                                form.getData(),
                                null);
                        activities.addAll(result.keySet());

                        writeMigrationHistory("form_submission",
                                item.getLeadUuid(),
                                item.getId(),
                                "cuochenthanhcong",
                                "output",
                                result,
                                "",
                                ""
                        );
                    } else if ("raocan".equals(form.getFormPath())) {
                        var result = getHurdleForm(item.getLeadUuid(),
                                Long.parseLong(item.getId()),
                                form.getData(),
                                null);
                        activities.addAll(result.keySet());

                        writeMigrationHistory("form_submission",
                                item.getLeadUuid(),
                                item.getId(),
                                "raocan",
                                "output",
                                result,
                                "done",
                                ""
                        );
                    } else if ("capnhatvebooking".equals(form.getFormPath())) {
                        writeMigrationHistory("form_submission",
                                item.getLeadUuid(),
                                item.getId(),
                                "capnhatvebooking",
                                "output",
                                form.getData(),
                                "done",
                                ""
                        );
                    } else if ("ghinhanhangmuctuvan".equals(form.getFormPath())) {
                        var result = consultingItem(item.getLeadUuid(),
                                Long.parseLong(item.getId()),
                                form.getData(),
                                null);
                        activities.addAll(result.keySet());

                        writeMigrationHistory("form_submission",
                                item.getLeadUuid(),
                                item.getId(),
                                "ghinhanhangmuctuvan",
                                "output",
                                result,
                                "done",
                                ""
                        );
                    } else if ("approachanddefineneeds".equals(form.getFormPath())) {
                        var result = approachAndDefineNeeds(item.getLeadUuid(),
                                Long.parseLong(item.getId()),
                                form.getData(),
                                null);
                        activities.addAll(result.keySet());

                        writeMigrationHistory("form_submission",
                                item.getLeadUuid(),
                                item.getId(),
                                "approachanddefineneeds",
                                "output",
                                result,
                                "done",
                                ""
                        );
                    } else if ("dexuatvaphanhoi".equals(form.getFormPath())) {
                        var result = suggestAndFeedback(item.getLeadUuid(),
                                Long.parseLong(item.getId()),
                                form.getData(),
                                null);
                        if (result != null)
                            activities.addAll(result.keySet());

                        writeMigrationHistory("form_submission",
                                item.getLeadUuid(),
                                item.getId(),
                                "dexuatvaphanhoi",
                                "output",
                                result,
                                "done",
                                ""
                        );
                    }
                });

                item.setFormActivities(activities);
            }

            writeMigrationHistory("all",
                    item.getLeadUuid(),
                    item.getId(),
                    "aggregated",
                    "output",
                    item,
                    "done",
                    ""
            );

            results.add(item);
        });

        return results;
    }

    @Override
    public List<SaleProcessOpportunityMigration> get(String id) {
        log.info("[OpportunityAgentRepo] start query opportunity by core_uuid");
        String opportunitiesSql = ""
            + "select "
            + "id as id, "
            + "core_uuid as leadUuid, "
            + "uuid as opportunityUuid, "
            + "main_views as mainViews, "
            + "balcony_directions as balconyDirections, "
            + "property_range_floor as propertyRangeFloor, "
            + "num_of_bedroom as numOfBedroom, "
            + "budget_limit_id as budgetLimitId, "
            + "apartment_type_ids as apartmentTypeIds, "
            + "mortgage_need as mortgageNeed, "
            + "create_time as createTime, "
            + "update_time as updateTime, "
            + "status as status "
            + "from opportunities "
            + "where core_uuid='"
            + id + "' and where update_time <> create_time;";

        List<Map<String, Object>> opportunities = this.getJdbcTemplate().queryForList(opportunitiesSql);
        log.info("[OpportunityAgentRepo] query opportunities");
        List<SaleProcessOpportunityMigration> results = CollectionUtils.isEmpty(opportunities) ? null :
            opportunities.stream().map(item -> {
                writeMigrationHistory("opportunities",
                    Objects.toString(item.get("leadUuid"), ""),
                    Objects.toString(item.get("id"), ""),
                    "opportunity",
                    "query",
                    item,
                    "",
                    ""
                );


                var saleProcessMigration = new SaleProcessOpportunityMigration();
                saleProcessMigration.setId(Objects.toString(item.get("id"), ""));
                saleProcessMigration.setLeadUuid(Objects.toString(item.get("leadUuid"), ""));
                saleProcessMigration.setOpportunityUuid(Objects.toString(item.get("opportunityUuid"), ""));
                saleProcessMigration.setMainViews(Objects.toString(item.get("mainViews"), ""));
                saleProcessMigration.setBalconyDirections(Objects.toString(item.get("balconyDirections"), ""));
                saleProcessMigration.setPropertyRangeFloor(Objects.toString(item.get("propertyRangeFloor"), ""));
                saleProcessMigration.setNumOfBedroom(Objects.toString(item.get("numOfBedroom"), ""));
                saleProcessMigration.setBudgetLimitId(Objects.toString(item.get("budgetLimitId"), ""));
                saleProcessMigration.setApartmentTypeIds(Objects.toString(item.get("apartmentTypeIds"), ""));
                saleProcessMigration.setMortgageNeed(item.get("mortgageNeed") == null ? false : true);
                saleProcessMigration.setCreateTime(Objects.toString(item.get("createTime"), ""));
                saleProcessMigration.setUpdateTime(Objects.toString(item.get("updateTime"), ""));
                saleProcessMigration.setStatus(Objects.toString(item.get("status"), ""));


                writeMigrationHistory("opportunities",
                    saleProcessMigration.getLeadUuid(),
                    saleProcessMigration.getId(),
                    "opportunity",
                    "processing",
                    saleProcessMigration,
                    "",
                    ""
                );

                return saleProcessMigration;
            }).collect(Collectors.toList());

        return results;
    }

    @Override
    public List<Task> getListTask(String uuid) {
        List<Task> taskList = new ArrayList<>();
        List<TaskMigrationDto> migrationTask = getOppHistories(uuid);
        log.info("[CRM][TaskMigration] Total valid submission form = {}",
                migrationTask.size());
        migrationTask.forEach(task -> {
            taskList.addAll(convertSubmissionToTask(task));
        });
        return taskList;
    }

    private List<SaleProcessOpportunityMigration> getOpportunityMigration() {
        log.info("[OpportunityAgentRepo] start query opportunities");
        String opportunitiesSql = ""
                + "select "
                + "id as id, "
                + "core_uuid as leadUuid, "
                + "uuid as opportunityUuid, "
                + "main_views as mainViews, "
                + "balcony_directions as balconyDirections, "
                + "property_range_floor as propertyRangeFloor, "
                + "num_of_bedroom as numOfBedroom, "
                + "budget_limit_id as budgetLimitId, "
                + "apartment_type_ids as apartmentTypeIds, "
                + "mortgage_need as mortgageNeed, "
                + "create_time as createTime, "
                + "update_time as updateTime, "
                + "status as status "
                + "from opportunities;";

        List<Map<String, Object>> opportunities = this.getJdbcTemplate().queryForList(opportunitiesSql);
        log.info("[OpportunityAgentRepo] query opportunities");
        List<SaleProcessOpportunityMigration> results = CollectionUtils.isEmpty(opportunities) ? null :
                opportunities.stream().map(item -> {
                    writeMigrationHistory("opportunities",
                            Objects.toString(item.get("leadUuid"), ""),
                            Objects.toString(item.get("id"), ""),
                            "opportunity",
                            "query",
                            item,
                            "",
                            ""
                    );


                    var saleProcessMigration = new SaleProcessOpportunityMigration();
                    saleProcessMigration.setId(Objects.toString(item.get("id"), ""));
                    saleProcessMigration.setLeadUuid(Objects.toString(item.get("leadUuid"), ""));
                    saleProcessMigration.setOpportunityUuid(Objects.toString(item.get("opportunityUuid"), ""));
                    saleProcessMigration.setMainViews(Objects.toString(item.get("mainViews"), ""));
                    saleProcessMigration.setBalconyDirections(Objects.toString(item.get("balconyDirections"), ""));
                    saleProcessMigration.setPropertyRangeFloor(Objects.toString(item.get("propertyRangeFloor"), ""));
                    saleProcessMigration.setNumOfBedroom(Objects.toString(item.get("numOfBedroom"), ""));
                    saleProcessMigration.setBudgetLimitId(Objects.toString(item.get("budgetLimitId"), ""));
                    saleProcessMigration.setApartmentTypeIds(Objects.toString(item.get("apartmentTypeIds"), ""));
                    saleProcessMigration.setMortgageNeed(item.get("mortgageNeed") == null ? false : true);
                    saleProcessMigration.setCreateTime(Objects.toString(item.get("createTime"), ""));
                    saleProcessMigration.setUpdateTime(Objects.toString(item.get("updateTime"), ""));
                    saleProcessMigration.setStatus(Objects.toString(item.get("status"), ""));

                    writeMigrationHistory("opportunities",
                            saleProcessMigration.getLeadUuid(),
                            saleProcessMigration.getId(),
                            "opportunity",
                            "processing",
                            saleProcessMigration,
                            "",
                            ""
                    );

                    return saleProcessMigration;
                }).collect(Collectors.toList());

        return results;
    }

    private List<String> getOpportunityAppointment(String leadUuid, String opportunityId) {
        String appointmentSql = ""
                + "select "
                + "    appointment_status as appointmentStatus "
                + "from opportunity_appointments "
                + "where opportunity_id=" + opportunityId
                + ";";

        List<Map<String, Object>> appointments = this.getJdbcTemplate().queryForList(appointmentSql);
        log.info("[OpportunityAgentRepo] query opportunity_appointments");
        List<String> appointmentResults = CollectionUtils.isEmpty(appointments) ? null :
                appointments.stream()
                        .map(item -> {
                            writeMigrationHistory("opportunity_appointment",
                                    leadUuid,
                                    opportunityId,
                                    "appointment",
                                    "input",
                                    item,
                                    "",
                                    ""
                            );

                            var status = Objects.toString(item.get("appointmentStatus"), "");

                            writeMigrationHistory("opportunity_appointment",
                                    leadUuid,
                                    opportunityId,
                                    "appointment",
                                    "processing",
                                    status,
                                    "",
                                    ""
                            );

                            if (StringUtils.hasText(status)) {
                                return SaleProcessTaskWorkflow.CREATED_AN_APPOINTMENT;
                            }

                            return status;
                        })
                        .collect(Collectors.toList());

        return appointmentResults;
    }

    private List<String> getOrdersMigration(String leadUuid, String opportunityId) {
        String ordersSql = ""
                + "select "
                + "    status as status "
                + "from orders "
                + "where opportunity_id=" + opportunityId + ";";
        List<Map<String, Object>> orders = this.getJdbcTemplate().queryForList(ordersSql);
        log.info("[OpportunityAgentRepo] query orders");
        List<String> ordersResults = CollectionUtils.isEmpty(orders) ? null :
                orders.stream()
                        .map(item -> {
                            writeMigrationHistory("orders",
                                    leadUuid,
                                    opportunityId,
                                    "query",
                                    "input",
                                    item,
                                    "",
                                    ""
                            );

                            var status = Objects.toString(item.get("status"), "");

                            writeMigrationHistory("orders",
                                    leadUuid,
                                    opportunityId,
                                    "order",
                                    "processing",
                                    status,
                                    "",
                                    ""
                            );

                            if (StringUtils.hasText(status)) {
                                if ("BOOKING_SENT".equals(status)) {
                                    return SaleProcessTaskWorkflow.AGENT_SENT_BOOKING_TO_PARTNER;
                                } else if ("SUCCESS".equals(status)) {
                                    return SaleProcessTaskWorkflow.PARTNER_UPDATED_BOOKING_STATUS_TO_SUCCESS;
                                }
                            }

                            return status;
                        })
                        .collect(Collectors.toList());

        if (ordersResults != null && ordersResults.contains(SaleProcessTaskWorkflow.PARTNER_UPDATED_BOOKING_STATUS_TO_SUCCESS)) {
            ordersResults.add(SaleProcessTaskWorkflow.AGENT_SENT_BOOKING_TO_PARTNER);
        }

        return ordersResults;
    }

    private OpportunityFormSubmissionMigration get(String leadUuid, String opportunityId) {
        String formSql = ""
                + "select "
                + "    opportunity_id as opportunityId, "
                + "    form_path as formPath, "
                + "    data as data "
                + "from form_submissions "
                + "where opportunity_id=" + opportunityId + ";";

        List<Map<String, Object>> forms = this.getJdbcTemplate().queryForList(formSql);
        log.info("[OpportunityAgentRepo] query form_submission");
        List<FormSubmissionMigration> formResults = CollectionUtils.isEmpty(forms) ? null :
                forms.stream().map(item -> {
                    writeMigrationHistory("form_submission",
                            leadUuid,
                            opportunityId,
                            "form_submission - " + Objects.toString(item.get("formPath"), ""),
                            "query",
                            item,
                            "",
                            ""
                    );

                    var formSubmissionMigration = new FormSubmissionMigration();
                    formSubmissionMigration.setFormPath(Objects.toString(item.get("formPath"), ""));
                    formSubmissionMigration.setData(Objects.toString(item.get("data"), ""));

                    writeMigrationHistory("form_submission",
                            leadUuid,
                            opportunityId,
                            "form_submission - " + formSubmissionMigration.getFormPath(),
                            "processing",
                            formSubmissionMigration,
                            "",
                            ""
                    );

                    return formSubmissionMigration;
                }).collect(Collectors.toList());

        var opportunityFormSubmissionMigration = new OpportunityFormSubmissionMigration();
        opportunityFormSubmissionMigration.setForms(formResults);

        return opportunityFormSubmissionMigration;
    }

    private List<TaskMigrationDto> getOppHistories(String uuid) {
        String formSql = ""
                + "select "
                + "    f.opportunity_id as opportunityId, "
                + "    f.form_path as formPath, "
                + "    f.data as data, "
                + "    f.create_time as createTime, "
                + "    oa.content as content, "
                + " op.core_uuid as coreUuid, "
                + " a.uuid as agentUuid, "
                + " a.email as agentEmail "
                + "from form_submissions f "
                + "join opportunity_activities oa on f.history_id = oa.id "
                + "join opportunities op on f.opportunity_id = op.id "
                + "join agent_opportunity ao on f.opportunity_id = ao.id "
                + "join agents a on ao.agent_id = a.id "
                + "where f.form_path is not null "
                + "and " + uuid + " is null or op.core_uuid = " + uuid + "";

        return this.getJdbcTemplate().query(formSql, (rs, row) ->
                new TaskMigrationDto(rs.getLong("opportunityId"),
                        rs.getString("coreUuid"),
                        rs.getString("data"),
                        rs.getString("formPath"),
                        rs.getString("content"),
                        rs.getString("agentUuid"),
                        rs.getString("agentEmail"),
                        rs.getTimestamp("createTime").toInstant()));
    }

    private List<Task> convertSubmissionToTask(TaskMigrationDto dto) {
        String formPath = dto.getFormPath();
        Map<String, Task> result = new HashMap<>();
        List<Task> taskList = new ArrayList<>();
        if (Objects.isNull(dto.getData()))
            return taskList;

        switch (formPath) {
            case "approachanddefineneeds":
                result = approachAndDefineNeeds(dto.getCoreUuid(),
                        dto.getOpportunityId(),
                        dto.getData(),
                        dto);
                break;
            case "cuochenthanhcong":
                result = recordMeetingSuccess(dto.getCoreUuid(),
                        dto.getOpportunityId(),
                        dto.getData(),
                        dto);
                break;
            case "ghinhanhangmuctuvan":
                result = consultingItem(dto.getCoreUuid(),
                        dto.getOpportunityId(),
                        dto.getData(),
                        dto);
                break;
            case "raocan":
                result = getHurdleForm(dto.getCoreUuid(),
                        dto.getOpportunityId(),
                        dto.getData(),
                        dto);
                break;
            case "dexuatvaphanhoi":
                result = suggestAndFeedback(dto.getCoreUuid(),
                        dto.getOpportunityId(),
                        dto.getData(),
                        dto);
                break;
            case "capnhatvebooking":
                result = updateBooking(dto.getCoreUuid(),
                        dto.getOpportunityId(),
                        dto.getData(),
                        dto);
                break;
            case "ghichepcanhan":
                break;
            default:
                break;
        }
        result.forEach((key, value) -> taskList.add(value));
        return taskList;
    }

    private Task initTaskData(TaskMigrationDto dto,
                              String taskName,
                              String note,
                              String code,
                              String taskType) {
        Task task = new Task();
        if (Objects.nonNull(dto)) {
            task.setEntityUuid(dto.getCoreUuid());
            task.setEntityType(ActEntityType.LEAD);
            task.setAssignedToUserUuid(dto.getAgentUuid());
            task.setAssignedEmail(dto.getAgentEmail());
            task.setStatus("COMPLETED");
            task.setCompletedDate(dto.getCreateTime());
            task.setStartDate(dto.getCreateTime());
            task.setDescription(dto.getContent());
        }
        task.setCode(code);
        task.setTaskTypeCode(contactingWayMapping.get(taskType));
        task.setName(taskName);
        task.setComments(note);

        return task;
    }

    /**
     * dexuatvaphanhoi
     */

    private Map<String, Task> updateBooking(String coreUuid, Long opportunityId, String jsonData, TaskMigrationDto dto) {
        Map<String, Task> taskList = new HashMap<>();
        String taskName = "Cập nhật về booking";
        try {
            var result = objectMapper.readValue(jsonData, Map.class);
            Task task = initTaskData(dto,
                    taskName,
                    (String) result.get("note"),
                    "UPDATE_BOOKING",
                    (String) result.get("contactingway"));
            taskList.put("UPDATE_BOOKING", task);
        } catch (Exception err) {
            log.error("[OpportunityAgentRepo -> updateBooking] core_uuid={}, opportunityId={}",
                    coreUuid,
                    opportunityId,
                    err
            );
        }
        return taskList;
    }

    private Map<String, Task> suggestAndFeedback(String coreUuid, Long opportunityId, String jsonData, TaskMigrationDto dto) {
        Map<String, Task> taskList = new HashMap<>();
        String taskName = "Đề xuất BĐS và phản hồi của khách";
        try {
            var result = objectMapper.readValue(jsonData, Map.class);
            AtomicBoolean customer_confirm = new AtomicBoolean(false);
            if (result.containsKey("editGrid")) {
                var grids = (List<Map<String, String>>) result.get("editGrid");
                if (grids != null && grids.size() > 0) {
                    grids.forEach(item -> {
                        if (item.containsKey("question2") && "Có".equals(item.get("question2"))) {
                            Task task = initTaskData(dto,
                                    taskName + "-" + "Khách muốn chốt theo đề xuất của chuyên viên tư vấn",
                                    (String) result.get("note"),
                                    "SUGGESTION_AND_FEEDBACK_SUCCESS",
                                    (String) result.get("contactingway"));
                            taskList.put("SUGGESTION_AND_FEEDBACK_SUCCESS", task);
                            customer_confirm.set(true);
                        }
                        ;
                    });
                }
                if (!customer_confirm.get()) {
                    Task task = initTaskData(dto,
                            taskName,
                            (String) result.get("note"),
                            "SUGGESTION_AND_FEEDBACK_ONLY",
                            (String) result.get("contactingway"));
                    taskList.put("SUGGESTION_AND_FEEDBACK_ONLY", task);
                    customer_confirm.set(true);
                }
            } else {
                Task task = initTaskData(dto,
                        taskName,
                        (String) result.get("note"),
                        "NONE",
                        (String) result.get("contactingway"));
                taskList.put("NONE", task);
            }
        } catch (Exception err) {
            log.info("[OpportunityAgentRepo -> consultingItem] Error core_uuid={}, opportunityId={}",
                    coreUuid,
                    opportunityId,
                    err
            );
            writeMigrationHistory("form_submission",
                    coreUuid,
                    String.valueOf(opportunityId),
                    "dexuatvaphanhoi",
                    "processing",
                    jsonData,
                    "error",
                    err.getMessage()
            );
        }

        return taskList;
    }

    /**
     * approachanddefineneeds -> contacted
     * approachanddefineneeds -> confirm_demand
     */
    private Map<String, Task> approachAndDefineNeeds(String coreUuid, Long opportunityId, String jsonData, TaskMigrationDto dto) {
        Map<String, Task> taskList = new HashMap<>();
        String taskName = "Tiếp cận và xác nhận nhu cầu";
        try {
            var result = objectMapper.readValue(jsonData, Map.class);
            if (result.containsKey("contacted")) {
                var contactedMap = (Map<String, Boolean>) result.get("contacted");
                if (contactedMap != null) {
                    contactedMap.forEach((key, value) -> {
                        if (value) {
                            Task task = initTaskData(dto,
                                    taskName + "-" + key,
                                    (String) result.get("note"),
                                    "APPROACH_CONTACTED_CTV",
                                    (String) result.get("contactingway"));
                            taskList.put("APPROACH_CONTACTED_CTV", task);
                        }
                    });
                }
            }

            if (result.containsKey("confirm_demand")) {
                var confirmDemand = (Map<String, Boolean>) result.get("confirm_demand");
                if (confirmDemand != null) {
                    AtomicReference<Integer> countDemand = new AtomicReference<>(0);
                    confirmDemand.forEach((key, value) -> {
                        if (value) {
                            countDemand.updateAndGet(v -> v + 1);
                        }
                    });

                    if (countDemand.get() > 0 && countDemand.get() == confirmDemand.size()) {
                        Task task = initTaskData(dto,
                            taskName + "-" + "confirm_demand",
                            (String) result.get("note"),
                            "APPROACH_CONTACTED_SUCCESS_CONFIRMED",
                            (String) result.get("contactingway"));
                        taskList.put("APPROACH_CONTACTED_SUCCESS_CONFIRMED", task);
                    }
                }
            }

            if (result.containsKey("activity_result")) {
                var activityResult = (String) result.get("activity_result");
                if (activityResult != null) {
                    if (ACTIVITY_RESULT.containsKey(activityResult)) {
                        Task task = initTaskData(dto,
                                taskName + "-" + activityResult,
                                (String) result.get("note"),
                                ACTIVITY_RESULT.get(activityResult),
                                (String) result.get("contactingway"));
                        taskList.put(ACTIVITY_RESULT.get(activityResult), task);
                    }
                }
            }

        } catch (Exception err) {
            log.error("[OpportunityAgentRepo -> approachAndDefineNeeds] Error: core_uuid={}, opportunityId={}",
                    coreUuid,
                    opportunityId,
                    err
            );
            writeMigrationHistory("form_submission",
                    coreUuid,
                    String.valueOf(opportunityId),
                    "approachanddefineneeds",
                    "processing",
                    jsonData,
                    "error",
                    err.getMessage()
            );
        }

        return taskList;
    }

    /**
     * cuochenthanhcong
     */
    private Map<String, Task> recordMeetingSuccess(String uuid, Long id, String jsonData, TaskMigrationDto dto) {
        Map<String, Task> taskList = new HashMap<>();
        String taskName = "Ghi nhận cuộc hẹn thành công";
        try {
            var result = objectMapper.readValue(jsonData, Map.class);
            Task task = initTaskData(dto,
                    taskName,
                    (String) result.get("note"),
                    "RECORD_MEETING_SUCCESS",
                    (String) result.get("contactingway"));
            taskList.put("RECORD_MEETING_SUCCESS", task);
        } catch (Exception ex) {
            log.error("[OpportunityAgentRepo -> recordMeetingSuccess] Error: core_uuid={}, opportunityId={}",
                    uuid,
                    id,
                    ex);
            writeMigrationHistory("form_submission",
                    uuid,
                    String.valueOf(id),
                    "recordMeetingSuccess",
                    "processing",
                    jsonData,
                    "error",
                    ex.getMessage()
            );
        }
        return taskList;
    }

    /**
     * ghinhanhangmuctuvan -> hangmuctuvan
     */
    private Map<String, Task> consultingItem(String coreUuid, Long opportunityId, String jsonData, TaskMigrationDto dto) {
        Map<String, Task> taskList = new HashMap<>();
        String taskName = "Hạng mục đã tư vấn";
        try {
            var result = objectMapper.readValue(jsonData, Map.class);
            if (result.containsKey("hangmuctuvan")) {
                var itemsMap = (Map<String, Boolean>) result.get("hangmuctuvan");
                itemsMap.forEach((key, value) -> {
                    if (CONSULTING_ITEMS.containsKey(key) && value) {
                        Task task = initTaskData(dto,
                                taskName + "-" + key,
                                (String) result.get("note"),
                                CONSULTING_ITEMS.get(key),
                                (String) result.get("contactingway"));
                        taskList.put(CONSULTING_ITEMS.get(key), task);
                    } else if (key.contains(consultingFinancialPreviousKey)) {
                        Task task = initTaskData(dto,
                            taskName + "-" + key,
                            (String) result.get("note"),
                            CONSULTING_FINANCIAL,
                            (String) result.get("contactingway"));
                        taskList.put(CONSULTING_FINANCIAL, task);
                    }
                });
            }
        } catch (Exception err) {
            log.error("[OpportunityAgentRepo -> consultingItem] Error: core_uuid={}, opportunityId={}",
                    coreUuid,
                    opportunityId,
                    err
            );

            writeMigrationHistory("form_submission",
                    coreUuid,
                    String.valueOf(opportunityId),
                    "ghinhanhangmuctuvan",
                    "processing",
                    jsonData,
                    "error",
                    err.getMessage()
            );
        }

        return taskList;
    }

    private Map<String, Task> getHurdleForm(String uuid, Long id, String jsonData, TaskMigrationDto dto) {
        Map<String, Task> taskList = new HashMap<>();
        String taskName = "Ghi nhận và giải quyết rào cản";
        String completed = RESOLVE_HURDLE.keySet().stream().findFirst().get();
        try {
            var result = objectMapper.readValue(jsonData, Map.class);
            if (result.containsKey("project")) {
                var itemsMapProj = (List<Map<String, String>>) result.get("project");
                itemsMapProj.forEach(item -> {
                    item.forEach((key, value) ->
                    {
                        if (key.indexOf("chon_rao_can") > 0) {
                            Task task = initTaskData(dto,
                                    taskName + " về dự án",
                                    (String) result.get("note"),
                                    raocanMappingProject.get(key),
                                    (String) result.get("contactingway"));
                            taskList.put(raocanMappingProject.get(key), task);
                        }
                    });
                });
            }
            if (result.containsKey("investor")) {
                var itemsMapInvestor = (List<Map<String, String>>) result.get("investor");
                itemsMapInvestor.forEach(item -> {
                    item.forEach((key, value) ->
                    {
                        if (key.indexOf("chon_rao_can") > 0) {
                            Task task = initTaskData(dto,
                                    taskName + " về chủ đầu tư",
                                    (String) result.get("note"),
                                    raocanMappingInvestor.get(key),
                                    (String) result.get("contactingway"));
                            taskList.put(raocanMappingInvestor.get(key), task);
                        }
                    });
                });
            }
            if (result.containsKey("property")) {
                var itemsMapProduct = (List<Map<String, String>>) result.get("property");
                itemsMapProduct.forEach(item -> {
                    item.forEach((key, value) ->
                    {
                        if (key.indexOf("chon_rao_can") > 0) {
                            Task task = initTaskData(dto,
                                    taskName + " về sản phẩm bất động sản",
                                    (String) result.get("note"),
                                    raocanMappingProduct.get(key),
                                    (String) result.get("contactingway"));
                            taskList.put(raocanMappingProduct.get(key), task);
                        }
                    });
                });
            }
            if (result.containsKey("selling_policy_finance1")) {
                var itemsMapPolicy = (List<Map<String, String>>) result.get("selling_policy_finance1");
                itemsMapPolicy.forEach(item -> {
                    item.forEach((key, value) ->
                    {
                        if (key.indexOf("chon_rao_can") > 0) {
                            Task task = initTaskData(dto,
                                    taskName + " về CSBH và khả năng tài chính",
                                    (String) result.get("note"),
                                    raocanMappingPolicy.get(key),
                                    (String) result.get("contactingway"));
                            taskList.put(raocanMappingPolicy.get(key), task);
                        }
                    });
                });
            }

            if (result.containsKey("completed")) {
                var itemsMap = (Map<String, Boolean>) result.get("completed");
                if (itemsMap.containsKey(completed) && itemsMap.get(completed)) {
                    Task task = initTaskData(dto,
                            taskName + "-" + "đã giải quyết toàn bộ rào cản trước đây",
                            (String) result.get("note"),
                            RESOLVE_HURDLE.get(completed),
                            (String) result.get("contactingway"));
                    taskList.put(RESOLVE_HURDLE.get(completed), task);
                }
            }
        } catch (Exception err) {
            log.error("[OpportunityAgentRepo -> HurdleForm] Error: core_uuid={} id={} ",
                    uuid,
                    id,
                    err);
            writeMigrationHistory("form_submission",
                    uuid,
                    String.valueOf(id),
                    "raocan",
                    "processing",
                    jsonData,
                    "error",
                    err.getMessage()
            );
        }
        return taskList;
    }

    @Override
    public void writeMigrationHistory(String source,
                                      String leadUuid,
                                      String opportunityId,
                                      String type,
                                      String stage,
                                      Object data,
                                      String status,
                                      String message) {
        var input = new MongoSaleProcessMigrationHistory();
        input.setSource(source);
        input.setLeadUuid(leadUuid);
        input.setOpportunityId(opportunityId);
        input.setType(type);
        input.setStage(stage);
        try {
            input.setData(objectMapper.writeValueAsString(data));
            input.setMigrationStatus(status);
            input.setMessage(message);
        } catch (JsonProcessingException e) {
            input.setMigrationStatus("error");
            input.setMessage(e.getMessage());
        }

        historyRepository.save(input);
    }
}
