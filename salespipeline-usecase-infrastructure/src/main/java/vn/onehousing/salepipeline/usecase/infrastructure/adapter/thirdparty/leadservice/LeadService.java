package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.leadservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.ILeadService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.request.CreateLeadReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.request.UpdateLeadReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.leadservice.response.LeadServiceResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.RequestThirdpartyException;
import vn.onehousing.salepipeline.usecase.common.shared.model.Lead;

import java.time.Duration;

@Slf4j
@Component
public class LeadService implements ILeadService {

    private final WebClient webClient;

    public LeadService(@Lazy WebClient leadServiceClient) {
        this.webClient = leadServiceClient;
    }

    @Override
    public Lead get(String leadUUID) {
        Mono<LeadServiceResponse> monoResp = this.webClient
                .get()
                .uri("/v1/leads/" + leadUUID)
                .exchangeToMono(response -> {
                    return response.bodyToMono(LeadServiceResponse.class);
                })
                .doOnError(
                        error -> {
                            log.error("LeadService - [{}] - exception occured", leadUUID, error);
                        }
                )
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
        LeadServiceResponse resp = monoResp.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdpartyException("LeadService", "get", resp.getMeta().toString());
        return resp.getData();
    }

    @Override
    public Lead update(String leadUuid, UpdateLeadReq req) {
        Mono<LeadServiceResponse> monoResp = this.webClient
                .put()
                .uri("/v1/leads/" + leadUuid)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(req), UpdateLeadReq.class)
                .exchangeToMono(response -> response.bodyToMono(LeadServiceResponse.class))
                .doOnError(
                        error -> {
                            log.error("LeadService - update - [{}] - exception occured", leadUuid, error);
                        }
                )
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        LeadServiceResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdpartyException("LeadService", "update", resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public Lead updateMigration(String leadUuid, UpdateLeadReq req) {
        Mono<LeadServiceResponse> monoResp = this.webClient
                .put()
                .uri("/v1/leads/" + leadUuid + "/migration")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(req), UpdateLeadReq.class)
                .exchangeToMono(response -> response.bodyToMono(LeadServiceResponse.class))
                .doOnError(
                        error -> {
                            log.error("LeadService - update - [{}] - exception occured", leadUuid, error);
                        }
                )
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        LeadServiceResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdpartyException("LeadService", "update", resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public Lead create(CreateLeadReq req) {
        Mono<LeadServiceResponse> monoResp = this.webClient
                .post()
                .uri("/v1/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(req), CreateLeadReq.class)
                .exchangeToMono(response -> response.bodyToMono(LeadServiceResponse.class))
                .doOnError(
                        error -> {
                            log.error("LeadService - create - [{}] - exception occured", req.getLeadUuid(), error);
                        }
                )
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        LeadServiceResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdpartyException("LeadService", "create", resp.getMeta().toString());

        return resp.getData();
    }
}
