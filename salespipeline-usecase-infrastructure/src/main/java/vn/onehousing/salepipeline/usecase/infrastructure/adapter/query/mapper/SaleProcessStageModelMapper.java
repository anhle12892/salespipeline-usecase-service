package vn.onehousing.salepipeline.usecase.infrastructure.adapter.query.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessInputActivity;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessActivity;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.SaleProcessStage;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo.MongoSaleProcessActivity;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo.MongoSaleProcessStage;

@Mapper(componentModel = "spring")
public interface SaleProcessStageModelMapper {
    SaleProcessStage from(SaleProcessInputActivity input);

    @Mapping(source = "input.stage", target = "saleStage")
    MongoSaleProcessStage from(SaleProcessStage input);

    MongoSaleProcessActivity from(SaleProcessActivity input);

    @Mapping(source = "input.saleStage", target = "stage")
    SaleProcessStage from(MongoSaleProcessStage input);

    SaleProcessActivity from(MongoSaleProcessActivity input);

//    @Mapping(source = "input.key", target = "value")
//    @Mapping(source = "input.value", target = "title")
//    SaleStageSchemaResponse from(MongoStageSchema input);
}
