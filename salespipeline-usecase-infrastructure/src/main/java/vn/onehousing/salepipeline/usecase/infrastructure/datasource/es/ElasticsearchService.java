package vn.onehousing.salepipeline.usecase.infrastructure.datasource.es;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.VersionType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.ExistsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import vn.onehousing.common.elasticsearch.config.ElasticsearchIndexProperties;
import vn.onehousing.common.elasticsearch.config.IndexActionListener;

import javax.annotation.Nullable;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.client.RequestOptions.DEFAULT;

/**
 * @author ltnguyen on 06/10/2019
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ElasticsearchService {

    private final ElasticsearchIndexProperties properties;
    private final RestHighLevelClient esClient;
    private final ObjectMapper objectMapper;
    private final IndexActionListener indexActionListener;

    /**
     * @param index   - The index name
     * @param id      - The id of event if it exists, otherwise it can be the id of the message bus (depends on the business requirement)
     * @param version -
     * @param data    -
     */
    public void insert(final String index, String id, @Nullable Long version, final Object data) {
        log.info("Insert document with id = {} (index {}) and body = {} event = {}", id, index, data.toString(), data.getClass().getName());
        final ElasticsearchIndexProperties.IndexProperties indexProperties = properties.getMappings().get(index);
        if (indexProperties != null && indexProperties.isEnable()) {
            try {
                final String indexAlias = indexProperties.getIndexAlias();
                final String source = objectMapper.writeValueAsString(data);
                final IndexRequest indexRequest = new IndexRequest(indexAlias).id(id).source(source, XContentType.JSON);

                if (version != null) {
                    indexRequest.version(version).versionType(VersionType.EXTERNAL);
                }

                Long timeoutInMillis = indexProperties.getIndexQueryTimeoutInMillis();
                if (timeoutInMillis != null) {
                    indexRequest.timeout(TimeValue.timeValueMillis(timeoutInMillis));
                }
                try {
                    final IndexResponse response = esClient.index(indexRequest, DEFAULT);
                    if (HttpStatus.valueOf(response.status().getStatus()).isError()) {
                        throw new ElasticsearchException("Index request with id [{}] (index: {}) returns error {}", id, index, response);
                    } else {
                        log.info("Success insert Document with id: [{}] - index: [{}] - data: [{}]", id, index, source);
                    }
                } catch (Exception e) {
                    log.error("Exception occurs when indexing document id [{}] (index: {}) ", id, index, e);
                    throw new RuntimeException(e);
                }
            } catch (JsonProcessingException e) {
                log.error("Exception occurs when indexing document id [{}] (index: {}) ", id, index, e);
                throw new RuntimeException(e);
            }
        } else {
            if (indexProperties == null) {
                log.error("Document [{}]:{} received but there's no ES index mappings defined for index {}", id, data, index);
            } else {
                log.info("Document [{}] received but index [{}] is disabled", id, index);
            }
        }
    }

    public void delete(final String index, String id) {
        final ElasticsearchIndexProperties.IndexProperties indexProperties = properties.getMappings().get(index);
        if (indexProperties != null && indexProperties.isEnable()) {
            final String indexAlias = indexProperties.getIndexAlias();
            try {
                DeleteRequest req = new DeleteRequest();
                req.id(id);
                req.index(indexAlias);
                DeleteResponse resp = esClient.delete(req, DEFAULT);
                log.info("Success delete document id: [{}] - index: [{}] - status: [{}]", id, indexAlias, resp.status().getStatus());
            } catch (Exception e) {
                log.error(e.getMessage());
                e.printStackTrace();
            }
        } else {
            if (indexProperties == null) {
                log.error("Document [{}] received but there's no ES index mappings defined for index {}", id, index);
            } else {
                log.info("Document [{}] received but index [{}] is disabled", id, index);
            }
        }
        log.info("Success insert Document with id = {}", id);
    }

    public List<Object> searchActivity(String index, String uuid, String phoneNumber) throws IOException {
        final TermQueryBuilder byEntityUuid = new TermQueryBuilder("entity_uuid", uuid);
        final TermQueryBuilder byPhoneNumber = new TermQueryBuilder("phone_number", phoneNumber);


        final BoolQueryBuilder bool = new BoolQueryBuilder();
        if (StringUtils.isNotEmpty(uuid)) {
            bool.must(byEntityUuid);
        } else if (StringUtils.isNotEmpty(phoneNumber)) {
            bool.must(byPhoneNumber);
        } else
            return Collections.emptyList();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(bool);
        searchSourceBuilder.sort("last_modified_date", SortOrder.DESC);

        return buildSearchSource(index, searchSourceBuilder);
    }

    public List<Object> searchByAccountUuid(String index, String accountUuid) throws IOException {
        final TermQueryBuilder byEntityUuid = new TermQueryBuilder("account_uuid", accountUuid);


        final BoolQueryBuilder bool = new BoolQueryBuilder();
        if (StringUtils.isNotEmpty(accountUuid)) {
            bool.must(byEntityUuid);
        } else
            return Collections.emptyList();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(bool);
        searchSourceBuilder.sort("last_modified_date", SortOrder.DESC);

        return buildSearchSource(index, searchSourceBuilder);
    }

    public List<Object> searchByContactUuid(String index, String contactUuid) throws IOException {
        final TermQueryBuilder byEntityUuid = new TermQueryBuilder("contact_uuid", contactUuid);


        final BoolQueryBuilder bool = new BoolQueryBuilder();
        if (StringUtils.isNotEmpty(contactUuid)) {
            bool.must(byEntityUuid);
        } else
            return Collections.emptyList();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(bool);
        searchSourceBuilder.sort("last_modified_date", SortOrder.DESC);

        return buildSearchSource(index, searchSourceBuilder);
    }

    public Optional<Object> searchLeadInformation(String index, String leadUuid, String eventType) throws IOException {
        final TermQueryBuilder byEntityUuid = new TermQueryBuilder("lead_uuid", leadUuid);
        final TermQueryBuilder byEventType = new TermQueryBuilder("event", eventType);
        final BoolQueryBuilder bool = new BoolQueryBuilder()
            .must(byEntityUuid)
            .must(byEventType);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(bool);
        searchSourceBuilder.sort("last_modified_date", SortOrder.DESC);

        SearchRequest searchRequest = new SearchRequest();

        final ElasticsearchIndexProperties.IndexProperties indexProperties = properties.getMappings().get(index);
        if (indexProperties == null) {
            throw new IllegalArgumentException("Unknown index name: " + index);
        }

        final String indexAlias = indexProperties.getIndexAlias();

        searchRequest.indices(indexAlias).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        if (searchResponse.getHits().getHits().length == 0)
            return Optional.empty();

        SearchHit searchHit = searchResponse.getHits().getHits()[0];

        return Optional.of(searchHit.getSourceAsString());
    }

    public <T> T getById(String index, String uuid, Class<T> generic) throws IOException {
        final ElasticsearchIndexProperties.IndexProperties indexProperties = properties.getMappings().get(index);
        if (indexProperties == null) {
            throw new IllegalArgumentException("Unknown index name: " + index);
        }

        final String indexAlias = indexProperties.getIndexAlias();

        final GetRequest getRequest = new GetRequest(indexAlias, uuid);

        GetResponse getResponse = esClient.get(getRequest, DEFAULT);
        if (!getResponse.isExists()) {
            return null;
        }

        T resp = objectMapper.readValue(getResponse.getSourceAsString(), generic);

        return resp;
    }

    public Optional<Object> searchByChildData(String index, List<String> fields, List<String> values) throws IOException {
        if (fields.size() > values.size()) {
            throw new IllegalArgumentException("Lack of values for searching");
        }
        final BoolQueryBuilder bool = new BoolQueryBuilder();
        for (int i = 0; i < fields.size(); i++) {
            bool.must(new TermQueryBuilder(fields.get(i), values.get(i)));
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(bool);
        searchSourceBuilder.sort("last_modified_date", SortOrder.DESC);
        SearchRequest searchRequest = new SearchRequest();

        final ElasticsearchIndexProperties.IndexProperties indexProperties = properties.getMappings().get(index);
        if (indexProperties == null) {
            throw new IllegalArgumentException("Unknown index name: " + index);
        }

        final String indexAlias = indexProperties.getIndexAlias();

        searchRequest.indices(indexAlias).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        if (searchResponse.getHits().getHits().length == 0)
            return Optional.empty();

        SearchHit searchHit = searchResponse.getHits().getHits()[0];

        return Optional.of(searchHit.getSourceAsString());
    }

    public List<Object> searchPagingHistories(String index, String uuid, int page, int size) throws IOException {
        final TermQueryBuilder byEntityUuid = new TermQueryBuilder("lead_uuid", uuid);
        final BoolQueryBuilder bool = new BoolQueryBuilder().must(byEntityUuid);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(bool);
        searchSourceBuilder.sort("created_time", SortOrder.DESC);
        //searchSourceBuilder.from(page * size);
        searchSourceBuilder.size(size);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        return buildSearchSource(index, searchSourceBuilder);
    }

    private List<Object> buildSearchSource(String index, SearchSourceBuilder searchSourceBuilder) throws IOException {
        SearchRequest searchRequest = new SearchRequest();

        final ElasticsearchIndexProperties.IndexProperties indexProperties = properties.getMappings().get(index);
        if (indexProperties == null) {
            throw new IllegalArgumentException("Unknown index name: " + index);
        }

        final String indexAlias = indexProperties.getIndexAlias();

        searchRequest.indices(indexAlias).source(searchSourceBuilder);
        searchRequest.indicesOptions(IndicesOptions.LENIENT_EXPAND_OPEN);

        SearchResponse searchResponse = esClient.search(searchRequest, DEFAULT);
        if (searchResponse.getHits().getHits().length == 0)
            return Collections.emptyList();

        SearchHit[] searchHit = searchResponse.getHits().getHits();
        if (searchHit.length > 0) {
            List<Object> result = new ArrayList<>(searchHit.length);
            for (int i = 0; i < searchHit.length; i++) {
                result.add(searchHit[i].getSourceAsString());
            }
            return result;
        }
        return Collections.emptyList();
    }

    public List<Object> findAllAssignedLeadByLastModifiedDate(String index, Instant lastModifiedDate) throws IOException {

        final BoolQueryBuilder bool = new BoolQueryBuilder();
        bool.must(new ExistsQueryBuilder("assigned_user_uuid"));
        bool.must(new ExistsQueryBuilder("lead_uuid"));
        bool.mustNot(new TermQueryBuilder("assigned_user_uuid", ""));
        if (Objects.nonNull(lastModifiedDate)) {
            var fromDateTime = lastModifiedDate.toEpochMilli();
            bool.must(QueryBuilders.rangeQuery("last_modified_date")
                .lt(fromDateTime));
        }

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(bool);
        searchSourceBuilder.size(100);
        searchSourceBuilder.sort("last_modified_date", SortOrder.DESC);
        log.info("[Get assigned lead from aggregates by lastModifiedDate search = {}", searchSourceBuilder);

        return buildSearchSource(index, searchSourceBuilder);
    }


}
