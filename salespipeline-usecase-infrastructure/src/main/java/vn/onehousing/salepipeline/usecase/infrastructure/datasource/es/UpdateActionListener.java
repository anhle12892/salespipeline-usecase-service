package vn.onehousing.salepipeline.usecase.infrastructure.datasource.es;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.support.replication.ReplicationResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UpdateActionListener implements ActionListener<UpdateResponse> {

    @Override
    public void onResponse(UpdateResponse indexResponse) {
        final String index = indexResponse.getIndex();
        final String id = indexResponse.getId();
        final long version = indexResponse.getVersion();
        if (indexResponse.getResult() == DocWriteResponse.Result.CREATED) {
            log.info("Document id [{}] is created successfully in index [{}] with version [{}]", id, index, version);
        } else if (indexResponse.getResult() == DocWriteResponse.Result.UPDATED) {
            log.info("Document id [{}] is updated successfully in index [{}] with version [{}]", id, index, version);
        }
        final ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();
        if (shardInfo.getFailed() > 0) {
            for (ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()) {
                log.error("Fail to index document id [{}] in index [{}], version [{}], shard [{}], reason [{}]", id, index, version, failure.shardId(), failure.reason());
            }
        }
    }

    @Override
    public void onFailure(Exception ex) {
        // TODO maintain a queue to reindex latter
        if (ex instanceof ElasticsearchException) {
            final ElasticsearchException e = (ElasticsearchException) ex;
            log.error("Error when indexing document id [{}] in index [{}], shard [{}]", e.getResourceId(),
                    e.getIndex(), e.getShardId(), ex);
        } else {
            log.error("Error when indexing a document", ex);
        }
    }
}
