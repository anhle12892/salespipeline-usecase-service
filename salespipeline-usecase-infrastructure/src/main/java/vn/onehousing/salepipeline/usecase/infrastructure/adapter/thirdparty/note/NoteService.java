package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.note;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.common.elasticsearch.service.RestHighLevelClientWrapper;
import vn.onehousing.salepipeline.usecase.business.thirdparty.note.INoteService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.note.request.NoteCreateDto;
import vn.onehousing.salepipeline.usecase.business.thirdparty.note.request.NoteUpdateAudit;
import vn.onehousing.salepipeline.usecase.business.thirdparty.note.respone.NoteResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.RequestThirdpartyException;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
public class NoteService implements INoteService {
    private final WebClient webClient;
    private final RestHighLevelClientWrapper restHighLevelClientWrapper;
    private final ObjectMapper objectMapper;

    public NoteService(@Lazy WebClient noteServiceClient,
                       @Lazy RestHighLevelClientWrapper documentServiceEsClient,
                       ObjectMapper objectMapper) {
        this.webClient = noteServiceClient;
        this.restHighLevelClientWrapper = documentServiceEsClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public Note createNote(NoteCreateDto req) {
        try {
            log.info("Create note request {}", objectMapper.writeValueAsString(req));
        } catch (JsonProcessingException ex) {
            log.error("Error ", ex);
        }
        Mono<NoteResponse> monoResp = this.webClient
            .post()
            .uri("/v1/notes")
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), NoteCreateDto.class)
            .exchangeToMono(response -> response.bodyToMono(NoteResponse.class))
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        NoteResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            throw new RequestThirdpartyException("NoteService", "createNote", resp.getMeta().toString());
        }

        return resp.getData();
    }

    @Override
    public Note createNoteWithoutEvent(NoteCreateDto req) {
        try {
            log.info("Create note request {}", objectMapper.writeValueAsString(req));
        } catch (JsonProcessingException ex) {
            log.error("Error ", ex);
        }
        Mono<NoteResponse> monoResp = this.webClient
            .post()
            .uri("/v1/notes/without-event")
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), NoteCreateDto.class)
            .exchangeToMono(response -> response.bodyToMono(NoteResponse.class))
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        NoteResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            throw new RequestThirdpartyException("NoteService", "createNote", resp.getMeta().toString());
        }

        return resp.getData();
    }

    @Override
    public Note updateNoteWithoutEvent(String noteUuid, NoteCreateDto req) {
        Mono<NoteResponse> monoResp = this.webClient
            .put()
            .uri("/v1/notes/" + noteUuid + "/without-event")
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), NoteCreateDto.class)
            .exchangeToMono(response -> response.bodyToMono(NoteResponse.class))
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        NoteResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            throw new RequestThirdpartyException("NoteService", "updateNote", resp.getMeta().toString());
        }

        return resp.getData();
    }

    @Override
    public Note updateNote(String noteUuid, NoteCreateDto req) {
        Mono<NoteResponse> monoResp = this.webClient
            .put()
            .uri("/v1/notes/" + noteUuid)
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), NoteCreateDto.class)
            .exchangeToMono(response -> response.bodyToMono(NoteResponse.class))
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        NoteResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            throw new RequestThirdpartyException("NoteService", "updateNote", resp.getMeta().toString());
        }

        return resp.getData();
    }

    @Override
    public Optional<String> getNoteUuidByParentUuidAndParentType(String parentUuid, String parentType) {
        final TermQueryBuilder parentUuidQuery = QueryBuilders.termQuery("parent_uuid", parentUuid);
        final TermQueryBuilder parentTypeQuery = QueryBuilders.termQuery("parent_type", parentType);
        final BoolQueryBuilder query = QueryBuilders.boolQuery().must(parentUuidQuery).must(parentTypeQuery);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().query(query);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        // TODO LTN config
        try {
            final RestHighLevelClient restHighLevelClient = restHighLevelClientWrapper.getRestHighLevelClient();
            final SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            SearchHit searchHit = search.getHits().getHits()[0];
            var note = objectMapper.readValue(searchHit.getSourceAsString(),
                Note.class);
            return Optional.of(note.getNoteUuid());
        } catch (IOException e) {
            log.error("Error while query elasticsearch ", e);
        }
        return Optional.empty();
    }

    @Override
    public List<Note> getNotesByParentTypeAndOwnerType(String parentType, String ownerType, String parentUuid) {

        final TermQueryBuilder parentTypeQuery = QueryBuilders.termQuery("parent_type", parentType);
        final TermQueryBuilder ownerTypeQuery = QueryBuilders.termQuery("owner_type", ownerType);

        BoolQueryBuilder query = QueryBuilders.boolQuery()
            .must(parentTypeQuery)
            .must(ownerTypeQuery);
        if (StringUtils.hasText(parentUuid)) {
            final TermQueryBuilder parentUuidQuery = QueryBuilders.termQuery("parent_uuid", parentUuid);
            query.must(parentUuidQuery);
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().query(query).size(1000);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        log.info("[SearchRequestMcNote] query request {} ", searchRequest);
        try {
            List<Note> notes = new ArrayList<>();
            final RestHighLevelClient restHighLevelClient = restHighLevelClientWrapper.getRestHighLevelClient();
            final SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            for (SearchHit hit : searchResponse.getHits().getHits()) {
                notes.add(objectMapper.readValue(hit.getSourceAsString(), Note.class));
            }
            log.info("[SearchRequestMcNote] total query data: {}", notes.size());

            return notes;
        } catch (IOException e) {
            log.error("Error while query note elasticsearch ", e);
        }
        return new ArrayList<>();
    }

    public Optional<Note> getNoteByUniqueData(String parentUuid,
                                              String parentType,
                                              String ownerType,
                                              String ownerUuid,
                                              List<String> ignoreUuids) {
        final TermQueryBuilder parentUuidQuery = QueryBuilders.termQuery("parent_uuid", parentUuid);
        final TermQueryBuilder parentTypeQuery = QueryBuilders.termQuery("parent_type", parentType);
        final TermQueryBuilder ownerTypeQuery = QueryBuilders.termQuery("owner_type", ownerType);
        final BoolQueryBuilder query = QueryBuilders
            .boolQuery()
            .must(parentUuidQuery)
            .must(parentTypeQuery)
            .must(ownerTypeQuery);
        if (Objects.nonNull(ownerUuid)) {
            final TermQueryBuilder ownerUuidQuery = QueryBuilders.termQuery("owner_uuid", ownerUuid);
            query.must(ownerUuidQuery);
        }
        if (Objects.nonNull(ignoreUuids) && ignoreUuids.size() > 0) {
            final var ignoreUuidQuery = QueryBuilders.termsQuery("note_uuid", ignoreUuids);
            query.mustNot(ignoreUuidQuery);
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().query(query);
        searchSourceBuilder.sort("last_modified_date", SortOrder.DESC);
        log.info("[ES] filter input = {}", searchSourceBuilder.toString());

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);

        try {
            final RestHighLevelClient restHighLevelClient = restHighLevelClientWrapper.getRestHighLevelClient();
            final SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            if (search.getHits().getHits().length > 0) {
                SearchHit searchHit = search.getHits().getHits()[0];
                var note = objectMapper.readValue(searchHit.getSourceAsString(),
                    Note.class);
                return Optional.of(note);
            }
        } catch (IOException e) {
            log.error("Error while query elasticsearch ", e);
        }
        return Optional.empty();
    }

    public String updateNoteAuditTime(String noteUuid, NoteUpdateAudit req) {
        Mono<String> monoResp = this.webClient
            .put()
            .uri("/v1/notes/migrate/date-migration/" + noteUuid)
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), NoteUpdateAudit.class)
            .exchangeToMono(response -> response.bodyToMono(String.class))
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        return monoResp.block();
    }

}
