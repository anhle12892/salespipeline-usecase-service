package vn.onehousing.salepipeline.usecase.infrastructure.datasource.mysql.repo;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.onehousing.salepipeline.usecase.business.usecase.createhistoryuc.response.OpportunityHistoryRes;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.ActEntityType;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;
import vn.onehousing.salepipeline.usecase.domain.model.saleprocess.migration.note.OpportunityHistoryRepository;
import vn.onehousing.salepipeline.usecase.infrastructure.datasource.mongo.migration.MongoSaleProcessMigrationHistoryRepository;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
@AllArgsConstructor
@Transactional
@Slf4j
public class OpportunityHistoryDao extends JdbcDaoSupport implements OpportunityHistoryRepository {
    private final ObjectMapper objectMapper;
    private MongoSaleProcessMigrationHistoryRepository historyRepository;

    @Autowired
    public OpportunityHistoryDao(DataSource dataSource,
                                 MongoSaleProcessMigrationHistoryRepository historyRepository,
                                 ObjectMapper objectMapper) {
        this.setDataSource(dataSource);
        this.historyRepository = historyRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public List<Note> getAllNotes(String coreUuid, String migratedDate) {
        return this.getJpaNotes(coreUuid, migratedDate);
    }

    @Override
    public List<OpportunityHistoryRes> getAllOpportunityHistory(String coreUuid) throws SQLException {
        return this.getJpaOpportunityHistory(coreUuid);
    }

    private List<Note> getJpaNotes(String uuid, String migratedDate) {
        uuid = StringUtils.trimToEmpty(uuid);
        migratedDate = StringUtils.trimToEmpty(migratedDate);
        String oppHistorySql = ""
                + "select oh.content as content, "
                + "case when oh.note_source in ('TRANSACTION', 'ACN') then 'CUSTOMER' "
                + "when op.revoked_reason is not null then 'BACK_OFFICE' "
                + "else oh.note_source end as ownerType, "
                + "oh.create_time as createdTime, "
                + "oh.update_time as lastModifiedTime, "
                + "case when oh.note_source in ('AGENT') then a.uuid "
                + "when oh.note_source in ('CUSTOMER', 'TRANSACTION') then op.core_uuid "
                + "else null end as ownerUuid, "
                + "case when oh.note_source in ('AGENT') then a.email "
                + "when oh.note_source in ('CUSTOMER', 'TRANSACTION', 'ACN') then op.email "
                + "else null end as ownerEmail, "
                + "'LEAD' as parentType, "
                + "case when op.revoked_reason is not null then op.revoked_reason "
                + "else oh.content end as content, "
                + "op.core_uuid as parentUuid, "
                + "op.id as id "
                + "from opportunity_activities oh "
                + "inner join opportunities op on oh.opportunity_id = op.id "
                + "inner join agent_opportunity ao on ao.opportunity_id = op.id "
                + "inner join agents a on ao.agent_id = a.id "
                + "where oh.record_type in ('AGENT_NOTE', 'CUSTOMER_MSG', 'CONTACT_CENTER_NOTE', 'REVOKE_OPP', 'BACK_OFFICE') "
                + "and ('" + uuid + "' = '' or op.core_uuid = '" + uuid + "')"
                + "and ('" + migratedDate + "' = '' or DATE(oh.create_time) <= '" + migratedDate + "')";

        return this.getJdbcTemplate().query(oppHistorySql, (rs, row) ->
                new Note(null,
                        null,
                        null,
                        rs.getString("ownerUuid"),
                        rs.getString("parentUuid"),
                        rs.getString("parentType"),
                        "Ghi chú",
                        rs.getString("content"),
                        rs.getString("ownerType"),
                        rs.getTimestamp("createdTime").toInstant(),
                        null,
                        rs.getTimestamp("lastModifiedTime").toInstant(),
                        null,
                        rs.getString("ownerEmail")));
    }

    private List<OpportunityHistoryRes> getJpaOpportunityHistory(String coreUuid) throws SQLException {
        List<OpportunityHistoryRes> ls = new ArrayList<>();
        try {
            coreUuid = StringUtils.trimToEmpty(coreUuid);
            String oppHistorySql = ""
                    + "select op.core_uuid as coreUuid, "
                    + "oh.opportunity_source as opportunitySource, "
                    + "oh.note_source as noteSource, "
                    + "oh.content as content, "
                    + "oh.suggestion_notes as suggestionNotes, "
                    + "oh.record_type as recordType, "
                    + "oh.buying_need_type as buyingNeedType, "
                    + "oh.landing_page_url as landingPageUrl, "
                    + "oh.create_time as createTime, "
                    + "oh.update_time as updateTime "
                    + "from opportunity_activities oh "
                    + "inner join opportunities op on oh.opportunity_id = op.id "
                    + "where 1=1 "
                    + "and ('" + coreUuid + "' = '' or op.core_uuid = '" + coreUuid + "')";

            List<Map<String, Object>> rs = this.getJdbcTemplate().queryForList(oppHistorySql);
            rs.forEach(item -> ls.add(new OpportunityHistoryRes(null,
                    Objects.toString(item.get("coreUuid")),
                    ActEntityType.LEAD,
                    null,
                    null,
                    Objects.nonNull(item.get("coreUuid")) ? Objects.toString(item.get("coreUuid")) : null,
                    Objects.nonNull(item.get("opportunitySource")) ? Objects.toString(item.get("opportunitySource")) : null,
                    Objects.nonNull(item.get("content")) ? Objects.toString(item.get("content")) : null,
                    Objects.nonNull(item.get("noteSource")) ? Objects.toString(item.get("noteSource")) : null,
                    Objects.nonNull(item.get("recordType")) ? Objects.toString(item.get("recordType")) : null,
                    null,
                    Objects.nonNull(item.get("suggestionNotes")) ?
                            Stream.of(item.get("suggestionNotes"))
                                    .map(Object::toString)
                                    .collect(Collectors.toList()) : null,
                    Objects.nonNull(item.get("buyingNeedType")) ? Objects.toString(item.get("buyingNeedType")) : null,
                    Objects.nonNull(item.get("landingPageUrl")) ? Objects.toString(item.get("landingPageUrl")) : null,
                    ((LocalDateTime) item.get("createTime")).toInstant(ZoneOffset.UTC),
                    ((LocalDateTime) item.get("createTime")).toEpochSecond(ZoneOffset.UTC)
            )));
        } catch (Exception e) {
            throw new SQLException(e);
        }
        return ls;
    }
}
