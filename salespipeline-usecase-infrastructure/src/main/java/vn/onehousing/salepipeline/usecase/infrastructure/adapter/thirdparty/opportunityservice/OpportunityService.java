package vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.opportunityservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.IOpportunityService;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.request.CreateOpportunityReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.request.UpdateOpportunityReq;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.response.GetOpportunityAttributeConfigsResp;
import vn.onehousing.salepipeline.usecase.business.thirdparty.opportunityservice.response.OpportunityServiceResp;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.HousingErrors;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.HousingException;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.LeadNotExistException;
import vn.onehousing.salepipeline.usecase.common.shared.exceptions.RequestThirdpartyException;
import vn.onehousing.salepipeline.usecase.common.shared.model.AttributeConfig;
import vn.onehousing.salepipeline.usecase.common.shared.model.Opportunity;

import java.time.Duration;
import java.util.Optional;

@Slf4j
@Component
public class OpportunityService implements IOpportunityService {

    private final WebClient webClient;
    private final ObjectMapper mapper;

    public OpportunityService(@Lazy WebClient opportunityServiceClient, ObjectMapper mapper) {
        this.webClient = opportunityServiceClient;
        this.mapper = mapper;
    }

    @Override
    public Opportunity create(CreateOpportunityReq req) throws JsonProcessingException {
        Mono<OpportunityServiceResp> monoResp = this.webClient.post()
                .uri("/v1/opportunities")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(req), CreateOpportunityReq.class)
                .exchangeToMono(response -> response.bodyToMono(OpportunityServiceResp.class))
                .switchIfEmpty(Mono.error(new HousingException(HousingErrors.CREATE_OPPORTUNITY_ERROR)))
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        OpportunityServiceResp resp = monoResp.block();

        System.out.println(mapper.writeValueAsString(req));
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdpartyException("OpportunityService","create",resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public Opportunity update(String opportunityUuid, UpdateOpportunityReq req) {
        Mono<OpportunityServiceResp> monoResp = this.webClient.put()
                .uri("/v1/opportunities/" + opportunityUuid)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(req), UpdateOpportunityReq.class)
                .exchangeToMono(response -> response.bodyToMono(OpportunityServiceResp.class))
                .switchIfEmpty(Mono.error(new HousingException(HousingErrors.CREATE_OPPORTUNITY_ERROR)))
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        OpportunityServiceResp resp = monoResp.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdpartyException("OpportunityService","update",resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public Opportunity updateMigration(String opportunityUuid, UpdateOpportunityReq req) {
        Mono<OpportunityServiceResp> monoResp = this.webClient.put()
                .uri("/v1/opportunities/" + opportunityUuid + "/migration")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(req), UpdateOpportunityReq.class)
                .exchangeToMono(response -> response.bodyToMono(OpportunityServiceResp.class))
                .switchIfEmpty(Mono.error(new HousingException(HousingErrors.CREATE_OPPORTUNITY_ERROR)))
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        OpportunityServiceResp resp = monoResp.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdpartyException("OpportunityService","update",resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public Opportunity get(String opportunityUuid) {
        //TODO
        Mono<OpportunityServiceResp> monoResp = this.webClient
                .get()
                .uri("/v1/opportunities/" + opportunityUuid)
                .exchangeToMono(response -> {
                    return response.bodyToMono(OpportunityServiceResp.class);
                })
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        OpportunityServiceResp resp = monoResp.block();
        if (resp.getMeta().getCode().equals("404002"))
            return null;
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdpartyException("OpportunityService","get",resp.getMeta().toString());
        return resp.getData();
    }

    @Override
    public AttributeConfig[] getAttributeConfigs() {
        Mono<GetOpportunityAttributeConfigsResp> monoResp = this.webClient
                .get()
                .uri("/v1/attributes/vn.onehousing.opportunity.domain.aggreate.opportunity.Opportunity")
                .exchangeToMono(response -> {
                    return response.bodyToMono(GetOpportunityAttributeConfigsResp.class);
                })
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        GetOpportunityAttributeConfigsResp resp = monoResp.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdpartyException("OpportunityService","getAttributeConfigs",resp.getMeta().toString());

        return resp.getData();
    }
}
