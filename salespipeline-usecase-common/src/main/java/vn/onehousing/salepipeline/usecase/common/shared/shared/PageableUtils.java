package vn.onehousing.salepipeline.usecase.common.shared.shared;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public interface PageableUtils {
    int MAX_ITEM_PER_PAGE = 1000;

    static Pageable of(int page, int size) {
        return PageRequest.of(page, Math.min(size, MAX_ITEM_PER_PAGE));
    }

    static Pageable of(int page, int size, Sort sort) {
        return PageRequest.of(page, Math.min(size, MAX_ITEM_PER_PAGE), sort);
    }
}
