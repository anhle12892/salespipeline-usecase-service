package vn.onehousing.salepipeline.usecase.common.shared.exceptions;

public class RequestThirdpartyException extends HousingException {

    public RequestThirdpartyException(String service, String action,String message) {
        super(
                HousingErrors.REQUEST_THIRDPARTY_EXCEPTION,
            String.format("Service: [%s] - Action: [%s] - Response: [%s]", service, action, message)
        );
    }
}
