package vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess;

public interface SaleProcessTaskWorkflow {
    String CONTACTED_WITH_COLLABORATOR = "CONTACTED_WITH_COLLABORATOR";
    String MARKED_ANY_RESULT_OF_CONTACT_WITH_CUSTOMER = "MARKED_ANY_RESULT_OF_CONTACT_WITH_CUSTOMER";

    String UPDATED_ANY_INFORMATION_OF_OPPORTUNITY = "UPDATED_ANY_INFORMATION_OF_OPPORTUNITY";

    String UPDATED_BUYING_DEMAND_AND_PROJECT_DEMAND = "UPDATED_BUYING_DEMAND_AND_PROJECT_DEMAND";

    String CREATED_AN_APPOINTMENT = "CREATED_AN_APPOINTMENT";

    String CREATED_NOTE_WITH_FORM_SUCCESS_APPOINTMENT = "CREATED_NOTE_WITH_FORM_SUCCESS_APPOINTMENT";
    String CONSULTING_ABOUT_PROJECT = "CONSULTING_ABOUT_PROJECT";
    String CONSULTING_ABOUT_INVESTOR = "CONSULTING_ABOUT_INVESTOR";
    String CONSULTING_ABOUT_PRODUCT = "CONSULTING_ABOUT_PRODUCT";
    String CONSULTING_ABOUT_POLICY = "CONSULTING_ABOUT_POLICY";
    String CONSULTING_ABOUT_FINANCIAL = "CONSULTING_ABOUT_FINANCIAL";

    // opportunity attributes updated
    String OPPORTUNITY_UPDATED_MAIN_VIEWS = "OPPORTUNITY_UPDATED_MAIN_VIEWS";
    String OPPORTUNITY_UPDATED_PROPERTY_TYPE = "OPPORTUNITY_UPDATED_PROPERTY_TYPE";
    String OPPORTUNITY_UPDATED_NB_BEDROOM = "OPPORTUNITY_UPDATED_NB_BEDROOM";
    String OPPORTUNITY_UPDATED_BUDGET_LIMIT = "OPPORTUNITY_UPDATED_BUDGET_LIMIT";
    String OPPORTUNITY_UPDATED_PROPERTY_RANGE_FLOOR = "OPPORTUNITY_UPDATED_PROPERTY_RANGE_FLOOR";
    String OPPORTUNITY_UPDATED_BALCONY_DIRECTIONS = "OPPORTUNITY_UPDATED_BALCONY_DIRECTIONS";
    String OPPORTUNITY_UPDATED_MORTGAGE_NEED = "OPPORTUNITY_UPDATED_MORTGAGE_NEED";

    String MARK_RESOLVED_ALL_HURDLES = "MARK_RESOLVED_ALL_HURDLES";
    String MARK_CUSTOMER_ACCEPT_WITH_CONSULTANT_RECOMMENDATION = "MARK_CUSTOMER_ACCEPT_WITH_CONSULTANT_RECOMMENDATION";

    // booking status
    String AGENT_SENT_BOOKING_TO_PARTNER = "SENT_BOOKING_TO_PARTNER";

    String CREATED_NOTE_WITH_FORM_UPDATE_BOOKING = "CREATED_NOTE_WITH_FORM_UPDATE_BOOKING";

    // booking status
    String PARTNER_UPDATED_BOOKING_STATUS_TO_SUCCESS = "PARTNER_UPDATED_BOOKING_STATUS_TO_SUCCESS";

    String ASSIGNED = "ASSIGNED";
}
