package vn.onehousing.salepipeline.usecase.common.shared.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BankAccount {
    String id;
    @JsonProperty("account_number")
    String accountNumber;
    @JsonProperty("account_holder")
    String accountHolder;
    @JsonProperty("bank_name")
    String bankName;
    @JsonProperty("region")
    String region;
    @JsonProperty("bank_branch")
    String bankBranch;
    @JsonProperty("bank_code")
    String bankCode;
}
