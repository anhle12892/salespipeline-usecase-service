package vn.onehousing.salepipeline.usecase.common.infrastructure.eventbus;

import org.apache.logging.log4j.util.Strings;

import java.util.Map;

public abstract class ApplicationEvent {

    protected Map<String, String> payload;

    public abstract String getType();

    public String getPayloadValue(String key) {
        if (this.payload.containsKey(key)) {
            return this.payload.get(key);
        }
        return Strings.EMPTY;
    }
}
