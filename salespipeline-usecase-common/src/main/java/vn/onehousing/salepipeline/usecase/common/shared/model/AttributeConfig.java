package vn.onehousing.salepipeline.usecase.common.shared.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AttributeConfig {
    private String fieldName;
    private String dataType;
}
