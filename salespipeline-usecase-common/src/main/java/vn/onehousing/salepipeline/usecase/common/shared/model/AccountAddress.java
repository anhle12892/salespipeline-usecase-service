package vn.onehousing.salepipeline.usecase.common.shared.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class AccountAddress {

    String addressUuid;

    String provinceName;

    String provinceCode;

    String districtName;

    String districtCode;

    String wardName;

    String wardCode;

    String phoneNumber;

    String address;

    String addressName;

    String contactName;
}
