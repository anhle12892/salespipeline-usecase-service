package vn.onehousing.salepipeline.usecase.common.shared.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Ward {
    @JsonProperty("ward_id")
    private String wardId;
    @JsonProperty("ward_name")
    private String wardName;
    @JsonProperty("ward_code")
    private String wardCode;
}
