package vn.onehousing.salepipeline.usecase.common.shared.exceptions;

public class LeadNotExistException extends HousingException {
    public LeadNotExistException(String message) {

        super(HousingErrors.LEAD_NOT_EXIST, message);
    }
}
