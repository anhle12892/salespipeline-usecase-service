package vn.onehousing.salepipeline.usecase.common.shared.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Province {
    @JsonProperty("province_id")
    private String provinceId;
    @JsonProperty("province_name")
    private String provinceName;
    @JsonProperty("province_code")
    private String provinceCode;
}
