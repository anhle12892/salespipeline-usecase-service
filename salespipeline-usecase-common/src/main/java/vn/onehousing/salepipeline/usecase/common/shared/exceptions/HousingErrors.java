package vn.onehousing.salepipeline.usecase.common.shared.exceptions;

import org.springframework.http.HttpStatus;


public class HousingErrors {

    /**
     * 400
     */
    public static final HousingBusinessError INVALID_PARAMETERS = new HousingBusinessError(400004, "Invalid parameters", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError MAX_FILE_SIZE = new HousingBusinessError(400, "File size exceeded the maximum size", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError LEAD_NOT_EXIST = new HousingBusinessError(404002, "Lead is not exist", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError SALESPIPELINE_NOT_EXIST = new HousingBusinessError(404002, "SalesPipeline is not exist", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError MISSING_ACCOUNT_CONTACT = new HousingBusinessError(400006, "Missing account/contact", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError BAD_RESOURCE = new HousingBusinessError(400002, "resource is bad", HttpStatus.BAD_REQUEST);

    /**
     * 401
     */
    public static final HousingBusinessError USER_NOT_UNAUTHORIZED = new HousingBusinessError(401, "User is unauthorized", HttpStatus.UNAUTHORIZED);

    /**
     * 403
     */
    public static final HousingBusinessError FORBIDDEN_ERROR = new HousingBusinessError(403003, "You don not have any permissions to access this resource", HttpStatus.FORBIDDEN);

    /**
     * 404
     */
    public static final HousingBusinessError LEAD_NOT_FOUND = new HousingBusinessError(404001, "Opportunity is not found", HttpStatus.NOT_FOUND);
    public static final HousingBusinessError OPPORTUNITY_NOT_FOUND = new HousingBusinessError(404002, "Lead is not found", HttpStatus.NOT_FOUND);
    public static final HousingBusinessError OPPORTUNITY_SALE_STAGE_NOT_FOUND = new HousingBusinessError(404003, "Sale stage is not found for: ", HttpStatus.NOT_FOUND);

    /**
     * 500
     */
    public static final HousingBusinessError INTERNAL_SERVER_ERROR = new HousingBusinessError(500002, "Internal server error", HttpStatus.INTERNAL_SERVER_ERROR);
    public static final HousingBusinessError REQUEST_THIRDPARTY_EXCEPTION = new HousingBusinessError(500003, "error occur when request to thirdparty", HttpStatus.INTERNAL_SERVER_ERROR);
    public static final HousingBusinessError CREATE_ACCOUNT_ERROR = new HousingBusinessError(500002, "create account error", HttpStatus.INTERNAL_SERVER_ERROR);
    public static final HousingBusinessError CREATE_CONTACT_ERROR = new HousingBusinessError(500002, "create contact error", HttpStatus.INTERNAL_SERVER_ERROR);
    public static final HousingBusinessError CREATE_OPPORTUNITY_ERROR = new HousingBusinessError(500002, "create account error", HttpStatus.INTERNAL_SERVER_ERROR);
    public static final HousingBusinessError UPDATE_SALES_PIPELINE_ERROR = new HousingBusinessError(500002, "update salespipeline error", HttpStatus.INTERNAL_SERVER_ERROR);


    /**
     * 405
     */
    public static final HousingBusinessError METHOD_NOT_ALLOWED = new HousingBusinessError(405, "Method Not Allowed", HttpStatus.METHOD_NOT_ALLOWED);

    /**
     * 415
     */
    public static final HousingBusinessError UNSUPPORTED_MEDIA_TYPE = new HousingBusinessError(415, "Unsupported Media Type", HttpStatus.UNSUPPORTED_MEDIA_TYPE);



    private HousingErrors() {
    }
}
