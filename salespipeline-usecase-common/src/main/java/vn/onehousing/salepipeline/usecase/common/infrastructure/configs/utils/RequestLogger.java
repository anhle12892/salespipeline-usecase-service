package vn.onehousing.salepipeline.usecase.common.infrastructure.configs.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.RequestHeaderConstant;
import javax.servlet.http.HttpServletRequest;

@Slf4j
public class RequestLogger {
    private static final String VERSION = "version";
    private static final String HTTP_STATUS = "httpStatus";
    private static final String URL = "url";
    private static final String METHOD = "method";
    private static final String RESPONSE = "response";

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private RequestLogger() {
    }

    public static final void error(WebRequest webRequest,
                                   HttpStatus httpStatus,
                                   BaseResponse.Metadata errResp,
                                   Throwable ex,
                                   String version) {

        int status = httpStatus.value();
        var msg = createMsg(webRequest, status, errResp, version);
        var logMsg = String.format("Failed Request %s - %s - %s - %s - %s",
            msg.get(RequestHeaderConstant.X_CLIENT_IP),
            msg.get(RequestHeaderConstant.X_REQUEST_ID),
            status,
            msg.get(METHOD),
            msg.get(URL));

        if (status < 400) {
            log.info(logMsg, ex);
        } else if (status < 500) {
            log.warn(logMsg, ex);
        } else {
            log.error(logMsg, ex);
        }
    }

//    public static void info(WebRequest webRequest, long startTime, String version) {
//        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();
//        var httpStatus = request.get;
//        if (httpStatus == null) {
//            log.warn("Can not get httpStatus code!");
//            return;
//        }
//        int status = httpStatus.value();
//        if (status < 300) {
//            JsonNode msg = createMsg(webRequest, status, version);
//            log.info("Successful Request {} - {} - {} - {} - {} - {} ms",
//                    msg.get(RequestHeaderConstant.X_CLIENT_IP),
//                    msg.get(RequestHeaderConstant.X_REQUEST_ID),
//                    status,
//                    msg.get(METHOD),
//                    msg.get(URL),
//                    (System.currentTimeMillis() - startTime) / 1000.0);
//        }
//    }

    private static ObjectNode createMsg(WebRequest webRequest,
                                        int httpStatus,
                                        String version) {
        var objectMessage = objectMapper.createObjectNode()
            .put(VERSION, version)
            .put(HTTP_STATUS, httpStatus);

        HttpServletRequest request = ((ServletWebRequest)webRequest).getRequest();

        var url = request.getRequestURL();
        if (request.getQueryString() != null) {
            url = url.append("?").append(request.getQueryString());
        }

        var requestId = request.getHeader(RequestHeaderConstant.X_REQUEST_ID);
        var clientIP = request.getHeader(RequestHeaderConstant.X_CLIENT_IP);
        var method = request.getMethod();
        var methodStr = (method != null)  ? method.toString() : null;

        objectMessage.put(URL, url.toString())
            .put(METHOD, methodStr)
            .put(RequestHeaderConstant.X_REQUEST_ID, requestId)
            .put(RequestHeaderConstant.X_CLIENT_IP, clientIP);

        return objectMessage;
    }

    private static JsonNode createMsg(WebRequest webRequest,
                                      int httpStatus,
                                      BaseResponse.Metadata errResp,
                                      String version) {
        return createMsg(webRequest, httpStatus, version)
            .set(RESPONSE, objectMapper.convertValue(errResp, JsonNode.class));
    }
}
