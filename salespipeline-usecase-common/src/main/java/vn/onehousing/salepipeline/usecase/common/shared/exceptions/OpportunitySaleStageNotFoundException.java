package vn.onehousing.salepipeline.usecase.common.shared.exceptions;

public class OpportunitySaleStageNotFoundException extends HousingException {
    public OpportunitySaleStageNotFoundException(String message) {
        super(HousingErrors.OPPORTUNITY_SALE_STAGE_NOT_FOUND, message);
    }
}
