package vn.onehousing.salepipeline.usecase.common.infrastructure.configs.utils;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;


@UtilityClass
public class CommonUtils {
    public static final int DEFAULT_MASKED_DISPLAY_NUM = 3;
    private static final String MASKED_DISPLAY = "***";
    private static final String MASKED_PHONE = "******";

    public static String maskedLongPhoneNumber(String phoneNumber, int displayNum) {
        return MASKED_PHONE + StringUtils.substring(phoneNumber, phoneNumber.length() - displayNum);
    }

    /**
     * Masked phone number with asterisk, display only last character(s)
     *
     * @param phoneNumber - A phone number need to mask
     * @param displayNum  - Number of last characters to display
     * @return Masked phone number, example: 0912345678 -> ******678 (with displayNum=3)
     */
    public static String maskedPhoneNumber(String phoneNumber, int displayNum) {
        return MASKED_DISPLAY + StringUtils.substring(phoneNumber, phoneNumber.length() - displayNum);
    }

    public static String maskedEmail(String email, int displayNum) {
        var spliced = StringUtils.split(email, "@");
        if (Objects.isNull(spliced) || spliced.length == 0) {
            return "";
        }
        var masked = StringUtils.substring(spliced[0], 0, displayNum) + MASKED_DISPLAY;

        if (spliced.length > 1) {
            masked = masked + "@" + spliced[1];
        }
        return masked;
    }

    public static String buildRedisLockKey(String processor, String data) {
        return processor + "_" + data;
    }
}
