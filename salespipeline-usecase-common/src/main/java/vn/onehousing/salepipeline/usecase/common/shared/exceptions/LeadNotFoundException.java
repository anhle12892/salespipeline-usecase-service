package vn.onehousing.salepipeline.usecase.common.shared.exceptions;

public class LeadNotFoundException extends HousingException {
    public LeadNotFoundException(String message) {
        super(HousingErrors.LEAD_NOT_FOUND, message);
    }
}
