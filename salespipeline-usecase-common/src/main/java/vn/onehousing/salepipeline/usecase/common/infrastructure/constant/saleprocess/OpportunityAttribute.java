package vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess;

import java.util.HashMap;
import java.util.Map;

public interface OpportunityAttribute {
    String MAIN_VIEWS = "main_views";
    String PROPERTY_TYPES = "property_types";
    String NB_BEDROOM = "nb_bedroom";
    String BUDGET_LIMIT = "budget_limit";
    String PROPERTY_RANGE_FLOOR = "property_range_floor";
    String BALCONY_DIRECTIONS = "balcony_directions";
    String MORTGAGE_NEED = "mortgage_need";

    Map<String, String> ATTRIBUTE_ACTIVITIES = new HashMap<String, String>() {{
        put(MAIN_VIEWS, SaleProcessTaskWorkflow.OPPORTUNITY_UPDATED_MAIN_VIEWS);
        put(PROPERTY_TYPES, SaleProcessTaskWorkflow.OPPORTUNITY_UPDATED_PROPERTY_TYPE);
        put(NB_BEDROOM, SaleProcessTaskWorkflow.OPPORTUNITY_UPDATED_NB_BEDROOM);
        put(BUDGET_LIMIT, SaleProcessTaskWorkflow.OPPORTUNITY_UPDATED_BUDGET_LIMIT);
        put(PROPERTY_RANGE_FLOOR, SaleProcessTaskWorkflow.OPPORTUNITY_UPDATED_PROPERTY_RANGE_FLOOR);
        put(BALCONY_DIRECTIONS, SaleProcessTaskWorkflow.OPPORTUNITY_UPDATED_BALCONY_DIRECTIONS);
        put(MORTGAGE_NEED, SaleProcessTaskWorkflow.OPPORTUNITY_UPDATED_MORTGAGE_NEED);
    }};
}
