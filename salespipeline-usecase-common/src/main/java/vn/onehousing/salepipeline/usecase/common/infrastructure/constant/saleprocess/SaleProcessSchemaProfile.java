package vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess;

public interface SaleProcessSchemaProfile {
    String IN_HOUSE = "1h";
    Integer IN_HOUSE_INITIAL_STAGE = 1;
}
