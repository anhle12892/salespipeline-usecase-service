package vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess;

import java.util.HashMap;
import java.util.Map;

public interface SaleProcessTaskActivity {
    String COMPLETED = "COMPLETED";

    Map<String, String> TASK_ACTIVITIES = new HashMap<String, String>() {{
        put("APPROACH_CONTACTED_CTV", SaleProcessTaskWorkflow.CONTACTED_WITH_COLLABORATOR);
        put("APPROACH_CONTACTED_SUCCESS_NOT_CONFIRM", SaleProcessTaskWorkflow.MARKED_ANY_RESULT_OF_CONTACT_WITH_CUSTOMER);
        put("APPROACH_CONTACTED_SUCCESS_CONFIRMED", SaleProcessTaskWorkflow.UPDATED_BUYING_DEMAND_AND_PROJECT_DEMAND);
        put("RECORD_MEETING_SUCCESS", SaleProcessTaskWorkflow.CREATED_NOTE_WITH_FORM_SUCCESS_APPOINTMENT);
        put("CONSULTING_PROJECT", SaleProcessTaskWorkflow.CONSULTING_ABOUT_PROJECT);
        put("CONSULTING_INVESTOR", SaleProcessTaskWorkflow.CONSULTING_ABOUT_INVESTOR);
        put("CONSULTING_PRODUCT", SaleProcessTaskWorkflow.CONSULTING_ABOUT_PRODUCT);
        put("CONSULTING_POLICY", SaleProcessTaskWorkflow.CONSULTING_ABOUT_POLICY);
        put("CONSULTING_FINANCIAL", SaleProcessTaskWorkflow.CONSULTING_ABOUT_FINANCIAL);
        put("MARK_RESOLVED_ALL_HURDLES_IN_FORM", SaleProcessTaskWorkflow.MARK_RESOLVED_ALL_HURDLES);
        put("SUGGESTION_AND_FEEDBACK_SUCCESS", SaleProcessTaskWorkflow.MARK_CUSTOMER_ACCEPT_WITH_CONSULTANT_RECOMMENDATION);
        put("UPDATE_BOOKING", SaleProcessTaskWorkflow.CREATED_NOTE_WITH_FORM_UPDATE_BOOKING);
    }};
}
