package vn.onehousing.salepipeline.usecase.common.shared.exceptions;

public class SalesPipelineNotExistException extends HousingException {
    public SalesPipelineNotExistException(String message) {
        super(HousingErrors.SALESPIPELINE_NOT_EXIST, message);
    }
}
