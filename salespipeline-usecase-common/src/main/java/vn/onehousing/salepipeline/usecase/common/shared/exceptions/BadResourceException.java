package vn.onehousing.salepipeline.usecase.common.shared.exceptions;

/**
 * BadResourceException.
 */
public class BadResourceException extends HousingException {
    public BadResourceException(HousingBusinessError errorCode) {
        super(errorCode);
    }

    public BadResourceException(String message) {
        super(HousingErrors.BAD_RESOURCE, message);
    }

}
