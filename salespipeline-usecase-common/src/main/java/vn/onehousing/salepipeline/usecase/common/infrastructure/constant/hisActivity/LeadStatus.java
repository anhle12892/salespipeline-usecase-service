package vn.onehousing.salepipeline.usecase.common.infrastructure.constant.hisActivity;

public class LeadStatus {
    public static final String STOP = "STOP";
    public static final String SUCCESS = "SUCCESS";
    public static final String BOOKING = "BOOKING";
    public static final String REVOKED = "REVOKED";
}
