package vn.onehousing.salepipeline.usecase.common.infrastructure.constant.hisActivity;

public class BuyingNeedType {
    public static final String HOME_CONSULTING = "HOME_CONSULTING";
    public static final String FINANCE_CONSULTING = "FINANCE_CONSULTING";
    public static final String PROJECT_VISITING = "PROJECT_VISITING";
    public static final String BOOKING = "BOOKING";
    public static final String REGISTER_EVENT = "REGISTER_EVENT";
    public static final String APARTMENT_SELLING = "APARTMENT_SELLING";
}
