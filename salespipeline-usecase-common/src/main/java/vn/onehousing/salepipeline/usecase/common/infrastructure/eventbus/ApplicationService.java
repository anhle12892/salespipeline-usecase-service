package vn.onehousing.salepipeline.usecase.common.infrastructure.eventbus;

import java.util.Objects;

public interface ApplicationService {
    default <E extends ApplicationEvent> void publishEvent(E event) {
        var eventBus = getEventBus();
        if (Objects.nonNull(eventBus)) {
            eventBus.publish(event);
        }
    }

    default <E extends ApplicationEvent> void subscribe(String eventType, EventSubscriber subscriber) {
        var eventBus = getEventBus();
        if (Objects.nonNull(eventBus)) {
            eventBus.subscribe(eventType, subscriber);
        }
    }

    default <E extends ApplicationEvent> void unsubscribe(String eventType, EventSubscriber subscriber) {
        var eventBus = getEventBus();
        if (Objects.nonNull(eventBus)) {
            eventBus.unsubscribe(eventType, subscriber);
        }
    }

    EventBus getEventBus();
}
