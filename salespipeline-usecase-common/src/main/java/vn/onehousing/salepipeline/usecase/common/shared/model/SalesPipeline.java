package vn.onehousing.salepipeline.usecase.common.shared.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SalesPipeline {
    String id;
    String uuid;
    String leadUuid;
    String leadCode;
    String assignedUserUuid;
    String assignedUserCode;
    String accountUuid;
    String accountCode;
    String contactUuid;
    String contactCode;
    String campaignUuid;
    String campaignCode;
    String opportunityUuid;
    String opportunityCode;
    String referredByUserUuid;
    String referredByUserCode;
    String saleStage;
    String source;
    String channel;
    String note;
    String disqualifiedReason;
    String closedLostReason;
    String duplicatedLeadUuid;
    String duplicatedLeadCode;
    Instant closedDate;
    Instant createdDate;
    Instant lastModifiedDate;
    String createdBy;
    String lastModifiedBy;
}
