package vn.onehousing.salepipeline.usecase.common.shared.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import vn.onehousing.salepipeline.usecase.common.shared.constants.NoteParentType;
import vn.onehousing.salepipeline.usecase.common.shared.shared.AggregateRoot;

import java.time.Instant;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class Note implements AggregateRoot<Note> {
    Long id;
    String noteUuid;
    String noteCode;
    String ownerUuid;
    String parentUuid;
    String parentType;
    String title;
    String content;
    String ownerType;
    Instant createdDate;
    String createdBy;
    Instant lastModifiedDate;
    String lastModifiedBy;
    String ownerEmail;

    public void markParentTypeToLead() {
        this.parentType = NoteParentType.LEAD;
    }

    public void defaultTitle() {
        this.title = "Ghi chú";
    }

    @Override
    public Note clone() {
        return null;
    }
}
