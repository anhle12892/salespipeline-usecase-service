package vn.onehousing.salepipeline.usecase.common.shared.shared;

public interface AggregateRoot<T> extends Entity<T> {
    T clone();
}
