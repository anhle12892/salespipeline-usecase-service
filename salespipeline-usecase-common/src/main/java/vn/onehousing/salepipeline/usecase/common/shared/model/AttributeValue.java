package vn.onehousing.salepipeline.usecase.common.shared.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AttributeValue {
    @NotBlank(message = "dynamic attribute name can't not be null")
    String name;
    @NotNull(message = "dynamic attribute value can't not be null")
    Object value;
}
