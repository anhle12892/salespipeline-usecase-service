package vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class FieldViolation {
    private String field;
    private String description;

}

