package vn.onehousing.salepipeline.usecase.common.shared.exceptions;

public class OpportunityNotFoundException extends HousingException {
    public OpportunityNotFoundException(String message) {
        super(HousingErrors.OPPORTUNITY_NOT_FOUND, message);
    }
}
