package vn.onehousing.salepipeline.usecase.common.infrastructure.constant;

public interface RequestHeaderConstant {
    String X_REQUEST_ID = "X-Request-ID";
    String X_DEVICE_ID = "X-Device-ID";
    String X_DEVICE_SESSION_ID = "X-Device-Session-ID";
    String X_USER_ID = "X_User_ID";
    String X_CLIENT_IP = "True-Client-IP";
}
