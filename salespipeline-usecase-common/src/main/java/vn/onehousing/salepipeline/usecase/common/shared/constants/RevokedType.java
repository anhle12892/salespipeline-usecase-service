package vn.onehousing.salepipeline.usecase.common.shared.constants;

public class RevokedType {
    public static final String OPP_OVERDUE_CC_QUALIFIED = "OPP_OVERDUE_CC_QUALIFIED";
    public static final String OPP_OVERDUE_CC_UNQUALIFIED = "OPP_OVERDUE_CC_UNQUALIFIED";
}
