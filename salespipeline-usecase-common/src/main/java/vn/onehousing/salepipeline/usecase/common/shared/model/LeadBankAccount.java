package vn.onehousing.salepipeline.usecase.common.shared.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LeadBankAccount {
    String id;
    String accountNumber;
    String accountHolder;
    String bankName;
    String bankCode;
    String bankBranch;
}
