package vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess;

public class SaleProcessInputActivityType {
    public static final int UNKNOWN = -1;
    public static final int SALES_PIPELINE_CREATED = 0;
    public static final int OPPORTUNITY_CREATED = 1;
    public static final int OPPORTUNITY_UPDATED = 2;
    public static final int NEW_ACTIVITY = 3;
    public static final int AN_APPOINTMENT_CREATED = 4;
    public static final int BOOKING_STATUS_UPDATED = 5;
    public static final int ASSIGNMENT_ACTIVITY_CREATED = 6;
}
