package vn.onehousing.salepipeline.usecase.common.infrastructure.constant.hisActivity;

public class RecordType {
    public static final String ASSIGN_OPP = "ASSIGN_OPP";
    public static final String UPDATE_OPP = "UPDATE_OPP";
    public static final String STOP_OPP = "STOP_OPP";
    public static final String SUCCESS_OPP = "SUCCESS_OPP";
    public static final String REVOKE_OPP = "REVOKE_OPP";
    public static final String REDISTRIBUTE_OPP = "REDISTRIBUTE_OPP";
    public static final String AGENT_NOTE = "AGENT_NOTE";
    public static final String CUSTOMER_MSG = "CUSTOMER_MSG";
    public static final String CONTACT_CENTER_NOTE = "CONTACT_CENTER_NOTE";
    public static final String BOOKING = "BOOKING";
    public static final String APPOINTMENT = "APPOINTMENT";
    public static final String REQUEST_MC = "REQUEST_MC";
}
