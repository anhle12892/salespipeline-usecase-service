package vn.onehousing.salepipeline.usecase.common.shared.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Contact {
    Long id;
    String contactCode;
    String contactUuid;
    String contactName;
    String contactStatus;
    PhoneNumber phoneNumber;
    String email;
    LocalDate dateOfBirth;
    String job;
    Instant lastRequestContactDate;
    String gender;
    Instant lastContactedDate;
    String accountUuid;
    String accountCode;
    String leadSource;
    String description;
    String facebook;
    String maritalStatus;
    String referredByUserUuid;
    String referredByUserCode;
    String contactRoles;
    Integer numberOfChildren;
    private Instant lastModifiedDate;

    Integer rangeAge;

    String childrenSchool;

    String nationality;

    Address permanentAddress;

    BigDecimal annualRevenue;

    List<LeadIdDocument> idDocuments;

    String company;

    Long timestamp;
}
