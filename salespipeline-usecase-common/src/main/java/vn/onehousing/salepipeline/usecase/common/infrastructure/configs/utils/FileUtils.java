package vn.onehousing.salepipeline.usecase.common.infrastructure.configs.utils;

import java.io.File;
import java.util.Objects;

public final class FileUtils {
    private FileUtils() {
    }

    public static boolean isFileExists(String path) {
        if (Objects.isNull(path) || path.trim().isEmpty()) {
            return false;
        } else {
            return new File(path).exists();
        }
    }

    public static String gsToUrl(String gsUrl) {
        if (Objects.isNull(gsUrl)) {
            return gsUrl;
        }
        return gsUrl.replace("gs://", "https://");
    }
}
