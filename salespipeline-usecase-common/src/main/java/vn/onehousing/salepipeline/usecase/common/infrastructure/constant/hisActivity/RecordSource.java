package vn.onehousing.salepipeline.usecase.common.infrastructure.constant.hisActivity;

public class RecordSource {
    // Agent
    public static final String AGENT = "AGENT";
    // OneHousing
    public static final String TRANSACTION = "TRANSACTION";
    // ACN
    public static final String ACN = "ACN";
    // Contact center
    public static final String CONTACT_CENTER = "CONTACT_CENTER";

    public static final String CUSTOMER = "CUSTOMER";

    public static final String BACK_OFFICE = "BACK_OFFICE";

    public static final String LANDING_PAGE = "LANDING_PAGE";

}
