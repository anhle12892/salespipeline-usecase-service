package vn.onehousing.salepipeline.usecase.common.shared.exceptions;

public class AccountException extends HousingException {

    public AccountException(String message) {
        super(
            HousingErrors.REQUEST_THIRDPARTY_EXCEPTION,
            String.format(message)
        );
    }
}
