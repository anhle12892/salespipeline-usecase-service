package vn.onehousing.salepipeline.usecase.common.infrastructure.constant;

public interface ActEventConst {
    String CREATED_LEAD = "LeadCreatedEvent";
    String UPDATED_LEAD = "LeadUpdatedEvent";
    String CREATED_ACCOUNT = "AccountCreatedEvent";
    String CREATED_CONTACT = "ContactCreatedEvent";
    String CREATED_OPPORTUNITY = "OpportunityCreatedEvent";
    String UPDATED_OPPORTUNITY = "OpportunityUpdatedEvent";

}
