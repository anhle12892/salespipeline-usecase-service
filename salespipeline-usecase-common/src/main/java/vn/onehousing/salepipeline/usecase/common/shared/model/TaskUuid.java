package vn.onehousing.salepipeline.usecase.common.shared.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.Objects;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class TaskUuid {
    private String taskUuid;

    @Builder
    public TaskUuid(String taskUuid) {
        this.taskUuid = Objects.requireNonNull(taskUuid);
    }

}
