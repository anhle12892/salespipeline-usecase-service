package vn.onehousing.salepipeline.usecase.common.shared.exceptions;

public class InvalidParameterException extends HousingException{
    public InvalidParameterException(String message) {
        super(HousingErrors.INVALID_PARAMETERS, message);
    }
}
