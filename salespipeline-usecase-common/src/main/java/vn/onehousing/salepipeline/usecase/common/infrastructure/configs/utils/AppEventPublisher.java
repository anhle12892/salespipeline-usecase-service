package vn.onehousing.salepipeline.usecase.common.infrastructure.configs.utils;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public class AppEventPublisher {

    private final ApplicationEventPublisher publisher;

    /**
     * This method can be invoked by the others logic, and it maybe throw an
     * Exception. To avoid transaction rollback, use "try catch" block
     */
    public <T> void publishEvent(T event) {
        try {
            log.info("Published a event ({})", event);
            publisher.publishEvent(event);
        } catch (Exception e) {
            log.error("[AppEventPublisher.publishEvent]: {}", e.getMessage(), e);
        }
    }
}
