package vn.onehousing.salepipeline.usecase.common.shared.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Opportunity {
    Long opportunityId;
    String opportunityUuid;
    String opportunityCode;
    String leadUuid;
    String name;
    String description;
    String accountUuid;
    String accountCode;
    String productCode;
    String productUuid;
    String referredByUserUuid;
    String referredByUserCode;
    String assignedUserUuid;
    String assignedUserCode;
    String note;
    String campaignUuid;
    String campaignCode;
    Instant closedDate;
    List<AttributeValue> attributes;
    String source;
    @JsonProperty("is_sign_contract")
    Boolean isSignContract;
    String status;
    Instant recordedDate;
    String unassignedReason;
    Instant lastModifiedDate;
    String createdBy;
    String lastModifiedBy;
    Instant createdDate;
    String channel;
    String phoneNumber;
    String gender;
    String unqualifiedReason;
    String priority;
    Instant assignedDate;
    List<AdditionalPhone> additionalPhones;
    Boolean isFromSys;
    List<User> assignedUsers;
}
