package vn.onehousing.salepipeline.usecase.common.infrastructure.constant.saleprocess;

public interface AgentSaleProcessBookingStatus {
    String AGENT_SENT_BOOKING_TO_PARTNER = "BOOKING_SENT";
    String AGENT_PARTNER_UPDATED_BOOKING_STATUS_TO_SUCCESS = "SUCCESS";
}
