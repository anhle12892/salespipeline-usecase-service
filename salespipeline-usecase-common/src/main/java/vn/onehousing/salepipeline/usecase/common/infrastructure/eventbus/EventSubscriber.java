package vn.onehousing.salepipeline.usecase.common.infrastructure.eventbus;


public interface EventSubscriber {
    <E extends ApplicationEvent> void onEvent(E event);
}
