package vn.onehousing.salepipeline.usecase.common.shared.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Lead {
    String leadId;

    String leadName;

    PhoneNumber phoneNumber;

    String email;

    String leadUuid;

    String leadCode;

    String assignedUserUuid;

    String assignedUserCode;

    Instant assignedDate;

    String campaignUuid;

    String campaignCode;

    String status;

    Boolean isConverted;

    String referredByUserUuid;

    String referredByUserCode;

    String attributeSetUuid;

    BigDecimal score;

    String priority;

    String unqualifiedReason;

    String unassignedReason;

    Instant closedDate;

    String leadProfileUuid;

    String accountUuid;

    String accountCode;

    String contactUuid;

    String contactCode;

    String opportunityUuid;

    String opportunityCode;

    String attributeSetId;

    String source;

    String gender;

    String version;

    LeadBankAccount bankAccount;

    String duplicatedLeadUuid;

    String duplicatedLeadCode;

    Instant convertedDate;

    Address address;

    Address permanentAddress;

    LeadProfile profile;

    List<AttributeValue> attributes;

    String eavType;

    Instant recordedDate;

    String channel;

    String expectedAssignedUserCode;

    String expectedAssignUserUuid;

    List<LeadIdDocument> idDocuments;

    List<User> assignedUsers;

    List<User> expectedUsers;

    String note;
    String partnerLeadUuid;

    Instant lastModifiedDate;
    Instant createdDate;
    String createdBy;
    String lastModifiedBy;
}
