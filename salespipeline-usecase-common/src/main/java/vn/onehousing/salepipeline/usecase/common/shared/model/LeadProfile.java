package vn.onehousing.salepipeline.usecase.common.shared.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class LeadProfile {

    @JsonProperty("lead_profile_uuid")
    private String leadProfileUuid;

    @JsonProperty("attribute_set_uuid")
    private String attributeSetUuid;

    @JsonProperty("date_of_birth")
    private LocalDate dateOfBirth;

    @JsonProperty("facebook")
    private String facebook;

    @JsonProperty("job")
    private String job;

    @JsonProperty("address_uuid")
    private String addressUuid;

    @JsonProperty("marital_status")
    private String maritalStatus;

    @JsonProperty("income")
    private BigDecimal income;

    @JsonProperty("company")
    private String company;

    @JsonProperty("number_of_children")
    private Integer numberOfChildren;

    @JsonProperty("range_age")
    private Integer rangeAge;

    @JsonProperty("range_ages")
    private String rangeAges;

    @JsonProperty("children_school")
    private String childrenSchool;

    @JsonProperty("shipping_address_id")
    private String shippingAddressId;

    @JsonProperty("nationality")
    private String nationality;

    @JsonProperty("annual_revenue")
    private BigDecimal annualRevenue;

    private BigDecimal monthlyIncome;
    private BigDecimal monthlyPaymentLoan;
    private BigDecimal monthlyExpense;
}
