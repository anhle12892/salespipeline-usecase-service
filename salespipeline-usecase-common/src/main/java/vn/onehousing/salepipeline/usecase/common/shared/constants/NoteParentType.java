package vn.onehousing.salepipeline.usecase.common.shared.constants;

public class NoteParentType {
    public static final String OPPORTUNITY = "OPPORTUNITY";
    public static final String LEAD = "LEAD";
    public static final String CONTACT = "CONTACT";
    public static final String ACCOUNT = "ACCOUNT";
    public static final String TASK = "TASK";
    public static final String REQUEST_MC = "REQUEST_MC";
    public static final String CUSTOMER_MSG = "CUSTOMER_MSG";
}
