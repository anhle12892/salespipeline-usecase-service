package vn.onehousing.salepipeline.usecase.common.shared.constants;

public class RedisLockName {
    public static final String SALESPIPELINE_USECASE_INDEX_AGGREGATE_LOCK = "salespipeline_usecase_index_aggregate";
    public static final String SALESPIPELINE_USECASE_SALE_PROCESS = "salespipeline_usecase_sale_process";
}
