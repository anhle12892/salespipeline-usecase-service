package vn.onehousing.salepipeline.usecase.common.infrastructure.constant;

public interface ActEntityType {
    String OPPORTUNITY = "OPPORTUNITY";
    String LEAD = "LEAD";
    String CONTACT = "CONTACT";
    String ACCOUNT = "ACCOUNT";
    String SALES_PIPELINE = "SALES_PIPELINE";
    String ASSIGNMENT = "ASSIGNMENT";
    String EVENT = "EVENT";
    String FINANCE = "FINANCE";
    String TASK = "TASK";
    String NOTE = "NOTE";
    String APPOINTMENT = "APPOINTMENT";
    String CUSTOMER_MSG = "CUSTOMER_MSG";
}
