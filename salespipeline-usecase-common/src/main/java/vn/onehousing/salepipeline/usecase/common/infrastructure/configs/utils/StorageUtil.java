package vn.onehousing.salepipeline.usecase.common.infrastructure.configs.utils;

import lombok.experimental.UtilityClass;

import java.net.URL;
import java.util.Optional;

@UtilityClass
public class StorageUtil {
    public static String getCdnUrlFromStorageSignUrl(String storagePath) {

        return Optional.ofNullable(storagePath)
                .map(ThrowingFunction.unchecked(URL::new))
                .map(URL::getPath)
                .map(p -> "https://" + p.substring(1))
                .orElse("");
    }
}
