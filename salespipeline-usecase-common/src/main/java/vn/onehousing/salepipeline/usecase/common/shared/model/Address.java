package vn.onehousing.salepipeline.usecase.common.shared.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Address {
    @JsonProperty("address")
    private String address;
    @JsonProperty("province")
    private Province province;
    @JsonProperty("district")
    private District district;
    @JsonProperty("ward")
    private Ward ward;
    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty("address_name")
    private String addressName;

    @JsonProperty("contact_name")
    private String contactName;
}
