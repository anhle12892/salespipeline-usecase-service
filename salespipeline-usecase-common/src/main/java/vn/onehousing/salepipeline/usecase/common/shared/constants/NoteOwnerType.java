package vn.onehousing.salepipeline.usecase.common.shared.constants;

public class NoteOwnerType {
    public static final String CONTACT_CENTER = "CONTACT_CENTER";
    public static final String AGENT = "AGENT";
}
