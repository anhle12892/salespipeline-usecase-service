package vn.onehousing.salepipeline.usecase.common.infrastructure.constant;

public interface BaseUri {
    interface SALESPIPELINE_USECASE {
        String V1 = "/v1";
    }
}
