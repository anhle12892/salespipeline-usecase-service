package vn.onehousing.salepipeline.usecase.common.shared.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    public Long id;
    public String accountCode;
    public String accountUuid;
    public String accountName;
    public String accountStatus;
    public PhoneNumber phoneNumber;
    public String email;
    public Instant lastModifiedDate;
    public String lastModifiedBy;
    public Instant createdDate;
    public String createdBy;
    public String leadSource;
    public String leadChannel;
    public String description;
    public String code;
    public String bankAccountNumber;
    public String bankAccountHolder;
    private String bankBranch;
    public String bankCode;
    public String bankName;
    public String shippingAddressUuid;
    public String campaignCode;
    public String campaignUuid;
    public String referredByUserUuid;
    public String referredByUserCode;
    public String primaryContactUuid;
    public String primaryContactCode;
    public Address address;
    public String note;
    public BigDecimal annualRevenue;
    private BigDecimal monthlyIncome;
    private BigDecimal monthlyPaymentLoan;
    private BigDecimal monthlyExpense;
    private Long timestamp;
}
