package vn.onehousing.salepipeline.usecase.application.controller;

import org.hamcrest.core.IsEqual;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import vn.onehousing.salepipeline.usecase.application.test.IntegrationTest;
import vn.onehousing.salepipeline.usecase.business.thirdparty.task.CreateTaskReq;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.matchingJsonPath;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.putRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static io.restassured.RestAssured.given;

public class PipelineControllerIT extends IntegrationTest {

    @Test
    public void call_create_task_should_return_401() {
        given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body("{}")
                .when()
                .post("/v1/leads/abc/tasks")
                .then()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    public void call_create_task_should_return_200() {
        wireMockRule.stubFor(post(urlEqualTo("/activity/v1/tasks"))
                .withRequestBody(matchingJsonPath("$.entity_type", equalTo("LEAD")))
                .withRequestBody(matchingJsonPath("$.entity_uuid", equalTo("abc")))
                .willReturn(aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("create_task_1.json")
                ));

        final CreateTaskReq req = new CreateTaskReq();
        req.setEntityType("LEAD");
        req.setAssignedToUserUuid("user-id");
        req.setName("a task");
        req.setStatus("COMPLETED");

        given()
                .auth().preemptive().basic("partner-gateway", "test")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(req)
                .when()
                .post("/v1/leads/abc/tasks")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("data.task_uuid", IsEqual.equalTo("task-uuid"))
                .body("data.entity_uuid", IsEqual.equalTo("abc"))
                .body("data.entity_type", IsEqual.equalTo("LEAD"))
                .body("data.name", IsEqual.equalTo("a task name"))
                ;
    }

    @Test
    public void call_create_task_with_note_should_return_200() {
        wireMockRule.stubFor(post(urlEqualTo("/activity/v1/tasks"))
                .withRequestBody(matchingJsonPath("$.entity_type", equalTo("LEAD")))
                .withRequestBody(matchingJsonPath("$.entity_uuid", equalTo("abc")))
                .willReturn(aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("create_task_1.json")
                ));

        wireMockRule.stubFor(post(urlEqualTo("/document/v1/notes"))
                .withRequestBody(matchingJsonPath("$.parent_uuid", equalTo("task-uuid")))
                .withRequestBody(matchingJsonPath("$.parent_type", equalTo("TASK")))
                .willReturn(aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("create_note_1.json")
                ));

        final CreateTaskReq req = new CreateTaskReq();
        req.setEntityType("LEAD");
        req.setAssignedToUserUuid("user-id");
        req.setName("a task");
        req.setStatus("COMPLETED");
        req.setComments("a comments");

        given()
                .auth().preemptive().basic("partner-gateway", "test")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(req)
                .when()
                .post("/v1/leads/abc/tasks")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("data.task_uuid", IsEqual.equalTo("task-uuid"))
                .body("data.entity_uuid", IsEqual.equalTo("abc"))
                .body("data.entity_type", IsEqual.equalTo("LEAD"))
                .body("data.name", IsEqual.equalTo("a task name"))
        ;

        wireMockRule.verify(1, postRequestedFor(urlEqualTo("/document/v1/notes")));
    }

    @Test
    public void call_update_task_with_new_note_should_return_200() {
        wireMockRule.stubFor(put(urlEqualTo("/activity/v1/tasks/task-uuid"))
                .withRequestBody(matchingJsonPath("$.entity_type", equalTo("LEAD")))
                .withRequestBody(matchingJsonPath("$.entity_uuid", equalTo("abc")))
                .willReturn(aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("create_task_1.json")
                ));

        wireMockRule.stubFor(post(urlPathEqualTo("/document/v1/notes/_search"))
                .willReturn(aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("search_note_empty.json")
                ));

        wireMockRule.stubFor(post(urlEqualTo("/document/v1/notes"))
                .withRequestBody(matchingJsonPath("$.parent_uuid", equalTo("task-uuid")))
                .withRequestBody(matchingJsonPath("$.parent_type", equalTo("TASK")))
                .willReturn(aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("create_note_1.json")
                ));

        final CreateTaskReq req = new CreateTaskReq();
        req.setEntityType("LEAD");
        req.setAssignedToUserUuid("user-id");
        req.setName("a task");
        req.setStatus("COMPLETED");
        req.setComments("a comments");

        given()
                .auth().preemptive().basic("partner-gateway", "test")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(req)
                .when()
                .put("/v1/leads/abc/tasks/task-uuid")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("data.task_uuid", IsEqual.equalTo("task-uuid"))
                .body("data.entity_uuid", IsEqual.equalTo("abc"))
                .body("data.entity_type", IsEqual.equalTo("LEAD"))
                .body("data.name", IsEqual.equalTo("a task name"))
        ;

        wireMockRule.verify(1, postRequestedFor(urlEqualTo("/document/v1/notes")));
    }

    @Test
    public void call_update_task_with_existing_note_should_return_200() {
        wireMockRule.stubFor(put(urlEqualTo("/activity/v1/tasks/task-uuid"))
                .withRequestBody(matchingJsonPath("$.entity_type", equalTo("LEAD")))
                .withRequestBody(matchingJsonPath("$.entity_uuid", equalTo("abc")))
                .willReturn(aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("create_task_1.json")
                ));

        wireMockRule.stubFor(post(urlPathEqualTo("/document/v1/notes/_search"))
                .willReturn(aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("search_note_return_1.json")
                ));

        wireMockRule.stubFor(put(urlEqualTo("/document/v1/notes/note-uuid"))
                .withRequestBody(matchingJsonPath("$.parent_uuid", equalTo("task-uuid")))
                .withRequestBody(matchingJsonPath("$.parent_type", equalTo("TASK")))
                .willReturn(aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("create_note_1.json")
                ));

        final CreateTaskReq req = new CreateTaskReq();
        req.setEntityType("LEAD");
        req.setAssignedToUserUuid("user-id");
        req.setName("a task");
        req.setStatus("COMPLETED");
        req.setComments("a comments");

        given()
                .auth().preemptive().basic("partner-gateway", "test")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(req)
                .when()
                .put("/v1/leads/abc/tasks/task-uuid")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("data.task_uuid", IsEqual.equalTo("task-uuid"))
                .body("data.entity_uuid", IsEqual.equalTo("abc"))
                .body("data.entity_type", IsEqual.equalTo("LEAD"))
                .body("data.name", IsEqual.equalTo("a task name"))
        ;

        wireMockRule.verify(1, putRequestedFor(urlEqualTo("/document/v1/notes/note-uuid")));
    }
}
