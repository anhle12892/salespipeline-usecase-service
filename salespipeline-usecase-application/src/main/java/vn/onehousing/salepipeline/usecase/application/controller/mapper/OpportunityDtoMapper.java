package vn.onehousing.salepipeline.usecase.application.controller.mapper;

import org.mapstruct.Mapper;
import vn.onehousing.salepipeline.usecase.application.controller.response.OpportunityDto;
import vn.onehousing.salepipeline.usecase.common.shared.model.Opportunity;

@Mapper(componentModel = "spring")
public interface OpportunityDtoMapper {
    OpportunityDto from(Opportunity opportunity);
}
