package vn.onehousing.salepipeline.usecase.application.controller.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NoteCreateReq {
    public String noteCode;
    public String noteUuid;
    public String ownerUuid;
    @NotBlank
    public String parentUuid;
    public String parentType;
    public String title;
    @NotBlank
    public String content;
    public String ownerType;
    public String ownerId;
    public String ownerEmail;
}
