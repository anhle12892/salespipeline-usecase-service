package vn.onehousing.salepipeline.usecase.application.controller.response.hisActivity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OppActivityDto {
    private Long opportunityId;
    private Long opportunitySourceId;
    private String opportunitySource;
    private String opportunitySourceName;
    private String recordSource;
    private String content;
    private List<String> suggestionNotes;
    private String recordType;
    private String buyingNeedType;
    private String landingPageUrl;
    private Instant createTime;
}

