package vn.onehousing.salepipeline.usecase.application.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.web.utils.HttpServletRequestForwarder;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.BaseUri;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping(BaseUri.SALESPIPELINE_USECASE.V1)
public class SalespipelineAggregateSearchController {

    private final HttpServletRequestForwarder httpRequestForwarder;

    public SalespipelineAggregateSearchController(@Lazy WebClient esLeadOppAggregateWebClient) {
        httpRequestForwarder = new HttpServletRequestForwarder(esLeadOppAggregateWebClient);
    }

    @PostMapping("/salespipeline-aggregates/_search")
    @Operation(
        requestBody = @RequestBody(
            description = "body",
            content = {
                    @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = String.class)
                    )
            }
        )
    )
    public ResponseEntity<byte[]> search(HttpServletRequest request) {
        return forwardWithPath(request, "_search");
    }

    @PostMapping("/salespipeline-aggregates/_count")
    @Operation(
            requestBody = @RequestBody(
                    description = "body",
                    content = {
                            @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = String.class)
                            )
                    }
            )
    )
    public ResponseEntity<byte[]> count(HttpServletRequest request) {
        return forwardWithPath(request, "_count");
    }

    private ResponseEntity<byte[]> forwardWithPath(HttpServletRequest request, String path) {
        return httpRequestForwarder.forwards(request,
                uriBuilder -> {
                    return uriBuilder.pathSegment(path);
                });
    }
}
