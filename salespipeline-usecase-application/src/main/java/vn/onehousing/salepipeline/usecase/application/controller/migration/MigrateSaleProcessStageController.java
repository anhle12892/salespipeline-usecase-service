package vn.onehousing.salepipeline.usecase.application.controller.migration;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.migrate.IMigrateSaleProcessStage;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request.SaleProcessMigrationRequest;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request.TaskMigrationRequest;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response.SaleProcessMigrationResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.BaseUri;

import javax.validation.Valid;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(BaseUri.SALESPIPELINE_USECASE.V1)
public class MigrateSaleProcessStageController {
    private final IMigrateSaleProcessStage migrateSaleProcessStage;

    @PutMapping("/sale-process/agent/migration")
    public Mono<BaseResponse<SaleProcessMigrationResponse>> migrate(@Valid @RequestBody SaleProcessMigrationRequest request) {
        return Mono.just(migrateSaleProcessStage.migrate(request)).map(BaseResponse::ofSucceeded);
    }

    @PostMapping("/sale-process/task/migration")
    public BaseResponse<Void> migrateTask(@RequestBody TaskMigrationRequest request) {
        migrateSaleProcessStage.migrateTask(request);
        return BaseResponse.ofSucceeded();
    }
}
