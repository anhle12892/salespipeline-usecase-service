package vn.onehousing.salepipeline.usecase.application.controller.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalesPipelineSaleStageRequest {
    @NotBlank
    private String opportunityUuid;
}
