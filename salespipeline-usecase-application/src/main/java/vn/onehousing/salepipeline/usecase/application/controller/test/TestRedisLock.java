//package vn.onehousing.salepipeline.usecase.application.controller.test;
//
//import lombok.AllArgsConstructor;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Component;
//import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;
//import vn.onehousing.salepipeline.usecase.infrastructure.adapter.thirdparty.distributedlockservice.RedisDistributedLockService;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//import java.util.concurrent.CountDownLatch;
//
//@Component
//@AllArgsConstructor
//public class TestRedisLock implements CommandLineRunner {
//
//    private final int threadCount = 5;
//    private final RedisDistributedLockService lockService;
//    private final SalesPipeline salesPipeline = new SalesPipeline();
//    private final String leadUuid = "123456789";
//    private final CountDownLatch latch = new CountDownLatch(this.threadCount);
//
//    @Override
//    public void run(String... args) throws Exception {
//        System.out.println("// With LOCK ///////////////////////////////////////////////////");
//        salesPipeline.setAccountUuid("123456789");
//        salesPipeline.setLeadUuid(leadUuid);
//
//        System.out.println("// object trans before process - accountUuid = " + this.salesPipeline.getAccountUuid());
//        List<Thread> testThreads = new ArrayList<>();
//        for (int i = 0; i < this.threadCount; i++) {
//            Thread th = new Thread(new Runnable() {
//
//                @Override
//                public void run() {
//
//                    String threadName = Thread.currentThread().getName();
//                    boolean acquireLock = lockService.getLock(leadUuid);
//                    if (acquireLock) {
//                        System.out.println("// " + threadName + " | acquired lock =  " + acquireLock + ", process data - START");
//                        try {
//                            TestRedisLock.this.salesPipeline.setAccountUuid("processed by " + threadName + " (" + System.currentTimeMillis() + ")"); // đặt giá trị vào đối tượng trans
//                            System.out.println("// " + threadName + " | process data - set trans.name = " + TestRedisLock.this.salesPipeline.getAccountUuid());
//                            Thread.sleep(2000L);
//                        } catch (InterruptedException ex) {
//                            ex.printStackTrace();
//                        }
//                        System.out.println("// " + threadName + " | process data - END");
//                    } else {
//                        // không thực hiện được khóa, trên restful API có thể trả về response thông báo lỗi dữ liệu đang được xử lý hoặc tương úng ...
//                        System.out.println("// " + threadName + " | cannot acquire lock, not process data - END - accountUuid = " + TestRedisLock.this.salesPipeline.getAccountUuid());
//                    }
//                    lockService.releaseLock(leadUuid);
//                    System.out.println("// " + threadName + " | unlocked");
//                    TestRedisLock.this.latch.countDown(); // đánh dấu 1 thread đã chạy xong ...
//                }
//            });
//            th.setName("TestThread-" + i);
//            testThreads.add(th);
//
//        }
//        Collections.shuffle(testThreads);
//        // chạy tất cả thread ...
//        for (Thread th : testThreads) {
//            th.start();
//        }
//
//        // sau khi tất cả thread đã chạy xong, kiểm tra lại giá trị đối tượng bị tác động ...
//        this.latch.await();
//        System.out.println("// object trans after process - accountUuid = " + this.salesPipeline.getAccountUuid());
//
//    }
//}
