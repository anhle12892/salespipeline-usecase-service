package vn.onehousing.salepipeline.usecase.application.controller.mapper;

import org.mapstruct.Mapper;
import vn.onehousing.salepipeline.usecase.application.controller.response.hisActivity.OppActivityDto;
import vn.onehousing.salepipeline.usecase.common.shared.model.OpportunityHistory;

@Mapper(componentModel = "spring")
public interface OpportunityHistoryMapper {
    OppActivityDto from(OpportunityHistory opportunity);
}
