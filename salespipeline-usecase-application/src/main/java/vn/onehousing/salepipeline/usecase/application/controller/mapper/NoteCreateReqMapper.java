package vn.onehousing.salepipeline.usecase.application.controller.mapper;

import org.mapstruct.Mapper;
import vn.onehousing.salepipeline.usecase.application.controller.request.NoteCreateReq;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;

@Mapper(componentModel = "spring")
public interface NoteCreateReqMapper {
    Note from(NoteCreateReq createReq);
}
