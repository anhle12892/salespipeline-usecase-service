package vn.onehousing.salepipeline.usecase.application.controller.migration;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.onehousing.salepipeline.usecase.business.usecase.noteuc.migrate.INoteMigrationUsecase;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.BaseUri;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(BaseUri.SALESPIPELINE_USECASE.V1)
public class MigrationAgentNotesController {
    private final INoteMigrationUsecase migrationUsecase;

    @PostMapping("/leads/migration/notes/")
    public BaseResponse<Void> migrate(@RequestParam(name = "core_uuid", required = false) String coreUuid) {
        migrationUsecase.migrateNote(coreUuid);
        return BaseResponse.ofSucceeded();
    }

    @PostMapping("/leads/migration/notes/audit-date")
    public BaseResponse<Void> updateTime(@RequestParam(name = "core_uuid", required = false) String coreUuid,
                                         @RequestParam(name = "migrate_time", required = false) String migrate_time) {
        migrationUsecase.migrateAuditTime(coreUuid, migrate_time);
        return BaseResponse.ofSucceeded();
    }


    @PostMapping("/leads/migration/request-mc/notes")
    public BaseResponse<Void> migrateRequestMcNote(@RequestParam(name = "core_uuid", required = false) String coreUuid) {
        migrationUsecase.migrateRequestMcNote(coreUuid);
        return BaseResponse.ofSucceeded();
    }

}
