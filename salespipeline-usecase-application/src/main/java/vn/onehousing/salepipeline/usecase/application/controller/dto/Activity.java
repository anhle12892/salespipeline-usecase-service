package vn.onehousing.salepipeline.usecase.application.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Activity {
    @JsonProperty("entity_uuid")
    private String entityUuid;
    @JsonProperty("phone_number")
    private String phoneNumber;
    @JsonProperty("entity_type")
    private String entityType;
    @JsonProperty("event_uuid")
    private String eventUuid;
    @JsonProperty("event_type")
    private String eventType;
    @JsonProperty("data")
    private Map<String, Object> data;
    @JsonProperty("created_by")
    private String createdBy;
    @JsonProperty("created_date")
    private Instant createdDate;
    @JsonProperty("last_modified_by")
    private String lastModifiedBy;
    @JsonProperty("last_modified_date")
    private Instant lastModifiedDate;
}
