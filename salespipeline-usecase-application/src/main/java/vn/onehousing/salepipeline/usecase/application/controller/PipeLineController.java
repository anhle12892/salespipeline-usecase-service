package vn.onehousing.salepipeline.usecase.application.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import vn.onehousing.salepipeline.usecase.application.controller.mapper.NoteCreateReqMapper;
import vn.onehousing.salepipeline.usecase.application.controller.request.*;
import vn.onehousing.salepipeline.usecase.business.thirdparty.task.CreateTaskReq;
import vn.onehousing.salepipeline.usecase.business.usecase.activityuc.ITaskUseCase;
import vn.onehousing.salepipeline.usecase.business.usecase.createaccountuc.ICreateAccountContactUseCase;
import vn.onehousing.salepipeline.usecase.business.usecase.createopportunityuc.ICreateOpportunityUseCase;
import vn.onehousing.salepipeline.usecase.business.usecase.createsalespipelineuc.ICreateSalesPipelineUseCase;
import vn.onehousing.salepipeline.usecase.business.usecase.noteuc.INoteUseCase;
import vn.onehousing.salepipeline.usecase.business.usecase.prequalifiedleaduc.IPreQualificationLeadUc;
import vn.onehousing.salepipeline.usecase.business.usecase.prequalifiedleaduc.request.UpdateLeadPreQualifiedReq;
import vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.IUpdateLeadUc;
import vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.request.UpdateLeadUcReq;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.BaseUri;
import vn.onehousing.salepipeline.usecase.common.shared.model.Note;
import vn.onehousing.salepipeline.usecase.common.shared.model.Opportunity;
import vn.onehousing.salepipeline.usecase.common.shared.model.SalesPipeline;
import vn.onehousing.salepipeline.usecase.common.shared.model.Task;
import vn.onehousing.salepipeline.usecase.query.model.aggregateleadandopportunityview.SalespipelineAggregateView;
import vn.onehousing.salepipeline.usecase.business.usecase.getsalespipelineaggregateviewuc.IGetSalesPipelineAggregateViewUc;

import javax.validation.Valid;
import java.io.IOException;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(BaseUri.SALESPIPELINE_USECASE.V1)
public class PipeLineController {

    private final ICreateAccountContactUseCase useCase;
    private final ICreateOpportunityUseCase opportunityCreateUseCase;
    private final ICreateSalesPipelineUseCase createSalesPipelineUseCase;
    private final IUpdateLeadUc updateLeadUc;
    private final IPreQualificationLeadUc preQualificationLeadUc;
    private final INoteUseCase noteUseCase;
    private final ITaskUseCase taskUseCase;
    private final NoteCreateReqMapper noteCreateReqMapper;
    private final IGetSalesPipelineAggregateViewUc getSalespipelineAggregateViewUc;

    /**
     * @param request Lead -> contacts (may be account and opportunity)
     * @return
     */
    @PostMapping("/contacts")
    public Mono<BaseResponse<SalesPipeline>> createContact(@Valid @RequestBody CreateContactReq request) {
        return Mono.just(useCase.process(request.getLeadUuid()))
            .map(BaseResponse::ofSucceeded);
    }

    @PostMapping("/opportunities")
    public Mono<BaseResponse<Opportunity>> createOpportunity(@Valid @RequestBody CreateOpportunityReq request) throws JsonProcessingException {
        return Mono.just(opportunityCreateUseCase.create(request.getLeadUuid()))
            .map(BaseResponse::ofSucceeded);
    }

    @PostMapping("/salespipelines")
    public Mono<BaseResponse<SalesPipeline>> createSalesPipeline(@Valid @RequestBody CreateSalesPipeLineReq request) throws JsonProcessingException {
        return Mono.just(createSalesPipelineUseCase.process(request.getLeadUuid()))
            .map(BaseResponse::ofSucceeded);
    }

    @PutMapping("/leads/{core_uuid}")
    public Mono<BaseResponse<SalesPipeline>> updateLead(
        @PathVariable(value = "core_uuid") String coreUuid,
        @Valid @RequestBody UpdateLeadUcReq request
    ) throws JsonProcessingException, IllegalAccessException {
        return Mono.just(updateLeadUc.process(coreUuid, request))
            .map(BaseResponse::ofSucceeded);
    }

    @PutMapping("/leads/prequalification/{core_uuid}")
    public Mono<BaseResponse<SalesPipeline>> receiveLeadPreQualified(
            @PathVariable(value = "core_uuid") String coreUuid,
            @Valid @RequestBody UpdateLeadPreQualifiedReq request
    ) throws JsonProcessingException, IllegalAccessException {
        return Mono.just(preQualificationLeadUc.process(coreUuid, request))
                .map(BaseResponse::ofSucceeded);
    }

    @PutMapping("/leads/{core_uuid}/migration")
    public Mono<BaseResponse<SalesPipeline>> updateLeadMigration(
        @PathVariable(value = "core_uuid") String coreUuid,
        @Valid @RequestBody UpdateLeadUcReq request
    ) throws JsonProcessingException, IllegalAccessException {
        return Mono.just(updateLeadUc.processMigration(coreUuid, request))
            .map(BaseResponse::ofSucceeded);
    }

    @GetMapping("/leads/{core_uuid}")
    public Mono<BaseResponse<SalespipelineAggregateView>> getLeadOrOpportunity(@PathVariable("core_uuid") String coreUuid) {
        return Mono.just(getSalespipelineAggregateViewUc.process(coreUuid))
            .map(BaseResponse::ofSucceeded);
    }

    @PostMapping("/leads/{core_uuid}/notes")
    Mono<BaseResponse<Note>> createNote(@PathVariable("core_uuid") String coreUuid, @Valid @RequestBody NoteCreateReq request) {
        return Mono.just(noteUseCase.createNote(coreUuid, noteCreateReqMapper.from(request))).map(BaseResponse::ofSucceeded);
    }

    @PutMapping("/leads/{core_uuid}/notes/{note_uuid}")
    Mono<BaseResponse<Note>> updateNote(@PathVariable("core_uuid") String coreUuid,
                                        @PathVariable("note_uuid") String noteUuid,
                                        @Valid @RequestBody NoteCreateReq request) {
        return Mono.just(noteUseCase.updateNote(coreUuid, noteUuid, noteCreateReqMapper.from(request))).map(BaseResponse::ofSucceeded);
    }

    @PostMapping("/leads/{core_uuid}/tasks")
    public Mono<BaseResponse<Task>> createTask(@PathVariable("core_uuid") String coreUuid, @Valid @RequestBody CreateTaskReq request) {
        request.setEntityUuid(coreUuid);
        return Mono.just(taskUseCase.createTask(request)).map(BaseResponse::ofSucceeded);
    }

    @PutMapping("/leads/{core_uuid}/tasks/{task_uuid}")
    public Mono<BaseResponse<Task>> updateTask(@PathVariable("core_uuid") String coreUuid,
                                               @PathVariable("task_uuid") String taskUuid,
                                               @Valid @RequestBody CreateTaskReq request) throws IOException {
        request.setEntityUuid(coreUuid);
        return Mono.just(taskUseCase.updateTask(taskUuid, request)).map(BaseResponse::ofSucceeded);
    }
}
