package vn.onehousing.salepipeline.usecase.application.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.getsalestage.IGetSaleProcessStage;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request.SaleProcessStageRequest;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.BaseUri;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response.SaleProcessStageResponse;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(BaseUri.SALESPIPELINE_USECASE.V1)
public class SalesPipelineSaleProcessStageController {
    private final IGetSaleProcessStage action;

    @GetMapping("/sale-process/sale-stage")
    public Mono<BaseResponse<SaleProcessStageResponse>> get(@RequestParam String lead_uuid) {
        var request = new SaleProcessStageRequest(lead_uuid);
        return Mono.just(action.get(request)).map(BaseResponse::ofSucceeded);
    }
}
