package vn.onehousing.salepipeline.usecase.application.controller.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OpportunityDto {
    private Long opportunityId;
    private String opportunityUuid;
    private String opportunityCode;
    private String name;
    private String description;
    private String accountUuid;
    private String accountCode;
    private String productCode;
    private String productUuid;
    private String referredByUserUuid;
    private String referredByUserCode;
    private String assignedUserUuid;
    private String assignedUserCode;
    private String note;
    private String campaignUuid;
    private String campaignCode;
    private Instant closedDate;
    private String attributeSetValue;
    private String leadSource;
    private String leadUuid;
//    private String salesPipelineUuid;
}
