package vn.onehousing.salepipeline.usecase.application.controller.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateOpportunityReq {
    @NotBlank
    private String leadUuid;
//    @NotBlank
//    private String salesPipelineUuid;
}
