package vn.onehousing.salepipeline.usecase.application.controller.migration;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.migrate.IMigrateStageJob;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.request.SaleProcessMigrationRequest;
import vn.onehousing.salepipeline.usecase.business.usecase.saleprocess.response.SaleProcessMigrationResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.BaseUri;

import javax.validation.Valid;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(BaseUri.SALESPIPELINE_USECASE.V1)
public class MigrateStageJobController {
    private final IMigrateStageJob job;

    @PutMapping("/sale-process/stages/migration")
    public Mono<BaseResponse<SaleProcessMigrationResponse>> migrate(@Valid @RequestBody SaleProcessMigrationRequest request) {
        return Mono.just(job.migrate(request)).map(BaseResponse::ofSucceeded);
    }
}
