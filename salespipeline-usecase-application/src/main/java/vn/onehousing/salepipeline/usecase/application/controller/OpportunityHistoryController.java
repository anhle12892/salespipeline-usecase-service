package vn.onehousing.salepipeline.usecase.application.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import vn.onehousing.salepipeline.usecase.application.controller.mapper.OpportunityHistoryMapper;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.BaseUri;
import vn.onehousing.salepipeline.usecase.common.shared.shared.PageableUtils;
import vn.onehousing.salepipeline.usecase.query.model.agenthistoryview.AgentHistoryView;
import vn.onehousing.salepipeline.usecase.query.model.opportunityactivityview.OpportunityActivityView;
import vn.onehousing.salepipeline.usecase.business.usecase.ISalesPipelineUsecaseService;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(BaseUri.SALESPIPELINE_USECASE.V1)
public class OpportunityHistoryController {

    private final ISalesPipelineUsecaseService createOppActivityUseCase;

    private final OpportunityHistoryMapper opportunityHistoryMapper;

    @GetMapping("/leads/{lead_uuid}/agent-histories")
    public Mono<BaseResponse<List<AgentHistoryView>>> getOpportunityHistory(@PathVariable String lead_uuid,
                                                                            @RequestParam(defaultValue = "0") int page,
                                                                            @RequestParam(defaultValue = "20") int size) throws IOException {
        return Mono.just(createOppActivityUseCase.getOppHistories(lead_uuid, PageableUtils.of(page, size))
                        .stream().collect(Collectors.toList()))
                .map(BaseResponse::ofSucceeded);
    }

    @GetMapping("/leads/{lead_uuid}/histories")
    public Mono<BaseResponse<List<OpportunityActivityView>>> getActivitiesByUuid(@PathVariable String lead_uuid) throws IOException {
        return Mono.just(createOppActivityUseCase.getByUuid(lead_uuid))
                .map(BaseResponse::ofSucceeded);
    }

    @GetMapping("/leads/phone/{phone_number}/histories")
    public Mono<BaseResponse<List<OpportunityActivityView>>> getActivitiesByPhone(@PathVariable String phone_number) throws IOException {
        return Mono.just(createOppActivityUseCase.getByPhone(phone_number))
                .map(BaseResponse::ofSucceeded);
    }
}
