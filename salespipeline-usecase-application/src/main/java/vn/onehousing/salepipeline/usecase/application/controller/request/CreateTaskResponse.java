package vn.onehousing.salepipeline.usecase.application.controller.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
public class CreateTaskResponse {
    private Long taskId;
    private String taskUuid;
    private String status;
    private String name;
    private String code;
    private String entityType;
    private String entityUuid;
    private String assignedToUserUuid;
    private String comments;
    private String description;
    private Instant startDate;
    private Instant dueDate;
    private String priority;
    private Instant completedDate;
    private String taskTypeCode;
    private Instant remindDate;
    private Boolean isDeleted;

    private Instant lastModifiedDate;
    private String createdBy;
    private String lastModifiedBy;
    private Instant createdDate;
}
