package vn.onehousing.salepipeline.usecase.application.controller.migration;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.onehousing.salepipeline.usecase.business.usecase.createhistoryuc.ICreateOppHistoryUseCase;
import vn.onehousing.salepipeline.usecase.business.usecase.updateleaduc.IUpdateLeadUc;
import vn.onehousing.salepipeline.usecase.common.infrastructure.configs.rest.BaseResponse;
import vn.onehousing.salepipeline.usecase.common.infrastructure.constant.BaseUri;

import java.io.IOException;
import java.sql.SQLException;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(BaseUri.SALESPIPELINE_USECASE.V1)
public class MigrationController {
    private final ICreateOppHistoryUseCase migrationUsecase;
    private final IUpdateLeadUc updateLeadUc;

    @PostMapping("/leads/migration/histories")
    public BaseResponse<Void> migrate(@RequestParam(name = "core_uuid", required = false) String coreUuid) throws SQLException {
        migrationUsecase.migrate(coreUuid);
        return BaseResponse.ofSucceeded();
    }

    @PostMapping("/leads/migration/assigned-users")
    public BaseResponse<Boolean> migrate() {
        return BaseResponse.ofSucceeded(updateLeadUc.updateAllLead());
    }

}
