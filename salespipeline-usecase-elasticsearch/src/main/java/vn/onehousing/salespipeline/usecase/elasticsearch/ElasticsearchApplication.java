package vn.onehousing.salespipeline.usecase.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ElasticsearchApplication {

    public static void main(String[] args) {
        final ConfigurableApplicationContext context = SpringApplication.run(ElasticsearchApplication.class, args);
        context.close();
    }
}
